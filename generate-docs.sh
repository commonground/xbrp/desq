#!/bin/bash
protoc \
    -I=. \
	-I=${GOPATH}/src \
	-I=${GOPATH}/src/github.com/gogo/protobuf/protobuf \
    --doc_out=./documentation \
    --doc_opt=html,index.html \
    ./command-api/command/command.proto \
    ./common/events/events.proto
