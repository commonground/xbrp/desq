# catalog-db

# Databaase management

We use the excellent [pgmodeler](https://pgmodeler.io) to model the desq databases.

## Installation Instructions

Buy and download pgmodeler from the official website or build it from source using the following instructions. See [https://pgmodeler.io/support/installation](https://pgmodeler.io/support/installation) for the official installation instructions.

### Linux

TODO

### MacOSX

#### Installation requirements

Use `brew` to install the installation requirements

```bash
brew install qt
brew link qt6 --force
brew install postgresql
brew install libxml2
```

Checkout the pgmodeler source code:

```bash
git clone https://github.com/pgmodeler/pgmodeler.git
git checkout master
```

Edit pgmodeler.pri and find the section for OSX settings:

```
  PGSQL_LIB = /usr/local/opt/postgresql/lib/libpq.dylib
  PGSQL_INC = /usr/local/opt/postgresql/include
  XML_INC = /usr/local/opt/libxml2/include/libxml2
  XML_LIB = /usr/local/opt/libxml2/lib/libxml2.dylib
```

Build pgmodeler and install it

```bash
export PGMODELER_ROOT=/Applications/pgmodeler.app/
qmake -r pgmodeler.pro
make -j 10
make install
macdeployqt $PGMODELER_ROOT \
  -executable=$PGMODELER_ROOT/Contents/MacOS/pgmodeler-ch \
  -executable=$PGMODELER_ROOT/Contents/MacOS/pgmodeler-cli
```

## Database modeling

The `db` component contains the catalog-db database model, migrations and
testdata. It also contains helper-scripts for developers to maintain the model
and migrations, as well as the Dockerfile and scripts for automatic rollout of
the db migrations.

## How to develop the database model

Edit `catalog.dbm` in [pgmodeler](https://pgmodeler.io/).

Run `./diff.sh` to find differences between the model.dbm and the migrations.

Add migrations as a new file in the migrations folder.

## Testdata

Testdata can be automatically inserted/updated through adding sql files to the
`testdata/` directory. Prefix the testdata file with the number of the migration
_after_ which it must be executed.

## Dockerfile

This dockerfile assumes the repository root as build directory.

## Local development aka `kubedev`

The docker container and helm charts for this component assume that a
postgres-operator is running and watching the namespace that contains the
`acid-catalog-db` postgresql manifest.

To get started on microk8s:

```bash
git clone https://github.com/zalando/postgres-operator.git
cd postgres-operator
git checkout 41858a702ce678ee05351f58d942628f2b0f4426
helm install postgres-operator ./charts/postgres-operator -f ./charts/postgres-operator/values.yaml --set 'watched_namespaces=*'
```
