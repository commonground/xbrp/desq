-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---
-- -- object: catalog_update_api | type: ROLE --
-- -- DROP ROLE IF EXISTS catalog_update_api;
-- CREATE ROLE catalog_update_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE catalog_update_api IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- -- object: catalog | type: DATABASE --
-- -- DROP DATABASE IF EXISTS catalog;
-- CREATE DATABASE catalog;
-- -- ddl-end --
-- 

-- object: public.persons_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.persons_id_seq CASCADE;
CREATE SEQUENCE public.persons_id_seq
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE public.persons_id_seq OWNER TO postgres;
-- ddl-end --

-- object: public.persons | type: TABLE --
-- DROP TABLE IF EXISTS public.persons CASCADE;
CREATE TABLE public.persons (
	id bigint NOT NULL DEFAULT nextval('public.persons_id_seq'::regclass),
	person_reference bytea NOT NULL,
	journal_zone text NOT NULL,
	journal_height integer NOT NULL,
	journal_hash bytea,
	CONSTRAINT persons_pk PRIMARY KEY (id),
	CONSTRAINT persons_uq_person_reference UNIQUE (person_reference),
	CONSTRAINT persons_ck_hash CHECK ((journal_height = 0) = (journal_hash IS NULL))

);
-- ddl-end --
ALTER TABLE public.persons OWNER TO postgres;
-- ddl-end --

-- object: "grant_U_c752aa283b" | type: PERMISSION --
GRANT USAGE
   ON SEQUENCE public.persons_id_seq
   TO catalog_update_api;
-- ddl-end --

-- object: grant_raw_e5b50d4640 | type: PERMISSION --
GRANT SELECT,INSERT,UPDATE
   ON TABLE public.persons
   TO catalog_update_api;
-- ddl-end --


