#!/bin/bash
set -euxo pipefail

## This diff script helps in development of the database. It starts a container which runs modd using diff/modd.conf
## Modd watches for changes to the model or migrations, and runs calc-model-diff.sh, which verifies that the migrations match the model.

dockerCmd=''
dockerRunArgs='-ti'
if [ ${1:-disabled} = 'ci-once' ]
then
    # When in CI/CD, run the test only once and exit the status
    dockerCmd='/diff/calc-model-diff.sh'
    dockerRunArgs=''
fi

catalogDbRoot=$(git rev-parse --show-toplevel)/catalog-db

# Not using gcr.io prefix in --tag image name because this image isn't meant to be released; only for local/development use.
docker build \
    --tag catalog-db-diff:latest \
    ${catalogDbRoot}/diff

docker run ${dockerRunArgs} \
    --volume ${catalogDbRoot}:/catalog-db \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    catalog-db-diff:latest ${dockerCmd}
