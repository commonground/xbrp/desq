#!/bin/bash
set -euxo pipefail

true "Drop existing database \"${PGDATABASE}\""
psql "postgres" -c "DROP DATABASE IF EXISTS ${PGDATABASE} WITH (FORCE)"
psql "postgres" -c "CREATE DATABASE ${PGDATABASE}"

true "Creating database structure from migrations and adding testdata"
dbVersion=0
for _unused in $(find /db-migrations -name "*.up.sql" -print0 | sort -z | xargs -r0 echo); do
	let dbVersion=dbVersion+1
	true "Increment db version to ${dbVersion}"
	# TODO: Don't use PGPASSWORD as commandline argument
	migrate --path /db-migrations/ --database "postgresql://${PGUSER}:${PGPASSWORD}@${PGHOST}/${PGDATABASE}?connect_timeout=5" up 1

	true "Add test data"
	# TODO: add a feature-switch to explicitly enable adding test-data
	dbVersionZerofill=$(printf "%03d" ${dbVersion})
	for dataFile in $(find /db-testdata -name "${dbVersionZerofill}_*.sql" -print0 | sort -z | xargs -r0 echo); do
		psql --echo-errors --variable "ON_ERROR_STOP=1" ${PGDATABASE} < ${dataFile} | awk "\$0=\"[${dataFile/.sql/}] \"\$0"
	done
done
