package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"

	"github.com/svent/go-flags"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/commonground/xbrp/desq/management-api/management"
)

type Options struct {
	Network string `long:"network" env:"DESQ_NETWORK" default:"demo"`
	Zone    string `long:"zone" env:"DESQ_ZONE" default:"bovenhove"`
}

var options Options

func main() {
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}

	fmt.Println("Example client that executes a ZoneStatus RPC")

	// TODO: difference between public zone urls and internal urls
	// dailZone := ecosystem.NetworkByName(options.Network).ZoneByZID(options.Zone)
	// u, _ := url.Parse(dailZone.Address)

	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}
	creds := credentials.NewTLS(tlsConfig)

	dialAddress := "localhost:8013"
	cc, err := grpc.Dial(dialAddress, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("Failed to dail: %v", err)
	}
	defer cc.Close()

	c := management.NewManagementClient(cc)
	fmt.Printf("Created client and connected to: %s\n", dialAddress)

	req := &management.EventsRequest{}
	res, err := c.Events(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while executing Events: %v", err)
	}
	log.Printf("Status: %s", res.Events)

}
