#!/bin/bash

set -euxo pipefail

# zoneAddress="command-api.bovenhove.desq.demo:30021"
# zoneAddress="command-api.oudescha.desq.demo:30022"
# zoneAddress="command-api.groenestede.desq.demo:30023"
zoneAddress="command-api.heidedal.desq.demo:30024"


personReference=$(grpcurl \
	-protoset desq.protoset \
	-d '{"datum_ingeschreven": "2017-01-15T01:30:15.01Z"}' \
	-insecure \
	${zoneAddress} \
	command.Command/PersoonIngeschreven | jq '.personReference')

echo $personReference;

grpcurl \
	-protoset desq.protoset \
	-d '{"persoon_pr": '"${personReference}"', "voornamen": ["Jill", "Johanna"], "voorvoegsel_geslachtsnaam": "", "geslachtsnaam": "Bloem", "ingangsdatum_namen": "2017-01-15T01:30:15.01Z"}' \
	-insecure \
	${zoneAddress} \
	command.Command/NamenGeregistreerd

grpcurl \
	-protoset desq.protoset \
	-d '{"persoon_pr": '"${personReference}"', "BSN": "123456789", "ingangsdatum_bsn": "2017-01-15T01:30:15.01Z"}' \
	-insecure \
	${zoneAddress} \
	command.Command/BSNToegekend

grpcurl \
	-protoset desq.protoset \
	-d '{"persoon_pr": '"${personReference}"', "verwijzing_woonadres_BAG": "BAGID X", "gemeente": "Groenestede", "startdatum_woonadres": "2017-01-15T01:30:15.01Z", "aangifte": "AANGIFTE_Ambtshalve"}' \
	-insecure \
	${zoneAddress} \
	command.Command/WoonadresNLGeregistreerd

grpcurl \
	-protoset desq.protoset \
	-d '{"persoon_pr": '"${personReference}"', "geslachtsaanduiding": "GESLACHT_V", "ingangsdatum_geslachtsaanduiding": "2017-01-15T01:30:15.01Z"}' \
	-insecure \
	${zoneAddress} \
	command.Command/GeslachtsaanduidingGeregistreerd

