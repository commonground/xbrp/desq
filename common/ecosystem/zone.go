// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package ecosystem

// ZID is an identifier for a zone
type ZID string

// Zone models a zone in the DESQ network
type Zone struct {
	ZID     string `json:"zid"`
	Name    string `json:"name"`
	Address string `json:"address"`
}
