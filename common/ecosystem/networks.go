// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package ecosystem

var networks = make(map[string]Network)

func init() {
	for _, net := range []Network{
		{Name: "development", Environment: EnvDev},    // plain golang executables
		{Name: "kubedev", Environment: EnvDev},        // kubedev using microk8s + custom dns
		{Name: "xbrp-raspi-net", Environment: EnvDev}, // Rasberry Pi setup
		{Name: "demo", Environment: EnvTest},          // Used for demo's
		{Name: "meterkasten", Environment: EnvTest},
		{Name: "cloudnet", Environment: EnvTest},
		{Name: "in2029staging", Environment: EnvStaging},
		{Name: "in2029", Environment: EnvProd},
		{Name: "main", Environment: EnvProd},
	} {
		networks[net.Name] = net
	}
}

// NetworkByName returns the Network with a particular name
func NetworkByName(name string) Network {
	return networks[name]
}
