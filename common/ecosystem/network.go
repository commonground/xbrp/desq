// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package ecosystem

// Network models a particular DESQ network
type Network struct {
	Name        string      `json:"name"`
	Environment Environment `json:"environment"`
}

// Zones returns a map of Zones for the network
func (n Network) Zones() map[string]Zone {
	return zones[n.Name]
}

// ZoneByZID returns the zone with the speficied zid
func (n Network) ZoneByZID(zid string) Zone {
	return zones[n.Name][zid]
}
