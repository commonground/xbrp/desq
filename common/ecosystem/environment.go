// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package ecosystem

// Environment models the runtime quality of a certain network
type Environment string

// Definitions of Environments
const (
	EnvDev     Environment = "dev"
	EnvTest    Environment = "test"
	EnvStaging Environment = "staging"
	EnvProd    Environment = "prod"
)

func (e Environment) String() string {
	return (string)(e)
}
