// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package ecosystem

var zones = make(map[string]map[string]Zone)

func init() {
	// Local development network
	zones["local"] = make(map[string]Zone)
	for _, zone := range []Zone{
		{ZID: "bovenhove", Name: "Bovenhove", Address: "http://localhost:3001"},
		{ZID: "oudescha", Name: "Oudescha", Address: "http://localhost:3002"},
		{ZID: "groenestede", Name: "Groenestede", Address: "http://localhost:3003"},
		{ZID: "heidedal", Name: "Heidedal", Address: "http://localhost:3004"},
	} {
		zones["local"][zone.ZID] = zone
	}

	// Kubernetes development internal network
	zones["kubedev"] = make(map[string]Zone)
	for _, zone := range []Zone{
		{ZID: "bovenhove", Name: "Bovenhove", Address: "desq-zone-bovenhove.svc.cluster.local:8031"},
		{ZID: "oudescha", Name: "Oudescha", Address: "desq-zone-oudescha.svc.cluster.local:8032"},
		{ZID: "groenestede", Name: "Groenestede", Address: "desq-zone-groenestede.svc.cluster.local:8033"},
		{ZID: "heidedal", Name: "Heidedal", Address: "desq-zone-heidedal.svc.cluster.local:8034"},
	} {
		zones["kubedev"][zone.ZID] = zone
	}

	// Demo network
	zones["demo"] = make(map[string]Zone)
	for _, zone := range []Zone{
		{ZID: "bovenhove", Name: "Bovenhove", Address: "bovenhove.desq.demo:30031"},
		{ZID: "oudescha", Name: "Oudescha", Address: "oudescha.desq.demo:30032"},
		{ZID: "groenestede", Name: "Groenestede", Address: "groenestede.desq.demo:30033"},
		{ZID: "heidedal", Name: "Heidedal", Address: "heidedal.desq.demo:30034"},
	} {
		zones["demo"][zone.ZID] = zone
	}

	// Meterkasten network
	zones["xbrp-raspi-net"] = make(map[string]Zone)
	for _, zone := range []Zone{
		{ZID: "bovenhove", Name: "Bovenhove", Address: "http://bovenhove.desq.demo:3001"},
		{ZID: "oudescha", Name: "Oudescha", Address: "http://zone-api.oudescha.desq.demo:"},
		{ZID: "groenestede", Name: "Groenestede", Address: "http://zone-api.groenestede.desq.demo"},
		{ZID: "heidedal", Name: "Heidedal", Address: "http://heidedal.desq.demo:3004"},
	} {
		zones["xbrp-raspi-net"][zone.ZID] = zone
	}
}
