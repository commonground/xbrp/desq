// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package catalogs

import (
	"errors"
	"io/ioutil"
	"net/http"
)

// PersonCatalog models a catalog in the DESQ network
type PersonCatalog struct {
	Name    string
	Address string
}

// PersonCatalogForNetwork returns the PersonCatalog in the DESQ network
func PersonCatalogForNetwork(network string) (*PersonCatalog, error) {
	switch network {
	case "development":
		return &PersonCatalog{
			Name:    "PersonCatalog",
			Address: "http://localhost:5001",
		}, nil
	default:
		return nil, errors.New("No PersonCatalog exists for network " + network)
	}
}

// Status returns the status of the PersonCatalog
func (pc PersonCatalog) Status() (string, error) {
	var client http.Client
	resp, err := client.Get(pc.Address + "/status")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", errors.New("Unable to read response body")
		}
		bodyString := string(bodyBytes)
		return bodyString, nil
	}
	return "", errors.New("Contacting PersonCatalog failed with status code: " + resp.Status)
}
