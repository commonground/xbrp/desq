package commonflags

import (
	"fmt"
)

// EventDB provides flag/env parsing struct for EventDB parameters
type EventDB struct {
	Host     string `long:"event-db-host" env:"DESQ_EVENT_DB_HOST" default:"acid-event-db" description:"EventDB host"`
	Port     uint16 `long:"event-db-port" env:"DESQ_EVENT_DB_PORT" default:"5432" description:"EventDB port"`
	Username string `long:"event-db-username" env:"DESQ_EVENT_DB_USERNAME" description:"EventDB username"`
	Password string `long:"event-db-password" env:"DESQ_EVENT_DB_PASSWORD" description:"EventDB password"`
	Database string `long:"event-db-database" env:"DESQ_EVENT_DB_DATABASE" default:"event" description:"EventDB database name"`
	SSLMode  string `long:"event-db-sslmode" env:"DESQ_EVENT_DB_SSLMODE" default:"require" description:"EventDB sslmode"`

	MaxIdleConns_ int `long:"event-db-max-idle-conns" env:"DESQ_EVENT_DB_MAX_IDLE_CONNS" default:"5" description:"Maximum idle connections allowed in the pg db pool."`
	MaxOpenConns_ int `long:"event-db-max-open-conns" env:"DESQ_EVENT_DB_MAX_OPEN_CONNS" default:"10" description:"Maximum open connections allowed in the pg db pool."`
}

// DSN returns a DSN string for the EventDB parameters
func (d EventDB) DSN() string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s", d.Host, d.Port, d.Database, d.Username, d.Password, d.SSLMode)
}

// DSNSafe returns a DSN string that is safe to print
func (d EventDB) DSNSafe() string {
	dCensored := d
	dCensored.Password = "CENSORED"
	return dCensored.DSN()
}

func (d EventDB) MaxIdleConns() int {
	return d.MaxIdleConns_
}

func (d EventDB) MaxOpenConns() int {
	return d.MaxOpenConns_
}
