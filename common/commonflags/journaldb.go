package commonflags

import (
	"fmt"
)

// JournalDB provides flag/env parsing struct for JournalDB parameters
type JournalDB struct {
	Host     string `long:"journal-db-host" env:"DESQ_JOURNAL_DB_HOST" default:"acid-journal-db" description:"JournalDB host"`
	Port     uint16 `long:"journal-db-port" env:"DESQ_JOURNAL_DB_PORT" default:"5432" description:"JournalDB port"`
	Username string `long:"journal-db-username" env:"DESQ_JOURNAL_DB_USERNAME" description:"JournalDB username"`
	Password string `long:"journal-db-password" env:"DESQ_JOURNAL_DB_PASSWORD" description:"JournalDB password"`
	Database string `long:"journal-db-database" env:"DESQ_JOURNAL_DB_DATABASE" default:"journal" description:"JournalDB database name"`
	SSLMode  string `long:"journal-db-sslmode" env:"DESQ_JOURNAL_DB_SSLMODE" default:"require" description:"JournalDB sslmode"`

	MaxIdleConns_ int `long:"journal-db-max-idle-conns" env:"DESQ_JOURNAL_DB_MAX_IDLE_CONNS" default:"5" description:"Maximum idle connections allowed in the pg db pool."`
	MaxOpenConns_ int `long:"journal-db-max-open-conns" env:"DESQ_JOURNAL_DB_MAX_OPEN_CONNS" default:"10" description:"Maximum open connections allowed in the pg db pool."`
}

// DSN returns a DSN string for the JournalDB parameters
func (d JournalDB) DSN() string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s", d.Host, d.Port, d.Database, d.Username, d.Password, d.SSLMode)
}

// DSNSafe returns a DSN string that is safe to print
func (d JournalDB) DSNSafe() string {
	dCensored := d
	dCensored.Password = "CENSORED"
	return dCensored.DSN()
}

func (d JournalDB) MaxIdleConns() int {
	return d.MaxIdleConns_
}

func (d JournalDB) MaxOpenConns() int {
	return d.MaxOpenConns_
}
