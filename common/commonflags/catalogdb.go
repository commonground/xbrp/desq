package commonflags

import (
	"fmt"
)

// CatalogDB provides flag/env parsing struct for CatalogDB parameters
type CatalogDB struct {
	Host     string `long:"catalog-db-host" env:"DESQ_CATALOG_DB_HOST" default:"acid-catalog-db" description:"CatalogDB host"`
	Port     uint16 `long:"catalog-db-port" env:"DESQ_CATALOG_DB_PORT" default:"5432" description:"CatalogDB port"`
	Username string `long:"catalog-db-username" env:"DESQ_CATALOG_DB_USERNAME" description:"CatalogDB username"`
	Password string `long:"catalog-db-password" env:"DESQ_CATALOG_DB_PASSWORD" description:"CatalogDB password"`
	Database string `long:"catalog-db-database" env:"DESQ_CATALOG_DB_DATABASE" default:"catalog" description:"CatalogDB database name"`
	SSLMode  string `long:"catalog-db-sslmode" env:"DESQ_CATALOG_DB_SSLMODE" default:"require" description:"CatalogDB sslmode"`

	MaxIdleConns_ int `long:"catalog-db-max-idle-conns" env:"DESQ_CATALOG_DB_MAX_IDLE_CONNS" default:"5" description:"Maximum idle connections allowed in the pg db pool."`
	MaxOpenConns_ int `long:"catalog-db-max-open-conns" env:"DESQ_CATALOG_DB_MAX_OPEN_CONNS" default:"10" description:"Maximum open connections allowed in the pg db pool."`
}

// DSN returns a DSN string for the CatalogDB parameters
func (d CatalogDB) DSN() string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s", d.Host, d.Port, d.Database, d.Username, d.Password, d.SSLMode)
}

// DSNSafe returns a DSN string that is safe to print
func (d CatalogDB) DSNSafe() string {
	dCensored := d
	dCensored.Password = "CENSORED"
	return dCensored.DSN()
}

func (d CatalogDB) MaxIdleConns() int {
	return d.MaxIdleConns_
}

func (d CatalogDB) MaxOpenConns() int {
	return d.MaxOpenConns_
}
