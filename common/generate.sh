#!/bin/bash

protoc events/events.proto --go_out=. --go-grpc_out=.
protoc --dart_out=grpc:../management_ui/lib events/events.proto google/protobuf/timestamp.proto
