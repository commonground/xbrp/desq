// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"crypto/rand"
	"database/sql/driver"

	"github.com/jackc/pgtype"
	"github.com/jxskiss/base62"
	"github.com/pkg/errors"
)

var (
	ErrInvalidEventIdentifier = errors.New("invalid eventIdentifier")
)

var emptyEventIdentifier = EventIdentifier{}

const eventIdentifierLength = 24

// EventIdentifier is a 192-bit byte array that
type EventIdentifier [eventIdentifierLength]byte

// NewEventIdentifier creates a new EventIdentifier. It is near-impossible for two calls to New
// to result in the same EventIdentifier.
//
// TODO: Add explanation about mathimatical imposibility.
//
// TODO: Add guide on proper random source being a requirement (system ops).
func NewEventIdentifier() (EventIdentifier, error) {
	var newEventIdentifier EventIdentifier
	_, err := rand.Read(newEventIdentifier[:])
	if err != nil {
		return emptyEventIdentifier, errors.Wrap(err, "failed to read random bytes for new eventIdentifier")
	}
	return newEventIdentifier, nil
}

// EventIdentifierFromBytes returns a EventIdentifier based on the human-readable string
// representation of a EventIdentifier.
func EventIdentifierFromString(eventIdentifierString string) (EventIdentifier, error) {
	prBytes, err := base62.DecodeString(eventIdentifierString)
	if err != nil {
		return emptyEventIdentifier, errors.Wrap(err, "failed decoding eventIdentifierString")
	}
	return EventIdentifierFromBytes(prBytes)
}

// EventIdentifierFromBytes returns a EventIdentifier from a byte slice. The byte slice must
// have a length of 24.
//
// TODO: Add note that this is not the same as FromString(string(eventIdentifierBytes)).
func EventIdentifierFromBytes(eventIdentifierBytes []byte) (EventIdentifier, error) {
	if len(eventIdentifierBytes) != eventIdentifierLength {
		return emptyEventIdentifier, ErrInvalidEventIdentifier
	}

	var pr EventIdentifier
	copy(pr[:], eventIdentifierBytes)
	return pr, nil
}

// String returns a human-friendly, url-friendly string representation of a
// EventIdentifier.
//
// TODO: Add docs on the string representation.
func (pr EventIdentifier) String() string {
	return base62.EncodeToString(pr[:])
}

// Bytes returns a byteslice for the given EventIdentifier. The slice is
// guaranteed to have a length of 16.
func (pr EventIdentifier) Bytes() []byte {
	return pr[:]
}

// Scan implements the Scanner interface.
func (pr *EventIdentifier) Scan(value interface{}) error {
	var ba pgtype.Bytea
	err := ba.Scan(value)
	if err != nil {
		return err
	}
	*pr, err = EventIdentifierFromBytes(ba.Bytes)
	if err != nil {
		return err
	}
	return nil
}

// Value implements the driver Valuer interface.
func (pr EventIdentifier) Value() (driver.Value, error) {
	ba := pgtype.Bytea{
		Bytes:  pr.Bytes(),
		Status: pgtype.Present,
	}
	return ba.Value()
}
