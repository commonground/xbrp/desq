// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"database/sql/driver"

	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type PersonReferenceArray []PersonReference

// Scan implements the Scanner interface.
func (pra *PersonReferenceArray) Scan(value interface{}) error {
	var ba pgtype.ByteaArray
	err := ba.Scan(value)
	if err != nil {
		return err
	}
	if len(ba.Dimensions) != 1 {
		return errors.New("invalid number of dimensions for PersonReferenceArray, expected 1")
	}

	*pra = make(PersonReferenceArray, len(ba.Elements))
	for i, element := range ba.Elements {
		pr, err := PersonReferenceFromBytes(element.Bytes)
		if err != nil {
			return err
		}
		(*pra)[i] = pr
	}
	return nil
}

// Value implements the driver Valuer interface.
func (pra PersonReferenceArray) Value() (driver.Value, error) {
	ba := pgtype.ByteaArray{
		Elements: make([]pgtype.Bytea, len(pra)),
		Dimensions: []pgtype.ArrayDimension{
			{
				Length:     int32(len(pra)),
				LowerBound: 0,
			},
		},
		Status: pgtype.Present,
	}
	for i, v := range pra {
		ba.Elements[i] = pgtype.Bytea{
			Bytes:  v.Bytes(),
			Status: pgtype.Present,
		}
	}
	return ba.Value()
}

func (pra PersonReferenceArray) BytesArray() [][]byte {
	ba := make([][]byte, 0, len(pra))
	for _, v := range pra {
		ba = append(ba, v.Bytes())
	}
	return ba
}
