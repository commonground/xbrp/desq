// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

// package references defines a datastructures and encoding/decodeing
// methods for references and identifiers.
//
// TODO: scan package `github.com/jxskiss/base62` for bugs/security issues.
package references
