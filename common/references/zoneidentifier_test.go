// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"testing"
)

func TestZoneIdentifierStringEncodeDecode(t *testing.T) {
	pr, err := NewZoneIdentifier()
	if err != nil {
		t.Fatal("failed to create new ZoneIdentifier")
	}
	prString := pr.String()
	t.Logf("Generated zone identifier string: %s", prString)
	pr2, err := ZoneIdentifierFromString(prString)
	if err != nil {
		t.Fatal("failed to create new ZoneIdentifier from string")
	}
	if pr != pr2 {
		t.Fatal("parsed ZoneIdentifier from string doesn't match original")
	}
}

// TODO: add more tests to cover the whole package.

// TODO: add more tests to verify edge-cases.
