// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"crypto/rand"
	"database/sql/driver"

	"github.com/jackc/pgtype"
	"github.com/jxskiss/base62"
	"github.com/pkg/errors"
)

var (
	ErrInvalidZoneIdentifier = errors.New("invalid zoneIdentifier")
)

var emptyZoneIdentifier = ZoneIdentifier{}

const zoneIdentifierLength = 8

// ZoneIdentifier is a 128-bit identifier that links to a unique zone in the
// desq system.
type ZoneIdentifier [zoneIdentifierLength]byte

// NewZoneIdentifier creates a new ZoneIdentifier. It is near-impossible for two calls to New
// to result in the same ZoneIdentifier.
//
// TODO: Add explanation about mathimatical imposibility.
//
// TODO: Add guide on proper random source being a requirement (system ops).
func NewZoneIdentifier() (ZoneIdentifier, error) {
	var newZoneIdentifier ZoneIdentifier
	_, err := rand.Read(newZoneIdentifier[:])
	if err != nil {
		return emptyZoneIdentifier, errors.Wrap(err, "failed to read random bytes for new zoneIdentifier")
	}
	return newZoneIdentifier, nil
}

// ZoneIdentifierFromBytes returns a ZoneIdentifier based on the human-readable string
// representation of a ZoneIdentifier.
func ZoneIdentifierFromString(zoneIdentifierString string) (ZoneIdentifier, error) {
	prBytes, err := base62.DecodeString(zoneIdentifierString)
	if err != nil {
		return emptyZoneIdentifier, errors.Wrap(err, "failed decoding zoneIdentifierString")
	}
	return ZoneIdentifierFromBytes(prBytes)
}

// ZoneIdentifierFromBytes returns a ZoneIdentifier from a byte slice. The byte slice must
// have a length of 8.
//
// TODO: Add note that this is not the same as FromString(string(zoneIdentifierBytes)).
func ZoneIdentifierFromBytes(zoneIdentifierBytes []byte) (ZoneIdentifier, error) {
	if len(zoneIdentifierBytes) != zoneIdentifierLength {
		return emptyZoneIdentifier, ErrInvalidZoneIdentifier
	}

	var pr ZoneIdentifier
	copy(pr[:], zoneIdentifierBytes)
	return pr, nil
}

// String returns a human-friendly, url-friendly string representation of a
// ZoneIdentifier.
//
// TODO: Add docs on the string representation.
func (pr ZoneIdentifier) String() string {
	return base62.EncodeToString(pr[:])
}

// Bytes returns a byteslice for the given ZoneIdentifier. The slice is
// guaranteed to have a length of 16.
func (pr ZoneIdentifier) Bytes() []byte {
	return pr[:]
}

// Scan implements the Scanner interface.
func (pr *ZoneIdentifier) Scan(value interface{}) error {
	var ba pgtype.Bytea
	err := ba.Scan(value)
	if err != nil {
		return err
	}
	*pr, err = ZoneIdentifierFromBytes(ba.Bytes)
	if err != nil {
		return err
	}
	return nil
}

// Value implements the driver Valuer interface.
func (pr ZoneIdentifier) Value() (driver.Value, error) {
	ba := pgtype.Bytea{
		Bytes:  pr.Bytes(),
		Status: pgtype.Present,
	}
	return ba.Value()
}
