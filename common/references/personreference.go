// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"crypto/rand"
	"database/sql/driver"

	"github.com/jackc/pgtype"
	"github.com/jxskiss/base62"
	"github.com/pkg/errors"
)

var (
	ErrInvalidPersonReference = errors.New("invalid personReference")
)

var emptyPersonReference = PersonReference{}

const personReferenceLength = 16

// PersonReference is a 128-bit reference that links to a unique person in the
// desq system.
type PersonReference [personReferenceLength]byte

// NewPersonReference creates a new PersonReference. It is near-impossible for two calls to New
// to result in the same PersonReference.
//
// TODO: Add explanation about mathimatical imposibility.
//
// TODO: Add guide on proper random source being a requirement (system ops).
func NewPersonReference() (PersonReference, error) {
	var newPersonReference PersonReference
	_, err := rand.Read(newPersonReference[:])
	if err != nil {
		return emptyPersonReference, errors.Wrap(err, "failed to read random bytes for new personReference")
	}
	return newPersonReference, nil
}

// PersonReferenceFromBytes returns a PersonReference based on the human-readable string
// representation of a PersonReference.
func PersonReferenceFromString(personReferenceString string) (PersonReference, error) {
	prBytes, err := base62.DecodeString(personReferenceString)
	if err != nil {
		return emptyPersonReference, errors.Wrap(err, "failed decoding personReferenceString")
	}
	return PersonReferenceFromBytes(prBytes)
}

// PersonReferenceFromBytes returns a PersonReference from a byte slice. The byte slice must
// have a length of 16.
//
// TODO: Add note that this is not the same as FromString(string(personReferenceBytes)).
func PersonReferenceFromBytes(personReferenceBytes []byte) (PersonReference, error) {
	if len(personReferenceBytes) != personReferenceLength {
		return emptyPersonReference, ErrInvalidPersonReference
	}

	var pr PersonReference
	copy(pr[:], personReferenceBytes)
	return pr, nil
}

// String returns a human-friendly, url-friendly string representation of a
// PersonReference.
//
// TODO: Add docs on the string representation.
func (pr PersonReference) String() string {
	return base62.EncodeToString(pr[:])
}

// Bytes returns a byteslice for the given PersonReference. The slice is
// guaranteed to have a length of 16.
func (pr PersonReference) Bytes() []byte {
	return pr[:]
}

// Scan implements the Scanner interface.
func (pr *PersonReference) Scan(value interface{}) error {
	var ba pgtype.Bytea
	err := ba.Scan(value)
	if err != nil {
		return err
	}
	*pr, err = PersonReferenceFromBytes(ba.Bytes)
	if err != nil {
		return err
	}
	return nil
}

// Value implements the driver Valuer interface.
func (pr PersonReference) Value() (driver.Value, error) {
	ba := pgtype.Bytea{
		Bytes:  pr.Bytes(),
		Status: pgtype.Present,
	}
	return ba.Value()
}
