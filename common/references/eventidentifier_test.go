// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"testing"
)

func TestEventIdentifierStringEncodeDecode(t *testing.T) {
	pr, err := NewEventIdentifier()
	if err != nil {
		t.Fatal("failed to create new EventIdentifier")
	}
	prString := pr.String()
	t.Logf("Generated event identifier string: %s", prString)
	pr2, err := EventIdentifierFromString(prString)
	if err != nil {
		t.Fatal("failed to create new EventIdentifier from string")
	}
	if pr != pr2 {
		t.Fatal("parsed EventIdentifier from string doesn't match original")
	}
}

// TODO: add more tests to cover the whole package.

// TODO: add more tests to verify edge-cases.
