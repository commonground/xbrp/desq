// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"reflect"
	"testing"
)

func TestEventReferenceStringEncodeDecode(t *testing.T) {
	pr, err := NewEventReference()
	if err != nil {
		t.Fatal("failed to create new EventReference")
	}
	prString := pr.String()
	t.Logf("Generated event reference string: %s", prString)
	pr2, err := EventReferenceFromString(prString)
	if err != nil {
		t.Fatal("failed to create new EventReference from string")
	}
	if pr != pr2 {
		t.Fatal("parsed EventReference from string doesn't match original")
	}
}

func TestEventReferenceFromIdentifiers(t *testing.T) {
	zoneIdentifier, err := ZoneIdentifierFromBytes([]byte{
		0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
	})
	if err != nil {
		t.Fatalf("failed to create zone identifier: %v", err)
	}

	eventIdentifier, err := EventIdentifierFromBytes([]byte{
		0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
		0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
	})
	if err != nil {
		t.Fatalf("failed to create event identifier: %v", err)
	}

	eventReference, err := EventReferenceFromIdentifiers(zoneIdentifier, eventIdentifier)
	if err != nil {
		t.Fatalf("failed to create event reference from identifiers: %v", err)
	}
	eventReferenceBytes := eventReference.Bytes()

	expectedEventReferenceBytes := []byte{
		0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
		0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
		0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
	}
	if !reflect.DeepEqual(
		eventReferenceBytes,
		expectedEventReferenceBytes,
	) {
		t.Fatalf("eventReference didn't match expected bytes. Got %x, want %x", eventReferenceBytes, expectedEventReferenceBytes)
	}
}

// TODO: add more tests to cover the whole package.

// TODO: add more tests to verify edge-cases.
