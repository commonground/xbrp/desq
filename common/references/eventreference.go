// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"crypto/rand"
	"database/sql/driver"

	"github.com/jackc/pgtype"
	"github.com/jxskiss/base62"
	"github.com/pkg/errors"
)

var (
	ErrInvalidEventReference = errors.New("invalid eventReference")
)

var emptyEventReference = EventReference{}

const eventReferenceLength = 32

// EventReference is a 256-bit reference that links to a unique event within a
// network.
type EventReference [eventReferenceLength]byte

// NewEventReference creates a new EventReference. It is near-impossible for two calls to New
// to result in the same EventReference.
//
// TODO: Add explanation about mathimatical imposibility.
//
// TODO: Add guide on proper random source being a requirement (system ops).
func NewEventReference() (EventReference, error) {
	var newEventReference EventReference
	_, err := rand.Read(newEventReference[:])
	if err != nil {
		return emptyEventReference, errors.Wrap(err, "failed to read random bytes for new eventReference")
	}
	return newEventReference, nil
}

// EventReferenceFromBytes returns an eventReference based on the human-readable string
// representation of an eventReference.
func EventReferenceFromString(eventReferenceString string) (EventReference, error) {
	erBytes, err := base62.DecodeString(eventReferenceString)
	if err != nil {
		return emptyEventReference, errors.Wrap(err, "failed decoding eventReferenceString")
	}
	return EventReferenceFromBytes(erBytes)
}

// EventReferenceFromBytes returns an eventReference from a byte slice. The byte slice must
// have a length of 32.
//
// TODO: Add note that this is not the same as FromString(string(eventReferenceBytes)).
func EventReferenceFromBytes(eventReferenceBytes []byte) (EventReference, error) {
	if len(eventReferenceBytes) != eventReferenceLength {
		return emptyEventReference, ErrInvalidEventReference
	}

	var er EventReference
	copy(er[:], eventReferenceBytes)
	return er, nil
}

// String returns a human-friendly, url-friendly string representation of a
// EventReference.
//
// TODO: Add docs on the string representation.
func (er EventReference) String() string {
	return base62.EncodeToString(er[:])
}

// Bytes returns a byteslice for the given EventReference. The slice is
// guaranteed to have a length of 16.
func (er EventReference) Bytes() []byte {
	return er[:]
}

// Scan implements the Scanner interface.
func (er *EventReference) Scan(value interface{}) error {
	var ba pgtype.Bytea
	err := ba.Scan(value)
	if err != nil {
		return err
	}
	*er, err = EventReferenceFromBytes(ba.Bytes)
	if err != nil {
		return err
	}
	return nil
}

// Value implements the driver Valuer interface.
func (er EventReference) Value() (driver.Value, error) {
	ba := pgtype.Bytea{
		Bytes:  er.Bytes(),
		Status: pgtype.Present,
	}
	return ba.Value()
}

// EventReferenceFromIdentifiers returns an eventReference from a ZoneIdentifier
// and an EventIdentifier.
//
// TODO: Add note that this is not the same as
// FromString(string(eventReferenceBytes)).
func EventReferenceFromIdentifiers(zoneIdentifier ZoneIdentifier, eventIdentifier EventIdentifier) (EventReference, error) {
	var er EventReference
	copy(er[:8], zoneIdentifier[:])
	copy(er[8:], eventIdentifier[:])
	return er, nil
}
