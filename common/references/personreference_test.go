// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package references

import (
	"testing"
)

func TestPersonReferenceStringEncodeDecode(t *testing.T) {
	pr, err := NewPersonReference()
	if err != nil {
		t.Fatal("failed to create new PersonReference")
	}
	prString := pr.String()
	t.Logf("Generated person reference string: %s", prString)
	pr2, err := PersonReferenceFromString(prString)
	if err != nil {
		t.Fatal("failed to create new PersonReference from string")
	}
	if pr != pr2 {
		t.Fatal("parsed PersonReference from string doesn't match original")
	}
}

// TODO: add more tests to cover the whole package.

// TODO: add more tests to verify edge-cases.
