// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package process

import (
	"os"
	"os/signal"
	"sync"
	"syscall"

	"go.uber.org/zap"
)

type Proc struct {
	chShutdown chan struct{}
}

var (
	pLock sync.Mutex
	p     *Proc
)

func New(log *zap.Logger) *Proc {
	pLock.Lock()
	defer pLock.Unlock()

	if p != nil {
		log.Info("process manager already initialized, returning existing instance")
	} else {
		log.Info("initializing process manager")
		p = &Proc{
			chShutdown: make(chan struct{}),
		}

		quitChannel := make(chan os.Signal, 1)
		signal.Notify(quitChannel, syscall.SIGINT)
		signal.Notify(quitChannel, syscall.SIGTERM)

		go func() {
			select {
			case sig := <-quitChannel:
				log.Info("received a signal, starting graceful shutdown", zap.String("signal", sig.String()))
				close(p.chShutdown)
				return
			}
		}()
	}

	return p
}

// Shutdown returns a receive-only channel that is closed when the process must
// be shut down.
func (p *Proc) Shutdown() <-chan struct{} {
	return p.chShutdown
}
