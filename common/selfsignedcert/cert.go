// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package selfsignedcert

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"math/big"
	"net"
	"time"

	"github.com/pkg/errors"
)

func TLSConfig() (*tls.Config, error) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		return nil, errors.Wrap(err, "failed to load net interfaces")
	}

	var ipAddress *net.IP

	for _, netInterface := range netInterfaces {
		ipAddresses, err := netInterface.Addrs()
		if err != nil {
			return nil, errors.Wrap(err, "failed to load addresses for net interface")
		}

		for _, addr := range ipAddresses {
			switch v := addr.(type) {
			case *net.IPNet:
				ipAddress = &v.IP
			case *net.IPAddr:
				ipAddress = &v.IP
			}

			if ipAddress != nil {
				break
			}
		}

		if ipAddress != nil {
			break
		}
	}

	if ipAddress == nil {
		return nil, errors.New("failed to discover IP address")
	}

	issuer := pkix.Name{CommonName: ipAddress.String()}

	caCertificate := &x509.Certificate{
		SerialNumber:          big.NewInt(time.Now().UnixNano()),
		Subject:               issuer,
		Issuer:                issuer,
		SignatureAlgorithm:    x509.SHA512WithRSA,
		PublicKeyAlgorithm:    x509.ECDSA,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(0, 0, 10),
		SubjectKeyId:          []byte{},
		BasicConstraintsValid: true,
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, errors.Wrap(err, "generate key failed")
	}

	caCertificateBinary, err := x509.CreateCertificate(rand.Reader, caCertificate, caCertificate, &privateKey.PublicKey, privateKey)
	if err != nil {
		return nil, errors.Wrap(err, "create cert failed")
	}

	caCertificateParsed, _ := x509.ParseCertificate(caCertificateBinary)

	certPool := x509.NewCertPool()
	certPool.AddCert(caCertificateParsed)

	tlsConfig := &tls.Config{
		ServerName: ipAddress.String(),
		Certificates: []tls.Certificate{{
			Certificate: [][]byte{caCertificateBinary},
			PrivateKey:  privateKey,
		}},
		RootCAs:    certPool,
		NextProtos: []string{"h2"},
	}

	return tlsConfig, nil
}
