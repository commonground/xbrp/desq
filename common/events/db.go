package events

import "gitlab.com/commonground/xbrp/desq/common/references"

// DBEvent is the generic DBEvent type
type DBEvent struct {
	Id               uint64
	Reference        references.EventReference
	PersonReferences references.PersonReferenceArray
	Kind             string
	Data             []byte // TODO: rename database field to Event
	JournalHash      []byte
}
