package main

import (
	"fmt"
	"log"

	"github.com/gogo/protobuf/proto"
	"gitlab.com/commonground/xbrp/desq/common/events"
)

func main() {
	geboorteVastgesteldBytes, err := proto.Marshal(&events.GeboorteVastgesteldEvent{
		GeboortePlaats: "Langedijk",
	})
	if err != nil {
		log.Fatalf("failed marshalling GeboorteVastgesteldEvent: %v", err)
	}
	namenVastgesteldBytes, err := proto.Marshal(&events.NamenVastgesteld{
		Voornamen: []string{"SuperAwesomeNaam"},
	})
	if err != nil {
		log.Fatalf("failed marshalling GeboorteVastgesteldEvent: %v", err)
	}
	var testEvents = []events.Event{
		{
			Kind: "GeboorteVastgesteldEvent",
			Data: geboorteVastgesteldBytes,
		},
		{
			Kind: "NamenVastgesteld",
			Data: namenVastgesteldBytes,
		},
	}

	for _, e := range testEvents {
		e.DebugPrint()
		fmt.Println("")
	}
}
