package events

import (
	"gitlab.com/commonground/xbrp/desq/common/references"
	"google.golang.org/protobuf/proto"
)

type EventData interface {
	proto.Message
	PersonReferences() (references.PersonReferenceArray, error)
}
