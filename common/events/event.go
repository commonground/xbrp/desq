// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package events

import (
	"fmt"
	reflect "reflect"

	"github.com/pkg/errors"
	"google.golang.org/protobuf/proto"
)

// UnmarshallEventData unmarshalls (*Event).Data into EventData.
func (e *Event) UnmarshallEventData() (EventData, error) {
	reflectType, exists := eventDataRegistry[e.Kind]
	if !exists {
		return nil, errors.New("EventData message not registered")
	}
	reflectValue := reflect.New(reflectType)
	message := reflectValue.Interface().(EventData)
	err := proto.Unmarshal(e.Data, message)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshall event data")
	}
	return message, nil
}

// IsGenesis indicates whether an event is a genesis event.
func (e *Event) IsGenesis() bool {
	switch e.Kind {
	// NOTE/TODO: We invision that another event kind that is genesis >
	// the MergedPersonEvent, which merges two or more persons into a >
	// timeline/journal. TODO: Remove "MergedPerson" if this will not >
	// case.
	case "NewPerson", "PersoonIngeschreven", "MergedPerson":
		return true
	default:
		return false
	}
}

// DebugPrint prints the event details to stdout.
func (e *Event) DebugPrint() {
	fmt.Printf("Event.Kind: %s\n", e.Kind)
	eventData, err := e.UnmarshallEventData()
	if err != nil {
		fmt.Printf("Failed to unmarshall: %v\n", err)
		return
	}

	fmt.Printf("eventData type: %T\n", eventData)
	fmt.Printf("eventData values: %v\n", eventData)

	switch typedEventData := eventData.(type) {
	// case *GeboorteVastgesteldEvent:
	// 	fmt.Printf("typedEventData type: %T", typedEventData)
	// 	fmt.Printf("typedEventData values: %v", typedEventData)
	case *NamenVastgesteld:
		fmt.Println("Switched to *NamenVastgesteld")
		fmt.Printf("typedEventData type: %T\n", typedEventData)
		fmt.Printf("typedEventData values: %v\n", typedEventData)
	default:
		fmt.Println("Switch does not know the type")
	}
}
