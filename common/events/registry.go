package events

import (
	"fmt"
	"reflect"
)

var eventDataRegistry map[string]reflect.Type

func addToRegistry(e EventData) {
	reflectType := reflect.TypeOf(e).Elem()
	eventName := reflectType.Name()
	fmt.Printf("Registering %s\n", eventName)
	eventDataRegistry[eventName] = reflectType
}
