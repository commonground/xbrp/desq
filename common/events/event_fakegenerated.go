package events

import (
	"fmt"
	reflect "reflect"

	"gitlab.com/commonground/xbrp/desq/common/references"
)

func init() {
	eventDataRegistry = make(map[string]reflect.Type)

	//  TODO: generate this list for all EventData's
	addToRegistry(&NamenVastgesteld{})
	fmt.Println("")
}

// TODO: generate this method for all EventData's
func (e *NamenVastgesteld) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes(e.PersoonHr)
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}

func (e *PersoonIngeschreven) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes([]byte(e.PersoonPr))
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}

func (e *NamenGeregistreerd) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes(e.PersoonPr)
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}

func (e *BSNToegekend) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes(e.PersoonPr)
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}

func (e *GeslachtsaanduidingGeregistreerd) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes(e.PersoonPr)
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}

func (e *WoonadresNLGeregistreerd) PersonReferences() (references.PersonReferenceArray, error) {
	pr, err := references.PersonReferenceFromBytes(e.PersoonPr)
	if err != nil {
		return nil, err
	}
	return references.PersonReferenceArray{pr}, nil
}
