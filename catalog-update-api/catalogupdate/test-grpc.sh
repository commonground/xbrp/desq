#!/bin/bash

set -euxo pipefail

# TODO: doesn't work yet, field names incorrect
grpcurl \
	-protoset catalog-update.protoset \
	-d '{"person_reference": "MTIzNDU2Nzg5MDEyMzQ1Ng==", "journal_zone": "myzone", "journal_height": 0}' \
	-v \
	-insecure \
	localhost:8000 \
	catalogupdate.CatalogUpdate/UpdatePerson

grpcurl \
	-protoset catalog-update.protoset \
	-d '{"person_reference": "MTIzNDU2Nzg5MDEyMzQ1Ng==", "journal_zone": "myzone", "journal_height": 1, "journal_hash": "Zm9vYmFy"}' \
	-v \
	-insecure \
	localhost:8000 \
	catalogupdate.CatalogUpdate/UpdatePerson
