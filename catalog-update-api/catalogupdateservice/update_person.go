// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package catalogupdateservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/commonground/xbrp/desq/catalog-update-api/catalogupdate"
	"gitlab.com/commonground/xbrp/desq/common/references"
)

// AddEntryHandler provides a AddEntry handler.
type AddEntryHandler struct {
	env string
	log *zap.Logger

	stmtUpsertPerson *sqlx.NamedStmt
}

// NewAddEntryHandler prepares a AddEntry handler.
func NewAddEntryHandler(setupContext context.Context, env string, log *zap.Logger, db *sqlx.DB) (*AddEntryHandler, error) {
	h := &AddEntryHandler{
		env: env,
		log: log,
	}

	var err error
	h.stmtUpsertPerson, err = db.PrepareNamedContext(setupContext, `
		INSERT INTO public.persons (
			person_reference,
			journal_zone,
			journal_height,
			journal_hash
		) VALUES (
			:person_reference,
			:journal_zone,
			:journal_height,
			:journal_hash
		)
		ON CONFLICT
			ON CONSTRAINT persons_uq_person_reference
				DO UPDATE
					SET journal_zone = EXCLUDED.journal_zone,
						journal_height = EXCLUDED.journal_height,
						journal_hash = EXCLUDED.journal_hash
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare stmtUpsertPerson")
	}

	return h, nil
}

func (h *AddEntryHandler) UpdatePerson(ctx context.Context, req *catalogupdate.UpdatePersonRequest) (*catalogupdate.UpdatePersonReply, error) {
	personReference, err := references.PersonReferenceFromBytes(req.PersonReference)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	log := h.log.With(zap.String("person_reference", personReference.String()), zap.Binary("person_reference", req.PersonReference))
	log.Info("UpdatePerson")

	_, err = h.stmtUpsertPerson.ExecContext(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert entry into db")
	}

	reply := &catalogupdate.UpdatePersonReply{}
	return reply, nil
}
