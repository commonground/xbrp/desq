// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

// package catalogupdateservice implements the Catlog Service/API as defined in
// catalog-update.proto.
package catalogupdateservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/catalog-update-api/catalogupdate"
)

// catalogupdateService implements the catalogupdate.CommandServer.
type catalogUpdateService struct {
	// See NOTES.md#UnsafeServer
	catalogupdate.UnsafeCatalogUpdateServer
	*AddEntryHandler
}

var _ catalogupdate.CatalogUpdateServer = &catalogUpdateService{}

// New prepares a new catalogUpdateService.
func New(setupContext context.Context, env string, log *zap.Logger, catalogDB *sqlx.DB) (*catalogUpdateService, error) {
	d := &catalogUpdateService{}

	var err error
	d.AddEntryHandler, err = NewAddEntryHandler(setupContext, env, log, catalogDB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare AddEntry handler")
	}

	return d, nil
}
