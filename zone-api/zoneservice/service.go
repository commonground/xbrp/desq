// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package zoneservice

import (
	"context"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/zone-api/zone"
)

// zoneservice implements the zone.ZoneServer.
type zoneService struct {
	// See NOTES.md#UnsafeServer
	zone.UnsafeZoneServer

	*ZoneStatusHandler
}

// A test to make sure the zoneService implements everything that is required by ZoneServer
var _ zone.ZoneServer = &zoneService{}

func New(setupContext context.Context, env string, log *zap.Logger, zid string, version string) (*zoneService, error) {
	s := &zoneService{}

	zsh, err := NewZoneStatusHandler(setupContext, env, log, zid, version)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare ZoneStatus handler")
	}
	s.ZoneStatusHandler = zsh

	return s, nil
}
