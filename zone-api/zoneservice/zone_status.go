// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package zoneservice

import (
	"context"

	"gitlab.com/commonground/xbrp/desq/zone-api/zone"
	"go.uber.org/zap"
)

// ZoneStatusHandler provides a ZoneStatus handler.
type ZoneStatusHandler struct {
	zid     string
	env     string
	version string
	log     *zap.Logger
}

// NewZoneStatusHandler prepares a ZoneStatus handler.
func NewZoneStatusHandler(setupContext context.Context, env string, log *zap.Logger, zid string, version string) (*ZoneStatusHandler, error) {
	h := &ZoneStatusHandler{
		zid:     zid,
		env:     env,
		version: version,
		log:     log,
	}

	return h, nil
}

func (h *ZoneStatusHandler) ZoneStatus(ctx context.Context, req *zone.ZoneStatusRequest) (*zone.ZoneStatusReply, error) {
	h.log.Info("ZoneStatus: {env: " + h.env + ", zid: " + h.zid + ", version: " + h.version + "}")

	reply := &zone.ZoneStatusReply{
		Status: &zone.ZoneStatus{
			Zid:     h.zid,
			Status:  "online",
			Version: h.version,
		},
	}
	return reply, nil
}
