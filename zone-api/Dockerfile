# Use go 1.x based on the latest alpine image.
FROM golang:1-alpine AS build

# Install build-time dependencies
RUN apk add --update --no-cache git

# Get grpc-health-probe for exec-based pod health checks
RUN go get github.com/grpc-ecosystem/grpc-health-probe

# Cache module dependencies
COPY go.mod /desq/go.mod
COPY go.sum /desq/go.sum
WORKDIR /desq
RUN go mod download

# Add and build-cache packages that we actually use
COPY common /desq/common
RUN go build ./common/...

# Add source and build the actual component
COPY zone-api /desq/zone-api
RUN go build -v -o dist/bin/desq-zone-api ./zone-api/cmd/desq-zone-api


# Release binary on latest alpine image.
FROM alpine:latest AS release

# Add non-privileged user as default user
RUN adduser -D -u 1001 appuser
USER appuser

# Copy binaries from build container
COPY --from=build /go/bin/grpc-health-probe /usr/local/bin/grpc-health-probe
COPY --from=build /desq/dist/bin/desq-zone-api /usr/local/bin/desq-zone-api

# Set the default command
CMD ["/usr/local/bin/desq-zone-api"]
