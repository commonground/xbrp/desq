// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	stdlog "log"
	"os"
	"time"

	flags "github.com/jessevdk/go-flags"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/common/process"
)

type Options struct {
	Network string `long:"network" env:"DESQ_NETWORK" default:"local"`
	Zone    string `long:"zone" env:"DESQ_ZONE" required:"true"`
}

var options Options

func main() {
	// Parse arguments
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		stdlog.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		stdlog.Fatalf("unexpected arguments: %v", args)
	}

	// Setup structured logger
	log, err := zap.NewDevelopment()
	if err != nil {
		stdlog.Fatalf("failed to setup zap logging: %v", err)
	}
	log = log.With(zap.String("zone", options.Zone))
	log.Info("starting the journal-worker")

	// Write a log before shutting down
	proc := process.New(log)
	go func() {
		<-proc.Shutdown()
		log.Info("shutting down the journal-worker")
		os.Exit(0)
	}()

	for {
		log.Info("I log so I live.")
		time.Sleep(10 * time.Second)
	}
}
