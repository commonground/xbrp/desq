// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	"context"
	"crypto/tls"
	"database/sql"
	"fmt"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/commonground/xbrp/desq/common/references"
	"gitlab.com/commonground/xbrp/desq/journal-update-api/journalupdate"
)

var ErrDone = errors.New("done: no work")

// syncSingleEvent selects a single event and synchronizes it to the journal.
func (w *Worker) syncSingleEvent() error {

	// TODO: Instead of creating a new connection for each processing iteration,
	// we should create a global pool. This should eventually become a pool per
	// remote zone.
	tc := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: true,
	})
	var opts = []grpc.DialOption{
		grpc.WithTransportCredentials(tc),
	}
	grpcConn, err := grpc.Dial("journal-update-api:8000", opts...)
	if err != nil {
		return errors.Wrap(err, "failed opening grpc connection")
	}
	defer grpcConn.Close()
	journalUpdateClient := journalupdate.NewJournalUpdateClient(grpcConn)

	tx, err := w.db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		errRollback := tx.Rollback()
		if errRollback == sql.ErrTxDone {
			return // tx was already comitted
		}
		if errRollback != nil {
			w.log.Error("error rolling back transaction", zap.Error(errRollback))
		}
	}()

	var unsyncedEvent struct {
		ID              uint64
		PersonReference references.PersonReference
	}
	err = tx.Stmtx(w.stmtSelectUnsyncedEvent).Get(&unsyncedEvent)
	if err != nil {
		if err == sql.ErrNoRows {
			return ErrDone
		}
		return errors.Wrap(err, "selecting unhandled events failed")
	}

	reply, err := journalUpdateClient.AddEntry(context.Background(), &journalupdate.AddEntryRequest{
		PersonReference: unsyncedEvent.PersonReference.Bytes(),
		// TODO: we need to use the real Event Reference.
		EventReference: []byte(fmt.Sprintf("event-reference-%d", unsyncedEvent.ID)),
		EventHash:      []byte("fakeTODOfake"),
	})
	if err != nil {
		return errors.Wrap(err, "failed to add entry to journal")
	}
	_, err = tx.Stmtx(w.stmtUpdateEventJournalHash).Exec(unsyncedEvent.ID, reply.JournalHeightHash)
	if err != nil {
		return errors.Wrap(err, "failed updating event journal hash")
	}

	err = tx.Commit()
	if err != nil {
		return errors.Wrap(err, "committing transaction after syncing event failed")
	}

	return nil
}
