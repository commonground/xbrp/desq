// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	"context"
	stdlog "log"

	flags "github.com/jessevdk/go-flags"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/common/commondb"
	"gitlab.com/commonground/xbrp/desq/common/commonflags"
	"gitlab.com/commonground/xbrp/desq/common/process"
)

type Options struct {
	Network string `long:"network" env:"DESQ_NETWORK" default:"local"`
	Zone    string `long:"zone" env:"DESQ_ZONE" required:"true"`

	IntervalSeconds uint64 `long:"interval-seconds" env:"DESQ_INTERVAL_SECONDS" default:"60"`

	commonflags.EventDB
}

var options Options

func main() {
	// Parse arguments
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		stdlog.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		stdlog.Fatalf("unexpected arguments: %v", args)
	}

	// Setup structured logger
	log, err := zap.NewDevelopment()
	if err != nil {
		stdlog.Fatalf("failed to setup zap logging: %v", err)
	}
	log = log.With(zap.String("zone", options.Zone))
	log.Info("starting the event-worker")

	// Write a log before shutting down
	proc := process.New(log)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-proc.Shutdown()
		log.Info("gracefully shutting down the event-worker")
		cancel()
	}()

	// setup database connections
	log.Debug("setting up postgres connection to event db", zap.String("dsn", options.EventDB.DSNSafe()))
	eventDB := commondb.SetupDBOrFatal(log.With(zap.String("db", "event")), options.EventDB)

	worker, err := NewWorker(log, eventDB, options.EventDB.DSN(), options.Network, options.IntervalSeconds)
	if err != nil {
		log.Fatal("error setting up worker", zap.Error(err))
	}

	err = worker.run(ctx)
	if err != nil {
		log.Fatal("worker returned with error", zap.Error(err))
	}
	log.Info("any work was gracefully finished, now exiting")
	log.Sync()
}
