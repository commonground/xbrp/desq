package main

import (
	"context"
	"net"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

func (w *Worker) run(ctx context.Context) error {
	// chWork is send on when there is work to do.
	chWork := make(chan struct{}, 1)
	// Add a value to chWork to see if there is work to do on the first run
	// (without waiting for a notification).
	chWork <- struct{}{}

	// chWatcherError is used to send an error from the watching goroutine to the worker goroutine
	chWatcherError := make(chan error)

	conn, err := pgx.Connect(context.Background(), w.dsn)
	if err != nil {
		return errors.Wrap(err, "unable to connect to database")
	}

	_, err = conn.Exec(context.Background(), "LISTEN new_event")
	if err != nil {
		return errors.Wrap(err, "error executing LISTEN query")
	}

	// This goroutine is never cleaned up, which is why the run() function
	// expects the calling program to exit on returning.
	go func() {
		for {
			ctx, cancel := context.WithTimeout(context.Background(), time.Duration(w.intervalSeconds)*time.Second)
			notification, err := conn.WaitForNotification(ctx)
			cancel()
			if nerr, ok := err.(net.Error); ok && nerr.Timeout() {
				// We hit a timeout while waiting for notifications. Ping the db to
				// make sure we still have a good connection.
				err := conn.Ping(context.Background())
				if err != nil {
					chWatcherError <- errors.Wrap(err, "pinging the db failed")
					return
				}
				// Ping was succesful, lets wait again for notifications.
				continue
			}
			if err != nil {
				chWatcherError <- errors.Wrap(err, "error while waiting for notification")
				return
			}

			// TODO: How does this Debug call perform in when there is a high amount
			// of notifications comming through?
			w.log.Debug("received a notification",
				zap.Uint32("PID", notification.PID),
				zap.String("channel", notification.Channel),
				zap.String("payload", notification.Payload),
			)

			select {
			case chWork <- struct{}{}:
				// add work to channel (if there is space in thannel)
			default:
				// otherwise (when channel is full) do nothing
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return nil
		case err := <-chWatcherError:
			return err
		case <-chWork:
			for {
				err := w.syncSingleEvent()
				if err != nil {
					if err == ErrDone {
						w.log.Debug("there are no unsynced events, there is no work")
						break
					}
					return errors.Wrap(err, "failed to sync an event")
				}
				// Check if we need to stop in between processing iterations.
				select {
				case <-ctx.Done():
					return nil
				default:
				}
			}
		}
	}
}
