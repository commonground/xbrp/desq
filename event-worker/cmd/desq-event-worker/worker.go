// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type Worker struct {
	log                        *zap.Logger
	db                         *sqlx.DB
	dsn                        string
	network                    string
	intervalSeconds            uint64
	stmtSelectUnsyncedEvent    *sqlx.Stmt
	stmtUpdateEventJournalHash *sqlx.Stmt
}

func NewWorker(log *zap.Logger, db *sqlx.DB, dsn string, network string, intervalSeconds uint64) (*Worker, error) {
	n := &Worker{
		db:              db,
		log:             log,
		dsn:             dsn,
		network:         network,
		intervalSeconds: intervalSeconds,
	}

	var err error
	// TODO: Needs SKIP LOCKS in FOR UPDATE clause?
	// TODO: n-m table for person references?
	n.stmtSelectUnsyncedEvent, err = db.Preparex(`
		SELECT
			events.id,
			events.person_references[0] AS person_reference
		FROM events
		WHERE events.journal_hash IS NULL
		LIMIT 1
		FOR UPDATE OF events
	`)
	// TODO: Change the person_references array to a separate table?
	//
	// TODO: Change this query to only return 1 result (a single
	// person_reference with event hash)
	if err != nil {
		return nil, errors.Wrap(err, "failed preparing stmtSelectUnhandledEvents")
	}

	n.stmtUpdateEventJournalHash, err = db.Preparex(`
		UPDATE events
		SET journal_hash = $2
		WHERE ID = $1
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed preparing stmtUpdateEventJournalHash")
	}

	return n, nil
}
