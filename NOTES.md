# UnsafeServer

We choose to use the `Unsafe..Server` because we want to trigger errors at
compile-time whenever the proto definition of this service changes in an
incompatible way. This means that work is required in this Go implementation of
the service to align it with the service definition in proto again.
