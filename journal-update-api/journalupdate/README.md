# journal-update

This folder contains the gRPC/ProtocolBuffers definitions for the Command API. It also contains the generated Go code.

## Install build dependencies

To generate Go code, you need a number of tools installed.

- You have already installed [Go](https://golang.org).
- Install `make`. This is almost always available in your systems default software repository. e.g.: `sudo apt install build-essentials`. If you're a developer, this is probably already installed.
- Install [protoc](https://www.grpc.io/docs/protoc-installation/), the protocol buffers compiler.
- Install [protoc-gen-go](https://grpc.io/docs/languages/go/quickstart/), the protoc compiler for Go.
- Install [protoc-gen-gogofaster](https://github.com/gogo/protobuf), an alternative/overloaded protoc extension for Go that provides more optimizations and features than the default one.

## Run

Tun `make` in this directory to compile the proto to Go code, and to generate the `.protoset` file for use with a CLI client such as `grpcurl`.

```bash
cd ~/src/gitlab.com/commonground/xbrp/desq/journal-update-api/journal-update
make
```
