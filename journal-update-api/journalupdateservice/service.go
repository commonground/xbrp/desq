// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

// package journalupdateservice implements the Command Service/API as defined in
// journal-update.proto.
package journalupdateservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/journal-update-api/journalupdate"
)

// journalupdateService implements the journalupdate.CommandServer.
type journalUpdateService struct {
	// See NOTES.md#UnsafeServer
	journalupdate.UnsafeJournalUpdateServer
	*AddEntryHandler
}

var _ journalupdate.JournalUpdateServer = &journalUpdateService{}

// New prepares a new journalUpdateService.
func New(setupContext context.Context, env string, log *zap.Logger, journalDB *sqlx.DB) (*journalUpdateService, error) {
	d := &journalUpdateService{}

	var err error
	d.AddEntryHandler, err = NewAddEntryHandler(setupContext, env, log, journalDB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare AddEntry handler")
	}

	return d, nil
}
