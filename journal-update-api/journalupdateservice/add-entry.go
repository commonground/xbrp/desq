// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package journalupdateservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/commonground/xbrp/desq/common/references"
	"gitlab.com/commonground/xbrp/desq/journal-update-api/journalupdate"
)

// AddEntryHandler provides a AddEntry handler.
type AddEntryHandler struct {
	env string
	log *zap.Logger

	stmtAddEntry *sqlx.Stmt
}

// NewAddEntryHandler prepares a AddEntry handler.
func NewAddEntryHandler(setupContext context.Context, env string, log *zap.Logger, db *sqlx.DB) (*AddEntryHandler, error) {
	h := &AddEntryHandler{
		env: env,
		log: log,
	}

	var err error
	h.stmtAddEntry, err = db.PreparexContext(setupContext, `
		INSERT INTO public.entries (
			journal_id,
			zone_reference,
			event_reference
		) VALUES (
			(SELECT id FROM public.journals WHERE person_reference = $1),
			'TODO',
			$2
		)
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare stmtAddEntry")
	}

	return h, nil
}

func (h *AddEntryHandler) AddEntry(ctx context.Context, req *journalupdate.AddEntryRequest) (*journalupdate.AddEntryReply, error) {
	personReference, err := references.PersonReferenceFromBytes(req.PersonReference)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	log := h.log.With(zap.String("person_reference", personReference.String()), zap.Binary("event_reference", req.EventReference))
	log.Info("AddEntry")

	_, err = h.stmtAddEntry.ExecContext(ctx, personReference, req.EventReference)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert entry into db")
	}

	reply := &journalupdate.AddEntryReply{
		JournalHeight:     123,
		JournalHeightHash: append(req.EventHash, []byte("journalsign")...),
	}
	return reply, nil
}
