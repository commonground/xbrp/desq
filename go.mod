module gitlab.com/commonground/xbrp/desq

go 1.15

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4 // indirect
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/huandu/xstrings v1.3.2
	github.com/improbable-eng/grpc-web v0.14.0
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgtype v1.7.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/jxskiss/base62 v0.0.0-20191017122030-4f11678b909b
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/cors v1.7.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03
	github.com/ugorji/go v1.2.4 // indirect
	github.com/valyala/fasthttp v1.20.0 // indirect
	go.opencensus.io v0.22.6
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20210503195802-e9a32991a82e // indirect
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	google.golang.org/genproto v0.0.0-20210504143626-3b2ad6ccc450 // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	honnef.co/go/tools v0.1.1 // indirect
	nhooyr.io/websocket v1.8.6 // indirect
)
