# journal-db

The `db` component contains the journal-db database model, migrations and
testdata. It also contains helper-scripts for developers to maintain the model
and migrations, as well as the Dockerfile and scripts for automatic rollout of
the db migrations.

## How to develop the database model

Edit `journal.dbm` in [pgmodeler](https://pgmodeler.io/).

Run `./diff.sh` to find differences between the model.dbm and the migrations.

Add migrations as a new file in the migrations folder.

## Testdata

Testdata can be automatically inserted/updated through adding sql files to the
`testdata/` directory. Prefix the testdata file with the number of the migration
_after_ which it must be executed.

## Dockerfile

This dockerfile assumes the repository root as build directory.

## Local development aka `kubedev`

The docker container and helm charts for this component assume that a
postgres-operator is running and watching the namespace that contains the
`acid-journal-db` postgresql manifest.

To get started on microk8s:

```bash
git clone https://github.com/zalando/postgres-operator.git
cd postgres-operator
helm install postgres-operator ./charts/postgres-operator -f ./charts/postgres-operator/values.yaml --set 'watched_namespaces=*'
```
