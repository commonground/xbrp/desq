-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---
-- -- object: journal_update_api | type: ROLE --
-- -- DROP ROLE IF EXISTS journal_update_api;
-- CREATE ROLE journal_update_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE journal_update_api IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 
-- -- object: command_api | type: ROLE --
-- -- DROP ROLE IF EXISTS command_api;
-- CREATE ROLE command_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE command_api IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 
-- -- object: journal_worker | type: ROLE --
-- -- DROP ROLE IF EXISTS journal_worker;
-- CREATE ROLE journal_worker WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE journal_worker IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 
-- -- object: management_api | type: ROLE --
-- -- DROP ROLE IF EXISTS management_api;
-- CREATE ROLE management_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE management_api IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 


-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- -- object: journal | type: DATABASE --
-- -- DROP DATABASE IF EXISTS journal;
-- CREATE DATABASE journal;
-- -- ddl-end --
-- 

-- object: public.journals_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.journals_id_seq CASCADE;
CREATE SEQUENCE public.journals_id_seq
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE public.journals_id_seq OWNER TO postgres;
-- ddl-end --

-- object: public.entries_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.entries_id_seq CASCADE;
CREATE SEQUENCE public.entries_id_seq
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE public.entries_id_seq OWNER TO postgres;
-- ddl-end --

-- object: public.journals | type: TABLE --
-- DROP TABLE IF EXISTS public.journals CASCADE;
CREATE TABLE public.journals (
	id bigint NOT NULL DEFAULT nextval('public.journals_id_seq'::regclass),
	person_reference bytea NOT NULL,
	created timestamptz NOT NULL DEFAULT NOW(),
	CONSTRAINT journals_pk PRIMARY KEY (id),
	CONSTRAINT journals_uq_reference UNIQUE (person_reference),
	CONSTRAINT journals_ck_person_reference CHECK (length(person_reference) = 16)

);
-- ddl-end --
ALTER TABLE public.journals OWNER TO postgres;
-- ddl-end --

-- object: public.entries | type: TABLE --
-- DROP TABLE IF EXISTS public.entries CASCADE;
CREATE TABLE public.entries (
	id bigint NOT NULL DEFAULT nextval('public.entries_id_seq'::regclass),
	journal_id bigint NOT NULL,
	zone_reference text NOT NULL,
	event_reference text NOT NULL,
	CONSTRAINT entries_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.entries OWNER TO postgres;
-- ddl-end --

-- object: entries_fk_journal | type: CONSTRAINT --
-- ALTER TABLE public.entries DROP CONSTRAINT IF EXISTS entries_fk_journal CASCADE;
ALTER TABLE public.entries ADD CONSTRAINT entries_fk_journal FOREIGN KEY (journal_id)
REFERENCES public.journals (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: grant_ra_3e05837da6 | type: PERMISSION --
GRANT SELECT,INSERT
   ON TABLE public.journals
   TO command_api;
-- ddl-end --

-- object: grant_r_40b6db75ca | type: PERMISSION --
GRANT SELECT
   ON TABLE public.journals
   TO journal_update_api,journal_worker,management_api;
-- ddl-end --

-- object: "grant_U_a108278421" | type: PERMISSION --
GRANT USAGE
   ON SEQUENCE public.journals_id_seq
   TO command_api;
-- ddl-end --

-- object: grant_r_487a3d87e0 | type: PERMISSION --
GRANT SELECT
   ON TABLE public.entries
   TO journal_worker,management_api;
-- ddl-end --

-- object: grant_ra_03b23147aa | type: PERMISSION --
GRANT SELECT,INSERT
   ON TABLE public.entries
   TO journal_update_api;
-- ddl-end --

-- object: "grant_U_36d9d11c86" | type: PERMISSION --
GRANT USAGE
   ON SEQUENCE public.entries_id_seq
   TO journal_update_api;
-- ddl-end --


