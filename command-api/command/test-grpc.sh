#!/bin/bash

set -euxo pipefail

personReference=$(grpcurl \
	-protoset command.protoset \
	-d '{}' \
	-insecure \
	command-api.groenestede.desq.demo:30023 \
	command.Command/NewPerson | jq '.personReference')

echo $personReference;

grpcurl \
	-protoset command.protoset \
	-d '{"personReference": '"${personReference}"', "note": "Dit is een notitie over een persoon."}' \
	-insecure \
	command-api.groenestede.desq.demo:30023 \
	command.Command/PersonNote

grpcurl \
	-protoset command.protoset \
	-d '{"persoon_hr": '"${personReference}"', "voornamen": ["Jill", "Johanna"], "voorvoegsel_geslachtsnaam": "", "geslachtsnaam": "Bloem"}' \
	-insecure \
	command-api.groenestede.desq.demo:30023 \
	command.Command/NamenVastgesteld
