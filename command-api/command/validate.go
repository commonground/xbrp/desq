// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package command

// import "errors"

// var (
// 	ErrTimetravelNotImplemented          = errors.New("timetravel is not implemented yet)")
// 	ErrPersonIdentifierEidNotImplemented = errors.New("query based on eid is not implemented yet")
// 	ErrMissingPersonIdentifier           = errors.New("missing person identifier (bsn) in query")
// )

// func (r *AggregationQuery) Validate() error {
// 	if r.Timetravel != nil {
// 		// timestravel is not implemented yet
// 		return ErrTimetravelNotImplemented
// 	}
// 	if r.PersonIdentifier.GetEid() != "" {
// 		return ErrPersonIdentifierEidNotImplemented
// 	}
// 	if r.PersonIdentifier.GetBsn() == "" {
// 		return ErrMissingPersonIdentifier
// 	}
// 	return nil
// }
