// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package commandservice

import (
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

const commandReferencePrefix = `desq-command-`

var ErrInvalidCommandReference = errors.New("invalid command reference")

func EventIDToCommandReference(eventID uint64) string {
	return commandReferencePrefix + strconv.FormatUint(eventID, 10)
}

func CommandReferenceToEventID(commandReference string) (uint64, error) {
	if !strings.HasPrefix(commandReference, commandReferencePrefix) {
		return 0, ErrInvalidCommandReference
	}

	// strip the prefix and parse the remaining number
	eventID, err := strconv.ParseUint(strings.TrimPrefix(commandReference, commandReferencePrefix), 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, ErrInvalidCommandReference.Error())
	}

	return eventID, nil
}

// TODO: perhaps we should include the network and zone name in the reference to
// avoid bugs where a reference from zone A is used to get the status at zone B
// (which may just work, but returns faulty information).
