// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package commandservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/commonground/xbrp/desq/command-api/command"
	"go.uber.org/zap"
)

// StatusHandler provides a Status handler.
type StatusHandler struct {
	env string
	log *zap.Logger

	stmtGetEvent *sqlx.Stmt
}

// NewStatusHandler prepares a Status handler.
func NewStatusHandler(setupContext context.Context, env string, log *zap.Logger, db *sqlx.DB) (*StatusHandler, error) {
	h := &StatusHandler{
		env: env,
		log: log,
	}

	var err error
	h.stmtGetEvent, err = db.PreparexContext(setupContext, `SELECT kind FROM events WHERE id = $1`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare stmtGetEvent")
	}

	return h, nil
}

func (h *StatusHandler) Status(ctx context.Context, req *command.StatusRequest) (*command.StatusReply, error) {
	log := h.log.With(zap.String("command_reference", req.CommandReference))
	log.Info("Status")

	eventID, err := CommandReferenceToEventID(req.CommandReference)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse commandReference")
	}

	var kind string
	err = h.stmtGetEvent.QueryRowxContext(ctx, eventID).Scan(&kind)
	if err != nil {
		// TODO: handle ErrNoRows and inform the client that the command
		// reference is invalid (not existing)
		return nil, errors.Wrap(err, "failed to get event")
	}
	log.Debug("Status request information", zap.String("kind", kind))

	reply := &command.StatusReply{
		Journaled: false, // Not supported yet
		Published: false, // Not supported yet
	}
	return reply, nil
}
