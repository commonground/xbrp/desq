// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

// package commandservice implements the Command Service/API as defined in
// command.proto.
package commandservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/command-api/command"
)

// commandService implements the command.CommandServer.
type commandService struct {
	// See NOTES.md#UnsafeServer
	command.UnsafeCommandServer

	*NewPersonHandler
	*PersonNoteHandler
	*CommandHandler
	*StatusHandler
}

var _ command.CommandServer = &commandService{}

// New prepares a new commandService.
func New(setupContext context.Context, env string, log *zap.Logger, eventDB, journalDB *sqlx.DB) (*commandService, error) {
	d := &commandService{}

	stmtInsertEvent, err := eventDB.PreparexContext(setupContext, `
		INSERT INTO events (person_references, kind, data)
			VALUES ($1, $2, $3)
			RETURNING id
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtInsertEvent")
	}

	d.NewPersonHandler, err = NewNewPersonHandler(setupContext, env, log, stmtInsertEvent, journalDB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare NewPerson handler")
	}
	d.PersonNoteHandler, err = NewPersonNoteHandler(setupContext, env, log, stmtInsertEvent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare PersonNote handler")
	}
	d.CommandHandler, err = NewCommandHandler(setupContext, env, log, stmtInsertEvent, journalDB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare CommandHandler")
	}
	d.StatusHandler, err = NewStatusHandler(setupContext, env, log, eventDB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare Status handler")
	}

	return d, nil
}
