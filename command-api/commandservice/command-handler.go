// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package commandservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/commonground/xbrp/desq/command-api/command"
	"gitlab.com/commonground/xbrp/desq/common/events"
	"gitlab.com/commonground/xbrp/desq/common/references"
)

// CommandHandler provides a CommandHandler.
type CommandHandler struct {
	env string
	log *zap.Logger

	stmtInsertEvent      *sqlx.Stmt
	stmtNewPersonJournal *sqlx.Stmt
}

// NewCommandHandler prepares a CommandHandler.
func NewCommandHandler(setupContext context.Context, env string, log *zap.Logger, stmtInsertEvent *sqlx.Stmt, journalDB *sqlx.DB) (*CommandHandler, error) {
	h := &CommandHandler{
		env:             env,
		log:             log,
		stmtInsertEvent: stmtInsertEvent,
	}

	var err error
	h.stmtNewPersonJournal, err = journalDB.PreparexContext(setupContext, `
		INSERT INTO public.journals (person_reference)
			VALUES ($1)
			RETURNING id
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtNewPersonJournal")
	}

	return h, nil
}

// Dummy Command

func (h *CommandHandler) NamenVastgesteld(ctx context.Context, eventData *events.NamenVastgesteld) (*command.BasicReply, error) {
	h.log.Debug("CommandHandler NamenVastgesteld")
	eventID, err := h.persistEvent(ctx, "NamenVastgesteld", eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return NewBasicReply(*eventID), nil
}

// Real Commands

func (h *CommandHandler) PersoonIngeschreven(ctx context.Context, c *command.PersoonIngeschrevenCommand) (*command.PersoonIngeschrevenCommandReply, error) {
	personReference, err := h.newPerson(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	eventData := events.PersoonIngeschreven{
		PersoonPr:         personReference.Bytes(),
		DatumIngeschreven: c.DatumIngeschreven,
	}

	h.log.Debug("CommandHandler PersoonIngeschreven")
	eventID, err := h.persistEvent(ctx, "PersoonIngeschreven", &eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	reply := &command.PersoonIngeschrevenCommandReply{
		CommandReference: EventIDToCommandReference(*eventID),
		PersonReference:  personReference.Bytes(),
	}
	return reply, nil
}

func (h *CommandHandler) NamenGeregistreerd(ctx context.Context, eventData *events.NamenGeregistreerd) (*command.BasicReply, error) {
	h.log.Debug("CommandHandler NamenGeregistreerd")
	eventID, err := h.persistEvent(ctx, "NamenGeregistreerd", eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return NewBasicReply(*eventID), nil
}

func (h *CommandHandler) BSNToegekend(ctx context.Context, eventData *events.BSNToegekend) (*command.BasicReply, error) {
	h.log.Debug("CommandHandler BSNToegekend")
	eventID, err := h.persistEvent(ctx, "BSNToegekend", eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return NewBasicReply(*eventID), nil
}

func (h *CommandHandler) GeslachtsaanduidingGeregistreerd(ctx context.Context, eventData *events.GeslachtsaanduidingGeregistreerd) (*command.BasicReply, error) {
	h.log.Debug("CommandHandler GeslachtsaanduidingGeregistreerd")
	eventID, err := h.persistEvent(ctx, "GeslachtsaanduidingGeregistreerd", eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return NewBasicReply(*eventID), nil
}

func (h *CommandHandler) WoonadresNLGeregistreerd(ctx context.Context, eventData *events.WoonadresNLGeregistreerd) (*command.BasicReply, error) {
	h.log.Debug("CommandHandler WoonadresNLGeregistreerd")
	eventID, err := h.persistEvent(ctx, "WoonadresNLGeregistreerd", eventData)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return NewBasicReply(*eventID), nil
}

func (h *CommandHandler) newPerson(ctx context.Context) (*references.PersonReference, error) {
	personReference, err := references.NewPersonReference()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create a new personReference")
	}
	h.log.Debug("NewPerson", zap.String("person_reference", personReference.String()))

	var journalID uint64
	err = h.stmtNewPersonJournal.QueryRowxContext(ctx, personReference).Scan(&journalID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new journal")
	}
	return &personReference, nil
}

func (h *CommandHandler) persistEvent(ctx context.Context, kind string, eventData events.EventData) (*uint64, error) {
	eventDataBytes, err := proto.Marshal(eventData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal eventData")
	}
	eventReference, err := references.NewEventReference()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create event reference")
	}

	event := &events.Event{
		Reference: eventReference.Bytes(),
		Kind:      kind,
		Data:      eventDataBytes,
		Date:      timestamppb.Now(),
	}
	eventBytes, err := proto.Marshal(event)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal event")
	}

	personReferences, err := eventData.PersonReferences()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse personreferences")
	}

	var eventID uint64
	err = h.stmtInsertEvent.QueryRowxContext(ctx, personReferences, kind, eventBytes).Scan(&eventID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert event into db")
	}

	return &eventID, nil
}

func NewBasicReply(eventID uint64) *command.BasicReply {
	return &command.BasicReply{
		CommandReference: EventIDToCommandReference(eventID),
	}
}
