// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package commandservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/commonground/xbrp/desq/command-api/command"
	"gitlab.com/commonground/xbrp/desq/common/events"
	"gitlab.com/commonground/xbrp/desq/common/references"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// PersonNoteHandler provides a PersonNote handler.
type PersonNoteHandler struct {
	env string
	log *zap.Logger

	stmtInsertEvent *sqlx.Stmt
}

// NewPersonNoteHandler prepares a PersonNote handler.
func NewPersonNoteHandler(setupContext context.Context, env string, log *zap.Logger, stmtInsertEvent *sqlx.Stmt) (*PersonNoteHandler, error) {
	h := &PersonNoteHandler{
		env:             env,
		log:             log,
		stmtInsertEvent: stmtInsertEvent,
	}

	return h, nil
}

func (h *PersonNoteHandler) PersonNote(ctx context.Context, req *command.PersonNoteRequest) (*command.BasicReply, error) {
	personReference, err := references.PersonReferenceFromBytes(req.PersonReference)
	if err != nil {
		// TODO: status code bad request?
		return nil, errors.Wrap(err, "failed to obtain person reference from request")
	}

	log := h.log.With(zap.String("person_reference", personReference.String()))
	log.Debug("PersonNote", zap.String("note", req.Note))

	personNoteEvent := &events.PersonNoteEvent{
		PersonReference: personReference.Bytes(),
		Note:            req.Note,
	}
	eventDataBytes, err := proto.Marshal(personNoteEvent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal person note event data")
	}

	eventReference, err := references.NewEventReference()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create event reference")
	}

	event := &events.Event{
		Reference: eventReference.Bytes(),
		Kind:      "PersonNoteEvent",
		Data:      eventDataBytes,
		Date:      timestamppb.Now(),
	}
	eventBytes, err := proto.Marshal(event)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal event")
	}

	var eventID uint64
	err = h.stmtInsertEvent.QueryRowxContext(ctx, references.PersonReferenceArray{personReference}, event.Kind, eventBytes).Scan(&eventID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert event into db")
	}

	reply := &command.BasicReply{
		CommandReference: EventIDToCommandReference(eventID),
	}
	return reply, nil
}
