// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package commandservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/commonground/xbrp/desq/command-api/command"
	"gitlab.com/commonground/xbrp/desq/common/events"
	"gitlab.com/commonground/xbrp/desq/common/references"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// NewPersonHandler provides a NewPerson handler.
type NewPersonHandler struct {
	env string
	log *zap.Logger

	stmtInsertEvent      *sqlx.Stmt
	stmtNewPersonJournal *sqlx.Stmt
}

// NewNewPersonHandler prepares a NewPerson handler.
func NewNewPersonHandler(setupContext context.Context, env string, log *zap.Logger, stmtInsertEvent *sqlx.Stmt, journalDB *sqlx.DB) (*NewPersonHandler, error) {
	h := &NewPersonHandler{
		env: env,
		log: log,

		stmtInsertEvent: stmtInsertEvent,
	}

	var err error
	h.stmtNewPersonJournal, err = journalDB.PreparexContext(setupContext, `
		INSERT INTO public.journals (person_reference)
			VALUES ($1)
			RETURNING id
	`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtNewPersonJournal")
	}

	return h, nil
}

func (h *NewPersonHandler) NewPerson(ctx context.Context, req *command.NewPersonRequest) (*command.NewPersonReply, error) {
	personReference, err := references.NewPersonReference()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create a new personReference")
	}
	h.log.Debug("NewPerson", zap.String("person_reference", personReference.String()))

	var journalID uint64
	err = h.stmtNewPersonJournal.QueryRowxContext(ctx, personReference).Scan(&journalID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create new journal")
	}

	newPersonEvent := &events.NewPersonEvent{PersonReference: personReference.Bytes()}
	eventDataBytes, err := proto.Marshal(newPersonEvent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal new person eventData")
	}

	eventReference, err := references.NewEventReference()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create event reference")
	}

	event := &events.Event{
		Reference: eventReference.Bytes(),
		Kind:      "NewPersonEvent",
		Data:      eventDataBytes,
		Date:      timestamppb.Now(),
	}
	eventBytes, err := proto.Marshal(event)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal event")
	}

	var eventID uint64
	err = h.stmtInsertEvent.QueryRowxContext(ctx, references.PersonReferenceArray{personReference}, event.Kind, eventBytes).Scan(&eventID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert event into db")
	}

	reply := &command.NewPersonReply{
		CommandReference: EventIDToCommandReference(eventID),
		PersonReference:  personReference.Bytes(),
	}
	return reply, nil
}
