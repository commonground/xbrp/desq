// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	"context"
	"crypto/tls"
	stdlog "log"
	"net/http"
	"os"
	"strings"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/svent/go-flags"
	"go.opencensus.io/plugin/ocgrpc"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/net/http2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"

	"gitlab.com/commonground/xbrp/desq/command-api/command"
	"gitlab.com/commonground/xbrp/desq/command-api/commandservice"
	"gitlab.com/commonground/xbrp/desq/common/commondb"
	"gitlab.com/commonground/xbrp/desq/common/commonflags"
	"gitlab.com/commonground/xbrp/desq/common/ecosystem"
	"gitlab.com/commonground/xbrp/desq/common/process"
	"gitlab.com/commonground/xbrp/desq/common/selfsignedcert"
)

type Options struct {
	Network       string `long:"network" env:"DESQ_NETWORK" default:"local"`
	ListenAddress string `long:"listen-address" env:"DESQ_LISTEN_ADDRESS" default:"127.0.0.1:4000"`

	HTTPInsecure bool `long:"http-insecure" env:"DESQ_HTTP_INSECURE" description:"Listen for HTTP connections without TLS"`

	commonflags.EventDB
	commonflags.JournalDB
}

var options Options

func main() {
	// Parse arguments
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		stdlog.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		stdlog.Fatalf("unexpected arguments: %v", args)
	}
	// Setup structured logger
	log, err := zap.NewDevelopment()
	if err != nil {
		stdlog.Fatalf("failed to setup zap logging: %v", err)
	}
	log.Info("starting the command-api")

	// Write a log before shutting down
	proc := process.New(log)

	// connect zap for global grpc logging
	grpc_zap.ReplaceGrpcLoggerV2(log)

	// create grpc_zap options for stream and unary inteceptors
	grpcZapOptions := []grpc_zap.Option{
		grpc_zap.WithDecider(func(fullMethodName string, err error) bool {
			return fullMethodName != "/grpc.health.v1.Health/Check"
		}),
		grpc_zap.WithDurationField(func(duration time.Duration) zapcore.Field {
			return zap.Duration("grpc.time_ns", duration)
		}),
	}

	environment := ecosystem.NetworkByName(options.Network).Environment.String()

	// setup database connections
	log.Debug("setting up postgres connection to event db", zap.String("dsn", options.EventDB.DSNSafe()))
	eventDB := commondb.SetupDBOrFatal(log.With(zap.String("db", "event")), options.EventDB)
	log.Debug("setting up postgres connection to journal db", zap.String("dsn", options.JournalDB.DSNSafe()))
	journalDB := commondb.SetupDBOrFatal(log.With(zap.String("db", "journal")), options.JournalDB)

	// TODO: db version assertions

	// setup service
	commandService, err := commandservice.New(context.Background(), environment, log, eventDB, journalDB)
	if err != nil {
		log.Fatal("error creating command service", zap.Error(err))
	}

	tlsConfig, err := selfsignedcert.TLSConfig()
	if err != nil {
		log.Fatal("failed generating tls config", zap.Error(err))
	}

	// setup grpc server
	grpcServerOptions := []grpc.ServerOption{
		grpc.Creds(credentials.NewTLS(tlsConfig)),
		grpc.StatsHandler(&ocgrpc.ServerHandler{}),
		grpc_middleware.WithStreamServerChain(
			grpc_ctxtags.StreamServerInterceptor(),
			grpc_zap.StreamServerInterceptor(log, grpcZapOptions...),
			grpc_recovery.StreamServerInterceptor(),
		),
		grpc_middleware.WithUnaryServerChain(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_zap.UnaryServerInterceptor(log, grpcZapOptions...),
			grpc_recovery.UnaryServerInterceptor(),
		),
	}

	healthServer := health.NewServer()
	healthServer.SetServingStatus("command.Command", grpc_health_v1.HealthCheckResponse_SERVING)

	grpcServer := grpc.NewServer(grpcServerOptions...)
	command.RegisterCommandServer(grpcServer, commandService)
	grpc_health_v1.RegisterHealthServer(grpcServer, healthServer)

	grpcWebServer := grpcweb.WrapServer(grpcServer)

	httpServer := &http.Server{
		Addr:      options.ListenAddress,
		TLSConfig: tlsConfig,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// log.Info("Received request from", zap.String("content-type", r.Header.Get("content-type")), zap.String("Content-Type", r.Header.Get("Content-Type")), zap.String("path", r.URL.Path), zap.String("remoteAddr", r.RemoteAddr), zap.String("UA", r.UserAgent()))
			// CORS for grpc-web
			if grpcWebServer.IsAcceptableGrpcCorsRequest(r) {
				grpcWebCors(w, r)
				return
			}
			// grpc-web
			if grpcWebServer.IsGrpcWebRequest(r) {
				grpcWebCors(w, r)
				grpcWebServer.HandleGrpcWebRequest(w, r)
				return
			}
			// "Native" grpc
			if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
				grpcServer.ServeHTTP(w, r)
				return
			}
			// Generic HTTP request, e.g. Pod Readiness Probe and Ingress HealthCheck
			if r.URL.Path == "/" {
				w.WriteHeader(http.StatusOK)
				w.Write([]byte("healthy"))
				return
			}
			http.NotFound(w, r)
		}),
	}

	http2.ConfigureServer(httpServer, &http2.Server{})

	httpListener, err := tls.Listen("tcp", options.ListenAddress, tlsConfig)
	if err != nil {
		log.Fatal("failed to listen on tcp port", zap.Error(err))
		return
	}

	go func() {
		<-proc.Shutdown()
		log.Info("received a shutdown signal, shutting down the command-api")
		err := httpServer.Shutdown(context.Background())
		if err != nil {
			log.Fatal("failed to gracefully shutdown http server", zap.Error(err))
		}
		os.Exit(0)
	}()
	err = httpServer.Serve(httpListener)
	if err != nil {
		if err == http.ErrServerClosed {
			// Wait indefinitely (blocking main goroutine from returning) so that
			// the graceful shutdown can be completed.
			select {}
		}
		log.Fatal("failed running http server", zap.Error(err))
	}
}

func grpcWebCors(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-User-Agent, X-Grpc-Web, accesskey")
}
