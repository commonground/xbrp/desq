## Generating GRPC code

Make sure you have the Dart generator plugin configured

    $ flutter pub global activate protoc_plugin

Make sure the plugin is in your PATH

    $ export PATH="$PATH":"$HOME/.pub-cache/bin"
    or
    $ set -gx PATH $HOME/.pub-cache/bin $PATH

Then run

    $ ./generate.sh

For more documentation see: https://grpc.io/docs/languages/dart/quickstart/
