#!/bin/bash

protoc management/management.proto --go_out=. --go-grpc_out=.
protoc --dart_out=grpc:../management_ui/lib management/management.proto
