// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package managementservice

import (
	"context"

	"gitlab.com/commonground/xbrp/desq/common/references"
	"gitlab.com/commonground/xbrp/desq/management-api/management"
	"go.uber.org/zap"
)

type DBJournal struct {
	Id              uint64
	PersonReference references.PersonReference `db:"person_reference"`
	Created         string
}

func (h *ManagementService) Journals(ctx context.Context, req *management.JournalsRequest) (*management.JournalsReply, error) {
	h.log.Info("Journals")

	dbJournals := []DBJournal{}
	err := h.stmtGetJournals.Select(&dbJournals)
	if err != nil {
		// TODO: handle ErrNoRows and other errors and inform the client
		h.log.Info(err.Error())
		// return nil, errors.Wrap(err, "failed to get journals")
	}

	journals := []*management.Journal{}
	for _, dbJournal := range dbJournals {
		h.log.Info(dbJournal.Created)
		journal := management.Journal{
			PersonReference: dbJournal.PersonReference.Bytes(),
		}
		h.log.Info("Journals", zap.ByteString("journal.PersonReference", journal.PersonReference))
		journals = append(journals, &journal)
	}

	reply := &management.JournalsReply{
		Journals: journals,
	}
	return reply, nil
}
