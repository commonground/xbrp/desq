// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package managementservice

import (
	"context"

	"gitlab.com/commonground/xbrp/desq/zone-api/zone"
)

func (h *ManagementService) ZoneStatus(ctx context.Context, req *zone.ZoneStatusRequest) (*zone.ZoneStatusReply, error) {
	h.log.Info("ZoneStatus: {env: " + h.env + ", zid: " + "DUMMY" + ", version: " + h.version + "}")
	// TODO call zone-api instead of re-implementing here

	reply := &zone.ZoneStatusReply{
		Status: &zone.ZoneStatus{
			Zid:     "DUMMY",
			Status:  "online",
			Version: h.version,
		},
	}
	return reply, nil
}
