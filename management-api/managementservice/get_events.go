// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package managementservice

import (
	"context"

	"gitlab.com/commonground/xbrp/desq/common/events"
	"gitlab.com/commonground/xbrp/desq/management-api/management"
	"go.uber.org/zap"
)

func (h *ManagementService) Events(ctx context.Context, req *management.EventsRequest) (*management.EventsReply, error) {
	// log := h.log.With(zap.String("command_reference", req.CommandReference))
	h.log.Info("Events")

	dbEvents := []events.DBEvent{}
	err := h.stmtGetEvents.Select(&dbEvents)
	if err != nil {
		// TODO: handle ErrNoRows and other errors and inform the client
		h.log.Info(err.Error())
		// return nil, errors.Wrap(err, "failed to get events")
	}

	// Note: we use a custom Event type for the Management API here to be able to add the Event zone, and leave out the database id, personreferences and journalhash.
	events := []*management.ManagementAPIEvent{}
	h.log.Info("NumEvents: ", zap.Int("Num Events", len(dbEvents)))

	for _, dbEvent := range dbEvents {
		event := management.ManagementAPIEvent{
			Zid:   h.zid,
			Event: dbEvent.Data, // TODO: rename dbEvent.Data to dbEvent.Event
		}
		events = append(events, &event)
	}

	reply := &management.EventsReply{
		Events: events,
	}
	return reply, nil
}
