// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

package managementservice

import (
	"context"
	"crypto/tls"
	"fmt"

	"gitlab.com/commonground/xbrp/desq/common/ecosystem"
	"gitlab.com/commonground/xbrp/desq/management-api/management"
	"gitlab.com/commonground/xbrp/desq/zone-api/zone"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func (h *ManagementService) NetworkStatus(ctx context.Context, req *management.NetworkStatusRequest) (*management.NetworkStatusReply, error) {
	h.log.Info("NetworkStatus")
	networkName := "kubedev"

	// TODO move to a separate network-status-worker
	zones := ecosystem.NetworkByName(networkName).Zones()
	fmt.Println(fmt.Sprint(len(zones)) + " ZONES")
	zoneStatusMap := make([]*zone.ZoneStatus, 0)
	for _, z := range zones {
		zoneStatus := StatusForZone(z)
		zoneStatusMap = append(zoneStatusMap, zoneStatus)
	}

	reply := &management.NetworkStatusReply{
		Zones: zoneStatusMap,
	}
	return reply, nil
}

// Status returns the status of the Zone
func StatusForZone(z ecosystem.Zone) *zone.ZoneStatus {
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}
	creds := credentials.NewTLS(tlsConfig)

	dialAddress := "zone-api." + z.Address
	cc, err := grpc.Dial(dialAddress, grpc.WithTransportCredentials(creds))
	if err != nil {
		// Unable to connect
		return &zone.ZoneStatus{
			Zid:     z.ZID,
			Status:  "offline",
			Version: "unknown",
		}
	}
	defer cc.Close()

	c := zone.NewZoneClient(cc)

	req := &zone.ZoneStatusRequest{}
	res, err := c.ZoneStatus(context.Background(), req)
	if err != nil {
		// Connected, but unavailable
		return &zone.ZoneStatus{
			Zid:     z.ZID,
			Status:  "unavailable",
			Version: "unknown",
		}
	}
	return res.Status
}
