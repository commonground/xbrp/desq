// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package managementservice

import (
	"context"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/commonground/xbrp/desq/management-api/management"
)

// managementservice implements the management.ManagementServer.
type ManagementService struct {
	// See NOTES.md#UnsafeServer
	management.UnsafeManagementServer
	env             string
	version         string
	zid             string
	log             *zap.Logger
	stmtGetEvents   *sqlx.Stmt
	eventDB         *sqlx.DB
	stmtGetJournals *sqlx.Stmt
	journalDB       *sqlx.DB
}

// A test to make sure the zoneService implements everything that is required by ZoneServer
var _ management.ManagementServer = &ManagementService{}

func New(setupContext context.Context, env string, log *zap.Logger, eventDB *sqlx.DB, journalDB *sqlx.DB, version string, zid string) (*ManagementService, error) {
	h := &ManagementService{
		env:       env,
		log:       log,
		version:   version,
		zid:       zid,
		eventDB:   eventDB,
		journalDB: journalDB,
	}

	var err error
	h.stmtGetEvents, err = eventDB.PreparexContext(setupContext, `SELECT id, person_references, kind, data, journal_hash FROM events`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare stmtGetEvents")
	}

	h.stmtGetJournals, err = journalDB.PreparexContext(setupContext, `SELECT id, person_reference, created FROM journals`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare stmtGetJournals")
	}

	return h, nil
}
