// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package management

import (
	context "context"
	zone "gitlab.com/commonground/xbrp/desq/zone-api/zone"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ManagementClient is the client API for Management service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ManagementClient interface {
	// ZoneStatus returns the status of the zone
	ZoneStatus(ctx context.Context, in *zone.ZoneStatusRequest, opts ...grpc.CallOption) (*zone.ZoneStatusReply, error)
	// NetworkStatus returns the status of the entire network
	NetworkStatus(ctx context.Context, in *NetworkStatusRequest, opts ...grpc.CallOption) (*NetworkStatusReply, error)
	// Events returns the Events in the zone
	Events(ctx context.Context, in *EventsRequest, opts ...grpc.CallOption) (*EventsReply, error)
	// Journals returns the Events in the zone
	Journals(ctx context.Context, in *JournalsRequest, opts ...grpc.CallOption) (*JournalsReply, error)
}

type managementClient struct {
	cc grpc.ClientConnInterface
}

func NewManagementClient(cc grpc.ClientConnInterface) ManagementClient {
	return &managementClient{cc}
}

func (c *managementClient) ZoneStatus(ctx context.Context, in *zone.ZoneStatusRequest, opts ...grpc.CallOption) (*zone.ZoneStatusReply, error) {
	out := new(zone.ZoneStatusReply)
	err := c.cc.Invoke(ctx, "/management.Management/ZoneStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *managementClient) NetworkStatus(ctx context.Context, in *NetworkStatusRequest, opts ...grpc.CallOption) (*NetworkStatusReply, error) {
	out := new(NetworkStatusReply)
	err := c.cc.Invoke(ctx, "/management.Management/NetworkStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *managementClient) Events(ctx context.Context, in *EventsRequest, opts ...grpc.CallOption) (*EventsReply, error) {
	out := new(EventsReply)
	err := c.cc.Invoke(ctx, "/management.Management/Events", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *managementClient) Journals(ctx context.Context, in *JournalsRequest, opts ...grpc.CallOption) (*JournalsReply, error) {
	out := new(JournalsReply)
	err := c.cc.Invoke(ctx, "/management.Management/Journals", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ManagementServer is the server API for Management service.
// All implementations must embed UnimplementedManagementServer
// for forward compatibility
type ManagementServer interface {
	// ZoneStatus returns the status of the zone
	ZoneStatus(context.Context, *zone.ZoneStatusRequest) (*zone.ZoneStatusReply, error)
	// NetworkStatus returns the status of the entire network
	NetworkStatus(context.Context, *NetworkStatusRequest) (*NetworkStatusReply, error)
	// Events returns the Events in the zone
	Events(context.Context, *EventsRequest) (*EventsReply, error)
	// Journals returns the Events in the zone
	Journals(context.Context, *JournalsRequest) (*JournalsReply, error)
	mustEmbedUnimplementedManagementServer()
}

// UnimplementedManagementServer must be embedded to have forward compatible implementations.
type UnimplementedManagementServer struct {
}

func (UnimplementedManagementServer) ZoneStatus(context.Context, *zone.ZoneStatusRequest) (*zone.ZoneStatusReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ZoneStatus not implemented")
}
func (UnimplementedManagementServer) NetworkStatus(context.Context, *NetworkStatusRequest) (*NetworkStatusReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NetworkStatus not implemented")
}
func (UnimplementedManagementServer) Events(context.Context, *EventsRequest) (*EventsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Events not implemented")
}
func (UnimplementedManagementServer) Journals(context.Context, *JournalsRequest) (*JournalsReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Journals not implemented")
}
func (UnimplementedManagementServer) mustEmbedUnimplementedManagementServer() {}

// UnsafeManagementServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ManagementServer will
// result in compilation errors.
type UnsafeManagementServer interface {
	mustEmbedUnimplementedManagementServer()
}

func RegisterManagementServer(s grpc.ServiceRegistrar, srv ManagementServer) {
	s.RegisterService(&Management_ServiceDesc, srv)
}

func _Management_ZoneStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(zone.ZoneStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ManagementServer).ZoneStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/management.Management/ZoneStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ManagementServer).ZoneStatus(ctx, req.(*zone.ZoneStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Management_NetworkStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NetworkStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ManagementServer).NetworkStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/management.Management/NetworkStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ManagementServer).NetworkStatus(ctx, req.(*NetworkStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Management_Events_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EventsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ManagementServer).Events(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/management.Management/Events",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ManagementServer).Events(ctx, req.(*EventsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Management_Journals_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JournalsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ManagementServer).Journals(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/management.Management/Journals",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ManagementServer).Journals(ctx, req.(*JournalsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Management_ServiceDesc is the grpc.ServiceDesc for Management service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Management_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "management.Management",
	HandlerType: (*ManagementServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ZoneStatus",
			Handler:    _Management_ZoneStatus_Handler,
		},
		{
			MethodName: "NetworkStatus",
			Handler:    _Management_NetworkStatus_Handler,
		},
		{
			MethodName: "Events",
			Handler:    _Management_Events_Handler,
		},
		{
			MethodName: "Journals",
			Handler:    _Management_Journals_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "management-api/management/management.proto",
}
