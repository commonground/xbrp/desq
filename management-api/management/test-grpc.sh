#!/bin/bash

set -euxo pipefail

status=$(grpcurl \
	-protoset desq.protoset \
	-d '{}' \
	-insecure \
	management-api.groenestede.desq.demo:30013 \
	management.Management/ZoneStatus)

echo $status;


# grpcurl \
# 	-protoset command.protoset \
# 	-d '{"personReference": '"${personReference}"', "note": "Dit is een notitie over een persoon."}' \
# 	-insecure \
# 	groenestede.desq.demo:30023 \
# 	command.Command/PersonNote

# grpcurl \
# 	-protoset command.protoset \
# 	-d '{"persoon_hr": '"${personReference}"', "voornamen": ["Jill", "Johanna"], "voorvoegsel_geslachtsnaam": "", "geslachtsnaam": "Bloem"}' \
# 	-insecure \
# 	groenestede.desq.demo:30023 \
# 	command.Command/NamenVastgesteld
