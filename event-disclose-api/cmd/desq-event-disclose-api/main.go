// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

package main

import (
	stdlog "log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	flags "github.com/jessevdk/go-flags"
	"gitlab.com/commonground/xbrp/desq/common/process"
	"go.uber.org/zap"
)

type Options struct {
	Network string `long:"network" env:"DESQ_NETWORK" default:"local"`
	Zone    string `long:"zone" env:"DESQ_ZONE" required:"true"`

	ListenAddress string `long:"listen-address" env:"DESQ_LISTEN_ADDRESS"`
}

var options Options

func main() {
	// Parse arguments
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		stdlog.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		stdlog.Fatalf("unexpected arguments: %v", args)
	}

	// Setup structured logger
	log, err := zap.NewDevelopment()
	if err != nil {
		stdlog.Fatalf("failed to setup zap logging: %v", err)
	}
	log = log.With(zap.String("zone", options.Zone))
	log.Info("starting the eventservice")

	// Write a log before shutting down
	proc := process.New(log)
	go func() {
		<-proc.Shutdown()
		log.Info("shutting down the eventservice")
		os.Exit(0)
	}()


	app := fiber.New(fiber.Config{
		CaseSensitive:         true,
		StrictRouting:         true,
		DisableStartupMessage: true,
	})

	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders:     "",
		AllowCredentials: false,
		ExposeHeaders:    "",
		MaxAge:           0,
	}))

	app.Get("/health", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})

	err = app.Listen(options.ListenAddress)
	if err != nil {
		log.Fatal("failed to listen and serve http", zap.Error(err))
	}
}
