-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.3
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---
-- -- object: command_api | type: ROLE --
-- -- DROP ROLE IF EXISTS command_api;
-- CREATE ROLE command_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE command_api IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 
-- -- object: event_worker | type: ROLE --
-- -- DROP ROLE IF EXISTS event_worker;
-- CREATE ROLE event_worker WITH 
-- 	LOGIN;
-- -- ddl-end --
-- COMMENT ON ROLE event_worker IS E'This user must exist before the db migrations are ran.';
-- -- ddl-end --
-- 
-- -- object: management_api | type: ROLE --
-- -- DROP ROLE IF EXISTS management_api;
-- CREATE ROLE management_api WITH 
-- 	LOGIN;
-- -- ddl-end --
-- 

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- -- object: event | type: DATABASE --
-- -- DROP DATABASE IF EXISTS event;
-- CREATE DATABASE event;
-- -- ddl-end --
-- 

SET check_function_bodies = false;
-- ddl-end --

-- object: public.events_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.events_id_seq CASCADE;
CREATE SEQUENCE public.events_id_seq
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE public.events_id_seq OWNER TO postgres;
-- ddl-end --

-- object: public.events_notify_new_event | type: FUNCTION --
-- DROP FUNCTION IF EXISTS public.events_notify_new_event() CASCADE;
CREATE FUNCTION public.events_notify_new_event ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
	NOTIFY new_event;
	RETURN NEW;
END;
$$;
-- ddl-end --
ALTER FUNCTION public.events_notify_new_event() OWNER TO postgres;
-- ddl-end --

-- object: public.events | type: TABLE --
-- DROP TABLE IF EXISTS public.events CASCADE;
CREATE TABLE public.events (
	id bigint NOT NULL DEFAULT nextval('public.events_id_seq'::regclass),
	person_references bytea[] NOT NULL,
	kind text NOT NULL,
	data bytea NOT NULL,
	journal_hash bytea,
	CONSTRAINT events_pk PRIMARY KEY (id),
	CONSTRAINT events_ck_person_references CHECK ((array_length(person_references, 1) >= 1)
--AND
--(octet_length(array_agg(person_references, '')) = array_length(person_references, 1)*16)
)

);
-- ddl-end --
ALTER TABLE public.events OWNER TO postgres;
-- ddl-end --

-- object: events_tr_notify_new_event | type: TRIGGER --
-- DROP TRIGGER IF EXISTS events_tr_notify_new_event ON public.events CASCADE;
CREATE TRIGGER events_tr_notify_new_event
	AFTER INSERT 
	ON public.events
	FOR EACH STATEMENT
	EXECUTE PROCEDURE public.events_notify_new_event();
-- ddl-end --

-- object: grant_raw_83ddbf596c | type: PERMISSION --
GRANT SELECT,INSERT,UPDATE
   ON TABLE public.events
   TO command_api;
-- ddl-end --

-- object: grant_rw_d84a37e83b | type: PERMISSION --
GRANT SELECT,UPDATE
   ON TABLE public.events
   TO event_worker;
-- ddl-end --

-- object: "grant_U_3bfd1926ae" | type: PERMISSION --
GRANT USAGE
   ON SEQUENCE public.events_id_seq
   TO command_api;
-- ddl-end --

-- object: grant_r_7b8950780c | type: PERMISSION --
GRANT SELECT
   ON TABLE public.events
   TO management_api;
-- ddl-end --


