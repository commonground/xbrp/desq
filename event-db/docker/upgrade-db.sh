#!/bin/bash
set -euxo pipefail

/usr/local/bin/migrate \
    --database "postgres://${PGUSER}:${PGPASSWORD}@${PGHOST}:5432/${PGDATABASE}?connect_timeout=5" \
    --lock-timeout 600 \
    --path /db-migrations/ \
    up
