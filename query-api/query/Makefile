PROTO_ROOT_DIR = ${HOME}/apps/protobuf/include
RESIDENT_UI_DIR = ${HOME}/src/gitlab.com/commonground/xbrp/desq/resident_ui
WEDDING_UI_DIR = ${HOME}/src/gitlab.com/commonground/xbrp/desq/wedding_ui

.PHONY: all
all: gen

## Dart requires you to manually ship all google provided proto files too.
.PHONY: _gendart
_gendart:
	@protoc -I=. -I=${GOPATH}/src --dart_out=grpc:${RESIDENT_UI_DIR}/lib/query query.proto
	@protoc -I$(PROTO_ROOT_DIR) --dart_out=${RESIDENT_UI_DIR}/lib/query $(PROTO_ROOT_DIR)/google/protobuf/*.proto

	@protoc -I=. -I=${GOPATH}/src --dart_out=grpc:${WEDDING_UI_DIR}/lib/query query.proto
	@protoc -I$(PROTO_ROOT_DIR) --dart_out=${WEDDING_UI_DIR}/lib/query $(PROTO_ROOT_DIR)/google/protobuf/*.proto

.PHONY: _gengo
_gengo:
	@protoc \
		-I=. \
		-I=${GOPATH}/src \
		-I=${GOPATH}/src/github.com/gogo/protobuf/protobuf \
		--gogofaster_out=\
plugins=grpc,\
Mgoogle/protobuf/any.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/struct.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/wrappers.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types:\
. \
		./query.proto

.PHONY: _genprotoset
_genprotoset:
	@protoc \
		-I=. \
		-I=${GOPATH}/src \
		-I=${GOPATH}/src/github.com/gogo/protobuf/protobuf \
    	--descriptor_set_out=query.protoset \
    	--include_imports \
    query.proto

.PHONY: gen
gen: _gengo _genprotoset _gendart
