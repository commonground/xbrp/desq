// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';
import 'package:management_ui/providers/ecosystem_provider_mock.dart';
import 'package:management_ui/providers/event_provider.dart';
import 'package:management_ui/providers/event_provider_mock.dart';
import 'package:management_ui/providers/personjournal_provider.dart';
import 'package:management_ui/providers/personjournal_provider_mock.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:provider/provider.dart';
import 'package:management_ui/app.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<EnvironmentRepository>(
          create: (_) => EnvironmentRepository(
            municipality: "bovenhove",
          ),
        ),
        Provider<EventProvider>(
          create: (_) => MockEventProvider(),
        ),
        Provider<PersonJournalProvider>(
          create: (_) => MockPersonJournalProvider(),
        ),
        Provider<EcosystemProvider>(
          create: (_) => MockEcosystemProvider(),
        ),
      ],
      child: const ManagementApp(
        municipality: "bovenhove",
      ),
    ),
  );
}
