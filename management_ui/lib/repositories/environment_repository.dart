// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

class EnvironmentRepository {
  final String municipality;

  EnvironmentRepository({this.municipality});

  String getMunicipalityName() {
    const Map<String, String> municipalityNames = {
      'bovenhove': 'Bovenhove',
      'groenestede': 'Groenestede',
      'oudescha': 'Oudescha',
      'heidedal': 'Heidedal',
    };
    return municipalityNames[municipality] ?? 'Onbekende gemeente';
  }
}
