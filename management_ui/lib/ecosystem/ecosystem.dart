// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:management_ui/ecosystem/network.dart';
import 'package:management_ui/ecosystem/zone.dart';

class Ecosystem {
  static Network getNetworkByName(String name) {
    switch (name) {
      case 'development':
        return Network('development', {
          'bovenhove': Zone(
            zid: 'bovenhove',
            name: 'Bovenhove',
            host: 'localhost',
            port: 3001,
            insecure: true,
          ),
          'oudescha': Zone(
            zid: 'oudescha',
            name: 'Oudescha',
            host: 'localhost',
            port: 3002,
            insecure: true,
          ),
          'groenestede': Zone(
            zid: 'groenestede',
            name: 'Groenestede',
            host: 'localhost',
            port: 3003,
            insecure: true,
          ),
          'heidedal': Zone(
            zid: 'heidedal',
            name: 'Heidedal',
            host: 'localhost',
            port: 3004,
            insecure: true,
          ),
        });
      case 'kubedev':
        return Network('kubedev', {
          'bovenhove': Zone(
            zid: 'bovenhove',
            name: 'Bovenhove',
            host: 'management-api.bovenhove.desq.kubedev',
            port: 80,
            insecure: true,
          ),
          'oudescha': Zone(
            zid: 'oudescha',
            name: 'Oudescha',
            host: 'management-api.oudescha.desq.kubedev',
            port: 80,
            insecure: true,
          ),
          'groenestede': Zone(
            zid: 'groenestede',
            name: 'Groenestede',
            host: 'management-api.groenestede.desq.kubedev',
            port: 80,
            insecure: true,
          ),
          'heidedal': Zone(
            zid: 'heidedal',
            name: 'Heidedal',
            host: 'management-api.heidedal.desq.kubedev',
            port: 80,
            insecure: true,
          ),
        });
      case 'demo':
        return Network('demo', {
          'bovenhove': Zone(
            zid: 'bovenhove',
            name: 'Bovenhove',
            host: 'management-api.bovenhove.desq.demo',
            port: 80,
            insecure: true,
          ),
          'oudescha': Zone(
            zid: 'oudescha',
            name: 'Oudescha',
            host: 'management-api.oudescha.desq.demo',
            port: 80,
            insecure: true,
          ),
          'groenestede': Zone(
            zid: 'groenestede',
            name: 'Groenestede',
            host: 'management-api.groenestede.desq.demo',
            port: 80,
            insecure: true,
          ),
          'heidedal': Zone(
            zid: 'heidedal',
            name: 'Heidedal',
            host: 'management-api.heidedal.desq.demo',
            port: 80,
            insecure: true,
          ),
        });
      case 'xbrp-raspi-net':
        return Network('xbrp-raspi-net', {
          'bovenhove': Zone(
            zid: 'bovenhove',
            name: 'Bovenhove',
            host: '',
            port: 80,
            insecure: true,
          ),
          'oudescha': Zone(
            zid: 'oudescha',
            name: 'Oudescha',
            host: '',
            port: 80,
            insecure: true,
          ),
          'groenestede': Zone(
            zid: 'groenestede',
            name: 'Groenestede',
            host: '',
            port: 80,
            insecure: true,
          ),
          'heidedal': Zone(
            zid: 'heidedal',
            name: 'Heidedal',
            host: '',
            port: 80,
            insecure: true,
          ),
        });
      case 'meterkasten':
      case 'cloudnet':
      case 'in2029staging':
      case 'in2029':
      case 'main':
      default:
        return Network('unimplemented', {});
    }
  }
}
