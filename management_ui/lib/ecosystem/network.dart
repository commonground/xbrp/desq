// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:management_ui/ecosystem/zone.dart';

class Network {
  String nid;
  Map<String, Zone> zones;

  Network(this.nid, this.zones);

  Zone getZoneByZID(String zid) {
    return zones[zid];
  }
}
