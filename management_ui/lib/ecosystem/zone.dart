class Zone {
  // TODO distinqush between 'public' zoneHost/zonePort and internal managementAPI
  String zid;
  String name;
  String host;
  int port;
  bool insecure;

  String get address => '$host:$port';

  Zone({this.zid, this.name, this.host, this.port, this.insecure = false});
}
