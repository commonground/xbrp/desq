// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/desq/zone-api/zone/zone.pb.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:management_ui/screens/events_screen.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  static const routePath = '/login';

  @override
  Widget build(BuildContext context) {
    final String municipalityName =
        Provider.of<EnvironmentRepository>(context).getMunicipalityName();

    return Scaffold(
      appBar: AppBar(
        title: Text('Beheeromgeving $municipalityName'),
      ),
      backgroundColor: Colors.grey[200],
      body: LoginForm(),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String municipality;
  Future<ZoneStatus> zoneStatus;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    municipality = Provider.of<EnvironmentRepository>(context).municipality;
    zoneStatus = Provider.of<EcosystemProvider>(context).getZoneStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 400,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: FutureBuilder(
                future: zoneStatus,
                initialData: ZoneStatus(
                  zid: municipality,
                  status: "Loading...",
                ),
                builder:
                    (BuildContext context, AsyncSnapshot<ZoneStatus> snapshot) {
                  if (snapshot.hasError) {
                    return Center(child: Text("Error: ${snapshot.error}"));
                  }
                  if (!snapshot.hasData) {
                    return const Center(child: Text("Loading.."));
                  }
                  return Center(
                    child: Text(
                        "${snapshot.data.zid}, ${snapshot.data.status}, ${snapshot.data.version}"),
                  );
                },
              ),
            ),
            Image.asset('assets/images/$municipality.png', height: 202),
            const SizedBox(height: 40),
            // ignore: deprecated_member_use
            FlatButton(
              minWidth: 300,
              height: 50,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0),
              ),
              color: Theme.of(context).primaryColor,
              onPressed: () {
                Navigator.of(context).pushNamed(EventsScreen.routePath);
              },
              child: const Text(
                "Inloggen",
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
