// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:management_ui/util/base62.dart';
import 'package:management_ui/models/management_ui_event_model.dart';
import 'package:management_ui/desq/common/events/events.pb.dart';
import 'package:management_ui/providers/event_provider.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:management_ui/widgets/menu.dart';
import 'package:provider/provider.dart';

class EventsScreen extends StatefulWidget {
  static const routePath = '/gebeurtenissen';

  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  Future<List<ManagementUIEvent>> eventsFuture;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    eventsFuture = Provider.of<EventProvider>(context).getEvents();
  }

  @override
  Widget build(BuildContext context) {
    final String municipalityName =
        Provider.of<EnvironmentRepository>(context).getMunicipalityName();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Gebeurtenissen"),
      ),
      drawer: MenuDrawer(),
      body: FutureBuilder(
        future: eventsFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ManagementUIEvent>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error: ${snapshot.error}"));
          }
          if (!snapshot.hasData) {
            return const Center(child: Text("Loading.."));
          }
          final events = snapshot.data;
          debugPrint("Events: ${events.length}");
          if (events.isEmpty) {
            return const Center(
                child: Text(
              "Er zijn nog geen gebeurtenissen.",
              style: TextStyle(fontSize: 16),
            ));
          }
          return Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(15),
                child: Text(
                  "Gebeurtenissen $municipalityName",
                  style: const TextStyle(fontSize: 18),
                ),
              ),
              Expanded(
                child: ListView.separated(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  itemCount: events.length,
                  itemBuilder: (BuildContext context, int index) {
                    final ManagementUIEvent event = events[index];
                    return Card(
                      child: Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: Icon(_iconDataForEvent(event),
                                      size: 24,
                                      color: _iconColorForZid(event.zid)),
                                ),
                                Expanded(
                                    child: _buildEventDetailsContainer(event)),
                              ],
                            ),
                          ),
                          Container(
                            color: _backgroundColorForZid(event.zid),
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  _zidToVaststeller(event.zid),
                                  style: const TextStyle(
                                      fontSize: 14, color: Color(0xFF333333)),
                                ),
                                Text(
                                  DateFormat('dd-MM-yyyy hh:mm')
                                      .format(event.date),
                                  style: const TextStyle(
                                      fontSize: 14, color: Color(0xFF333333)),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(height: 5),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  Widget _buildEventDetailsContainer(ManagementUIEvent event) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(_eventTypeToCaption(event.eventData.runtimeType),
              style: const TextStyle(
                  color: Color(0xFF4f4f4f),
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  height: 2)),
          Text(_propertiesTextForEvent(event),
              style: const TextStyle(color: Color(0xFF6B6B6B))),
        ],
      ),
    );
  }

  String _eventTypeToCaption(Type eventType) {
    switch (eventType) {
      case NewPersonEvent:
        return 'NewPersonEvent';
      case PersonNoteEvent:
        return 'NoteEvent';
      case NamenVastgesteld:
        return 'Namen Vastgesteld';
      case PersoonIngeschreven:
        return 'Persoon Ingeschreven';
      case BSNToegekend:
        return 'BSN Toegekend';
      case WoonadresNLGeregistreerd:
        return 'Woonadres Nederland Geregistreerd';
      case NamenGeregistreerd:
        return 'Namen Geregistreerd';
      case GeslachtsaanduidingGeregistreerd:
        return 'Geslachtsaanduiding Geregistreerd';
      default:
        return 'Onbekend event:$eventType';
    }
  }

  String _propertiesTextForEvent(ManagementUIEvent event) {
    switch (event.eventData.runtimeType) {
      case NewPersonEvent:
        final desqEvent = event.eventData as NewPersonEvent;
        final personReference =
            Base62.encodeFromList(desqEvent.personReference);
        return "PersonReference: $personReference";
      case PersonNoteEvent:
        final desqEvent = event.eventData as PersonNoteEvent;
        final personReference =
            Base62.encodeFromList(desqEvent.personReference);
        return "PersonReference: $personReference\nNotitie: ${desqEvent.note}";
      case NamenVastgesteld:
        final desqEvent = event.eventData as NamenVastgesteld;
        final voornamen =
            desqEvent.voornamen.reduce((value, element) => "$value $element");
        final voorvoegselsOrPlaceholder =
            desqEvent.voorvoegselGeslachtsnaam == ""
                ? "<geen>"
                : desqEvent.voorvoegselGeslachtsnaam;
        final personReference = Base62.encodeFromList(desqEvent.persoonHr);
        return "PersonReference: $personReference\nVoornamen: $voornamen\nVoorvoegsels: $voorvoegselsOrPlaceholder\nGeslachtsnaam: ${desqEvent.geslachtsnaam}";
      case PersoonIngeschreven:
        final desqEvent = event.eventData as PersoonIngeschreven;
        final personReference = Base62.encodeFromList(desqEvent.persoonPr);
        return "PersonReference: $personReference\nDatum ingeschreven: ${desqEvent.datumIngeschreven.toDateTime()}";
      case BSNToegekend:
        final desqEvent = event.eventData as BSNToegekend;
        final personReference = Base62.encodeFromList(desqEvent.persoonPr);
        return "PersonReference: $personReference\nIngangsdatum BSN: ${desqEvent.ingangsdatumBsn.toDateTime()}";
      case WoonadresNLGeregistreerd:
        final desqEvent = event.eventData as WoonadresNLGeregistreerd;
        final personReference = Base62.encodeFromList(desqEvent.persoonPr);
        return "PersonReference: $personReference\nVerwijzing BAG: ${desqEvent.verwijzingWoonadresBAG}\nGemeente: ${desqEvent.gemeente}\nStartdatum woonadres: ${desqEvent.startdatumWoonadres.toDateTime()}\nAangifte: ${desqEvent.aangifte.name}";
      case NamenGeregistreerd:
        final desqEvent = event.eventData as NamenGeregistreerd;
        final voornamen =
            desqEvent.voornamen.reduce((value, element) => "$value $element");
        final voorvoegselsOrPlaceholder =
            desqEvent.voorvoegselGeslachtsnaam == ""
                ? "<geen>"
                : desqEvent.voorvoegselGeslachtsnaam;
        final personReference = Base62.encodeFromList(desqEvent.persoonPr);
        return "PersonReference: $personReference\nVoornamen: $voornamen\nVoorvoegsels: $voorvoegselsOrPlaceholder\nGeslachtsnaam: ${desqEvent.geslachtsnaam}\nIngangsdatum namen: ${desqEvent.ingangsdatumNamen.toDateTime()}";
      case GeslachtsaanduidingGeregistreerd:
        final desqEvent = event.eventData as GeslachtsaanduidingGeregistreerd;
        final personReference = Base62.encodeFromList(desqEvent.persoonPr);
        return "PersonReference: $personReference\nGeslachtsaanduiding: ${desqEvent.geslachtsaanduiding.name}\nDatum ingeschreven: ${desqEvent.ingangsdatumGeslachtsaanduiding.toDateTime()}";
      default:
        return "Onbekend event";
    }
  }

  IconData _iconDataForEvent(ManagementUIEvent event) {
    switch (event.eventData.runtimeType) {
      case NewPersonEvent:
        return Icons.person;
      case PersonNoteEvent:
        return Icons.note_add;
      case NamenVastgesteld:
        return Icons.face;
      case PersoonIngeschreven:
        return Icons.person_add;
      case BSNToegekend:
        return Icons.fiber_pin_outlined;
      case WoonadresNLGeregistreerd:
        return Icons.home;
      case NamenGeregistreerd:
        return Icons.ballot;
      case GeslachtsaanduidingGeregistreerd:
        return Icons.bug_report;
      default:
        return Icons.help_outline;
    }
  }

  String _zidToVaststeller(String zid) {
    const Map<String, String> zidVaststellers = {
      'bovenhove': 'Bovenhove',
      'groenestede': 'Groenestede',
      'oudescha': 'Oudescha',
      'heidedal': 'Heidedal',
    };
    return zidVaststellers[zid] ?? 'onbekend';
  }

  Color _iconColorForZid(String zid) {
    // We do this here instead of themedata because we use the colors of the event's zid
    const Map<String, Color> iconColors = {
      'bovenhove': Color(0xFFAD1B13),
      'groenestede': Color(0xFF67A030),
      'oudescha': Color(0xFF77491E),
      'heidedal': Color(0xFF84046F),
    };
    return iconColors[zid] ?? const Color(0xFF888888);
  }

  Color _backgroundColorForZid(String zid) {
    // We do this here instead of themedata because we use the colors of the event's zid
    const Map<String, Color> backgroundColors = {
      'bovenhove': Color(0xFFFBE5E4),
      'groenestede': Color(0xFFF1F8E9),
      'oudescha': Color(0xFFE1DAD1),
      'heidedal': Color(0xFFDEC7DA),
    };
    return backgroundColors[zid] ?? const Color(0xFF333333);
  }
}
