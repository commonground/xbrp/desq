// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';
import 'package:management_ui/desq/zone-api/zone/zone.pb.dart';
import 'package:management_ui/widgets/menu.dart';
import 'package:provider/provider.dart';

class ZonesScreen extends StatefulWidget {
  static const routePath = '/zones';

  @override
  _ZonesScreenState createState() => _ZonesScreenState();
}

class _ZonesScreenState extends State<ZonesScreen> {
  Future<List<ZoneStatus>> networkStatus;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    networkStatus = Provider.of<EcosystemProvider>(context).getNetworkStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Zones"),
      ),
      drawer: MenuDrawer(),
      body: FutureBuilder(
        future: networkStatus,
        builder:
            (BuildContext context, AsyncSnapshot<List<ZoneStatus>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error: ${snapshot.error}"));
          }
          if (!snapshot.hasData) {
            return const Center(child: Text("Loading.."));
          }
          return Column(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  'images/landkaart.png',
                  fit: BoxFit.cover,
                ),
              ),
              dividerLine,
              Container(
                color: const Color(0xFFFFFFFF),
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(40),
                child: const Text(
                  "Zones in het netwerk",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              dividerLine,
              Expanded(
                child: Container(
                  color: const Color(0xFFFFFFFF),
                  child: ListView.separated(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      final ZoneStatus zoneStatus = snapshot.data[index];
                      debugPrint(zoneStatus.toDebugString());
                      return Container(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, bottom: 20, top: 20),
                        child: ListTile(
                          leading: Image.asset('images/zone.png'),
                          title: Text(
                            zoneStatus.zid,
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.normal),
                          ),
                          trailing: _buildIconForZoneStatus(zoneStatus.status),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => dividerLine,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  Widget _buildIconForZoneStatus(String status) {
    switch (status) {
      case 'online':
        return Wrap(
          spacing: 5, // space between two icons
          children: const <Widget>[
            greenBox,
            greenBox,
            greenBox,
          ],
        );
      case 'offline':
        return Wrap(
          spacing: 5, // space between two icons
          children: <Widget>[
            redCircle,
          ],
        );
      case 'error':
      default:
        return Wrap(
          spacing: 5, // space between two icons
          children: <Widget>[
            orangeTriangle,
            orangeTriangle,
          ],
        );
    }
  }
}

Divider dividerLine = const Divider(color: Color(0xFFBDBDBD), height: 1);

Container redCircle = Container(
  width: 16,
  height: 16,
  decoration: const BoxDecoration(
    color: Color(0xFFEB5757),
    shape: BoxShape.circle,
  ),
);

SizedBox orangeTriangle = const SizedBox(
  width: 16,
  height: 16,
  child: Icon(
    Icons.warning,
    size: 20,
    color: Color(0xFFF2C94C),
  ),
);

const greenBox = SizedBox(
  width: 16,
  height: 16,
  child: DecoratedBox(
    decoration: BoxDecoration(color: Color(0xFF6FCF97)),
  ),
);
