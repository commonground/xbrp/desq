// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/desq/management-api/management/management.pb.dart';
import 'package:management_ui/providers/personjournal_provider.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:management_ui/util/base62.dart';
import 'package:management_ui/widgets/menu.dart';
import 'package:provider/provider.dart';

class PersonJournalsScreen extends StatefulWidget {
  static const routePath = '/persoonsjournaals';

  @override
  _PersonJournalsScreenState createState() => _PersonJournalsScreenState();
}

class _PersonJournalsScreenState extends State<PersonJournalsScreen> {
  Future<List<Journal>> journalsFuture;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    journalsFuture = Provider.of<PersonJournalProvider>(context).getJournals();
  }

  @override
  Widget build(BuildContext context) {
    final String municipalityName = Provider.of<EnvironmentRepository>(context).getMunicipalityName();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Persoon-journaals"),
      ),
      drawer: MenuDrawer(),
      body: FutureBuilder(
        future: journalsFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Journal>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Error: ${snapshot.error}"));
          }
          if (!snapshot.hasData) {
            return const Center(child: Text("Loading.."));
          }
          final journals = snapshot.data;
          if (journals.isEmpty) {
            return const Center(
                child: Text(
              "Er zijn nog geen persoon-journaals.",
              style: TextStyle(fontSize: 16),
            ));
          }
          return Column(
            children: [
              Container(
                color: const Color(0xFFFFFFFF),
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(40),
                child: Text(
                  "Persoon-journaals $municipalityName",
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              dividerLine,
              Expanded(
                child: Container(
                  color: const Color(0xFFFFFFFF),
                  child: ListView.separated(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      final Journal journal = snapshot.data[index];
                      return Container(
                        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 20),
                        child: ListTile(
                          leading: Icon(
                            Icons.account_circle,
                            color: Theme.of(context).accentColor,
                          ),
                          title: Text(
                            Base62.encodeFromList(journal.personReference),
                            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => dividerLine,
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  Divider dividerLine = const Divider(color: Color(0xFFBDBDBD), height: 1);
}
