// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'main_live_web.dart' as original_main;

void main() {
  original_main.main();
}
