// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/ecosystem/ecosystem.dart';
import 'package:management_ui/ecosystem/zone.dart';
import 'package:management_ui/grpc/grpc_client_web.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';
import 'package:management_ui/providers/ecosystem_provider_live.dart';
import 'package:management_ui/providers/event_provider.dart';
import 'package:management_ui/providers/event_provider_live.dart';
import 'package:management_ui/providers/personjournal_provider.dart';
import 'package:management_ui/providers/personjournal_provider_live.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:provider/provider.dart';
import 'package:management_ui/app.dart';

void main() {
  // default to local bovenhove
  const String defaultEnvironment = 'demo';
  const String defaultMunicipality = 'bovenhove';

  const String envEnvironment =
      String.fromEnvironment('environment', defaultValue: defaultEnvironment);
  const String envMunicipality =
      String.fromEnvironment('municipality', defaultValue: defaultMunicipality);

  // TODO: connection info should probably just be set using an environment variable
  final Zone zone =
      Ecosystem.getNetworkByName(envEnvironment).getZoneByZID(envMunicipality);

  runApp(
    MultiProvider(
      providers: [
        Provider<EnvironmentRepository>(
          create: (_) => EnvironmentRepository(
            municipality: envMunicipality,
          ),
        ),
        Provider<EventProvider>(
          create: (_) => EventProviderLive(
            grpcClient: GrpcWebClient(
              host: zone.host,
              port: zone.port,
              insecure: zone.insecure,
            ),
          ),
        ),
        Provider<PersonJournalProvider>(
          create: (_) => PersonJournalProviderLive(
            grpcClient: GrpcWebClient(
              host: zone.host,
              port: zone.port,
              insecure: zone.insecure,
            ),
          ),
        ),
        Provider<EcosystemProvider>(
          create: (_) => EcosystemProviderLive(
            municipality: envMunicipality,
            grpcClient: GrpcWebClient(
              host: zone.host,
              port: zone.port,
              insecure: zone.insecure,
            ),
          ),
        ),
      ],
      child: const ManagementApp(
        municipality: envMunicipality,
      ),
    ),
  );
}
