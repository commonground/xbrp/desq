// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/screens/events_screen.dart';
import 'package:management_ui/screens/personjournals_screen.dart';
import 'package:management_ui/screens/zones_screen.dart';

class MenuDrawer extends StatefulWidget {
  @override
  _MenuDrawerState createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        appBar: AppBar(centerTitle: true, title: const Text('Menu')),
        body: Column(
          children: <Widget>[
            ListTile(
              title: const Text('Gebeurtenissen'),
              onTap: () {
                Navigator.of(context).pushNamed(EventsScreen.routePath);
              },
            ),
            ListTile(
              title: const Text('Persoon-journaals'),
              onTap: () {
                Navigator.of(context).pushNamed(PersonJournalsScreen.routePath);
              },
            ),
            ListTile(
              title: const Text('Zones'),
              onTap: () {
                Navigator.of(context).pushNamed(ZonesScreen.routePath);
              },
            ),
          ],
        ),
      ),
    );
  }
}
