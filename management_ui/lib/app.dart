// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/screens/personjournals_screen.dart';
import 'package:management_ui/screens/zones_screen.dart';
import 'package:management_ui/theme.dart';
import 'package:management_ui/screens/events_screen.dart';
import 'package:management_ui/screens/login_screen.dart';

class ManagementApp extends StatelessWidget {
  final String municipality;

  const ManagementApp({@required this.municipality}) : assert(municipality != null);

  @override
  Widget build(BuildContext context) {
    final routes = <String, WidgetBuilder>{
      LoginScreen.routePath: (context) => LoginScreen(),
      EventsScreen.routePath: (context) => EventsScreen(),
      PersonJournalsScreen.routePath: (context) => PersonJournalsScreen(),
      ZonesScreen.routePath: (context) => ZonesScreen(),
    };

    final managementTheme = ManagementTheme(municipality);

    return MaterialApp(
      title: 'Management',
      debugShowCheckedModeBanner: false,
      theme: managementTheme.themeData,
      initialRoute: LoginScreen.routePath,
      routes: routes,
    );
  }
}
