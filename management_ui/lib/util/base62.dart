// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'dart:typed_data';
import 'package:base_x/base_x.dart';

// ignore: non_constant_identifier_names
final Base62Codec = BaseXCodec('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

class Base62 {
  static String encodeFromList(List<int> list) {
    return Base62Codec.encode(Uint8List.fromList(list));
  }
}
