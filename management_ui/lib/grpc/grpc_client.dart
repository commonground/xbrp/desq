// ignore: implementation_imports
import 'package:grpc/src/client/channel.dart' as channel;

abstract class GrpcClient {
  channel.ClientChannel get client;
}
