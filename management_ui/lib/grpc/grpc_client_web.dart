import 'package:management_ui/grpc/grpc_client.dart';
import 'package:grpc/grpc_web.dart';
// ignore: implementation_imports
import 'package:grpc/src/client/channel.dart' as channel;

class GrpcWebClient implements GrpcClient {
  channel.ClientChannel _client;

  GrpcWebClient({String host, int port = 443, bool insecure = false}) {
    _client = GrpcWebClientChannel.xhr(Uri(
      scheme: insecure ? "http" : "https",
      host: host,
      port: port,
    ));
  }

  @override
  channel.ClientChannel get client => _client;
}
