import 'package:management_ui/grpc/grpc_client.dart';
import 'package:grpc/grpc.dart';
// ignore: implementation_imports
import 'package:grpc/src/client/channel.dart' as channel;

class GrpcNativeClient implements GrpcClient {
  channel.ClientChannel _client;

  GrpcNativeClient({
    String host,
    int port = 443,
    bool insecure = false,
  }) {
    _client = ClientChannel(
      host,
      port: port,
      options: ChannelOptions(
        credentials: insecure ? const ChannelCredentials.insecure() : const ChannelCredentials.secure(),
        idleTimeout: const Duration(minutes: 1),
      ),
    );
  }

  @override
  channel.ClientChannel get client => _client;
}
