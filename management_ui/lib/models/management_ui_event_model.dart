// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

// import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:management_ui/desq/common/events/events.pb.dart' as desq;
import 'package:management_ui/desq/management-api/management/management.pb.dart';
// import 'package:management_ui/util/base62.dart';

class ManagementUIEvent {
  String zid;
  String kind;
  DateTime date; // TODO types of dates
  Object eventData;

  ManagementUIEvent(
    this.zid,
    this.kind,
    this.date,
    this.eventData,
  );

  // Note: needs to be a static function because constructors cannot be passed as arguments
  // ignore: prefer_constructors_over_static_methods
  static ManagementUIEvent fromAPIEvent(ManagementAPIEvent e) {
    final event = desq.Event.fromBuffer(e.event);

    Object eventData;
    switch (event.kind) {
      case 'NewPersonEvent':
        eventData = desq.NewPersonEvent.fromBuffer(event.data);
        break;
      case 'PersonNoteEvent':
        eventData = desq.PersonNoteEvent.fromBuffer(event.data);
        break;
      case 'NamenVastgesteld':
        eventData = desq.NamenVastgesteld.fromBuffer(event.data);
        break;
      case 'PersoonIngeschreven':
        eventData = desq.PersoonIngeschreven.fromBuffer(event.data);
        break;
      case 'BSNToegekend':
        eventData = desq.BSNToegekend.fromBuffer(event.data);
        break;
      case 'WoonadresNLGeregistreerd':
        eventData = desq.WoonadresNLGeregistreerd.fromBuffer(event.data);
        break;
      case 'NamenGeregistreerd':
        eventData = desq.NamenGeregistreerd.fromBuffer(event.data);
        break;
      case 'GeslachtsaanduidingGeregistreerd':
        eventData =
            desq.GeslachtsaanduidingGeregistreerd.fromBuffer(event.data);
        break;
      default:
        eventData = UnknownEvent();
    }
    final ManagementUIEvent uiEvent = ManagementUIEvent(
      e.zid,
      event.kind,
      event.date.toDateTime(),
      eventData,
    );
    debugPrint(event.kind);
    return uiEvent;
  }
}

class UnknownEvent {} // TODO maybe move to event types?
