// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:flutter/material.dart';

class ManagementTheme {
  ThemeData themeData;

  ManagementTheme(String municipality) {
    Color accentColor;
    Color primaryColor;
    String fontFamily;

    switch (municipality) {
      case 'groenestede':
        accentColor = const Color(0xFF055D5E);
        primaryColor = const Color(0xFF4F7664);
        fontFamily = 'Quattrocento Sans';
        break;
      case 'heidedal':
        accentColor = const Color(0xFF64485C);
        primaryColor = const Color(0xFF530043);
        fontFamily = 'Open Sans';
        break;
      case 'bovenhove':
        accentColor = const Color(0xFFAD1B13);
        primaryColor = const Color(0xFF871610);
        fontFamily = 'Montserrat';
        break;
      case 'oudescha':
        accentColor = const Color(0xFF5C4842);
        primaryColor = const Color(0xFF5C4842);
        fontFamily = 'Libre Baskerville';
        break;
    }

    // MaterialColor residentRed = const MaterialColor(0XFF9F2B33, const {
    //   50: const Color(0XFFF6EBEC),
    //   100: const Color(0XFFE4C5C7),
    //   200: const Color(0XFFD39EA2),
    //   300: const Color(0XFFC1787D),
    //   400: const Color(0XFFB05158),
    //   500: const Color(0XFF9F2B33),
    //   600: const Color(0XFF91282F),
    //   700: const Color(0XFF83242A),
    //   800: const Color(0XFF742026),
    //   900: const Color(0XFF661C21)
    // });

    final TextTheme residentTextTheme = Typography.englishLike2018.merge(
      TextTheme(
        headline1: TextStyle(fontFamily: fontFamily),
        headline2: TextStyle(fontFamily: fontFamily),
        headline3: TextStyle(fontFamily: fontFamily),
        headline4: TextStyle(fontFamily: fontFamily),
        headline5: TextStyle(fontFamily: fontFamily),
        headline6: TextStyle(fontFamily: fontFamily),
        subtitle1: TextStyle(
          // Emphasize
          fontFamily: fontFamily,
          fontWeight: FontWeight.bold,
        ),
        subtitle2: TextStyle(fontFamily: fontFamily),
        bodyText1: TextStyle(fontFamily: fontFamily),
        bodyText2: TextStyle(fontFamily: fontFamily),
        button: TextStyle(
          // Buttons
          fontFamily: fontFamily,
          fontWeight: FontWeight.bold,
        ),
        caption: TextStyle(fontFamily: fontFamily),
        overline: TextStyle(fontFamily: fontFamily),
      ),
    );

    themeData = ThemeData(
      brightness: Brightness.light,
      canvasColor: const Color.fromRGBO(242, 242, 242, 1),
      primaryColor: primaryColor,
      accentColor: accentColor,
      textTheme: Typography.blackMountainView.merge(residentTextTheme),
      primaryTextTheme: Typography.whiteMountainView.merge(residentTextTheme),
      accentTextTheme: Typography.whiteMountainView.merge(residentTextTheme),
    );
  }
}
