///
//  Generated code. Do not modify.
//  source: common/events/events.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Nationaliteitskenmerk extends $pb.ProtobufEnum {
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Nog_niet_bekend = Nationaliteitskenmerk._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Nog_niet_bekend');
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Nederlander = Nationaliteitskenmerk._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Nederlander');
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Behandeld_als_Nederlander = Nationaliteitskenmerk._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Behandeld_als_Nederlander');
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Buitenlandse_Nationaliteit_en = Nationaliteitskenmerk._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Buitenlandse_Nationaliteit_en');
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Vastgesteld_niet_Nederlander = Nationaliteitskenmerk._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Vastgesteld_niet_Nederlander');
  static const Nationaliteitskenmerk NATIONALITEITSKENMERK_Staatloos = Nationaliteitskenmerk._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NATIONALITEITSKENMERK_Staatloos');

  static const $core.List<Nationaliteitskenmerk> values = <Nationaliteitskenmerk> [
    NATIONALITEITSKENMERK_Nog_niet_bekend,
    NATIONALITEITSKENMERK_Nederlander,
    NATIONALITEITSKENMERK_Behandeld_als_Nederlander,
    NATIONALITEITSKENMERK_Buitenlandse_Nationaliteit_en,
    NATIONALITEITSKENMERK_Vastgesteld_niet_Nederlander,
    NATIONALITEITSKENMERK_Staatloos,
  ];

  static final $core.Map<$core.int, Nationaliteitskenmerk> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Nationaliteitskenmerk? valueOf($core.int value) => _byValue[value];

  const Nationaliteitskenmerk._($core.int v, $core.String n) : super(v, n);
}

class Geslachtsaanduiding extends $pb.ProtobufEnum {
  static const Geslachtsaanduiding GESLACHT_ONBEKEND = Geslachtsaanduiding._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GESLACHT_ONBEKEND');
  static const Geslachtsaanduiding GESLACHT_M = Geslachtsaanduiding._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GESLACHT_M');
  static const Geslachtsaanduiding GESLACHT_V = Geslachtsaanduiding._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GESLACHT_V');
  static const Geslachtsaanduiding GESLACHT_X = Geslachtsaanduiding._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GESLACHT_X');

  static const $core.List<Geslachtsaanduiding> values = <Geslachtsaanduiding> [
    GESLACHT_ONBEKEND,
    GESLACHT_M,
    GESLACHT_V,
    GESLACHT_X,
  ];

  static final $core.Map<$core.int, Geslachtsaanduiding> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Geslachtsaanduiding? valueOf($core.int value) => _byValue[value];

  const Geslachtsaanduiding._($core.int v, $core.String n) : super(v, n);
}

class Naamgebruik extends $pb.ProtobufEnum {
  static const Naamgebruik NAAMGEBRUIK_E = Naamgebruik._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NAAMGEBRUIK_E');
  static const Naamgebruik NAAMGEBRUIK_P = Naamgebruik._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NAAMGEBRUIK_P');
  static const Naamgebruik NAAMGEBRUIK_EP = Naamgebruik._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NAAMGEBRUIK_EP');
  static const Naamgebruik NAAMGEBRUIK_PE = Naamgebruik._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NAAMGEBRUIK_PE');

  static const $core.List<Naamgebruik> values = <Naamgebruik> [
    NAAMGEBRUIK_E,
    NAAMGEBRUIK_P,
    NAAMGEBRUIK_EP,
    NAAMGEBRUIK_PE,
  ];

  static final $core.Map<$core.int, Naamgebruik> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Naamgebruik? valueOf($core.int value) => _byValue[value];

  const Naamgebruik._($core.int v, $core.String n) : super(v, n);
}

class Verstrekkingsbeperking extends $pb.ProtobufEnum {
  static const Verstrekkingsbeperking VERSTREKKINGSBEPERKING_Geen_beperking = Verstrekkingsbeperking._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'VERSTREKKINGSBEPERKING_Geen_beperking');
  static const Verstrekkingsbeperking VERSTREKKINGSBEPERKING_Volledige_beperking = Verstrekkingsbeperking._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'VERSTREKKINGSBEPERKING_Volledige_beperking');

  static const $core.List<Verstrekkingsbeperking> values = <Verstrekkingsbeperking> [
    VERSTREKKINGSBEPERKING_Geen_beperking,
    VERSTREKKINGSBEPERKING_Volledige_beperking,
  ];

  static final $core.Map<$core.int, Verstrekkingsbeperking> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Verstrekkingsbeperking? valueOf($core.int value) => _byValue[value];

  const Verstrekkingsbeperking._($core.int v, $core.String n) : super(v, n);
}

class Aangifte extends $pb.ProtobufEnum {
  static const Aangifte AANGIFTE_Ingeschrevene = Aangifte._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Ingeschrevene');
  static const Aangifte AANGIFTE_Ambtshalve = Aangifte._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Ambtshalve');
  static const Aangifte AANGIFTE_Ministerieel_besluit = Aangifte._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Ministerieel_besluit');
  static const Aangifte AANGIFTE_Gezaghouder = Aangifte._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Gezaghouder');
  static const Aangifte AANGIFTE_Hoofd_instelling = Aangifte._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Hoofd_instelling');
  static const Aangifte AANGIFTE_Meerderjarig_inwonend_kind_voor_ouder = Aangifte._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Meerderjarig_inwonend_kind_voor_ouder');
  static const Aangifte AANGIFTE_Meerderjarige_gemachtigde = Aangifte._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Meerderjarige_gemachtigde');
  static const Aangifte AANGIFTE_Inwonende_ouder_voor_meerderjarig_kind = Aangifte._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Inwonende_ouder_voor_meerderjarig_kind');
  static const Aangifte AANGIFTE_Echtgenoot_of_Geregistreerd_partner = Aangifte._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'AANGIFTE_Echtgenoot_of_Geregistreerd_partner');

  static const $core.List<Aangifte> values = <Aangifte> [
    AANGIFTE_Ingeschrevene,
    AANGIFTE_Ambtshalve,
    AANGIFTE_Ministerieel_besluit,
    AANGIFTE_Gezaghouder,
    AANGIFTE_Hoofd_instelling,
    AANGIFTE_Meerderjarig_inwonend_kind_voor_ouder,
    AANGIFTE_Meerderjarige_gemachtigde,
    AANGIFTE_Inwonende_ouder_voor_meerderjarig_kind,
    AANGIFTE_Echtgenoot_of_Geregistreerd_partner,
  ];

  static final $core.Map<$core.int, Aangifte> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Aangifte? valueOf($core.int value) => _byValue[value];

  const Aangifte._($core.int v, $core.String n) : super(v, n);
}

