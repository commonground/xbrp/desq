///
//  Generated code. Do not modify.
//  source: common/events/events.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use nationaliteitskenmerkDescriptor instead')
const Nationaliteitskenmerk$json = const {
  '1': 'Nationaliteitskenmerk',
  '2': const [
    const {'1': 'NATIONALITEITSKENMERK_Nog_niet_bekend', '2': 0},
    const {'1': 'NATIONALITEITSKENMERK_Nederlander', '2': 1},
    const {'1': 'NATIONALITEITSKENMERK_Behandeld_als_Nederlander', '2': 2},
    const {'1': 'NATIONALITEITSKENMERK_Buitenlandse_Nationaliteit_en', '2': 3},
    const {'1': 'NATIONALITEITSKENMERK_Vastgesteld_niet_Nederlander', '2': 4},
    const {'1': 'NATIONALITEITSKENMERK_Staatloos', '2': 5},
  ],
};

/// Descriptor for `Nationaliteitskenmerk`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List nationaliteitskenmerkDescriptor = $convert.base64Decode('ChVOYXRpb25hbGl0ZWl0c2tlbm1lcmsSKQolTkFUSU9OQUxJVEVJVFNLRU5NRVJLX05vZ19uaWV0X2Jla2VuZBAAEiUKIU5BVElPTkFMSVRFSVRTS0VOTUVSS19OZWRlcmxhbmRlchABEjMKL05BVElPTkFMSVRFSVRTS0VOTUVSS19CZWhhbmRlbGRfYWxzX05lZGVybGFuZGVyEAISNwozTkFUSU9OQUxJVEVJVFNLRU5NRVJLX0J1aXRlbmxhbmRzZV9OYXRpb25hbGl0ZWl0X2VuEAMSNgoyTkFUSU9OQUxJVEVJVFNLRU5NRVJLX1Zhc3RnZXN0ZWxkX25pZXRfTmVkZXJsYW5kZXIQBBIjCh9OQVRJT05BTElURUlUU0tFTk1FUktfU3RhYXRsb29zEAU=');
@$core.Deprecated('Use geslachtsaanduidingDescriptor instead')
const Geslachtsaanduiding$json = const {
  '1': 'Geslachtsaanduiding',
  '2': const [
    const {'1': 'GESLACHT_ONBEKEND', '2': 0},
    const {'1': 'GESLACHT_M', '2': 1},
    const {'1': 'GESLACHT_V', '2': 2},
    const {'1': 'GESLACHT_X', '2': 3},
  ],
};

/// Descriptor for `Geslachtsaanduiding`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List geslachtsaanduidingDescriptor = $convert.base64Decode('ChNHZXNsYWNodHNhYW5kdWlkaW5nEhUKEUdFU0xBQ0hUX09OQkVLRU5EEAASDgoKR0VTTEFDSFRfTRABEg4KCkdFU0xBQ0hUX1YQAhIOCgpHRVNMQUNIVF9YEAM=');
@$core.Deprecated('Use naamgebruikDescriptor instead')
const Naamgebruik$json = const {
  '1': 'Naamgebruik',
  '2': const [
    const {'1': 'NAAMGEBRUIK_E', '2': 0},
    const {'1': 'NAAMGEBRUIK_P', '2': 1},
    const {'1': 'NAAMGEBRUIK_EP', '2': 2},
    const {'1': 'NAAMGEBRUIK_PE', '2': 3},
  ],
};

/// Descriptor for `Naamgebruik`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List naamgebruikDescriptor = $convert.base64Decode('CgtOYWFtZ2VicnVpaxIRCg1OQUFNR0VCUlVJS19FEAASEQoNTkFBTUdFQlJVSUtfUBABEhIKDk5BQU1HRUJSVUlLX0VQEAISEgoOTkFBTUdFQlJVSUtfUEUQAw==');
@$core.Deprecated('Use verstrekkingsbeperkingDescriptor instead')
const Verstrekkingsbeperking$json = const {
  '1': 'Verstrekkingsbeperking',
  '2': const [
    const {'1': 'VERSTREKKINGSBEPERKING_Geen_beperking', '2': 0},
    const {'1': 'VERSTREKKINGSBEPERKING_Volledige_beperking', '2': 1},
  ],
};

/// Descriptor for `Verstrekkingsbeperking`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List verstrekkingsbeperkingDescriptor = $convert.base64Decode('ChZWZXJzdHJla2tpbmdzYmVwZXJraW5nEikKJVZFUlNUUkVLS0lOR1NCRVBFUktJTkdfR2Vlbl9iZXBlcmtpbmcQABIuCipWRVJTVFJFS0tJTkdTQkVQRVJLSU5HX1ZvbGxlZGlnZV9iZXBlcmtpbmcQAQ==');
@$core.Deprecated('Use aangifteDescriptor instead')
const Aangifte$json = const {
  '1': 'Aangifte',
  '2': const [
    const {'1': 'AANGIFTE_Ingeschrevene', '2': 0},
    const {'1': 'AANGIFTE_Ambtshalve', '2': 1},
    const {'1': 'AANGIFTE_Ministerieel_besluit', '2': 2},
    const {'1': 'AANGIFTE_Gezaghouder', '2': 3},
    const {'1': 'AANGIFTE_Hoofd_instelling', '2': 4},
    const {'1': 'AANGIFTE_Meerderjarig_inwonend_kind_voor_ouder', '2': 5},
    const {'1': 'AANGIFTE_Meerderjarige_gemachtigde', '2': 6},
    const {'1': 'AANGIFTE_Inwonende_ouder_voor_meerderjarig_kind', '2': 7},
    const {'1': 'AANGIFTE_Echtgenoot_of_Geregistreerd_partner', '2': 8},
  ],
};

/// Descriptor for `Aangifte`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List aangifteDescriptor = $convert.base64Decode('CghBYW5naWZ0ZRIaChZBQU5HSUZURV9Jbmdlc2NocmV2ZW5lEAASFwoTQUFOR0lGVEVfQW1idHNoYWx2ZRABEiEKHUFBTkdJRlRFX01pbmlzdGVyaWVlbF9iZXNsdWl0EAISGAoUQUFOR0lGVEVfR2V6YWdob3VkZXIQAxIdChlBQU5HSUZURV9Ib29mZF9pbnN0ZWxsaW5nEAQSMgouQUFOR0lGVEVfTWVlcmRlcmphcmlnX2lud29uZW5kX2tpbmRfdm9vcl9vdWRlchAFEiYKIkFBTkdJRlRFX01lZXJkZXJqYXJpZ2VfZ2VtYWNodGlnZGUQBhIzCi9BQU5HSUZURV9JbndvbmVuZGVfb3VkZXJfdm9vcl9tZWVyZGVyamFyaWdfa2luZBAHEjAKLEFBTkdJRlRFX0VjaHRnZW5vb3Rfb2ZfR2VyZWdpc3RyZWVyZF9wYXJ0bmVyEAg=');
@$core.Deprecated('Use eventDescriptor instead')
const Event$json = const {
  '1': 'Event',
  '2': const [
    const {'1': 'reference', '3': 1, '4': 1, '5': 12, '10': 'reference'},
    const {'1': 'kind', '3': 2, '4': 1, '5': 9, '10': 'kind'},
    const {'1': 'data', '3': 3, '4': 1, '5': 12, '10': 'data'},
    const {'1': 'date', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'date'},
  ],
};

/// Descriptor for `Event`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventDescriptor = $convert.base64Decode('CgVFdmVudBIcCglyZWZlcmVuY2UYASABKAxSCXJlZmVyZW5jZRISCgRraW5kGAIgASgJUgRraW5kEhIKBGRhdGEYAyABKAxSBGRhdGESLgoEZGF0ZRgEIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSBGRhdGU=');
@$core.Deprecated('Use newPersonEventDescriptor instead')
const NewPersonEvent$json = const {
  '1': 'NewPersonEvent',
  '2': const [
    const {'1': 'person_reference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
  ],
};

/// Descriptor for `NewPersonEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newPersonEventDescriptor = $convert.base64Decode('Cg5OZXdQZXJzb25FdmVudBIpChBwZXJzb25fcmVmZXJlbmNlGAEgASgMUg9wZXJzb25SZWZlcmVuY2U=');
@$core.Deprecated('Use personNoteEventDescriptor instead')
const PersonNoteEvent$json = const {
  '1': 'PersonNoteEvent',
  '2': const [
    const {'1': 'person_reference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
    const {'1': 'note', '3': 2, '4': 1, '5': 9, '10': 'note'},
  ],
};

/// Descriptor for `PersonNoteEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List personNoteEventDescriptor = $convert.base64Decode('Cg9QZXJzb25Ob3RlRXZlbnQSKQoQcGVyc29uX3JlZmVyZW5jZRgBIAEoDFIPcGVyc29uUmVmZXJlbmNlEhIKBG5vdGUYAiABKAlSBG5vdGU=');
@$core.Deprecated('Use geboorteVastgesteldEventDescriptor instead')
const GeboorteVastgesteldEvent$json = const {
  '1': 'GeboorteVastgesteldEvent',
  '2': const [
    const {'1': 'kind_pr', '3': 1, '4': 1, '5': 12, '10': 'kindPr'},
    const {'1': 'geboorte_datum', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'geboorteDatum'},
    const {'1': 'geboorte_plaats', '3': 3, '4': 1, '5': 9, '10': 'geboortePlaats'},
    const {'1': 'geboorte_gemeente', '3': 4, '4': 1, '5': 9, '10': 'geboorteGemeente'},
    const {'1': 'geboorte_land', '3': 5, '4': 1, '5': 9, '10': 'geboorteLand'},
  ],
};

/// Descriptor for `GeboorteVastgesteldEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geboorteVastgesteldEventDescriptor = $convert.base64Decode('ChhHZWJvb3J0ZVZhc3RnZXN0ZWxkRXZlbnQSFwoHa2luZF9wchgBIAEoDFIGa2luZFByEkEKDmdlYm9vcnRlX2RhdHVtGAIgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFINZ2Vib29ydGVEYXR1bRInCg9nZWJvb3J0ZV9wbGFhdHMYAyABKAlSDmdlYm9vcnRlUGxhYXRzEisKEWdlYm9vcnRlX2dlbWVlbnRlGAQgASgJUhBnZWJvb3J0ZUdlbWVlbnRlEiMKDWdlYm9vcnRlX2xhbmQYBSABKAlSDGdlYm9vcnRlTGFuZA==');
@$core.Deprecated('Use namenVastgesteldDescriptor instead')
const NamenVastgesteld$json = const {
  '1': 'NamenVastgesteld',
  '2': const [
    const {'1': 'persoon_hr', '3': 1, '4': 1, '5': 12, '10': 'persoonHr'},
    const {'1': 'voornamen', '3': 2, '4': 3, '5': 9, '10': 'voornamen'},
    const {'1': 'voorvoegsel_geslachtsnaam', '3': 3, '4': 1, '5': 9, '10': 'voorvoegselGeslachtsnaam'},
    const {'1': 'geslachtsnaam', '3': 4, '4': 1, '5': 9, '10': 'geslachtsnaam'},
    const {'1': 'ingangsdatum_namen', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumNamen'},
  ],
};

/// Descriptor for `NamenVastgesteld`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List namenVastgesteldDescriptor = $convert.base64Decode('ChBOYW1lblZhc3RnZXN0ZWxkEh0KCnBlcnNvb25faHIYASABKAxSCXBlcnNvb25IchIcCgl2b29ybmFtZW4YAiADKAlSCXZvb3JuYW1lbhI7Chl2b29ydm9lZ3NlbF9nZXNsYWNodHNuYWFtGAMgASgJUhh2b29ydm9lZ3NlbEdlc2xhY2h0c25hYW0SJAoNZ2VzbGFjaHRzbmFhbRgEIAEoCVINZ2VzbGFjaHRzbmFhbRJJChJpbmdhbmdzZGF0dW1fbmFtZW4YBSABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhFpbmdhbmdzZGF0dW1OYW1lbg==');
@$core.Deprecated('Use persoonIngeschrevenDescriptor instead')
const PersoonIngeschreven$json = const {
  '1': 'PersoonIngeschreven',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'datum_ingeschreven', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumIngeschreven'},
  ],
};

/// Descriptor for `PersoonIngeschreven`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List persoonIngeschrevenDescriptor = $convert.base64Decode('ChNQZXJzb29uSW5nZXNjaHJldmVuEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJJChJkYXR1bV9pbmdlc2NocmV2ZW4YAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhFkYXR1bUluZ2VzY2hyZXZlbg==');
@$core.Deprecated('Use bSNToegekendDescriptor instead')
const BSNToegekend$json = const {
  '1': 'BSNToegekend',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'BSN', '3': 2, '4': 1, '5': 9, '10': 'BSN'},
    const {'1': 'ingangsdatum_bsn', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumBsn'},
  ],
};

/// Descriptor for `BSNToegekend`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bSNToegekendDescriptor = $convert.base64Decode('CgxCU05Ub2VnZWtlbmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEhAKA0JTThgCIAEoCVIDQlNOEkUKEGluZ2FuZ3NkYXR1bV9ic24YAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUg9pbmdhbmdzZGF0dW1Cc24=');
@$core.Deprecated('Use immigratieGeregistreerdDescriptor instead')
const ImmigratieGeregistreerd$json = const {
  '1': 'ImmigratieGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'land_vanwaar_gekomen', '3': 2, '4': 1, '5': 9, '10': 'landVanwaarGekomen'},
    const {'1': 'datum_geimmigreerd', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumGeimmigreerd'},
    const {'1': 'aangifte', '3': 4, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `ImmigratieGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List immigratieGeregistreerdDescriptor = $convert.base64Decode('ChdJbW1pZ3JhdGllR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISMAoUbGFuZF92YW53YWFyX2dla29tZW4YAiABKAlSEmxhbmRWYW53YWFyR2Vrb21lbhJJChJkYXR1bV9nZWltbWlncmVlcmQYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhFkYXR1bUdlaW1taWdyZWVyZBIsCghhYW5naWZ0ZRgEIAEoDjIQLmV2ZW50cy5BYW5naWZ0ZVIIYWFuZ2lmdGU=');
@$core.Deprecated('Use woonadresNLGeregistreerdDescriptor instead')
const WoonadresNLGeregistreerd$json = const {
  '1': 'WoonadresNLGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verwijzing_woonadres_BAG', '3': 2, '4': 1, '5': 9, '10': 'verwijzingWoonadresBAG'},
    const {'1': 'gemeente', '3': 3, '4': 1, '5': 9, '10': 'gemeente'},
    const {'1': 'startdatum_woonadres', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumWoonadres'},
    const {'1': 'aangifte', '3': 5, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `WoonadresNLGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List woonadresNLGeregistreerdDescriptor = $convert.base64Decode('ChhXb29uYWRyZXNOTEdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjgKGHZlcndpanppbmdfd29vbmFkcmVzX0JBRxgCIAEoCVIWdmVyd2lqemluZ1dvb25hZHJlc0JBRxIaCghnZW1lZW50ZRgDIAEoCVIIZ2VtZWVudGUSTQoUc3RhcnRkYXR1bV93b29uYWRyZXMYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhNzdGFydGRhdHVtV29vbmFkcmVzEiwKCGFhbmdpZnRlGAUgASgOMhAuZXZlbnRzLkFhbmdpZnRlUghhYW5naWZ0ZQ==');
@$core.Deprecated('Use briefadresToegekendEventDescriptor instead')
const BriefadresToegekendEvent$json = const {
  '1': 'BriefadresToegekendEvent',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verwijzing_briefadres_BAG', '3': 2, '4': 1, '5': 9, '10': 'verwijzingBriefadresBAG'},
    const {'1': 'gemeente', '3': 3, '4': 1, '5': 9, '10': 'gemeente'},
    const {'1': 'startdatum_briefadres', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumBriefadres'},
    const {'1': 'aangifte', '3': 5, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `BriefadresToegekendEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List briefadresToegekendEventDescriptor = $convert.base64Decode('ChhCcmllZmFkcmVzVG9lZ2VrZW5kRXZlbnQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjoKGXZlcndpanppbmdfYnJpZWZhZHJlc19CQUcYAiABKAlSF3ZlcndpanppbmdCcmllZmFkcmVzQkFHEhoKCGdlbWVlbnRlGAMgASgJUghnZW1lZW50ZRJPChVzdGFydGRhdHVtX2JyaWVmYWRyZXMYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhRzdGFydGRhdHVtQnJpZWZhZHJlcxIsCghhYW5naWZ0ZRgFIAEoDjIQLmV2ZW50cy5BYW5naWZ0ZVIIYWFuZ2lmdGU=');
@$core.Deprecated('Use verhuizingInNLGeregistreerdDescriptor instead')
const VerhuizingInNLGeregistreerd$json = const {
  '1': 'VerhuizingInNLGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verwijzing_woonadres_BAG', '3': 2, '4': 1, '5': 9, '10': 'verwijzingWoonadresBAG'},
    const {'1': 'gemeente', '3': 3, '4': 1, '5': 9, '10': 'gemeente'},
    const {'1': 'datum_verhuisd', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVerhuisd'},
    const {'1': 'aangifte', '3': 5, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `VerhuizingInNLGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verhuizingInNLGeregistreerdDescriptor = $convert.base64Decode('ChtWZXJodWl6aW5nSW5OTEdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjgKGHZlcndpanppbmdfd29vbmFkcmVzX0JBRxgCIAEoCVIWdmVyd2lqemluZ1dvb25hZHJlc0JBRxIaCghnZW1lZW50ZRgDIAEoCVIIZ2VtZWVudGUSQQoOZGF0dW1fdmVyaHVpc2QYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUg1kYXR1bVZlcmh1aXNkEiwKCGFhbmdpZnRlGAUgASgOMhAuZXZlbnRzLkFhbmdpZnRlUghhYW5naWZ0ZQ==');
@$core.Deprecated('Use tijdelijkAdresNLGeregistreerdDescriptor instead')
const TijdelijkAdresNLGeregistreerd$json = const {
  '1': 'TijdelijkAdresNLGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verwijzing_woonadres_BAG', '3': 2, '4': 1, '5': 9, '10': 'verwijzingWoonadresBAG'},
    const {'1': 'gemeente', '3': 3, '4': 1, '5': 9, '10': 'gemeente'},
    const {'1': 'startdatum_tijdelijk_adres', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumTijdelijkAdres'},
  ],
};

/// Descriptor for `TijdelijkAdresNLGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tijdelijkAdresNLGeregistreerdDescriptor = $convert.base64Decode('Ch1UaWpkZWxpamtBZHJlc05MR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISOAoYdmVyd2lqemluZ193b29uYWRyZXNfQkFHGAIgASgJUhZ2ZXJ3aWp6aW5nV29vbmFkcmVzQkFHEhoKCGdlbWVlbnRlGAMgASgJUghnZW1lZW50ZRJYChpzdGFydGRhdHVtX3RpamRlbGlqa19hZHJlcxgEIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSGHN0YXJ0ZGF0dW1UaWpkZWxpamtBZHJlcw==');
@$core.Deprecated('Use tijdelijkAdresNLGewijzigdDescriptor instead')
const TijdelijkAdresNLGewijzigd$json = const {
  '1': 'TijdelijkAdresNLGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verwijzing_woonadres_BAG', '3': 2, '4': 1, '5': 9, '10': 'verwijzingWoonadresBAG'},
    const {'1': 'gemeente', '3': 3, '4': 1, '5': 9, '10': 'gemeente'},
    const {'1': 'datum_tijdelijk_adres_gewijzigd', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumTijdelijkAdresGewijzigd'},
  ],
};

/// Descriptor for `TijdelijkAdresNLGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tijdelijkAdresNLGewijzigdDescriptor = $convert.base64Decode('ChlUaWpkZWxpamtBZHJlc05MR2V3aWp6aWdkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchI4Chh2ZXJ3aWp6aW5nX3dvb25hZHJlc19CQUcYAiABKAlSFnZlcndpanppbmdXb29uYWRyZXNCQUcSGgoIZ2VtZWVudGUYAyABKAlSCGdlbWVlbnRlEmEKH2RhdHVtX3RpamRlbGlqa19hZHJlc19nZXdpanppZ2QYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhxkYXR1bVRpamRlbGlqa0FkcmVzR2V3aWp6aWdk');
@$core.Deprecated('Use tijdelijkAdresNLBeeindigdDescriptor instead')
const TijdelijkAdresNLBeeindigd$json = const {
  '1': 'TijdelijkAdresNLBeeindigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'einddatum_tijdelijk_adres', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'einddatumTijdelijkAdres'},
  ],
};

/// Descriptor for `TijdelijkAdresNLBeeindigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tijdelijkAdresNLBeeindigdDescriptor = $convert.base64Decode('ChlUaWpkZWxpamtBZHJlc05MQmVlaW5kaWdkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJWChllaW5kZGF0dW1fdGlqZGVsaWprX2FkcmVzGAIgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIXZWluZGRhdHVtVGlqZGVsaWprQWRyZXM=');
@$core.Deprecated('Use emigratieGeregistreerdDescriptor instead')
const EmigratieGeregistreerd$json = const {
  '1': 'EmigratieGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'land_van_emigratie', '3': 2, '4': 1, '5': 9, '10': 'landVanEmigratie'},
    const {'1': 'datum_geemigreerd', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumGeemigreerd'},
    const {'1': 'aangifte', '3': 4, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `EmigratieGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emigratieGeregistreerdDescriptor = $convert.base64Decode('ChZFbWlncmF0aWVHZXJlZ2lzdHJlZXJkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchIsChJsYW5kX3Zhbl9lbWlncmF0aWUYAiABKAlSEGxhbmRWYW5FbWlncmF0aWUSRwoRZGF0dW1fZ2VlbWlncmVlcmQYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhBkYXR1bUdlZW1pZ3JlZXJkEiwKCGFhbmdpZnRlGAQgASgOMhAuZXZlbnRzLkFhbmdpZnRlUghhYW5naWZ0ZQ==');
@$core.Deprecated('Use buitenlandsWoonadresGeregistreerdDescriptor instead')
const BuitenlandsWoonadresGeregistreerd$json = const {
  '1': 'BuitenlandsWoonadresGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'buitenlands_woonadres', '3': 2, '4': 1, '5': 9, '10': 'buitenlandsWoonadres'},
    const {'1': 'startdatum_buitenlands_woonadres', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumBuitenlandsWoonadres'},
    const {'1': 'aangifte', '3': 4, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `BuitenlandsWoonadresGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buitenlandsWoonadresGeregistreerdDescriptor = $convert.base64Decode('CiFCdWl0ZW5sYW5kc1dvb25hZHJlc0dlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjMKFWJ1aXRlbmxhbmRzX3dvb25hZHJlcxgCIAEoCVIUYnVpdGVubGFuZHNXb29uYWRyZXMSZAogc3RhcnRkYXR1bV9idWl0ZW5sYW5kc193b29uYWRyZXMYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUh5zdGFydGRhdHVtQnVpdGVubGFuZHNXb29uYWRyZXMSLAoIYWFuZ2lmdGUYBCABKA4yEC5ldmVudHMuQWFuZ2lmdGVSCGFhbmdpZnRl');
@$core.Deprecated('Use buitenlandsWoonadresGewijzigdDescriptor instead')
const BuitenlandsWoonadresGewijzigd$json = const {
  '1': 'BuitenlandsWoonadresGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'buitenlands_woonadres', '3': 2, '4': 1, '5': 9, '10': 'buitenlandsWoonadres'},
    const {'1': 'datum_buitenlands_woonadres_gewijzigd', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumBuitenlandsWoonadresGewijzigd'},
    const {'1': 'aangifte', '3': 4, '4': 1, '5': 14, '6': '.events.Aangifte', '10': 'aangifte'},
  ],
};

/// Descriptor for `BuitenlandsWoonadresGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List buitenlandsWoonadresGewijzigdDescriptor = $convert.base64Decode('Ch1CdWl0ZW5sYW5kc1dvb25hZHJlc0dld2lqemlnZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISMwoVYnVpdGVubGFuZHNfd29vbmFkcmVzGAIgASgJUhRidWl0ZW5sYW5kc1dvb25hZHJlcxJtCiVkYXR1bV9idWl0ZW5sYW5kc193b29uYWRyZXNfZ2V3aWp6aWdkGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIiZGF0dW1CdWl0ZW5sYW5kc1dvb25hZHJlc0dld2lqemlnZBIsCghhYW5naWZ0ZRgEIAEoDjIQLmV2ZW50cy5BYW5naWZ0ZVIIYWFuZ2lmdGU=');
@$core.Deprecated('Use onderzoekWoonadresGestartDescriptor instead')
const OnderzoekWoonadresGestart$json = const {
  '1': 'OnderzoekWoonadresGestart',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'startdatum_onderzoek', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumOnderzoek'},
  ],
};

/// Descriptor for `OnderzoekWoonadresGestart`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onderzoekWoonadresGestartDescriptor = $convert.base64Decode('ChlPbmRlcnpvZWtXb29uYWRyZXNHZXN0YXJ0Eh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJNChRzdGFydGRhdHVtX29uZGVyem9laxgCIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSE3N0YXJ0ZGF0dW1PbmRlcnpvZWs=');
@$core.Deprecated('Use onderzoekWoonadresBeeindigdDescriptor instead')
const OnderzoekWoonadresBeeindigd$json = const {
  '1': 'OnderzoekWoonadresBeeindigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'einddatum_onderzoek', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'einddatumOnderzoek'},
  ],
};

/// Descriptor for `OnderzoekWoonadresBeeindigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List onderzoekWoonadresBeeindigdDescriptor = $convert.base64Decode('ChtPbmRlcnpvZWtXb29uYWRyZXNCZWVpbmRpZ2QSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEksKE2VpbmRkYXR1bV9vbmRlcnpvZWsYAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhJlaW5kZGF0dW1PbmRlcnpvZWs=');
@$core.Deprecated('Use vertrekOnbekendWaarheenGeregsitreerdDescriptor instead')
const VertrekOnbekendWaarheenGeregsitreerd$json = const {
  '1': 'VertrekOnbekendWaarheenGeregsitreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'datum_vertrek_onbekend_waarheen', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVertrekOnbekendWaarheen'},
  ],
};

/// Descriptor for `VertrekOnbekendWaarheenGeregsitreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List vertrekOnbekendWaarheenGeregsitreerdDescriptor = $convert.base64Decode('CiRWZXJ0cmVrT25iZWtlbmRXYWFyaGVlbkdlcmVnc2l0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEmEKH2RhdHVtX3ZlcnRyZWtfb25iZWtlbmRfd2FhcmhlZW4YAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhxkYXR1bVZlcnRyZWtPbmJla2VuZFdhYXJoZWVu');
@$core.Deprecated('Use nationaliteitsgegevensGeregistreerdDescriptor instead')
const NationaliteitsgegevensGeregistreerd$json = const {
  '1': 'NationaliteitsgegevensGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'nationaliteitskenmerk', '3': 2, '4': 1, '5': 14, '6': '.events.Nationaliteitskenmerk', '10': 'nationaliteitskenmerk'},
    const {'1': 'buitenlandse_nationaliteit', '3': 3, '4': 3, '5': 9, '10': 'buitenlandseNationaliteit'},
    const {'1': 'EU_persoonsnummer', '3': 4, '4': 1, '5': 9, '10': 'EUPersoonsnummer'},
    const {'1': 'ingangsdatum_nationaliteitsgegevens', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumNationaliteitsgegevens'},
    const {'1': 'reden_opnemen_beeindigen_nationaliteitsgegevens', '3': 6, '4': 1, '5': 9, '10': 'redenOpnemenBeeindigenNationaliteitsgegevens'},
  ],
};

/// Descriptor for `NationaliteitsgegevensGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nationaliteitsgegevensGeregistreerdDescriptor = $convert.base64Decode('CiNOYXRpb25hbGl0ZWl0c2dlZ2V2ZW5zR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISUwoVbmF0aW9uYWxpdGVpdHNrZW5tZXJrGAIgASgOMh0uZXZlbnRzLk5hdGlvbmFsaXRlaXRza2VubWVya1IVbmF0aW9uYWxpdGVpdHNrZW5tZXJrEj0KGmJ1aXRlbmxhbmRzZV9uYXRpb25hbGl0ZWl0GAMgAygJUhlidWl0ZW5sYW5kc2VOYXRpb25hbGl0ZWl0EisKEUVVX3BlcnNvb25zbnVtbWVyGAQgASgJUhBFVVBlcnNvb25zbnVtbWVyEmsKI2luZ2FuZ3NkYXR1bV9uYXRpb25hbGl0ZWl0c2dlZ2V2ZW5zGAUgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIiaW5nYW5nc2RhdHVtTmF0aW9uYWxpdGVpdHNnZWdldmVucxJlCi9yZWRlbl9vcG5lbWVuX2JlZWluZGlnZW5fbmF0aW9uYWxpdGVpdHNnZWdldmVucxgGIAEoCVIscmVkZW5PcG5lbWVuQmVlaW5kaWdlbk5hdGlvbmFsaXRlaXRzZ2VnZXZlbnM=');
@$core.Deprecated('Use nationaliteitsgegevensGewijzigdDescriptor instead')
const NationaliteitsgegevensGewijzigd$json = const {
  '1': 'NationaliteitsgegevensGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'nationaliteitskenmerk', '3': 2, '4': 1, '5': 14, '6': '.events.Nationaliteitskenmerk', '10': 'nationaliteitskenmerk'},
    const {'1': 'buitenlandse_nationaliteit', '3': 3, '4': 3, '5': 9, '10': 'buitenlandseNationaliteit'},
    const {'1': 'EU_persoonsnummer', '3': 4, '4': 1, '5': 9, '10': 'EUPersoonsnummer'},
    const {'1': 'datum_nationaliteitsgegevens_gewijzigd', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumNationaliteitsgegevensGewijzigd'},
    const {'1': 'reden_opnemen_beeindigen_nationaliteitsgegevens', '3': 6, '4': 1, '5': 9, '10': 'redenOpnemenBeeindigenNationaliteitsgegevens'},
  ],
};

/// Descriptor for `NationaliteitsgegevensGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nationaliteitsgegevensGewijzigdDescriptor = $convert.base64Decode('Ch9OYXRpb25hbGl0ZWl0c2dlZ2V2ZW5zR2V3aWp6aWdkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJTChVuYXRpb25hbGl0ZWl0c2tlbm1lcmsYAiABKA4yHS5ldmVudHMuTmF0aW9uYWxpdGVpdHNrZW5tZXJrUhVuYXRpb25hbGl0ZWl0c2tlbm1lcmsSPQoaYnVpdGVubGFuZHNlX25hdGlvbmFsaXRlaXQYAyADKAlSGWJ1aXRlbmxhbmRzZU5hdGlvbmFsaXRlaXQSKwoRRVVfcGVyc29vbnNudW1tZXIYBCABKAlSEEVVUGVyc29vbnNudW1tZXIScAomZGF0dW1fbmF0aW9uYWxpdGVpdHNnZWdldmVuc19nZXdpanppZ2QYBSABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUiRkYXR1bU5hdGlvbmFsaXRlaXRzZ2VnZXZlbnNHZXdpanppZ2QSZQovcmVkZW5fb3BuZW1lbl9iZWVpbmRpZ2VuX25hdGlvbmFsaXRlaXRzZ2VnZXZlbnMYBiABKAlSLHJlZGVuT3BuZW1lbkJlZWluZGlnZW5OYXRpb25hbGl0ZWl0c2dlZ2V2ZW5z');
@$core.Deprecated('Use verblijfstitelGeregistreerdDescriptor instead')
const VerblijfstitelGeregistreerd$json = const {
  '1': 'VerblijfstitelGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'toegekende_verblijfstitel', '3': 2, '4': 1, '5': 9, '10': 'toegekendeVerblijfstitel'},
    const {'1': 'ingangsdatum_verblijfstitel', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumVerblijfstitel'},
    const {'1': 'eindddatum_verblijfstitel', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'eindddatumVerblijfstitel'},
  ],
};

/// Descriptor for `VerblijfstitelGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verblijfstitelGeregistreerdDescriptor = $convert.base64Decode('ChtWZXJibGlqZnN0aXRlbEdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjsKGXRvZWdla2VuZGVfdmVyYmxpamZzdGl0ZWwYAiABKAlSGHRvZWdla2VuZGVWZXJibGlqZnN0aXRlbBJbChtpbmdhbmdzZGF0dW1fdmVyYmxpamZzdGl0ZWwYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhppbmdhbmdzZGF0dW1WZXJibGlqZnN0aXRlbBJXChllaW5kZGRhdHVtX3ZlcmJsaWpmc3RpdGVsGAQgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIYZWluZGRkYXR1bVZlcmJsaWpmc3RpdGVs');
@$core.Deprecated('Use verblijfstitelGewijzigdDescriptor instead')
const VerblijfstitelGewijzigd$json = const {
  '1': 'VerblijfstitelGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'toegekende_verblijfstitel', '3': 2, '4': 1, '5': 9, '10': 'toegekendeVerblijfstitel'},
    const {'1': 'ingangsdatum_verblijfstitel', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumVerblijfstitel'},
    const {'1': 'datum_verblijfstitel_gewijzigd', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVerblijfstitelGewijzigd'},
  ],
};

/// Descriptor for `VerblijfstitelGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verblijfstitelGewijzigdDescriptor = $convert.base64Decode('ChdWZXJibGlqZnN0aXRlbEdld2lqemlnZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISOwoZdG9lZ2VrZW5kZV92ZXJibGlqZnN0aXRlbBgCIAEoCVIYdG9lZ2VrZW5kZVZlcmJsaWpmc3RpdGVsElsKG2luZ2FuZ3NkYXR1bV92ZXJibGlqZnN0aXRlbBgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSGmluZ2FuZ3NkYXR1bVZlcmJsaWpmc3RpdGVsEmAKHmRhdHVtX3ZlcmJsaWpmc3RpdGVsX2dld2lqemlnZBgEIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSHGRhdHVtVmVyYmxpamZzdGl0ZWxHZXdpanppZ2Q=');
@$core.Deprecated('Use bijzonderVerblijfsrechtGeregistreerdDescriptor instead')
const BijzonderVerblijfsrechtGeregistreerd$json = const {
  '1': 'BijzonderVerblijfsrechtGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'ingangsdatum_bijzonder_verblijfsrecht', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumBijzonderVerblijfsrecht'},
    const {'1': 'eindddatum_bijzonder_verblijfsrecht', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'eindddatumBijzonderVerblijfsrecht'},
  ],
};

/// Descriptor for `BijzonderVerblijfsrechtGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bijzonderVerblijfsrechtGeregistreerdDescriptor = $convert.base64Decode('CiRCaWp6b25kZXJWZXJibGlqZnNyZWNodEdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEm4KJWluZ2FuZ3NkYXR1bV9iaWp6b25kZXJfdmVyYmxpamZzcmVjaHQYAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUiNpbmdhbmdzZGF0dW1CaWp6b25kZXJWZXJibGlqZnNyZWNodBJqCiNlaW5kZGRhdHVtX2JpanpvbmRlcl92ZXJibGlqZnNyZWNodBgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSIWVpbmRkZGF0dW1CaWp6b25kZXJWZXJibGlqZnNyZWNodA==');
@$core.Deprecated('Use bijzonderVerblijfsrechtBeeindigdDescriptor instead')
const BijzonderVerblijfsrechtBeeindigd$json = const {
  '1': 'BijzonderVerblijfsrechtBeeindigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'eindddatum_bijzonder_verblijfsrecht', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'eindddatumBijzonderVerblijfsrecht'},
  ],
};

/// Descriptor for `BijzonderVerblijfsrechtBeeindigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bijzonderVerblijfsrechtBeeindigdDescriptor = $convert.base64Decode('CiBCaWp6b25kZXJWZXJibGlqZnNyZWNodEJlZWluZGlnZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISagojZWluZGRkYXR1bV9iaWp6b25kZXJfdmVyYmxpamZzcmVjaHQYAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUiFlaW5kZGRhdHVtQmlqem9uZGVyVmVyYmxpamZzcmVjaHQ=');
@$core.Deprecated('Use geboortegegevensGeregistreerdDescriptor instead')
const GeboortegegevensGeregistreerd$json = const {
  '1': 'GeboortegegevensGeregistreerd',
  '2': const [
    const {'1': 'kind_pr', '3': 1, '4': 1, '5': 12, '10': 'kindPr'},
    const {'1': 'moeder_uit_wie_pr', '3': 2, '4': 1, '5': 12, '10': 'moederUitWiePr'},
    const {'1': 'geboorte_datum', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'geboorteDatum'},
    const {'1': 'geboorte_plaats', '3': 4, '4': 1, '5': 9, '10': 'geboortePlaats'},
    const {'1': 'geboorte_gemeente', '3': 5, '4': 1, '5': 9, '10': 'geboorteGemeente'},
    const {'1': 'geboorte_land', '3': 6, '4': 1, '5': 9, '10': 'geboorteLand'},
  ],
};

/// Descriptor for `GeboortegegevensGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geboortegegevensGeregistreerdDescriptor = $convert.base64Decode('Ch1HZWJvb3J0ZWdlZ2V2ZW5zR2VyZWdpc3RyZWVyZBIXCgdraW5kX3ByGAEgASgMUgZraW5kUHISKQoRbW9lZGVyX3VpdF93aWVfcHIYAiABKAxSDm1vZWRlclVpdFdpZVByEkEKDmdlYm9vcnRlX2RhdHVtGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFINZ2Vib29ydGVEYXR1bRInCg9nZWJvb3J0ZV9wbGFhdHMYBCABKAlSDmdlYm9vcnRlUGxhYXRzEisKEWdlYm9vcnRlX2dlbWVlbnRlGAUgASgJUhBnZWJvb3J0ZUdlbWVlbnRlEiMKDWdlYm9vcnRlX2xhbmQYBiABKAlSDGdlYm9vcnRlTGFuZA==');
@$core.Deprecated('Use namenGeregistreerdDescriptor instead')
const NamenGeregistreerd$json = const {
  '1': 'NamenGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'voornamen', '3': 2, '4': 3, '5': 9, '10': 'voornamen'},
    const {'1': 'voorvoegsel_geslachtsnaam', '3': 3, '4': 1, '5': 9, '10': 'voorvoegselGeslachtsnaam'},
    const {'1': 'geslachtsnaam', '3': 4, '4': 1, '5': 9, '10': 'geslachtsnaam'},
    const {'1': 'ingangsdatum_namen', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumNamen'},
  ],
};

/// Descriptor for `NamenGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List namenGeregistreerdDescriptor = $convert.base64Decode('ChJOYW1lbkdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEhwKCXZvb3JuYW1lbhgCIAMoCVIJdm9vcm5hbWVuEjsKGXZvb3J2b2Vnc2VsX2dlc2xhY2h0c25hYW0YAyABKAlSGHZvb3J2b2Vnc2VsR2VzbGFjaHRzbmFhbRIkCg1nZXNsYWNodHNuYWFtGAQgASgJUg1nZXNsYWNodHNuYWFtEkkKEmluZ2FuZ3NkYXR1bV9uYW1lbhgFIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSEWluZ2FuZ3NkYXR1bU5hbWVu');
@$core.Deprecated('Use geslachtsaanduidingGeregistreerdDescriptor instead')
const GeslachtsaanduidingGeregistreerd$json = const {
  '1': 'GeslachtsaanduidingGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'geslachtsaanduiding', '3': 2, '4': 1, '5': 14, '6': '.events.Geslachtsaanduiding', '10': 'geslachtsaanduiding'},
    const {'1': 'ingangsdatum_geslachtsaanduiding', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumGeslachtsaanduiding'},
  ],
};

/// Descriptor for `GeslachtsaanduidingGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geslachtsaanduidingGeregistreerdDescriptor = $convert.base64Decode('CiBHZXNsYWNodHNhYW5kdWlkaW5nR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISTQoTZ2VzbGFjaHRzYWFuZHVpZGluZxgCIAEoDjIbLmV2ZW50cy5HZXNsYWNodHNhYW5kdWlkaW5nUhNnZXNsYWNodHNhYW5kdWlkaW5nEmUKIGluZ2FuZ3NkYXR1bV9nZXNsYWNodHNhYW5kdWlkaW5nGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIfaW5nYW5nc2RhdHVtR2VzbGFjaHRzYWFuZHVpZGluZw==');
@$core.Deprecated('Use adellijkeTitelPredikaatGeregistreerdDescriptor instead')
const AdellijkeTitelPredikaatGeregistreerd$json = const {
  '1': 'AdellijkeTitelPredikaatGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'adellijke_titel_predikaat', '3': 2, '4': 1, '5': 9, '10': 'adellijkeTitelPredikaat'},
    const {'1': 'ingangsdatum_adellijke_titel_predikaat', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumAdellijkeTitelPredikaat'},
  ],
};

/// Descriptor for `AdellijkeTitelPredikaatGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List adellijkeTitelPredikaatGeregistreerdDescriptor = $convert.base64Decode('CiRBZGVsbGlqa2VUaXRlbFByZWRpa2FhdEdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjoKGWFkZWxsaWprZV90aXRlbF9wcmVkaWthYXQYAiABKAlSF2FkZWxsaWprZVRpdGVsUHJlZGlrYWF0Em8KJmluZ2FuZ3NkYXR1bV9hZGVsbGlqa2VfdGl0ZWxfcHJlZGlrYWF0GAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIjaW5nYW5nc2RhdHVtQWRlbGxpamtlVGl0ZWxQcmVkaWthYXQ=');
@$core.Deprecated('Use naamgebruikGeregistreerdDescriptor instead')
const NaamgebruikGeregistreerd$json = const {
  '1': 'NaamgebruikGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'Naamgebruik', '3': 2, '4': 1, '5': 14, '6': '.events.Naamgebruik', '10': 'Naamgebruik'},
    const {'1': 'ingangsdatum_naamgebruik', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumNaamgebruik'},
  ],
};

/// Descriptor for `NaamgebruikGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List naamgebruikGeregistreerdDescriptor = $convert.base64Decode('ChhOYWFtZ2VicnVpa0dlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEjUKC05hYW1nZWJydWlrGAIgASgOMhMuZXZlbnRzLk5hYW1nZWJydWlrUgtOYWFtZ2VicnVpaxJVChhpbmdhbmdzZGF0dW1fbmFhbWdlYnJ1aWsYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhdpbmdhbmdzZGF0dW1OYWFtZ2VicnVpaw==');
@$core.Deprecated('Use naamgebruikGewijzigdDescriptor instead')
const NaamgebruikGewijzigd$json = const {
  '1': 'NaamgebruikGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'Naamgebruik', '3': 2, '4': 1, '5': 14, '6': '.events.Naamgebruik', '10': 'Naamgebruik'},
    const {'1': 'datum_naamgebruik_gewijzigd', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumNaamgebruikGewijzigd'},
  ],
};

/// Descriptor for `NaamgebruikGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List naamgebruikGewijzigdDescriptor = $convert.base64Decode('ChROYWFtZ2VicnVpa0dld2lqemlnZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISNQoLTmFhbWdlYnJ1aWsYAiABKA4yEy5ldmVudHMuTmFhbWdlYnJ1aWtSC05hYW1nZWJydWlrEloKG2RhdHVtX25hYW1nZWJydWlrX2dld2lqemlnZBgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSGWRhdHVtTmFhbWdlYnJ1aWtHZXdpanppZ2Q=');
@$core.Deprecated('Use overlijdenGeregistreerdDescriptor instead')
const OverlijdenGeregistreerd$json = const {
  '1': 'OverlijdenGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'datum_van_overlijden', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVanOverlijden'},
    const {'1': 'plaats_van_overlijden', '3': 3, '4': 1, '5': 9, '10': 'plaatsVanOverlijden'},
    const {'1': 'gemeente_van_overlijden', '3': 4, '4': 1, '5': 9, '10': 'gemeenteVanOverlijden'},
    const {'1': 'land_van_overlijden', '3': 5, '4': 1, '5': 9, '10': 'landVanOverlijden'},
  ],
};

/// Descriptor for `OverlijdenGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List overlijdenGeregistreerdDescriptor = $convert.base64Decode('ChdPdmVybGlqZGVuR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISTAoUZGF0dW1fdmFuX292ZXJsaWpkZW4YAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhJkYXR1bVZhbk92ZXJsaWpkZW4SMgoVcGxhYXRzX3Zhbl9vdmVybGlqZGVuGAMgASgJUhNwbGFhdHNWYW5PdmVybGlqZGVuEjYKF2dlbWVlbnRlX3Zhbl9vdmVybGlqZGVuGAQgASgJUhVnZW1lZW50ZVZhbk92ZXJsaWpkZW4SLgoTbGFuZF92YW5fb3ZlcmxpamRlbhgFIAEoCVIRbGFuZFZhbk92ZXJsaWpkZW4=');
@$core.Deprecated('Use namenGewijzigdDescriptor instead')
const NamenGewijzigd$json = const {
  '1': 'NamenGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'voornamen', '3': 2, '4': 3, '5': 9, '10': 'voornamen'},
    const {'1': 'voorvoegsel_geslachtsnaam', '3': 3, '4': 1, '5': 9, '10': 'voorvoegselGeslachtsnaam'},
    const {'1': 'geslachtsnaam', '3': 4, '4': 1, '5': 9, '10': 'geslachtsnaam'},
    const {'1': 'ingangsdatum_naamswijziging', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumNaamswijziging'},
  ],
};

/// Descriptor for `NamenGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List namenGewijzigdDescriptor = $convert.base64Decode('Cg5OYW1lbkdld2lqemlnZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISHAoJdm9vcm5hbWVuGAIgAygJUgl2b29ybmFtZW4SOwoZdm9vcnZvZWdzZWxfZ2VzbGFjaHRzbmFhbRgDIAEoCVIYdm9vcnZvZWdzZWxHZXNsYWNodHNuYWFtEiQKDWdlc2xhY2h0c25hYW0YBCABKAlSDWdlc2xhY2h0c25hYW0SWwobaW5nYW5nc2RhdHVtX25hYW1zd2lqemlnaW5nGAUgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIaaW5nYW5nc2RhdHVtTmFhbXN3aWp6aWdpbmc=');
@$core.Deprecated('Use geslachtsaanduidingGewijzigdDescriptor instead')
const GeslachtsaanduidingGewijzigd$json = const {
  '1': 'GeslachtsaanduidingGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'geslachtsaanduiding', '3': 2, '4': 1, '5': 14, '6': '.events.Geslachtsaanduiding', '10': 'geslachtsaanduiding'},
    const {'1': 'ingangsdatum_wijziging_geslachtsaanduiding', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumWijzigingGeslachtsaanduiding'},
  ],
};

/// Descriptor for `GeslachtsaanduidingGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geslachtsaanduidingGewijzigdDescriptor = $convert.base64Decode('ChxHZXNsYWNodHNhYW5kdWlkaW5nR2V3aWp6aWdkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJNChNnZXNsYWNodHNhYW5kdWlkaW5nGAIgASgOMhsuZXZlbnRzLkdlc2xhY2h0c2FhbmR1aWRpbmdSE2dlc2xhY2h0c2FhbmR1aWRpbmcSeAoqaW5nYW5nc2RhdHVtX3dpanppZ2luZ19nZXNsYWNodHNhYW5kdWlkaW5nGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIoaW5nYW5nc2RhdHVtV2lqemlnaW5nR2VzbGFjaHRzYWFuZHVpZGluZw==');
@$core.Deprecated('Use erkenningOngeborenVruchtGeregistreerdDescriptor instead')
const ErkenningOngeborenVruchtGeregistreerd$json = const {
  '1': 'ErkenningOngeborenVruchtGeregistreerd',
  '2': const [
    const {'1': 'ongeborenvrucht_pr', '3': 1, '4': 1, '5': 12, '10': 'ongeborenvruchtPr'},
    const {'1': 'moeder_uit_wie_pr', '3': 2, '4': 1, '5': 12, '10': 'moederUitWiePr'},
    const {'1': 'erkenner_pr', '3': 3, '4': 1, '5': 12, '10': 'erkennerPr'},
    const {'1': 'keuze_geslachtsnaam', '3': 4, '4': 1, '5': 9, '10': 'keuzeGeslachtsnaam'},
    const {'1': 'datum_erkenning', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumErkenning'},
  ],
};

/// Descriptor for `ErkenningOngeborenVruchtGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List erkenningOngeborenVruchtGeregistreerdDescriptor = $convert.base64Decode('CiVFcmtlbm5pbmdPbmdlYm9yZW5WcnVjaHRHZXJlZ2lzdHJlZXJkEi0KEm9uZ2Vib3JlbnZydWNodF9wchgBIAEoDFIRb25nZWJvcmVudnJ1Y2h0UHISKQoRbW9lZGVyX3VpdF93aWVfcHIYAiABKAxSDm1vZWRlclVpdFdpZVByEh8KC2Vya2VubmVyX3ByGAMgASgMUgplcmtlbm5lclByEi8KE2tldXplX2dlc2xhY2h0c25hYW0YBCABKAlSEmtldXplR2VzbGFjaHRzbmFhbRJDCg9kYXR1bV9lcmtlbm5pbmcYBSABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUg5kYXR1bUVya2VubmluZw==');
@$core.Deprecated('Use erkenningKindGeregistreerdDescriptor instead')
const ErkenningKindGeregistreerd$json = const {
  '1': 'ErkenningKindGeregistreerd',
  '2': const [
    const {'1': 'kind_pr', '3': 1, '4': 1, '5': 12, '10': 'kindPr'},
    const {'1': 'moeder_uit_wie_pr', '3': 2, '4': 1, '5': 12, '10': 'moederUitWiePr'},
    const {'1': 'erkenner_pr', '3': 3, '4': 1, '5': 12, '10': 'erkennerPr'},
    const {'1': 'keuze_geslachtsnaam', '3': 4, '4': 1, '5': 9, '10': 'keuzeGeslachtsnaam'},
    const {'1': 'datum_erkenning', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumErkenning'},
  ],
};

/// Descriptor for `ErkenningKindGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List erkenningKindGeregistreerdDescriptor = $convert.base64Decode('ChpFcmtlbm5pbmdLaW5kR2VyZWdpc3RyZWVyZBIXCgdraW5kX3ByGAEgASgMUgZraW5kUHISKQoRbW9lZGVyX3VpdF93aWVfcHIYAiABKAxSDm1vZWRlclVpdFdpZVByEh8KC2Vya2VubmVyX3ByGAMgASgMUgplcmtlbm5lclByEi8KE2tldXplX2dlc2xhY2h0c25hYW0YBCABKAlSEmtldXplR2VzbGFjaHRzbmFhbRJDCg9kYXR1bV9lcmtlbm5pbmcYBSABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUg5kYXR1bUVya2VubmluZw==');
@$core.Deprecated('Use erkenningVernietigdDescriptor instead')
const ErkenningVernietigd$json = const {
  '1': 'ErkenningVernietigd',
  '2': const [
    const {'1': 'kind_pr', '3': 1, '4': 1, '5': 12, '10': 'kindPr'},
    const {'1': 'moeder_pr', '3': 2, '4': 1, '5': 12, '10': 'moederPr'},
    const {'1': 'erkenner_pr', '3': 3, '4': 1, '5': 12, '10': 'erkennerPr'},
    const {'1': 'datum_vernietiging_erkenning', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVernietigingErkenning'},
  ],
};

/// Descriptor for `ErkenningVernietigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List erkenningVernietigdDescriptor = $convert.base64Decode('ChNFcmtlbm5pbmdWZXJuaWV0aWdkEhcKB2tpbmRfcHIYASABKAxSBmtpbmRQchIbCgltb2VkZXJfcHIYAiABKAxSCG1vZWRlclByEh8KC2Vya2VubmVyX3ByGAMgASgMUgplcmtlbm5lclByElwKHGRhdHVtX3Zlcm5pZXRpZ2luZ19lcmtlbm5pbmcYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhpkYXR1bVZlcm5pZXRpZ2luZ0Vya2VubmluZw==');
@$core.Deprecated('Use juridischOuderGeregistreerdDescriptor instead')
const JuridischOuderGeregistreerd$json = const {
  '1': 'JuridischOuderGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'ouder_pr', '3': 2, '4': 1, '5': 12, '10': 'ouderPr'},
    const {'1': 'startdatum_relatie', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumRelatie'},
  ],
};

/// Descriptor for `JuridischOuderGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List juridischOuderGeregistreerdDescriptor = $convert.base64Decode('ChtKdXJpZGlzY2hPdWRlckdlcmVnaXN0cmVlcmQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEhkKCG91ZGVyX3ByGAIgASgMUgdvdWRlclByEkkKEnN0YXJ0ZGF0dW1fcmVsYXRpZRgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSEXN0YXJ0ZGF0dW1SZWxhdGll');
@$core.Deprecated('Use juridischOuderschapOntkendDescriptor instead')
const JuridischOuderschapOntkend$json = const {
  '1': 'JuridischOuderschapOntkend',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'ouder_pr', '3': 2, '4': 1, '5': 12, '10': 'ouderPr'},
    const {'1': 'datum_ontkenning_ouderschap', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumOntkenningOuderschap'},
  ],
};

/// Descriptor for `JuridischOuderschapOntkend`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List juridischOuderschapOntkendDescriptor = $convert.base64Decode('ChpKdXJpZGlzY2hPdWRlcnNjaGFwT250a2VuZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISGQoIb3VkZXJfcHIYAiABKAxSB291ZGVyUHISWgobZGF0dW1fb250a2VubmluZ19vdWRlcnNjaGFwGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIZZGF0dW1PbnRrZW5uaW5nT3VkZXJzY2hhcA==');
@$core.Deprecated('Use juridischOuderschapGerechtelijkVastgesteldDescriptor instead')
const JuridischOuderschapGerechtelijkVastgesteld$json = const {
  '1': 'JuridischOuderschapGerechtelijkVastgesteld',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'ouder_pr', '3': 2, '4': 1, '5': 12, '10': 'ouderPr'},
    const {'1': 'ingangsdatum_ouderschap', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'ingangsdatumOuderschap'},
  ],
};

/// Descriptor for `JuridischOuderschapGerechtelijkVastgesteld`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List juridischOuderschapGerechtelijkVastgesteldDescriptor = $convert.base64Decode('CipKdXJpZGlzY2hPdWRlcnNjaGFwR2VyZWNodGVsaWprVmFzdGdlc3RlbGQSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEhkKCG91ZGVyX3ByGAIgASgMUgdvdWRlclByElMKF2luZ2FuZ3NkYXR1bV9vdWRlcnNjaGFwGAMgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIWaW5nYW5nc2RhdHVtT3VkZXJzY2hhcA==');
@$core.Deprecated('Use huwelijkGeregistreerdDescriptor instead')
const HuwelijkGeregistreerd$json = const {
  '1': 'HuwelijkGeregistreerd',
  '2': const [
    const {'1': 'persoon1_pr', '3': 1, '4': 1, '5': 12, '10': 'persoon1Pr'},
    const {'1': 'persoon2_pr', '3': 2, '4': 1, '5': 12, '10': 'persoon2Pr'},
    const {'1': 'datum_huwelijk_voltrokken', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumHuwelijkVoltrokken'},
    const {'1': 'plaats_huwelijk_voltrokken', '3': 4, '4': 1, '5': 9, '10': 'plaatsHuwelijkVoltrokken'},
    const {'1': 'land_huwelijk_voltrokken', '3': 5, '4': 1, '5': 9, '10': 'landHuwelijkVoltrokken'},
  ],
};

/// Descriptor for `HuwelijkGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List huwelijkGeregistreerdDescriptor = $convert.base64Decode('ChVIdXdlbGlqa0dlcmVnaXN0cmVlcmQSHwoLcGVyc29vbjFfcHIYASABKAxSCnBlcnNvb24xUHISHwoLcGVyc29vbjJfcHIYAiABKAxSCnBlcnNvb24yUHISVgoZZGF0dW1faHV3ZWxpamtfdm9sdHJva2tlbhgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSF2RhdHVtSHV3ZWxpamtWb2x0cm9ra2VuEjwKGnBsYWF0c19odXdlbGlqa192b2x0cm9ra2VuGAQgASgJUhhwbGFhdHNIdXdlbGlqa1ZvbHRyb2trZW4SOAoYbGFuZF9odXdlbGlqa192b2x0cm9ra2VuGAUgASgJUhZsYW5kSHV3ZWxpamtWb2x0cm9ra2Vu');
@$core.Deprecated('Use geregistreerd_PartnerschapGeregistreerdDescriptor instead')
const Geregistreerd_PartnerschapGeregistreerd$json = const {
  '1': 'Geregistreerd_PartnerschapGeregistreerd',
  '2': const [
    const {'1': 'persoon1_pr', '3': 1, '4': 1, '5': 9, '10': 'persoon1Pr'},
    const {'1': 'persoon2_pr', '3': 2, '4': 1, '5': 9, '10': 'persoon2Pr'},
    const {'1': 'startdatum_geregistreerd_partnerschap', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumGeregistreerdPartnerschap'},
    const {'1': 'plaats_geregistreerd_partnerschap', '3': 4, '4': 1, '5': 9, '10': 'plaatsGeregistreerdPartnerschap'},
    const {'1': 'land_geregistreerd_partnerschap', '3': 5, '4': 1, '5': 9, '10': 'landGeregistreerdPartnerschap'},
  ],
};

/// Descriptor for `Geregistreerd_PartnerschapGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geregistreerd_PartnerschapGeregistreerdDescriptor = $convert.base64Decode('CidHZXJlZ2lzdHJlZXJkX1BhcnRuZXJzY2hhcEdlcmVnaXN0cmVlcmQSHwoLcGVyc29vbjFfcHIYASABKAlSCnBlcnNvb24xUHISHwoLcGVyc29vbjJfcHIYAiABKAlSCnBlcnNvb24yUHISbgolc3RhcnRkYXR1bV9nZXJlZ2lzdHJlZXJkX3BhcnRuZXJzY2hhcBgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSI3N0YXJ0ZGF0dW1HZXJlZ2lzdHJlZXJkUGFydG5lcnNjaGFwEkoKIXBsYWF0c19nZXJlZ2lzdHJlZXJkX3BhcnRuZXJzY2hhcBgEIAEoCVIfcGxhYXRzR2VyZWdpc3RyZWVyZFBhcnRuZXJzY2hhcBJGCh9sYW5kX2dlcmVnaXN0cmVlcmRfcGFydG5lcnNjaGFwGAUgASgJUh1sYW5kR2VyZWdpc3RyZWVyZFBhcnRuZXJzY2hhcA==');
@$core.Deprecated('Use huwelijkOntbondenDescriptor instead')
const HuwelijkOntbonden$json = const {
  '1': 'HuwelijkOntbonden',
  '2': const [
    const {'1': 'persoon1_pr', '3': 1, '4': 1, '5': 12, '10': 'persoon1Pr'},
    const {'1': 'persoon2_pr', '3': 2, '4': 1, '5': 12, '10': 'persoon2Pr'},
    const {'1': 'datum_huwelijk_ontbonden', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumHuwelijkOntbonden'},
    const {'1': 'plaats_huwelijk_ontbonden', '3': 4, '4': 1, '5': 9, '10': 'plaatsHuwelijkOntbonden'},
    const {'1': 'land_huwelijk_ontbonden', '3': 5, '4': 1, '5': 9, '10': 'landHuwelijkOntbonden'},
    const {'1': 'reden_huwelijk_ontbonden', '3': 6, '4': 1, '5': 9, '10': 'redenHuwelijkOntbonden'},
  ],
};

/// Descriptor for `HuwelijkOntbonden`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List huwelijkOntbondenDescriptor = $convert.base64Decode('ChFIdXdlbGlqa09udGJvbmRlbhIfCgtwZXJzb29uMV9wchgBIAEoDFIKcGVyc29vbjFQchIfCgtwZXJzb29uMl9wchgCIAEoDFIKcGVyc29vbjJQchJUChhkYXR1bV9odXdlbGlqa19vbnRib25kZW4YAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhZkYXR1bUh1d2VsaWprT250Ym9uZGVuEjoKGXBsYWF0c19odXdlbGlqa19vbnRib25kZW4YBCABKAlSF3BsYWF0c0h1d2VsaWprT250Ym9uZGVuEjYKF2xhbmRfaHV3ZWxpamtfb250Ym9uZGVuGAUgASgJUhVsYW5kSHV3ZWxpamtPbnRib25kZW4SOAoYcmVkZW5faHV3ZWxpamtfb250Ym9uZGVuGAYgASgJUhZyZWRlbkh1d2VsaWprT250Ym9uZGVu');
@$core.Deprecated('Use geregistreerdPartnerschapOntbondenDescriptor instead')
const GeregistreerdPartnerschapOntbonden$json = const {
  '1': 'GeregistreerdPartnerschapOntbonden',
  '2': const [
    const {'1': 'persoon1_pr', '3': 1, '4': 1, '5': 12, '10': 'persoon1Pr'},
    const {'1': 'persoon2_pr', '3': 2, '4': 1, '5': 12, '10': 'persoon2Pr'},
    const {'1': 'datum_geregistreerd_partnerschap_ontbonden', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumGeregistreerdPartnerschapOntbonden'},
    const {'1': 'plaats_geregistreerd_partnerschap_ontbonden', '3': 4, '4': 1, '5': 9, '10': 'plaatsGeregistreerdPartnerschapOntbonden'},
    const {'1': 'land_geregistreerd_partnerschap_ontbonden', '3': 5, '4': 1, '5': 9, '10': 'landGeregistreerdPartnerschapOntbonden'},
    const {'1': 'reden_geregistreerd_partnerschap_ontbonden', '3': 6, '4': 1, '5': 9, '10': 'redenGeregistreerdPartnerschapOntbonden'},
  ],
};

/// Descriptor for `GeregistreerdPartnerschapOntbonden`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List geregistreerdPartnerschapOntbondenDescriptor = $convert.base64Decode('CiJHZXJlZ2lzdHJlZXJkUGFydG5lcnNjaGFwT250Ym9uZGVuEh8KC3BlcnNvb24xX3ByGAEgASgMUgpwZXJzb29uMVByEh8KC3BlcnNvb24yX3ByGAIgASgMUgpwZXJzb29uMlByEncKKmRhdHVtX2dlcmVnaXN0cmVlcmRfcGFydG5lcnNjaGFwX29udGJvbmRlbhgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSJ2RhdHVtR2VyZWdpc3RyZWVyZFBhcnRuZXJzY2hhcE9udGJvbmRlbhJdCitwbGFhdHNfZ2VyZWdpc3RyZWVyZF9wYXJ0bmVyc2NoYXBfb250Ym9uZGVuGAQgASgJUihwbGFhdHNHZXJlZ2lzdHJlZXJkUGFydG5lcnNjaGFwT250Ym9uZGVuElkKKWxhbmRfZ2VyZWdpc3RyZWVyZF9wYXJ0bmVyc2NoYXBfb250Ym9uZGVuGAUgASgJUiZsYW5kR2VyZWdpc3RyZWVyZFBhcnRuZXJzY2hhcE9udGJvbmRlbhJbCipyZWRlbl9nZXJlZ2lzdHJlZXJkX3BhcnRuZXJzY2hhcF9vbnRib25kZW4YBiABKAlSJ3JlZGVuR2VyZWdpc3RyZWVyZFBhcnRuZXJzY2hhcE9udGJvbmRlbg==');
@$core.Deprecated('Use contactgegevensGeregistreerdDescriptor instead')
const ContactgegevensGeregistreerd$json = const {
  '1': 'ContactgegevensGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'emailadres', '3': 2, '4': 1, '5': 9, '10': 'emailadres'},
    const {'1': 'mobiel_telefoonnummer', '3': 3, '4': 1, '5': 9, '10': 'mobielTelefoonnummer'},
    const {'1': 'startdatum_contactgegevens', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumContactgegevens'},
  ],
};

/// Descriptor for `ContactgegevensGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List contactgegevensGeregistreerdDescriptor = $convert.base64Decode('ChxDb250YWN0Z2VnZXZlbnNHZXJlZ2lzdHJlZXJkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchIeCgplbWFpbGFkcmVzGAIgASgJUgplbWFpbGFkcmVzEjMKFW1vYmllbF90ZWxlZm9vbm51bW1lchgDIAEoCVIUbW9iaWVsVGVsZWZvb25udW1tZXISWQoac3RhcnRkYXR1bV9jb250YWN0Z2VnZXZlbnMYBCABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhlzdGFydGRhdHVtQ29udGFjdGdlZ2V2ZW5z');
@$core.Deprecated('Use contactgegevensGewijzigdDescriptor instead')
const ContactgegevensGewijzigd$json = const {
  '1': 'ContactgegevensGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'emailadres', '3': 2, '4': 1, '5': 9, '10': 'emailadres'},
    const {'1': 'mobiel_telefoonnummer', '3': 3, '4': 1, '5': 9, '10': 'mobielTelefoonnummer'},
    const {'1': 'datum_contactgegevens_gewijzigd', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumContactgegevensGewijzigd'},
  ],
};

/// Descriptor for `ContactgegevensGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List contactgegevensGewijzigdDescriptor = $convert.base64Decode('ChhDb250YWN0Z2VnZXZlbnNHZXdpanppZ2QSHQoKcGVyc29vbl9wchgBIAEoDFIJcGVyc29vblByEh4KCmVtYWlsYWRyZXMYAiABKAlSCmVtYWlsYWRyZXMSMwoVbW9iaWVsX3RlbGVmb29ubnVtbWVyGAMgASgJUhRtb2JpZWxUZWxlZm9vbm51bW1lchJiCh9kYXR1bV9jb250YWN0Z2VnZXZlbnNfZ2V3aWp6aWdkGAQgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIdZGF0dW1Db250YWN0Z2VnZXZlbnNHZXdpanppZ2Q=');
@$core.Deprecated('Use verstrekkingsbeperkingGeregistreerdDescriptor instead')
const VerstrekkingsbeperkingGeregistreerd$json = const {
  '1': 'VerstrekkingsbeperkingGeregistreerd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verstrekkingsbeperking', '3': 2, '4': 1, '5': 14, '6': '.events.Verstrekkingsbeperking', '10': 'verstrekkingsbeperking'},
    const {'1': 'startdatum_verstrekkingsbeperking', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startdatumVerstrekkingsbeperking'},
  ],
};

/// Descriptor for `VerstrekkingsbeperkingGeregistreerd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verstrekkingsbeperkingGeregistreerdDescriptor = $convert.base64Decode('CiNWZXJzdHJla2tpbmdzYmVwZXJraW5nR2VyZWdpc3RyZWVyZBIdCgpwZXJzb29uX3ByGAEgASgMUglwZXJzb29uUHISVgoWdmVyc3RyZWtraW5nc2JlcGVya2luZxgCIAEoDjIeLmV2ZW50cy5WZXJzdHJla2tpbmdzYmVwZXJraW5nUhZ2ZXJzdHJla2tpbmdzYmVwZXJraW5nEmcKIXN0YXJ0ZGF0dW1fdmVyc3RyZWtraW5nc2JlcGVya2luZxgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSIHN0YXJ0ZGF0dW1WZXJzdHJla2tpbmdzYmVwZXJraW5n');
@$core.Deprecated('Use verstrekkingsbeperkingGewijzigdDescriptor instead')
const VerstrekkingsbeperkingGewijzigd$json = const {
  '1': 'VerstrekkingsbeperkingGewijzigd',
  '2': const [
    const {'1': 'persoon_pr', '3': 1, '4': 1, '5': 12, '10': 'persoonPr'},
    const {'1': 'verstrekkingsbeperking', '3': 2, '4': 1, '5': 14, '6': '.events.Verstrekkingsbeperking', '10': 'verstrekkingsbeperking'},
    const {'1': 'datum_verstrekkingsbeperking_gewijzigd', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumVerstrekkingsbeperkingGewijzigd'},
  ],
};

/// Descriptor for `VerstrekkingsbeperkingGewijzigd`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List verstrekkingsbeperkingGewijzigdDescriptor = $convert.base64Decode('Ch9WZXJzdHJla2tpbmdzYmVwZXJraW5nR2V3aWp6aWdkEh0KCnBlcnNvb25fcHIYASABKAxSCXBlcnNvb25QchJWChZ2ZXJzdHJla2tpbmdzYmVwZXJraW5nGAIgASgOMh4uZXZlbnRzLlZlcnN0cmVra2luZ3NiZXBlcmtpbmdSFnZlcnN0cmVra2luZ3NiZXBlcmtpbmcScAomZGF0dW1fdmVyc3RyZWtraW5nc2JlcGVya2luZ19nZXdpanppZ2QYAyABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUiRkYXR1bVZlcnN0cmVra2luZ3NiZXBlcmtpbmdHZXdpanppZ2Q=');
