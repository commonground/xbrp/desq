///
//  Generated code. Do not modify.
//  source: common/events/events.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../google/protobuf/timestamp.pb.dart' as $0;

import 'events.pbenum.dart';

export 'events.pbenum.dart';

class Event extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Event', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'reference', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kind')
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'data', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'date', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  Event._() : super();
  factory Event({
    $core.List<$core.int>? reference,
    $core.String? kind,
    $core.List<$core.int>? data,
    $0.Timestamp? date,
  }) {
    final _result = create();
    if (reference != null) {
      _result.reference = reference;
    }
    if (kind != null) {
      _result.kind = kind;
    }
    if (data != null) {
      _result.data = data;
    }
    if (date != null) {
      _result.date = date;
    }
    return _result;
  }
  factory Event.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Event.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Event clone() => Event()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Event copyWith(void Function(Event) updates) => super.copyWith((message) => updates(message as Event)) as Event; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Event create() => Event._();
  Event createEmptyInstance() => create();
  static $pb.PbList<Event> createRepeated() => $pb.PbList<Event>();
  @$core.pragma('dart2js:noInline')
  static Event getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Event>(create);
  static Event? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get reference => $_getN(0);
  @$pb.TagNumber(1)
  set reference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get kind => $_getSZ(1);
  @$pb.TagNumber(2)
  set kind($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasKind() => $_has(1);
  @$pb.TagNumber(2)
  void clearKind() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get data => $_getN(2);
  @$pb.TagNumber(3)
  set data($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasData() => $_has(2);
  @$pb.TagNumber(3)
  void clearData() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get date => $_getN(3);
  @$pb.TagNumber(4)
  set date($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDate() => $_has(3);
  @$pb.TagNumber(4)
  void clearDate() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDate() => $_ensure(3);
}

class NewPersonEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewPersonEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  NewPersonEvent._() : super();
  factory NewPersonEvent({
    $core.List<$core.int>? personReference,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    return _result;
  }
  factory NewPersonEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewPersonEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewPersonEvent clone() => NewPersonEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewPersonEvent copyWith(void Function(NewPersonEvent) updates) => super.copyWith((message) => updates(message as NewPersonEvent)) as NewPersonEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewPersonEvent create() => NewPersonEvent._();
  NewPersonEvent createEmptyInstance() => create();
  static $pb.PbList<NewPersonEvent> createRepeated() => $pb.PbList<NewPersonEvent>();
  @$core.pragma('dart2js:noInline')
  static NewPersonEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewPersonEvent>(create);
  static NewPersonEvent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);
}

class PersonNoteEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersonNoteEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'note')
    ..hasRequiredFields = false
  ;

  PersonNoteEvent._() : super();
  factory PersonNoteEvent({
    $core.List<$core.int>? personReference,
    $core.String? note,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    if (note != null) {
      _result.note = note;
    }
    return _result;
  }
  factory PersonNoteEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersonNoteEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersonNoteEvent clone() => PersonNoteEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersonNoteEvent copyWith(void Function(PersonNoteEvent) updates) => super.copyWith((message) => updates(message as PersonNoteEvent)) as PersonNoteEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersonNoteEvent create() => PersonNoteEvent._();
  PersonNoteEvent createEmptyInstance() => create();
  static $pb.PbList<PersonNoteEvent> createRepeated() => $pb.PbList<PersonNoteEvent>();
  @$core.pragma('dart2js:noInline')
  static PersonNoteEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersonNoteEvent>(create);
  static PersonNoteEvent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get note => $_getSZ(1);
  @$pb.TagNumber(2)
  set note($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNote() => $_has(1);
  @$pb.TagNumber(2)
  void clearNote() => clearField(2);
}

class GeboorteVastgesteldEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GeboorteVastgesteldEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kindPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteDatum', subBuilder: $0.Timestamp.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboortePlaats')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteGemeente')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteLand')
    ..hasRequiredFields = false
  ;

  GeboorteVastgesteldEvent._() : super();
  factory GeboorteVastgesteldEvent({
    $core.List<$core.int>? kindPr,
    $0.Timestamp? geboorteDatum,
    $core.String? geboortePlaats,
    $core.String? geboorteGemeente,
    $core.String? geboorteLand,
  }) {
    final _result = create();
    if (kindPr != null) {
      _result.kindPr = kindPr;
    }
    if (geboorteDatum != null) {
      _result.geboorteDatum = geboorteDatum;
    }
    if (geboortePlaats != null) {
      _result.geboortePlaats = geboortePlaats;
    }
    if (geboorteGemeente != null) {
      _result.geboorteGemeente = geboorteGemeente;
    }
    if (geboorteLand != null) {
      _result.geboorteLand = geboorteLand;
    }
    return _result;
  }
  factory GeboorteVastgesteldEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GeboorteVastgesteldEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GeboorteVastgesteldEvent clone() => GeboorteVastgesteldEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GeboorteVastgesteldEvent copyWith(void Function(GeboorteVastgesteldEvent) updates) => super.copyWith((message) => updates(message as GeboorteVastgesteldEvent)) as GeboorteVastgesteldEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GeboorteVastgesteldEvent create() => GeboorteVastgesteldEvent._();
  GeboorteVastgesteldEvent createEmptyInstance() => create();
  static $pb.PbList<GeboorteVastgesteldEvent> createRepeated() => $pb.PbList<GeboorteVastgesteldEvent>();
  @$core.pragma('dart2js:noInline')
  static GeboorteVastgesteldEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GeboorteVastgesteldEvent>(create);
  static GeboorteVastgesteldEvent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get kindPr => $_getN(0);
  @$pb.TagNumber(1)
  set kindPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKindPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearKindPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get geboorteDatum => $_getN(1);
  @$pb.TagNumber(2)
  set geboorteDatum($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGeboorteDatum() => $_has(1);
  @$pb.TagNumber(2)
  void clearGeboorteDatum() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureGeboorteDatum() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get geboortePlaats => $_getSZ(2);
  @$pb.TagNumber(3)
  set geboortePlaats($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGeboortePlaats() => $_has(2);
  @$pb.TagNumber(3)
  void clearGeboortePlaats() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get geboorteGemeente => $_getSZ(3);
  @$pb.TagNumber(4)
  set geboorteGemeente($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGeboorteGemeente() => $_has(3);
  @$pb.TagNumber(4)
  void clearGeboorteGemeente() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get geboorteLand => $_getSZ(4);
  @$pb.TagNumber(5)
  set geboorteLand($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasGeboorteLand() => $_has(4);
  @$pb.TagNumber(5)
  void clearGeboorteLand() => clearField(5);
}

class NamenVastgesteld extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NamenVastgesteld', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonHr', $pb.PbFieldType.OY)
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voornamen')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voorvoegselGeslachtsnaam')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geslachtsnaam')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumNamen', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  NamenVastgesteld._() : super();
  factory NamenVastgesteld({
    $core.List<$core.int>? persoonHr,
    $core.Iterable<$core.String>? voornamen,
    $core.String? voorvoegselGeslachtsnaam,
    $core.String? geslachtsnaam,
    $0.Timestamp? ingangsdatumNamen,
  }) {
    final _result = create();
    if (persoonHr != null) {
      _result.persoonHr = persoonHr;
    }
    if (voornamen != null) {
      _result.voornamen.addAll(voornamen);
    }
    if (voorvoegselGeslachtsnaam != null) {
      _result.voorvoegselGeslachtsnaam = voorvoegselGeslachtsnaam;
    }
    if (geslachtsnaam != null) {
      _result.geslachtsnaam = geslachtsnaam;
    }
    if (ingangsdatumNamen != null) {
      _result.ingangsdatumNamen = ingangsdatumNamen;
    }
    return _result;
  }
  factory NamenVastgesteld.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NamenVastgesteld.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NamenVastgesteld clone() => NamenVastgesteld()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NamenVastgesteld copyWith(void Function(NamenVastgesteld) updates) => super.copyWith((message) => updates(message as NamenVastgesteld)) as NamenVastgesteld; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NamenVastgesteld create() => NamenVastgesteld._();
  NamenVastgesteld createEmptyInstance() => create();
  static $pb.PbList<NamenVastgesteld> createRepeated() => $pb.PbList<NamenVastgesteld>();
  @$core.pragma('dart2js:noInline')
  static NamenVastgesteld getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NamenVastgesteld>(create);
  static NamenVastgesteld? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonHr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonHr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonHr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonHr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get voornamen => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get voorvoegselGeslachtsnaam => $_getSZ(2);
  @$pb.TagNumber(3)
  set voorvoegselGeslachtsnaam($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasVoorvoegselGeslachtsnaam() => $_has(2);
  @$pb.TagNumber(3)
  void clearVoorvoegselGeslachtsnaam() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get geslachtsnaam => $_getSZ(3);
  @$pb.TagNumber(4)
  set geslachtsnaam($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGeslachtsnaam() => $_has(3);
  @$pb.TagNumber(4)
  void clearGeslachtsnaam() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get ingangsdatumNamen => $_getN(4);
  @$pb.TagNumber(5)
  set ingangsdatumNamen($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasIngangsdatumNamen() => $_has(4);
  @$pb.TagNumber(5)
  void clearIngangsdatumNamen() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureIngangsdatumNamen() => $_ensure(4);
}

class PersoonIngeschreven extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersoonIngeschreven', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumIngeschreven', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  PersoonIngeschreven._() : super();
  factory PersoonIngeschreven({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? datumIngeschreven,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (datumIngeschreven != null) {
      _result.datumIngeschreven = datumIngeschreven;
    }
    return _result;
  }
  factory PersoonIngeschreven.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersoonIngeschreven.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersoonIngeschreven clone() => PersoonIngeschreven()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersoonIngeschreven copyWith(void Function(PersoonIngeschreven) updates) => super.copyWith((message) => updates(message as PersoonIngeschreven)) as PersoonIngeschreven; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschreven create() => PersoonIngeschreven._();
  PersoonIngeschreven createEmptyInstance() => create();
  static $pb.PbList<PersoonIngeschreven> createRepeated() => $pb.PbList<PersoonIngeschreven>();
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschreven getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersoonIngeschreven>(create);
  static PersoonIngeschreven? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get datumIngeschreven => $_getN(1);
  @$pb.TagNumber(2)
  set datumIngeschreven($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDatumIngeschreven() => $_has(1);
  @$pb.TagNumber(2)
  void clearDatumIngeschreven() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureDatumIngeschreven() => $_ensure(1);
}

class BSNToegekend extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BSNToegekend', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'BSN', protoName: 'BSN')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumBsn', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  BSNToegekend._() : super();
  factory BSNToegekend({
    $core.List<$core.int>? persoonPr,
    $core.String? bSN,
    $0.Timestamp? ingangsdatumBsn,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (bSN != null) {
      _result.bSN = bSN;
    }
    if (ingangsdatumBsn != null) {
      _result.ingangsdatumBsn = ingangsdatumBsn;
    }
    return _result;
  }
  factory BSNToegekend.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BSNToegekend.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BSNToegekend clone() => BSNToegekend()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BSNToegekend copyWith(void Function(BSNToegekend) updates) => super.copyWith((message) => updates(message as BSNToegekend)) as BSNToegekend; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BSNToegekend create() => BSNToegekend._();
  BSNToegekend createEmptyInstance() => create();
  static $pb.PbList<BSNToegekend> createRepeated() => $pb.PbList<BSNToegekend>();
  @$core.pragma('dart2js:noInline')
  static BSNToegekend getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BSNToegekend>(create);
  static BSNToegekend? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get bSN => $_getSZ(1);
  @$pb.TagNumber(2)
  set bSN($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBSN() => $_has(1);
  @$pb.TagNumber(2)
  void clearBSN() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumBsn => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumBsn($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumBsn() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumBsn() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumBsn() => $_ensure(2);
}

class ImmigratieGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ImmigratieGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landVanwaarGekomen')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumGeimmigreerd', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  ImmigratieGeregistreerd._() : super();
  factory ImmigratieGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? landVanwaarGekomen,
    $0.Timestamp? datumGeimmigreerd,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (landVanwaarGekomen != null) {
      _result.landVanwaarGekomen = landVanwaarGekomen;
    }
    if (datumGeimmigreerd != null) {
      _result.datumGeimmigreerd = datumGeimmigreerd;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory ImmigratieGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ImmigratieGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ImmigratieGeregistreerd clone() => ImmigratieGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ImmigratieGeregistreerd copyWith(void Function(ImmigratieGeregistreerd) updates) => super.copyWith((message) => updates(message as ImmigratieGeregistreerd)) as ImmigratieGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ImmigratieGeregistreerd create() => ImmigratieGeregistreerd._();
  ImmigratieGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<ImmigratieGeregistreerd> createRepeated() => $pb.PbList<ImmigratieGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static ImmigratieGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ImmigratieGeregistreerd>(create);
  static ImmigratieGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get landVanwaarGekomen => $_getSZ(1);
  @$pb.TagNumber(2)
  set landVanwaarGekomen($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLandVanwaarGekomen() => $_has(1);
  @$pb.TagNumber(2)
  void clearLandVanwaarGekomen() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumGeimmigreerd => $_getN(2);
  @$pb.TagNumber(3)
  set datumGeimmigreerd($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumGeimmigreerd() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumGeimmigreerd() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumGeimmigreerd() => $_ensure(2);

  @$pb.TagNumber(4)
  Aangifte get aangifte => $_getN(3);
  @$pb.TagNumber(4)
  set aangifte(Aangifte v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasAangifte() => $_has(3);
  @$pb.TagNumber(4)
  void clearAangifte() => clearField(4);
}

class WoonadresNLGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'WoonadresNLGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verwijzingWoonadresBAG', protoName: 'verwijzing_woonadres_BAG')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeente')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumWoonadres', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  WoonadresNLGeregistreerd._() : super();
  factory WoonadresNLGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? verwijzingWoonadresBAG,
    $core.String? gemeente,
    $0.Timestamp? startdatumWoonadres,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verwijzingWoonadresBAG != null) {
      _result.verwijzingWoonadresBAG = verwijzingWoonadresBAG;
    }
    if (gemeente != null) {
      _result.gemeente = gemeente;
    }
    if (startdatumWoonadres != null) {
      _result.startdatumWoonadres = startdatumWoonadres;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory WoonadresNLGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory WoonadresNLGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  WoonadresNLGeregistreerd clone() => WoonadresNLGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  WoonadresNLGeregistreerd copyWith(void Function(WoonadresNLGeregistreerd) updates) => super.copyWith((message) => updates(message as WoonadresNLGeregistreerd)) as WoonadresNLGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static WoonadresNLGeregistreerd create() => WoonadresNLGeregistreerd._();
  WoonadresNLGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<WoonadresNLGeregistreerd> createRepeated() => $pb.PbList<WoonadresNLGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static WoonadresNLGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<WoonadresNLGeregistreerd>(create);
  static WoonadresNLGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get verwijzingWoonadresBAG => $_getSZ(1);
  @$pb.TagNumber(2)
  set verwijzingWoonadresBAG($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerwijzingWoonadresBAG() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerwijzingWoonadresBAG() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get gemeente => $_getSZ(2);
  @$pb.TagNumber(3)
  set gemeente($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGemeente() => $_has(2);
  @$pb.TagNumber(3)
  void clearGemeente() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get startdatumWoonadres => $_getN(3);
  @$pb.TagNumber(4)
  set startdatumWoonadres($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStartdatumWoonadres() => $_has(3);
  @$pb.TagNumber(4)
  void clearStartdatumWoonadres() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureStartdatumWoonadres() => $_ensure(3);

  @$pb.TagNumber(5)
  Aangifte get aangifte => $_getN(4);
  @$pb.TagNumber(5)
  set aangifte(Aangifte v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasAangifte() => $_has(4);
  @$pb.TagNumber(5)
  void clearAangifte() => clearField(5);
}

class BriefadresToegekendEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BriefadresToegekendEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verwijzingBriefadresBAG', protoName: 'verwijzing_briefadres_BAG')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeente')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumBriefadres', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  BriefadresToegekendEvent._() : super();
  factory BriefadresToegekendEvent({
    $core.List<$core.int>? persoonPr,
    $core.String? verwijzingBriefadresBAG,
    $core.String? gemeente,
    $0.Timestamp? startdatumBriefadres,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verwijzingBriefadresBAG != null) {
      _result.verwijzingBriefadresBAG = verwijzingBriefadresBAG;
    }
    if (gemeente != null) {
      _result.gemeente = gemeente;
    }
    if (startdatumBriefadres != null) {
      _result.startdatumBriefadres = startdatumBriefadres;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory BriefadresToegekendEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BriefadresToegekendEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BriefadresToegekendEvent clone() => BriefadresToegekendEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BriefadresToegekendEvent copyWith(void Function(BriefadresToegekendEvent) updates) => super.copyWith((message) => updates(message as BriefadresToegekendEvent)) as BriefadresToegekendEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BriefadresToegekendEvent create() => BriefadresToegekendEvent._();
  BriefadresToegekendEvent createEmptyInstance() => create();
  static $pb.PbList<BriefadresToegekendEvent> createRepeated() => $pb.PbList<BriefadresToegekendEvent>();
  @$core.pragma('dart2js:noInline')
  static BriefadresToegekendEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BriefadresToegekendEvent>(create);
  static BriefadresToegekendEvent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get verwijzingBriefadresBAG => $_getSZ(1);
  @$pb.TagNumber(2)
  set verwijzingBriefadresBAG($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerwijzingBriefadresBAG() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerwijzingBriefadresBAG() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get gemeente => $_getSZ(2);
  @$pb.TagNumber(3)
  set gemeente($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGemeente() => $_has(2);
  @$pb.TagNumber(3)
  void clearGemeente() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get startdatumBriefadres => $_getN(3);
  @$pb.TagNumber(4)
  set startdatumBriefadres($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStartdatumBriefadres() => $_has(3);
  @$pb.TagNumber(4)
  void clearStartdatumBriefadres() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureStartdatumBriefadres() => $_ensure(3);

  @$pb.TagNumber(5)
  Aangifte get aangifte => $_getN(4);
  @$pb.TagNumber(5)
  set aangifte(Aangifte v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasAangifte() => $_has(4);
  @$pb.TagNumber(5)
  void clearAangifte() => clearField(5);
}

class VerhuizingInNLGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerhuizingInNLGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verwijzingWoonadresBAG', protoName: 'verwijzing_woonadres_BAG')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeente')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVerhuisd', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  VerhuizingInNLGeregistreerd._() : super();
  factory VerhuizingInNLGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? verwijzingWoonadresBAG,
    $core.String? gemeente,
    $0.Timestamp? datumVerhuisd,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verwijzingWoonadresBAG != null) {
      _result.verwijzingWoonadresBAG = verwijzingWoonadresBAG;
    }
    if (gemeente != null) {
      _result.gemeente = gemeente;
    }
    if (datumVerhuisd != null) {
      _result.datumVerhuisd = datumVerhuisd;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory VerhuizingInNLGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerhuizingInNLGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerhuizingInNLGeregistreerd clone() => VerhuizingInNLGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerhuizingInNLGeregistreerd copyWith(void Function(VerhuizingInNLGeregistreerd) updates) => super.copyWith((message) => updates(message as VerhuizingInNLGeregistreerd)) as VerhuizingInNLGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerhuizingInNLGeregistreerd create() => VerhuizingInNLGeregistreerd._();
  VerhuizingInNLGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<VerhuizingInNLGeregistreerd> createRepeated() => $pb.PbList<VerhuizingInNLGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static VerhuizingInNLGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerhuizingInNLGeregistreerd>(create);
  static VerhuizingInNLGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get verwijzingWoonadresBAG => $_getSZ(1);
  @$pb.TagNumber(2)
  set verwijzingWoonadresBAG($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerwijzingWoonadresBAG() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerwijzingWoonadresBAG() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get gemeente => $_getSZ(2);
  @$pb.TagNumber(3)
  set gemeente($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGemeente() => $_has(2);
  @$pb.TagNumber(3)
  void clearGemeente() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get datumVerhuisd => $_getN(3);
  @$pb.TagNumber(4)
  set datumVerhuisd($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatumVerhuisd() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatumVerhuisd() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDatumVerhuisd() => $_ensure(3);

  @$pb.TagNumber(5)
  Aangifte get aangifte => $_getN(4);
  @$pb.TagNumber(5)
  set aangifte(Aangifte v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasAangifte() => $_has(4);
  @$pb.TagNumber(5)
  void clearAangifte() => clearField(5);
}

class TijdelijkAdresNLGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TijdelijkAdresNLGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verwijzingWoonadresBAG', protoName: 'verwijzing_woonadres_BAG')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeente')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumTijdelijkAdres', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  TijdelijkAdresNLGeregistreerd._() : super();
  factory TijdelijkAdresNLGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? verwijzingWoonadresBAG,
    $core.String? gemeente,
    $0.Timestamp? startdatumTijdelijkAdres,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verwijzingWoonadresBAG != null) {
      _result.verwijzingWoonadresBAG = verwijzingWoonadresBAG;
    }
    if (gemeente != null) {
      _result.gemeente = gemeente;
    }
    if (startdatumTijdelijkAdres != null) {
      _result.startdatumTijdelijkAdres = startdatumTijdelijkAdres;
    }
    return _result;
  }
  factory TijdelijkAdresNLGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TijdelijkAdresNLGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLGeregistreerd clone() => TijdelijkAdresNLGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLGeregistreerd copyWith(void Function(TijdelijkAdresNLGeregistreerd) updates) => super.copyWith((message) => updates(message as TijdelijkAdresNLGeregistreerd)) as TijdelijkAdresNLGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLGeregistreerd create() => TijdelijkAdresNLGeregistreerd._();
  TijdelijkAdresNLGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<TijdelijkAdresNLGeregistreerd> createRepeated() => $pb.PbList<TijdelijkAdresNLGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TijdelijkAdresNLGeregistreerd>(create);
  static TijdelijkAdresNLGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get verwijzingWoonadresBAG => $_getSZ(1);
  @$pb.TagNumber(2)
  set verwijzingWoonadresBAG($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerwijzingWoonadresBAG() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerwijzingWoonadresBAG() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get gemeente => $_getSZ(2);
  @$pb.TagNumber(3)
  set gemeente($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGemeente() => $_has(2);
  @$pb.TagNumber(3)
  void clearGemeente() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get startdatumTijdelijkAdres => $_getN(3);
  @$pb.TagNumber(4)
  set startdatumTijdelijkAdres($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStartdatumTijdelijkAdres() => $_has(3);
  @$pb.TagNumber(4)
  void clearStartdatumTijdelijkAdres() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureStartdatumTijdelijkAdres() => $_ensure(3);
}

class TijdelijkAdresNLGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TijdelijkAdresNLGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verwijzingWoonadresBAG', protoName: 'verwijzing_woonadres_BAG')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeente')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumTijdelijkAdresGewijzigd', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  TijdelijkAdresNLGewijzigd._() : super();
  factory TijdelijkAdresNLGewijzigd({
    $core.List<$core.int>? persoonPr,
    $core.String? verwijzingWoonadresBAG,
    $core.String? gemeente,
    $0.Timestamp? datumTijdelijkAdresGewijzigd,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verwijzingWoonadresBAG != null) {
      _result.verwijzingWoonadresBAG = verwijzingWoonadresBAG;
    }
    if (gemeente != null) {
      _result.gemeente = gemeente;
    }
    if (datumTijdelijkAdresGewijzigd != null) {
      _result.datumTijdelijkAdresGewijzigd = datumTijdelijkAdresGewijzigd;
    }
    return _result;
  }
  factory TijdelijkAdresNLGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TijdelijkAdresNLGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLGewijzigd clone() => TijdelijkAdresNLGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLGewijzigd copyWith(void Function(TijdelijkAdresNLGewijzigd) updates) => super.copyWith((message) => updates(message as TijdelijkAdresNLGewijzigd)) as TijdelijkAdresNLGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLGewijzigd create() => TijdelijkAdresNLGewijzigd._();
  TijdelijkAdresNLGewijzigd createEmptyInstance() => create();
  static $pb.PbList<TijdelijkAdresNLGewijzigd> createRepeated() => $pb.PbList<TijdelijkAdresNLGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TijdelijkAdresNLGewijzigd>(create);
  static TijdelijkAdresNLGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get verwijzingWoonadresBAG => $_getSZ(1);
  @$pb.TagNumber(2)
  set verwijzingWoonadresBAG($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerwijzingWoonadresBAG() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerwijzingWoonadresBAG() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get gemeente => $_getSZ(2);
  @$pb.TagNumber(3)
  set gemeente($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGemeente() => $_has(2);
  @$pb.TagNumber(3)
  void clearGemeente() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get datumTijdelijkAdresGewijzigd => $_getN(3);
  @$pb.TagNumber(4)
  set datumTijdelijkAdresGewijzigd($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatumTijdelijkAdresGewijzigd() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatumTijdelijkAdresGewijzigd() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDatumTijdelijkAdresGewijzigd() => $_ensure(3);
}

class TijdelijkAdresNLBeeindigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TijdelijkAdresNLBeeindigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'einddatumTijdelijkAdres', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  TijdelijkAdresNLBeeindigd._() : super();
  factory TijdelijkAdresNLBeeindigd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? einddatumTijdelijkAdres,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (einddatumTijdelijkAdres != null) {
      _result.einddatumTijdelijkAdres = einddatumTijdelijkAdres;
    }
    return _result;
  }
  factory TijdelijkAdresNLBeeindigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TijdelijkAdresNLBeeindigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLBeeindigd clone() => TijdelijkAdresNLBeeindigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TijdelijkAdresNLBeeindigd copyWith(void Function(TijdelijkAdresNLBeeindigd) updates) => super.copyWith((message) => updates(message as TijdelijkAdresNLBeeindigd)) as TijdelijkAdresNLBeeindigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLBeeindigd create() => TijdelijkAdresNLBeeindigd._();
  TijdelijkAdresNLBeeindigd createEmptyInstance() => create();
  static $pb.PbList<TijdelijkAdresNLBeeindigd> createRepeated() => $pb.PbList<TijdelijkAdresNLBeeindigd>();
  @$core.pragma('dart2js:noInline')
  static TijdelijkAdresNLBeeindigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TijdelijkAdresNLBeeindigd>(create);
  static TijdelijkAdresNLBeeindigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get einddatumTijdelijkAdres => $_getN(1);
  @$pb.TagNumber(2)
  set einddatumTijdelijkAdres($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEinddatumTijdelijkAdres() => $_has(1);
  @$pb.TagNumber(2)
  void clearEinddatumTijdelijkAdres() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureEinddatumTijdelijkAdres() => $_ensure(1);
}

class EmigratieGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EmigratieGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landVanEmigratie')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumGeemigreerd', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  EmigratieGeregistreerd._() : super();
  factory EmigratieGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? landVanEmigratie,
    $0.Timestamp? datumGeemigreerd,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (landVanEmigratie != null) {
      _result.landVanEmigratie = landVanEmigratie;
    }
    if (datumGeemigreerd != null) {
      _result.datumGeemigreerd = datumGeemigreerd;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory EmigratieGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EmigratieGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EmigratieGeregistreerd clone() => EmigratieGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EmigratieGeregistreerd copyWith(void Function(EmigratieGeregistreerd) updates) => super.copyWith((message) => updates(message as EmigratieGeregistreerd)) as EmigratieGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EmigratieGeregistreerd create() => EmigratieGeregistreerd._();
  EmigratieGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<EmigratieGeregistreerd> createRepeated() => $pb.PbList<EmigratieGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static EmigratieGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EmigratieGeregistreerd>(create);
  static EmigratieGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get landVanEmigratie => $_getSZ(1);
  @$pb.TagNumber(2)
  set landVanEmigratie($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLandVanEmigratie() => $_has(1);
  @$pb.TagNumber(2)
  void clearLandVanEmigratie() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumGeemigreerd => $_getN(2);
  @$pb.TagNumber(3)
  set datumGeemigreerd($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumGeemigreerd() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumGeemigreerd() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumGeemigreerd() => $_ensure(2);

  @$pb.TagNumber(4)
  Aangifte get aangifte => $_getN(3);
  @$pb.TagNumber(4)
  set aangifte(Aangifte v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasAangifte() => $_has(3);
  @$pb.TagNumber(4)
  void clearAangifte() => clearField(4);
}

class BuitenlandsWoonadresGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BuitenlandsWoonadresGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buitenlandsWoonadres')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumBuitenlandsWoonadres', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  BuitenlandsWoonadresGeregistreerd._() : super();
  factory BuitenlandsWoonadresGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? buitenlandsWoonadres,
    $0.Timestamp? startdatumBuitenlandsWoonadres,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (buitenlandsWoonadres != null) {
      _result.buitenlandsWoonadres = buitenlandsWoonadres;
    }
    if (startdatumBuitenlandsWoonadres != null) {
      _result.startdatumBuitenlandsWoonadres = startdatumBuitenlandsWoonadres;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory BuitenlandsWoonadresGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BuitenlandsWoonadresGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BuitenlandsWoonadresGeregistreerd clone() => BuitenlandsWoonadresGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BuitenlandsWoonadresGeregistreerd copyWith(void Function(BuitenlandsWoonadresGeregistreerd) updates) => super.copyWith((message) => updates(message as BuitenlandsWoonadresGeregistreerd)) as BuitenlandsWoonadresGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BuitenlandsWoonadresGeregistreerd create() => BuitenlandsWoonadresGeregistreerd._();
  BuitenlandsWoonadresGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<BuitenlandsWoonadresGeregistreerd> createRepeated() => $pb.PbList<BuitenlandsWoonadresGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static BuitenlandsWoonadresGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BuitenlandsWoonadresGeregistreerd>(create);
  static BuitenlandsWoonadresGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get buitenlandsWoonadres => $_getSZ(1);
  @$pb.TagNumber(2)
  set buitenlandsWoonadres($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBuitenlandsWoonadres() => $_has(1);
  @$pb.TagNumber(2)
  void clearBuitenlandsWoonadres() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get startdatumBuitenlandsWoonadres => $_getN(2);
  @$pb.TagNumber(3)
  set startdatumBuitenlandsWoonadres($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartdatumBuitenlandsWoonadres() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartdatumBuitenlandsWoonadres() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureStartdatumBuitenlandsWoonadres() => $_ensure(2);

  @$pb.TagNumber(4)
  Aangifte get aangifte => $_getN(3);
  @$pb.TagNumber(4)
  set aangifte(Aangifte v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasAangifte() => $_has(3);
  @$pb.TagNumber(4)
  void clearAangifte() => clearField(4);
}

class BuitenlandsWoonadresGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BuitenlandsWoonadresGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buitenlandsWoonadres')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumBuitenlandsWoonadresGewijzigd', subBuilder: $0.Timestamp.create)
    ..e<Aangifte>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'aangifte', $pb.PbFieldType.OE, defaultOrMaker: Aangifte.AANGIFTE_Ingeschrevene, valueOf: Aangifte.valueOf, enumValues: Aangifte.values)
    ..hasRequiredFields = false
  ;

  BuitenlandsWoonadresGewijzigd._() : super();
  factory BuitenlandsWoonadresGewijzigd({
    $core.List<$core.int>? persoonPr,
    $core.String? buitenlandsWoonadres,
    $0.Timestamp? datumBuitenlandsWoonadresGewijzigd,
    Aangifte? aangifte,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (buitenlandsWoonadres != null) {
      _result.buitenlandsWoonadres = buitenlandsWoonadres;
    }
    if (datumBuitenlandsWoonadresGewijzigd != null) {
      _result.datumBuitenlandsWoonadresGewijzigd = datumBuitenlandsWoonadresGewijzigd;
    }
    if (aangifte != null) {
      _result.aangifte = aangifte;
    }
    return _result;
  }
  factory BuitenlandsWoonadresGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BuitenlandsWoonadresGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BuitenlandsWoonadresGewijzigd clone() => BuitenlandsWoonadresGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BuitenlandsWoonadresGewijzigd copyWith(void Function(BuitenlandsWoonadresGewijzigd) updates) => super.copyWith((message) => updates(message as BuitenlandsWoonadresGewijzigd)) as BuitenlandsWoonadresGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BuitenlandsWoonadresGewijzigd create() => BuitenlandsWoonadresGewijzigd._();
  BuitenlandsWoonadresGewijzigd createEmptyInstance() => create();
  static $pb.PbList<BuitenlandsWoonadresGewijzigd> createRepeated() => $pb.PbList<BuitenlandsWoonadresGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static BuitenlandsWoonadresGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BuitenlandsWoonadresGewijzigd>(create);
  static BuitenlandsWoonadresGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get buitenlandsWoonadres => $_getSZ(1);
  @$pb.TagNumber(2)
  set buitenlandsWoonadres($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBuitenlandsWoonadres() => $_has(1);
  @$pb.TagNumber(2)
  void clearBuitenlandsWoonadres() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumBuitenlandsWoonadresGewijzigd => $_getN(2);
  @$pb.TagNumber(3)
  set datumBuitenlandsWoonadresGewijzigd($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumBuitenlandsWoonadresGewijzigd() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumBuitenlandsWoonadresGewijzigd() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumBuitenlandsWoonadresGewijzigd() => $_ensure(2);

  @$pb.TagNumber(4)
  Aangifte get aangifte => $_getN(3);
  @$pb.TagNumber(4)
  set aangifte(Aangifte v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasAangifte() => $_has(3);
  @$pb.TagNumber(4)
  void clearAangifte() => clearField(4);
}

class OnderzoekWoonadresGestart extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnderzoekWoonadresGestart', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumOnderzoek', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  OnderzoekWoonadresGestart._() : super();
  factory OnderzoekWoonadresGestart({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? startdatumOnderzoek,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (startdatumOnderzoek != null) {
      _result.startdatumOnderzoek = startdatumOnderzoek;
    }
    return _result;
  }
  factory OnderzoekWoonadresGestart.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnderzoekWoonadresGestart.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnderzoekWoonadresGestart clone() => OnderzoekWoonadresGestart()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnderzoekWoonadresGestart copyWith(void Function(OnderzoekWoonadresGestart) updates) => super.copyWith((message) => updates(message as OnderzoekWoonadresGestart)) as OnderzoekWoonadresGestart; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnderzoekWoonadresGestart create() => OnderzoekWoonadresGestart._();
  OnderzoekWoonadresGestart createEmptyInstance() => create();
  static $pb.PbList<OnderzoekWoonadresGestart> createRepeated() => $pb.PbList<OnderzoekWoonadresGestart>();
  @$core.pragma('dart2js:noInline')
  static OnderzoekWoonadresGestart getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnderzoekWoonadresGestart>(create);
  static OnderzoekWoonadresGestart? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get startdatumOnderzoek => $_getN(1);
  @$pb.TagNumber(2)
  set startdatumOnderzoek($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasStartdatumOnderzoek() => $_has(1);
  @$pb.TagNumber(2)
  void clearStartdatumOnderzoek() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureStartdatumOnderzoek() => $_ensure(1);
}

class OnderzoekWoonadresBeeindigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OnderzoekWoonadresBeeindigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'einddatumOnderzoek', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  OnderzoekWoonadresBeeindigd._() : super();
  factory OnderzoekWoonadresBeeindigd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? einddatumOnderzoek,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (einddatumOnderzoek != null) {
      _result.einddatumOnderzoek = einddatumOnderzoek;
    }
    return _result;
  }
  factory OnderzoekWoonadresBeeindigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OnderzoekWoonadresBeeindigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OnderzoekWoonadresBeeindigd clone() => OnderzoekWoonadresBeeindigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OnderzoekWoonadresBeeindigd copyWith(void Function(OnderzoekWoonadresBeeindigd) updates) => super.copyWith((message) => updates(message as OnderzoekWoonadresBeeindigd)) as OnderzoekWoonadresBeeindigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OnderzoekWoonadresBeeindigd create() => OnderzoekWoonadresBeeindigd._();
  OnderzoekWoonadresBeeindigd createEmptyInstance() => create();
  static $pb.PbList<OnderzoekWoonadresBeeindigd> createRepeated() => $pb.PbList<OnderzoekWoonadresBeeindigd>();
  @$core.pragma('dart2js:noInline')
  static OnderzoekWoonadresBeeindigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OnderzoekWoonadresBeeindigd>(create);
  static OnderzoekWoonadresBeeindigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get einddatumOnderzoek => $_getN(1);
  @$pb.TagNumber(2)
  set einddatumOnderzoek($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEinddatumOnderzoek() => $_has(1);
  @$pb.TagNumber(2)
  void clearEinddatumOnderzoek() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureEinddatumOnderzoek() => $_ensure(1);
}

class VertrekOnbekendWaarheenGeregsitreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VertrekOnbekendWaarheenGeregsitreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVertrekOnbekendWaarheen', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  VertrekOnbekendWaarheenGeregsitreerd._() : super();
  factory VertrekOnbekendWaarheenGeregsitreerd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? datumVertrekOnbekendWaarheen,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (datumVertrekOnbekendWaarheen != null) {
      _result.datumVertrekOnbekendWaarheen = datumVertrekOnbekendWaarheen;
    }
    return _result;
  }
  factory VertrekOnbekendWaarheenGeregsitreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VertrekOnbekendWaarheenGeregsitreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VertrekOnbekendWaarheenGeregsitreerd clone() => VertrekOnbekendWaarheenGeregsitreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VertrekOnbekendWaarheenGeregsitreerd copyWith(void Function(VertrekOnbekendWaarheenGeregsitreerd) updates) => super.copyWith((message) => updates(message as VertrekOnbekendWaarheenGeregsitreerd)) as VertrekOnbekendWaarheenGeregsitreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VertrekOnbekendWaarheenGeregsitreerd create() => VertrekOnbekendWaarheenGeregsitreerd._();
  VertrekOnbekendWaarheenGeregsitreerd createEmptyInstance() => create();
  static $pb.PbList<VertrekOnbekendWaarheenGeregsitreerd> createRepeated() => $pb.PbList<VertrekOnbekendWaarheenGeregsitreerd>();
  @$core.pragma('dart2js:noInline')
  static VertrekOnbekendWaarheenGeregsitreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VertrekOnbekendWaarheenGeregsitreerd>(create);
  static VertrekOnbekendWaarheenGeregsitreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get datumVertrekOnbekendWaarheen => $_getN(1);
  @$pb.TagNumber(2)
  set datumVertrekOnbekendWaarheen($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDatumVertrekOnbekendWaarheen() => $_has(1);
  @$pb.TagNumber(2)
  void clearDatumVertrekOnbekendWaarheen() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureDatumVertrekOnbekendWaarheen() => $_ensure(1);
}

class NationaliteitsgegevensGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NationaliteitsgegevensGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Nationaliteitskenmerk>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nationaliteitskenmerk', $pb.PbFieldType.OE, defaultOrMaker: Nationaliteitskenmerk.NATIONALITEITSKENMERK_Nog_niet_bekend, valueOf: Nationaliteitskenmerk.valueOf, enumValues: Nationaliteitskenmerk.values)
    ..pPS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buitenlandseNationaliteit')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EUPersoonsnummer', protoName: 'EU_persoonsnummer')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumNationaliteitsgegevens', subBuilder: $0.Timestamp.create)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redenOpnemenBeeindigenNationaliteitsgegevens')
    ..hasRequiredFields = false
  ;

  NationaliteitsgegevensGeregistreerd._() : super();
  factory NationaliteitsgegevensGeregistreerd({
    $core.List<$core.int>? persoonPr,
    Nationaliteitskenmerk? nationaliteitskenmerk,
    $core.Iterable<$core.String>? buitenlandseNationaliteit,
    $core.String? eUPersoonsnummer,
    $0.Timestamp? ingangsdatumNationaliteitsgegevens,
    $core.String? redenOpnemenBeeindigenNationaliteitsgegevens,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (nationaliteitskenmerk != null) {
      _result.nationaliteitskenmerk = nationaliteitskenmerk;
    }
    if (buitenlandseNationaliteit != null) {
      _result.buitenlandseNationaliteit.addAll(buitenlandseNationaliteit);
    }
    if (eUPersoonsnummer != null) {
      _result.eUPersoonsnummer = eUPersoonsnummer;
    }
    if (ingangsdatumNationaliteitsgegevens != null) {
      _result.ingangsdatumNationaliteitsgegevens = ingangsdatumNationaliteitsgegevens;
    }
    if (redenOpnemenBeeindigenNationaliteitsgegevens != null) {
      _result.redenOpnemenBeeindigenNationaliteitsgegevens = redenOpnemenBeeindigenNationaliteitsgegevens;
    }
    return _result;
  }
  factory NationaliteitsgegevensGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NationaliteitsgegevensGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NationaliteitsgegevensGeregistreerd clone() => NationaliteitsgegevensGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NationaliteitsgegevensGeregistreerd copyWith(void Function(NationaliteitsgegevensGeregistreerd) updates) => super.copyWith((message) => updates(message as NationaliteitsgegevensGeregistreerd)) as NationaliteitsgegevensGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NationaliteitsgegevensGeregistreerd create() => NationaliteitsgegevensGeregistreerd._();
  NationaliteitsgegevensGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<NationaliteitsgegevensGeregistreerd> createRepeated() => $pb.PbList<NationaliteitsgegevensGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static NationaliteitsgegevensGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NationaliteitsgegevensGeregistreerd>(create);
  static NationaliteitsgegevensGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Nationaliteitskenmerk get nationaliteitskenmerk => $_getN(1);
  @$pb.TagNumber(2)
  set nationaliteitskenmerk(Nationaliteitskenmerk v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasNationaliteitskenmerk() => $_has(1);
  @$pb.TagNumber(2)
  void clearNationaliteitskenmerk() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.String> get buitenlandseNationaliteit => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get eUPersoonsnummer => $_getSZ(3);
  @$pb.TagNumber(4)
  set eUPersoonsnummer($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEUPersoonsnummer() => $_has(3);
  @$pb.TagNumber(4)
  void clearEUPersoonsnummer() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get ingangsdatumNationaliteitsgegevens => $_getN(4);
  @$pb.TagNumber(5)
  set ingangsdatumNationaliteitsgegevens($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasIngangsdatumNationaliteitsgegevens() => $_has(4);
  @$pb.TagNumber(5)
  void clearIngangsdatumNationaliteitsgegevens() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureIngangsdatumNationaliteitsgegevens() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.String get redenOpnemenBeeindigenNationaliteitsgegevens => $_getSZ(5);
  @$pb.TagNumber(6)
  set redenOpnemenBeeindigenNationaliteitsgegevens($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasRedenOpnemenBeeindigenNationaliteitsgegevens() => $_has(5);
  @$pb.TagNumber(6)
  void clearRedenOpnemenBeeindigenNationaliteitsgegevens() => clearField(6);
}

class NationaliteitsgegevensGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NationaliteitsgegevensGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Nationaliteitskenmerk>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nationaliteitskenmerk', $pb.PbFieldType.OE, defaultOrMaker: Nationaliteitskenmerk.NATIONALITEITSKENMERK_Nog_niet_bekend, valueOf: Nationaliteitskenmerk.valueOf, enumValues: Nationaliteitskenmerk.values)
    ..pPS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'buitenlandseNationaliteit')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EUPersoonsnummer', protoName: 'EU_persoonsnummer')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumNationaliteitsgegevensGewijzigd', subBuilder: $0.Timestamp.create)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redenOpnemenBeeindigenNationaliteitsgegevens')
    ..hasRequiredFields = false
  ;

  NationaliteitsgegevensGewijzigd._() : super();
  factory NationaliteitsgegevensGewijzigd({
    $core.List<$core.int>? persoonPr,
    Nationaliteitskenmerk? nationaliteitskenmerk,
    $core.Iterable<$core.String>? buitenlandseNationaliteit,
    $core.String? eUPersoonsnummer,
    $0.Timestamp? datumNationaliteitsgegevensGewijzigd,
    $core.String? redenOpnemenBeeindigenNationaliteitsgegevens,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (nationaliteitskenmerk != null) {
      _result.nationaliteitskenmerk = nationaliteitskenmerk;
    }
    if (buitenlandseNationaliteit != null) {
      _result.buitenlandseNationaliteit.addAll(buitenlandseNationaliteit);
    }
    if (eUPersoonsnummer != null) {
      _result.eUPersoonsnummer = eUPersoonsnummer;
    }
    if (datumNationaliteitsgegevensGewijzigd != null) {
      _result.datumNationaliteitsgegevensGewijzigd = datumNationaliteitsgegevensGewijzigd;
    }
    if (redenOpnemenBeeindigenNationaliteitsgegevens != null) {
      _result.redenOpnemenBeeindigenNationaliteitsgegevens = redenOpnemenBeeindigenNationaliteitsgegevens;
    }
    return _result;
  }
  factory NationaliteitsgegevensGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NationaliteitsgegevensGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NationaliteitsgegevensGewijzigd clone() => NationaliteitsgegevensGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NationaliteitsgegevensGewijzigd copyWith(void Function(NationaliteitsgegevensGewijzigd) updates) => super.copyWith((message) => updates(message as NationaliteitsgegevensGewijzigd)) as NationaliteitsgegevensGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NationaliteitsgegevensGewijzigd create() => NationaliteitsgegevensGewijzigd._();
  NationaliteitsgegevensGewijzigd createEmptyInstance() => create();
  static $pb.PbList<NationaliteitsgegevensGewijzigd> createRepeated() => $pb.PbList<NationaliteitsgegevensGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static NationaliteitsgegevensGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NationaliteitsgegevensGewijzigd>(create);
  static NationaliteitsgegevensGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Nationaliteitskenmerk get nationaliteitskenmerk => $_getN(1);
  @$pb.TagNumber(2)
  set nationaliteitskenmerk(Nationaliteitskenmerk v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasNationaliteitskenmerk() => $_has(1);
  @$pb.TagNumber(2)
  void clearNationaliteitskenmerk() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.String> get buitenlandseNationaliteit => $_getList(2);

  @$pb.TagNumber(4)
  $core.String get eUPersoonsnummer => $_getSZ(3);
  @$pb.TagNumber(4)
  set eUPersoonsnummer($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEUPersoonsnummer() => $_has(3);
  @$pb.TagNumber(4)
  void clearEUPersoonsnummer() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get datumNationaliteitsgegevensGewijzigd => $_getN(4);
  @$pb.TagNumber(5)
  set datumNationaliteitsgegevensGewijzigd($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDatumNationaliteitsgegevensGewijzigd() => $_has(4);
  @$pb.TagNumber(5)
  void clearDatumNationaliteitsgegevensGewijzigd() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureDatumNationaliteitsgegevensGewijzigd() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.String get redenOpnemenBeeindigenNationaliteitsgegevens => $_getSZ(5);
  @$pb.TagNumber(6)
  set redenOpnemenBeeindigenNationaliteitsgegevens($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasRedenOpnemenBeeindigenNationaliteitsgegevens() => $_has(5);
  @$pb.TagNumber(6)
  void clearRedenOpnemenBeeindigenNationaliteitsgegevens() => clearField(6);
}

class VerblijfstitelGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerblijfstitelGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'toegekendeVerblijfstitel')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumVerblijfstitel', subBuilder: $0.Timestamp.create)
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eindddatumVerblijfstitel', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  VerblijfstitelGeregistreerd._() : super();
  factory VerblijfstitelGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? toegekendeVerblijfstitel,
    $0.Timestamp? ingangsdatumVerblijfstitel,
    $0.Timestamp? eindddatumVerblijfstitel,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (toegekendeVerblijfstitel != null) {
      _result.toegekendeVerblijfstitel = toegekendeVerblijfstitel;
    }
    if (ingangsdatumVerblijfstitel != null) {
      _result.ingangsdatumVerblijfstitel = ingangsdatumVerblijfstitel;
    }
    if (eindddatumVerblijfstitel != null) {
      _result.eindddatumVerblijfstitel = eindddatumVerblijfstitel;
    }
    return _result;
  }
  factory VerblijfstitelGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerblijfstitelGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerblijfstitelGeregistreerd clone() => VerblijfstitelGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerblijfstitelGeregistreerd copyWith(void Function(VerblijfstitelGeregistreerd) updates) => super.copyWith((message) => updates(message as VerblijfstitelGeregistreerd)) as VerblijfstitelGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerblijfstitelGeregistreerd create() => VerblijfstitelGeregistreerd._();
  VerblijfstitelGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<VerblijfstitelGeregistreerd> createRepeated() => $pb.PbList<VerblijfstitelGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static VerblijfstitelGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerblijfstitelGeregistreerd>(create);
  static VerblijfstitelGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get toegekendeVerblijfstitel => $_getSZ(1);
  @$pb.TagNumber(2)
  set toegekendeVerblijfstitel($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToegekendeVerblijfstitel() => $_has(1);
  @$pb.TagNumber(2)
  void clearToegekendeVerblijfstitel() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumVerblijfstitel => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumVerblijfstitel($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumVerblijfstitel() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumVerblijfstitel() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumVerblijfstitel() => $_ensure(2);

  @$pb.TagNumber(4)
  $0.Timestamp get eindddatumVerblijfstitel => $_getN(3);
  @$pb.TagNumber(4)
  set eindddatumVerblijfstitel($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasEindddatumVerblijfstitel() => $_has(3);
  @$pb.TagNumber(4)
  void clearEindddatumVerblijfstitel() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureEindddatumVerblijfstitel() => $_ensure(3);
}

class VerblijfstitelGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerblijfstitelGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'toegekendeVerblijfstitel')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumVerblijfstitel', subBuilder: $0.Timestamp.create)
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVerblijfstitelGewijzigd', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  VerblijfstitelGewijzigd._() : super();
  factory VerblijfstitelGewijzigd({
    $core.List<$core.int>? persoonPr,
    $core.String? toegekendeVerblijfstitel,
    $0.Timestamp? ingangsdatumVerblijfstitel,
    $0.Timestamp? datumVerblijfstitelGewijzigd,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (toegekendeVerblijfstitel != null) {
      _result.toegekendeVerblijfstitel = toegekendeVerblijfstitel;
    }
    if (ingangsdatumVerblijfstitel != null) {
      _result.ingangsdatumVerblijfstitel = ingangsdatumVerblijfstitel;
    }
    if (datumVerblijfstitelGewijzigd != null) {
      _result.datumVerblijfstitelGewijzigd = datumVerblijfstitelGewijzigd;
    }
    return _result;
  }
  factory VerblijfstitelGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerblijfstitelGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerblijfstitelGewijzigd clone() => VerblijfstitelGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerblijfstitelGewijzigd copyWith(void Function(VerblijfstitelGewijzigd) updates) => super.copyWith((message) => updates(message as VerblijfstitelGewijzigd)) as VerblijfstitelGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerblijfstitelGewijzigd create() => VerblijfstitelGewijzigd._();
  VerblijfstitelGewijzigd createEmptyInstance() => create();
  static $pb.PbList<VerblijfstitelGewijzigd> createRepeated() => $pb.PbList<VerblijfstitelGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static VerblijfstitelGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerblijfstitelGewijzigd>(create);
  static VerblijfstitelGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get toegekendeVerblijfstitel => $_getSZ(1);
  @$pb.TagNumber(2)
  set toegekendeVerblijfstitel($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToegekendeVerblijfstitel() => $_has(1);
  @$pb.TagNumber(2)
  void clearToegekendeVerblijfstitel() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumVerblijfstitel => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumVerblijfstitel($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumVerblijfstitel() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumVerblijfstitel() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumVerblijfstitel() => $_ensure(2);

  @$pb.TagNumber(4)
  $0.Timestamp get datumVerblijfstitelGewijzigd => $_getN(3);
  @$pb.TagNumber(4)
  set datumVerblijfstitelGewijzigd($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatumVerblijfstitelGewijzigd() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatumVerblijfstitelGewijzigd() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDatumVerblijfstitelGewijzigd() => $_ensure(3);
}

class BijzonderVerblijfsrechtGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BijzonderVerblijfsrechtGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumBijzonderVerblijfsrecht', subBuilder: $0.Timestamp.create)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eindddatumBijzonderVerblijfsrecht', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  BijzonderVerblijfsrechtGeregistreerd._() : super();
  factory BijzonderVerblijfsrechtGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? ingangsdatumBijzonderVerblijfsrecht,
    $0.Timestamp? eindddatumBijzonderVerblijfsrecht,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (ingangsdatumBijzonderVerblijfsrecht != null) {
      _result.ingangsdatumBijzonderVerblijfsrecht = ingangsdatumBijzonderVerblijfsrecht;
    }
    if (eindddatumBijzonderVerblijfsrecht != null) {
      _result.eindddatumBijzonderVerblijfsrecht = eindddatumBijzonderVerblijfsrecht;
    }
    return _result;
  }
  factory BijzonderVerblijfsrechtGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BijzonderVerblijfsrechtGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BijzonderVerblijfsrechtGeregistreerd clone() => BijzonderVerblijfsrechtGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BijzonderVerblijfsrechtGeregistreerd copyWith(void Function(BijzonderVerblijfsrechtGeregistreerd) updates) => super.copyWith((message) => updates(message as BijzonderVerblijfsrechtGeregistreerd)) as BijzonderVerblijfsrechtGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BijzonderVerblijfsrechtGeregistreerd create() => BijzonderVerblijfsrechtGeregistreerd._();
  BijzonderVerblijfsrechtGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<BijzonderVerblijfsrechtGeregistreerd> createRepeated() => $pb.PbList<BijzonderVerblijfsrechtGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static BijzonderVerblijfsrechtGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BijzonderVerblijfsrechtGeregistreerd>(create);
  static BijzonderVerblijfsrechtGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get ingangsdatumBijzonderVerblijfsrecht => $_getN(1);
  @$pb.TagNumber(2)
  set ingangsdatumBijzonderVerblijfsrecht($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasIngangsdatumBijzonderVerblijfsrecht() => $_has(1);
  @$pb.TagNumber(2)
  void clearIngangsdatumBijzonderVerblijfsrecht() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureIngangsdatumBijzonderVerblijfsrecht() => $_ensure(1);

  @$pb.TagNumber(3)
  $0.Timestamp get eindddatumBijzonderVerblijfsrecht => $_getN(2);
  @$pb.TagNumber(3)
  set eindddatumBijzonderVerblijfsrecht($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEindddatumBijzonderVerblijfsrecht() => $_has(2);
  @$pb.TagNumber(3)
  void clearEindddatumBijzonderVerblijfsrecht() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureEindddatumBijzonderVerblijfsrecht() => $_ensure(2);
}

class BijzonderVerblijfsrechtBeeindigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BijzonderVerblijfsrechtBeeindigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eindddatumBijzonderVerblijfsrecht', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  BijzonderVerblijfsrechtBeeindigd._() : super();
  factory BijzonderVerblijfsrechtBeeindigd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? eindddatumBijzonderVerblijfsrecht,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (eindddatumBijzonderVerblijfsrecht != null) {
      _result.eindddatumBijzonderVerblijfsrecht = eindddatumBijzonderVerblijfsrecht;
    }
    return _result;
  }
  factory BijzonderVerblijfsrechtBeeindigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BijzonderVerblijfsrechtBeeindigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BijzonderVerblijfsrechtBeeindigd clone() => BijzonderVerblijfsrechtBeeindigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BijzonderVerblijfsrechtBeeindigd copyWith(void Function(BijzonderVerblijfsrechtBeeindigd) updates) => super.copyWith((message) => updates(message as BijzonderVerblijfsrechtBeeindigd)) as BijzonderVerblijfsrechtBeeindigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BijzonderVerblijfsrechtBeeindigd create() => BijzonderVerblijfsrechtBeeindigd._();
  BijzonderVerblijfsrechtBeeindigd createEmptyInstance() => create();
  static $pb.PbList<BijzonderVerblijfsrechtBeeindigd> createRepeated() => $pb.PbList<BijzonderVerblijfsrechtBeeindigd>();
  @$core.pragma('dart2js:noInline')
  static BijzonderVerblijfsrechtBeeindigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BijzonderVerblijfsrechtBeeindigd>(create);
  static BijzonderVerblijfsrechtBeeindigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get eindddatumBijzonderVerblijfsrecht => $_getN(1);
  @$pb.TagNumber(2)
  set eindddatumBijzonderVerblijfsrecht($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEindddatumBijzonderVerblijfsrecht() => $_has(1);
  @$pb.TagNumber(2)
  void clearEindddatumBijzonderVerblijfsrecht() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureEindddatumBijzonderVerblijfsrecht() => $_ensure(1);
}

class GeboortegegevensGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GeboortegegevensGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kindPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'moederUitWiePr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteDatum', subBuilder: $0.Timestamp.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboortePlaats')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteGemeente')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geboorteLand')
    ..hasRequiredFields = false
  ;

  GeboortegegevensGeregistreerd._() : super();
  factory GeboortegegevensGeregistreerd({
    $core.List<$core.int>? kindPr,
    $core.List<$core.int>? moederUitWiePr,
    $0.Timestamp? geboorteDatum,
    $core.String? geboortePlaats,
    $core.String? geboorteGemeente,
    $core.String? geboorteLand,
  }) {
    final _result = create();
    if (kindPr != null) {
      _result.kindPr = kindPr;
    }
    if (moederUitWiePr != null) {
      _result.moederUitWiePr = moederUitWiePr;
    }
    if (geboorteDatum != null) {
      _result.geboorteDatum = geboorteDatum;
    }
    if (geboortePlaats != null) {
      _result.geboortePlaats = geboortePlaats;
    }
    if (geboorteGemeente != null) {
      _result.geboorteGemeente = geboorteGemeente;
    }
    if (geboorteLand != null) {
      _result.geboorteLand = geboorteLand;
    }
    return _result;
  }
  factory GeboortegegevensGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GeboortegegevensGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GeboortegegevensGeregistreerd clone() => GeboortegegevensGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GeboortegegevensGeregistreerd copyWith(void Function(GeboortegegevensGeregistreerd) updates) => super.copyWith((message) => updates(message as GeboortegegevensGeregistreerd)) as GeboortegegevensGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GeboortegegevensGeregistreerd create() => GeboortegegevensGeregistreerd._();
  GeboortegegevensGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<GeboortegegevensGeregistreerd> createRepeated() => $pb.PbList<GeboortegegevensGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static GeboortegegevensGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GeboortegegevensGeregistreerd>(create);
  static GeboortegegevensGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get kindPr => $_getN(0);
  @$pb.TagNumber(1)
  set kindPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKindPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearKindPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get moederUitWiePr => $_getN(1);
  @$pb.TagNumber(2)
  set moederUitWiePr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMoederUitWiePr() => $_has(1);
  @$pb.TagNumber(2)
  void clearMoederUitWiePr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get geboorteDatum => $_getN(2);
  @$pb.TagNumber(3)
  set geboorteDatum($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasGeboorteDatum() => $_has(2);
  @$pb.TagNumber(3)
  void clearGeboorteDatum() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureGeboorteDatum() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get geboortePlaats => $_getSZ(3);
  @$pb.TagNumber(4)
  set geboortePlaats($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGeboortePlaats() => $_has(3);
  @$pb.TagNumber(4)
  void clearGeboortePlaats() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get geboorteGemeente => $_getSZ(4);
  @$pb.TagNumber(5)
  set geboorteGemeente($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasGeboorteGemeente() => $_has(4);
  @$pb.TagNumber(5)
  void clearGeboorteGemeente() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get geboorteLand => $_getSZ(5);
  @$pb.TagNumber(6)
  set geboorteLand($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasGeboorteLand() => $_has(5);
  @$pb.TagNumber(6)
  void clearGeboorteLand() => clearField(6);
}

class NamenGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NamenGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voornamen')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voorvoegselGeslachtsnaam')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geslachtsnaam')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumNamen', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  NamenGeregistreerd._() : super();
  factory NamenGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.Iterable<$core.String>? voornamen,
    $core.String? voorvoegselGeslachtsnaam,
    $core.String? geslachtsnaam,
    $0.Timestamp? ingangsdatumNamen,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (voornamen != null) {
      _result.voornamen.addAll(voornamen);
    }
    if (voorvoegselGeslachtsnaam != null) {
      _result.voorvoegselGeslachtsnaam = voorvoegselGeslachtsnaam;
    }
    if (geslachtsnaam != null) {
      _result.geslachtsnaam = geslachtsnaam;
    }
    if (ingangsdatumNamen != null) {
      _result.ingangsdatumNamen = ingangsdatumNamen;
    }
    return _result;
  }
  factory NamenGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NamenGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NamenGeregistreerd clone() => NamenGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NamenGeregistreerd copyWith(void Function(NamenGeregistreerd) updates) => super.copyWith((message) => updates(message as NamenGeregistreerd)) as NamenGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NamenGeregistreerd create() => NamenGeregistreerd._();
  NamenGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<NamenGeregistreerd> createRepeated() => $pb.PbList<NamenGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static NamenGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NamenGeregistreerd>(create);
  static NamenGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get voornamen => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get voorvoegselGeslachtsnaam => $_getSZ(2);
  @$pb.TagNumber(3)
  set voorvoegselGeslachtsnaam($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasVoorvoegselGeslachtsnaam() => $_has(2);
  @$pb.TagNumber(3)
  void clearVoorvoegselGeslachtsnaam() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get geslachtsnaam => $_getSZ(3);
  @$pb.TagNumber(4)
  set geslachtsnaam($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGeslachtsnaam() => $_has(3);
  @$pb.TagNumber(4)
  void clearGeslachtsnaam() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get ingangsdatumNamen => $_getN(4);
  @$pb.TagNumber(5)
  set ingangsdatumNamen($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasIngangsdatumNamen() => $_has(4);
  @$pb.TagNumber(5)
  void clearIngangsdatumNamen() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureIngangsdatumNamen() => $_ensure(4);
}

class GeslachtsaanduidingGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GeslachtsaanduidingGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Geslachtsaanduiding>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geslachtsaanduiding', $pb.PbFieldType.OE, defaultOrMaker: Geslachtsaanduiding.GESLACHT_ONBEKEND, valueOf: Geslachtsaanduiding.valueOf, enumValues: Geslachtsaanduiding.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumGeslachtsaanduiding', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  GeslachtsaanduidingGeregistreerd._() : super();
  factory GeslachtsaanduidingGeregistreerd({
    $core.List<$core.int>? persoonPr,
    Geslachtsaanduiding? geslachtsaanduiding,
    $0.Timestamp? ingangsdatumGeslachtsaanduiding,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (geslachtsaanduiding != null) {
      _result.geslachtsaanduiding = geslachtsaanduiding;
    }
    if (ingangsdatumGeslachtsaanduiding != null) {
      _result.ingangsdatumGeslachtsaanduiding = ingangsdatumGeslachtsaanduiding;
    }
    return _result;
  }
  factory GeslachtsaanduidingGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GeslachtsaanduidingGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GeslachtsaanduidingGeregistreerd clone() => GeslachtsaanduidingGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GeslachtsaanduidingGeregistreerd copyWith(void Function(GeslachtsaanduidingGeregistreerd) updates) => super.copyWith((message) => updates(message as GeslachtsaanduidingGeregistreerd)) as GeslachtsaanduidingGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GeslachtsaanduidingGeregistreerd create() => GeslachtsaanduidingGeregistreerd._();
  GeslachtsaanduidingGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<GeslachtsaanduidingGeregistreerd> createRepeated() => $pb.PbList<GeslachtsaanduidingGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static GeslachtsaanduidingGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GeslachtsaanduidingGeregistreerd>(create);
  static GeslachtsaanduidingGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Geslachtsaanduiding get geslachtsaanduiding => $_getN(1);
  @$pb.TagNumber(2)
  set geslachtsaanduiding(Geslachtsaanduiding v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGeslachtsaanduiding() => $_has(1);
  @$pb.TagNumber(2)
  void clearGeslachtsaanduiding() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumGeslachtsaanduiding => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumGeslachtsaanduiding($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumGeslachtsaanduiding() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumGeslachtsaanduiding() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumGeslachtsaanduiding() => $_ensure(2);
}

class AdellijkeTitelPredikaatGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AdellijkeTitelPredikaatGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'adellijkeTitelPredikaat')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumAdellijkeTitelPredikaat', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  AdellijkeTitelPredikaatGeregistreerd._() : super();
  factory AdellijkeTitelPredikaatGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? adellijkeTitelPredikaat,
    $0.Timestamp? ingangsdatumAdellijkeTitelPredikaat,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (adellijkeTitelPredikaat != null) {
      _result.adellijkeTitelPredikaat = adellijkeTitelPredikaat;
    }
    if (ingangsdatumAdellijkeTitelPredikaat != null) {
      _result.ingangsdatumAdellijkeTitelPredikaat = ingangsdatumAdellijkeTitelPredikaat;
    }
    return _result;
  }
  factory AdellijkeTitelPredikaatGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AdellijkeTitelPredikaatGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AdellijkeTitelPredikaatGeregistreerd clone() => AdellijkeTitelPredikaatGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AdellijkeTitelPredikaatGeregistreerd copyWith(void Function(AdellijkeTitelPredikaatGeregistreerd) updates) => super.copyWith((message) => updates(message as AdellijkeTitelPredikaatGeregistreerd)) as AdellijkeTitelPredikaatGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AdellijkeTitelPredikaatGeregistreerd create() => AdellijkeTitelPredikaatGeregistreerd._();
  AdellijkeTitelPredikaatGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<AdellijkeTitelPredikaatGeregistreerd> createRepeated() => $pb.PbList<AdellijkeTitelPredikaatGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static AdellijkeTitelPredikaatGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AdellijkeTitelPredikaatGeregistreerd>(create);
  static AdellijkeTitelPredikaatGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get adellijkeTitelPredikaat => $_getSZ(1);
  @$pb.TagNumber(2)
  set adellijkeTitelPredikaat($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAdellijkeTitelPredikaat() => $_has(1);
  @$pb.TagNumber(2)
  void clearAdellijkeTitelPredikaat() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumAdellijkeTitelPredikaat => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumAdellijkeTitelPredikaat($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumAdellijkeTitelPredikaat() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumAdellijkeTitelPredikaat() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumAdellijkeTitelPredikaat() => $_ensure(2);
}

class NaamgebruikGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NaamgebruikGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Naamgebruik>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Naamgebruik', $pb.PbFieldType.OE, protoName: 'Naamgebruik', defaultOrMaker: Naamgebruik.NAAMGEBRUIK_E, valueOf: Naamgebruik.valueOf, enumValues: Naamgebruik.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumNaamgebruik', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  NaamgebruikGeregistreerd._() : super();
  factory NaamgebruikGeregistreerd({
    $core.List<$core.int>? persoonPr,
    Naamgebruik? naamgebruik,
    $0.Timestamp? ingangsdatumNaamgebruik,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (naamgebruik != null) {
      _result.naamgebruik = naamgebruik;
    }
    if (ingangsdatumNaamgebruik != null) {
      _result.ingangsdatumNaamgebruik = ingangsdatumNaamgebruik;
    }
    return _result;
  }
  factory NaamgebruikGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NaamgebruikGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NaamgebruikGeregistreerd clone() => NaamgebruikGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NaamgebruikGeregistreerd copyWith(void Function(NaamgebruikGeregistreerd) updates) => super.copyWith((message) => updates(message as NaamgebruikGeregistreerd)) as NaamgebruikGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NaamgebruikGeregistreerd create() => NaamgebruikGeregistreerd._();
  NaamgebruikGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<NaamgebruikGeregistreerd> createRepeated() => $pb.PbList<NaamgebruikGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static NaamgebruikGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NaamgebruikGeregistreerd>(create);
  static NaamgebruikGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Naamgebruik get naamgebruik => $_getN(1);
  @$pb.TagNumber(2)
  set naamgebruik(Naamgebruik v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasNaamgebruik() => $_has(1);
  @$pb.TagNumber(2)
  void clearNaamgebruik() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumNaamgebruik => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumNaamgebruik($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumNaamgebruik() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumNaamgebruik() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumNaamgebruik() => $_ensure(2);
}

class NaamgebruikGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NaamgebruikGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Naamgebruik>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Naamgebruik', $pb.PbFieldType.OE, protoName: 'Naamgebruik', defaultOrMaker: Naamgebruik.NAAMGEBRUIK_E, valueOf: Naamgebruik.valueOf, enumValues: Naamgebruik.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumNaamgebruikGewijzigd', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  NaamgebruikGewijzigd._() : super();
  factory NaamgebruikGewijzigd({
    $core.List<$core.int>? persoonPr,
    Naamgebruik? naamgebruik,
    $0.Timestamp? datumNaamgebruikGewijzigd,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (naamgebruik != null) {
      _result.naamgebruik = naamgebruik;
    }
    if (datumNaamgebruikGewijzigd != null) {
      _result.datumNaamgebruikGewijzigd = datumNaamgebruikGewijzigd;
    }
    return _result;
  }
  factory NaamgebruikGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NaamgebruikGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NaamgebruikGewijzigd clone() => NaamgebruikGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NaamgebruikGewijzigd copyWith(void Function(NaamgebruikGewijzigd) updates) => super.copyWith((message) => updates(message as NaamgebruikGewijzigd)) as NaamgebruikGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NaamgebruikGewijzigd create() => NaamgebruikGewijzigd._();
  NaamgebruikGewijzigd createEmptyInstance() => create();
  static $pb.PbList<NaamgebruikGewijzigd> createRepeated() => $pb.PbList<NaamgebruikGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static NaamgebruikGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NaamgebruikGewijzigd>(create);
  static NaamgebruikGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Naamgebruik get naamgebruik => $_getN(1);
  @$pb.TagNumber(2)
  set naamgebruik(Naamgebruik v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasNaamgebruik() => $_has(1);
  @$pb.TagNumber(2)
  void clearNaamgebruik() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumNaamgebruikGewijzigd => $_getN(2);
  @$pb.TagNumber(3)
  set datumNaamgebruikGewijzigd($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumNaamgebruikGewijzigd() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumNaamgebruikGewijzigd() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumNaamgebruikGewijzigd() => $_ensure(2);
}

class OverlijdenGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OverlijdenGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVanOverlijden', subBuilder: $0.Timestamp.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'plaatsVanOverlijden')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'gemeenteVanOverlijden')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landVanOverlijden')
    ..hasRequiredFields = false
  ;

  OverlijdenGeregistreerd._() : super();
  factory OverlijdenGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $0.Timestamp? datumVanOverlijden,
    $core.String? plaatsVanOverlijden,
    $core.String? gemeenteVanOverlijden,
    $core.String? landVanOverlijden,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (datumVanOverlijden != null) {
      _result.datumVanOverlijden = datumVanOverlijden;
    }
    if (plaatsVanOverlijden != null) {
      _result.plaatsVanOverlijden = plaatsVanOverlijden;
    }
    if (gemeenteVanOverlijden != null) {
      _result.gemeenteVanOverlijden = gemeenteVanOverlijden;
    }
    if (landVanOverlijden != null) {
      _result.landVanOverlijden = landVanOverlijden;
    }
    return _result;
  }
  factory OverlijdenGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OverlijdenGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OverlijdenGeregistreerd clone() => OverlijdenGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OverlijdenGeregistreerd copyWith(void Function(OverlijdenGeregistreerd) updates) => super.copyWith((message) => updates(message as OverlijdenGeregistreerd)) as OverlijdenGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OverlijdenGeregistreerd create() => OverlijdenGeregistreerd._();
  OverlijdenGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<OverlijdenGeregistreerd> createRepeated() => $pb.PbList<OverlijdenGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static OverlijdenGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OverlijdenGeregistreerd>(create);
  static OverlijdenGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $0.Timestamp get datumVanOverlijden => $_getN(1);
  @$pb.TagNumber(2)
  set datumVanOverlijden($0.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDatumVanOverlijden() => $_has(1);
  @$pb.TagNumber(2)
  void clearDatumVanOverlijden() => clearField(2);
  @$pb.TagNumber(2)
  $0.Timestamp ensureDatumVanOverlijden() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get plaatsVanOverlijden => $_getSZ(2);
  @$pb.TagNumber(3)
  set plaatsVanOverlijden($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPlaatsVanOverlijden() => $_has(2);
  @$pb.TagNumber(3)
  void clearPlaatsVanOverlijden() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get gemeenteVanOverlijden => $_getSZ(3);
  @$pb.TagNumber(4)
  set gemeenteVanOverlijden($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGemeenteVanOverlijden() => $_has(3);
  @$pb.TagNumber(4)
  void clearGemeenteVanOverlijden() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get landVanOverlijden => $_getSZ(4);
  @$pb.TagNumber(5)
  set landVanOverlijden($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLandVanOverlijden() => $_has(4);
  @$pb.TagNumber(5)
  void clearLandVanOverlijden() => clearField(5);
}

class NamenGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NamenGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voornamen')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'voorvoegselGeslachtsnaam')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geslachtsnaam')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumNaamswijziging', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  NamenGewijzigd._() : super();
  factory NamenGewijzigd({
    $core.List<$core.int>? persoonPr,
    $core.Iterable<$core.String>? voornamen,
    $core.String? voorvoegselGeslachtsnaam,
    $core.String? geslachtsnaam,
    $0.Timestamp? ingangsdatumNaamswijziging,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (voornamen != null) {
      _result.voornamen.addAll(voornamen);
    }
    if (voorvoegselGeslachtsnaam != null) {
      _result.voorvoegselGeslachtsnaam = voorvoegselGeslachtsnaam;
    }
    if (geslachtsnaam != null) {
      _result.geslachtsnaam = geslachtsnaam;
    }
    if (ingangsdatumNaamswijziging != null) {
      _result.ingangsdatumNaamswijziging = ingangsdatumNaamswijziging;
    }
    return _result;
  }
  factory NamenGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NamenGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NamenGewijzigd clone() => NamenGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NamenGewijzigd copyWith(void Function(NamenGewijzigd) updates) => super.copyWith((message) => updates(message as NamenGewijzigd)) as NamenGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NamenGewijzigd create() => NamenGewijzigd._();
  NamenGewijzigd createEmptyInstance() => create();
  static $pb.PbList<NamenGewijzigd> createRepeated() => $pb.PbList<NamenGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static NamenGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NamenGewijzigd>(create);
  static NamenGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get voornamen => $_getList(1);

  @$pb.TagNumber(3)
  $core.String get voorvoegselGeslachtsnaam => $_getSZ(2);
  @$pb.TagNumber(3)
  set voorvoegselGeslachtsnaam($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasVoorvoegselGeslachtsnaam() => $_has(2);
  @$pb.TagNumber(3)
  void clearVoorvoegselGeslachtsnaam() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get geslachtsnaam => $_getSZ(3);
  @$pb.TagNumber(4)
  set geslachtsnaam($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasGeslachtsnaam() => $_has(3);
  @$pb.TagNumber(4)
  void clearGeslachtsnaam() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get ingangsdatumNaamswijziging => $_getN(4);
  @$pb.TagNumber(5)
  set ingangsdatumNaamswijziging($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasIngangsdatumNaamswijziging() => $_has(4);
  @$pb.TagNumber(5)
  void clearIngangsdatumNaamswijziging() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureIngangsdatumNaamswijziging() => $_ensure(4);
}

class GeslachtsaanduidingGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GeslachtsaanduidingGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Geslachtsaanduiding>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'geslachtsaanduiding', $pb.PbFieldType.OE, defaultOrMaker: Geslachtsaanduiding.GESLACHT_ONBEKEND, valueOf: Geslachtsaanduiding.valueOf, enumValues: Geslachtsaanduiding.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumWijzigingGeslachtsaanduiding', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  GeslachtsaanduidingGewijzigd._() : super();
  factory GeslachtsaanduidingGewijzigd({
    $core.List<$core.int>? persoonPr,
    Geslachtsaanduiding? geslachtsaanduiding,
    $0.Timestamp? ingangsdatumWijzigingGeslachtsaanduiding,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (geslachtsaanduiding != null) {
      _result.geslachtsaanduiding = geslachtsaanduiding;
    }
    if (ingangsdatumWijzigingGeslachtsaanduiding != null) {
      _result.ingangsdatumWijzigingGeslachtsaanduiding = ingangsdatumWijzigingGeslachtsaanduiding;
    }
    return _result;
  }
  factory GeslachtsaanduidingGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GeslachtsaanduidingGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GeslachtsaanduidingGewijzigd clone() => GeslachtsaanduidingGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GeslachtsaanduidingGewijzigd copyWith(void Function(GeslachtsaanduidingGewijzigd) updates) => super.copyWith((message) => updates(message as GeslachtsaanduidingGewijzigd)) as GeslachtsaanduidingGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GeslachtsaanduidingGewijzigd create() => GeslachtsaanduidingGewijzigd._();
  GeslachtsaanduidingGewijzigd createEmptyInstance() => create();
  static $pb.PbList<GeslachtsaanduidingGewijzigd> createRepeated() => $pb.PbList<GeslachtsaanduidingGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static GeslachtsaanduidingGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GeslachtsaanduidingGewijzigd>(create);
  static GeslachtsaanduidingGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Geslachtsaanduiding get geslachtsaanduiding => $_getN(1);
  @$pb.TagNumber(2)
  set geslachtsaanduiding(Geslachtsaanduiding v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasGeslachtsaanduiding() => $_has(1);
  @$pb.TagNumber(2)
  void clearGeslachtsaanduiding() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumWijzigingGeslachtsaanduiding => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumWijzigingGeslachtsaanduiding($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumWijzigingGeslachtsaanduiding() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumWijzigingGeslachtsaanduiding() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumWijzigingGeslachtsaanduiding() => $_ensure(2);
}

class ErkenningOngeborenVruchtGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ErkenningOngeborenVruchtGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ongeborenvruchtPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'moederUitWiePr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'erkennerPr', $pb.PbFieldType.OY)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'keuzeGeslachtsnaam')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumErkenning', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ErkenningOngeborenVruchtGeregistreerd._() : super();
  factory ErkenningOngeborenVruchtGeregistreerd({
    $core.List<$core.int>? ongeborenvruchtPr,
    $core.List<$core.int>? moederUitWiePr,
    $core.List<$core.int>? erkennerPr,
    $core.String? keuzeGeslachtsnaam,
    $0.Timestamp? datumErkenning,
  }) {
    final _result = create();
    if (ongeborenvruchtPr != null) {
      _result.ongeborenvruchtPr = ongeborenvruchtPr;
    }
    if (moederUitWiePr != null) {
      _result.moederUitWiePr = moederUitWiePr;
    }
    if (erkennerPr != null) {
      _result.erkennerPr = erkennerPr;
    }
    if (keuzeGeslachtsnaam != null) {
      _result.keuzeGeslachtsnaam = keuzeGeslachtsnaam;
    }
    if (datumErkenning != null) {
      _result.datumErkenning = datumErkenning;
    }
    return _result;
  }
  factory ErkenningOngeborenVruchtGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ErkenningOngeborenVruchtGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ErkenningOngeborenVruchtGeregistreerd clone() => ErkenningOngeborenVruchtGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ErkenningOngeborenVruchtGeregistreerd copyWith(void Function(ErkenningOngeborenVruchtGeregistreerd) updates) => super.copyWith((message) => updates(message as ErkenningOngeborenVruchtGeregistreerd)) as ErkenningOngeborenVruchtGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ErkenningOngeborenVruchtGeregistreerd create() => ErkenningOngeborenVruchtGeregistreerd._();
  ErkenningOngeborenVruchtGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<ErkenningOngeborenVruchtGeregistreerd> createRepeated() => $pb.PbList<ErkenningOngeborenVruchtGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static ErkenningOngeborenVruchtGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ErkenningOngeborenVruchtGeregistreerd>(create);
  static ErkenningOngeborenVruchtGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get ongeborenvruchtPr => $_getN(0);
  @$pb.TagNumber(1)
  set ongeborenvruchtPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOngeborenvruchtPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearOngeborenvruchtPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get moederUitWiePr => $_getN(1);
  @$pb.TagNumber(2)
  set moederUitWiePr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMoederUitWiePr() => $_has(1);
  @$pb.TagNumber(2)
  void clearMoederUitWiePr() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get erkennerPr => $_getN(2);
  @$pb.TagNumber(3)
  set erkennerPr($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasErkennerPr() => $_has(2);
  @$pb.TagNumber(3)
  void clearErkennerPr() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get keuzeGeslachtsnaam => $_getSZ(3);
  @$pb.TagNumber(4)
  set keuzeGeslachtsnaam($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasKeuzeGeslachtsnaam() => $_has(3);
  @$pb.TagNumber(4)
  void clearKeuzeGeslachtsnaam() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get datumErkenning => $_getN(4);
  @$pb.TagNumber(5)
  set datumErkenning($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDatumErkenning() => $_has(4);
  @$pb.TagNumber(5)
  void clearDatumErkenning() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureDatumErkenning() => $_ensure(4);
}

class ErkenningKindGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ErkenningKindGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kindPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'moederUitWiePr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'erkennerPr', $pb.PbFieldType.OY)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'keuzeGeslachtsnaam')
    ..aOM<$0.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumErkenning', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ErkenningKindGeregistreerd._() : super();
  factory ErkenningKindGeregistreerd({
    $core.List<$core.int>? kindPr,
    $core.List<$core.int>? moederUitWiePr,
    $core.List<$core.int>? erkennerPr,
    $core.String? keuzeGeslachtsnaam,
    $0.Timestamp? datumErkenning,
  }) {
    final _result = create();
    if (kindPr != null) {
      _result.kindPr = kindPr;
    }
    if (moederUitWiePr != null) {
      _result.moederUitWiePr = moederUitWiePr;
    }
    if (erkennerPr != null) {
      _result.erkennerPr = erkennerPr;
    }
    if (keuzeGeslachtsnaam != null) {
      _result.keuzeGeslachtsnaam = keuzeGeslachtsnaam;
    }
    if (datumErkenning != null) {
      _result.datumErkenning = datumErkenning;
    }
    return _result;
  }
  factory ErkenningKindGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ErkenningKindGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ErkenningKindGeregistreerd clone() => ErkenningKindGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ErkenningKindGeregistreerd copyWith(void Function(ErkenningKindGeregistreerd) updates) => super.copyWith((message) => updates(message as ErkenningKindGeregistreerd)) as ErkenningKindGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ErkenningKindGeregistreerd create() => ErkenningKindGeregistreerd._();
  ErkenningKindGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<ErkenningKindGeregistreerd> createRepeated() => $pb.PbList<ErkenningKindGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static ErkenningKindGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ErkenningKindGeregistreerd>(create);
  static ErkenningKindGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get kindPr => $_getN(0);
  @$pb.TagNumber(1)
  set kindPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKindPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearKindPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get moederUitWiePr => $_getN(1);
  @$pb.TagNumber(2)
  set moederUitWiePr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMoederUitWiePr() => $_has(1);
  @$pb.TagNumber(2)
  void clearMoederUitWiePr() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get erkennerPr => $_getN(2);
  @$pb.TagNumber(3)
  set erkennerPr($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasErkennerPr() => $_has(2);
  @$pb.TagNumber(3)
  void clearErkennerPr() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get keuzeGeslachtsnaam => $_getSZ(3);
  @$pb.TagNumber(4)
  set keuzeGeslachtsnaam($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasKeuzeGeslachtsnaam() => $_has(3);
  @$pb.TagNumber(4)
  void clearKeuzeGeslachtsnaam() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get datumErkenning => $_getN(4);
  @$pb.TagNumber(5)
  set datumErkenning($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasDatumErkenning() => $_has(4);
  @$pb.TagNumber(5)
  void clearDatumErkenning() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureDatumErkenning() => $_ensure(4);
}

class ErkenningVernietigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ErkenningVernietigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'kindPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'moederPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'erkennerPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVernietigingErkenning', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ErkenningVernietigd._() : super();
  factory ErkenningVernietigd({
    $core.List<$core.int>? kindPr,
    $core.List<$core.int>? moederPr,
    $core.List<$core.int>? erkennerPr,
    $0.Timestamp? datumVernietigingErkenning,
  }) {
    final _result = create();
    if (kindPr != null) {
      _result.kindPr = kindPr;
    }
    if (moederPr != null) {
      _result.moederPr = moederPr;
    }
    if (erkennerPr != null) {
      _result.erkennerPr = erkennerPr;
    }
    if (datumVernietigingErkenning != null) {
      _result.datumVernietigingErkenning = datumVernietigingErkenning;
    }
    return _result;
  }
  factory ErkenningVernietigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ErkenningVernietigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ErkenningVernietigd clone() => ErkenningVernietigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ErkenningVernietigd copyWith(void Function(ErkenningVernietigd) updates) => super.copyWith((message) => updates(message as ErkenningVernietigd)) as ErkenningVernietigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ErkenningVernietigd create() => ErkenningVernietigd._();
  ErkenningVernietigd createEmptyInstance() => create();
  static $pb.PbList<ErkenningVernietigd> createRepeated() => $pb.PbList<ErkenningVernietigd>();
  @$core.pragma('dart2js:noInline')
  static ErkenningVernietigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ErkenningVernietigd>(create);
  static ErkenningVernietigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get kindPr => $_getN(0);
  @$pb.TagNumber(1)
  set kindPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasKindPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearKindPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get moederPr => $_getN(1);
  @$pb.TagNumber(2)
  set moederPr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMoederPr() => $_has(1);
  @$pb.TagNumber(2)
  void clearMoederPr() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get erkennerPr => $_getN(2);
  @$pb.TagNumber(3)
  set erkennerPr($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasErkennerPr() => $_has(2);
  @$pb.TagNumber(3)
  void clearErkennerPr() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get datumVernietigingErkenning => $_getN(3);
  @$pb.TagNumber(4)
  set datumVernietigingErkenning($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatumVernietigingErkenning() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatumVernietigingErkenning() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDatumVernietigingErkenning() => $_ensure(3);
}

class JuridischOuderGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JuridischOuderGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ouderPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumRelatie', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  JuridischOuderGeregistreerd._() : super();
  factory JuridischOuderGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.List<$core.int>? ouderPr,
    $0.Timestamp? startdatumRelatie,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (ouderPr != null) {
      _result.ouderPr = ouderPr;
    }
    if (startdatumRelatie != null) {
      _result.startdatumRelatie = startdatumRelatie;
    }
    return _result;
  }
  factory JuridischOuderGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JuridischOuderGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JuridischOuderGeregistreerd clone() => JuridischOuderGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JuridischOuderGeregistreerd copyWith(void Function(JuridischOuderGeregistreerd) updates) => super.copyWith((message) => updates(message as JuridischOuderGeregistreerd)) as JuridischOuderGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JuridischOuderGeregistreerd create() => JuridischOuderGeregistreerd._();
  JuridischOuderGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<JuridischOuderGeregistreerd> createRepeated() => $pb.PbList<JuridischOuderGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static JuridischOuderGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JuridischOuderGeregistreerd>(create);
  static JuridischOuderGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get ouderPr => $_getN(1);
  @$pb.TagNumber(2)
  set ouderPr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOuderPr() => $_has(1);
  @$pb.TagNumber(2)
  void clearOuderPr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get startdatumRelatie => $_getN(2);
  @$pb.TagNumber(3)
  set startdatumRelatie($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartdatumRelatie() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartdatumRelatie() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureStartdatumRelatie() => $_ensure(2);
}

class JuridischOuderschapOntkend extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JuridischOuderschapOntkend', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ouderPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumOntkenningOuderschap', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  JuridischOuderschapOntkend._() : super();
  factory JuridischOuderschapOntkend({
    $core.List<$core.int>? persoonPr,
    $core.List<$core.int>? ouderPr,
    $0.Timestamp? datumOntkenningOuderschap,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (ouderPr != null) {
      _result.ouderPr = ouderPr;
    }
    if (datumOntkenningOuderschap != null) {
      _result.datumOntkenningOuderschap = datumOntkenningOuderschap;
    }
    return _result;
  }
  factory JuridischOuderschapOntkend.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JuridischOuderschapOntkend.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JuridischOuderschapOntkend clone() => JuridischOuderschapOntkend()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JuridischOuderschapOntkend copyWith(void Function(JuridischOuderschapOntkend) updates) => super.copyWith((message) => updates(message as JuridischOuderschapOntkend)) as JuridischOuderschapOntkend; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JuridischOuderschapOntkend create() => JuridischOuderschapOntkend._();
  JuridischOuderschapOntkend createEmptyInstance() => create();
  static $pb.PbList<JuridischOuderschapOntkend> createRepeated() => $pb.PbList<JuridischOuderschapOntkend>();
  @$core.pragma('dart2js:noInline')
  static JuridischOuderschapOntkend getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JuridischOuderschapOntkend>(create);
  static JuridischOuderschapOntkend? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get ouderPr => $_getN(1);
  @$pb.TagNumber(2)
  set ouderPr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOuderPr() => $_has(1);
  @$pb.TagNumber(2)
  void clearOuderPr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumOntkenningOuderschap => $_getN(2);
  @$pb.TagNumber(3)
  set datumOntkenningOuderschap($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumOntkenningOuderschap() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumOntkenningOuderschap() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumOntkenningOuderschap() => $_ensure(2);
}

class JuridischOuderschapGerechtelijkVastgesteld extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JuridischOuderschapGerechtelijkVastgesteld', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ouderPr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ingangsdatumOuderschap', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  JuridischOuderschapGerechtelijkVastgesteld._() : super();
  factory JuridischOuderschapGerechtelijkVastgesteld({
    $core.List<$core.int>? persoonPr,
    $core.List<$core.int>? ouderPr,
    $0.Timestamp? ingangsdatumOuderschap,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (ouderPr != null) {
      _result.ouderPr = ouderPr;
    }
    if (ingangsdatumOuderschap != null) {
      _result.ingangsdatumOuderschap = ingangsdatumOuderschap;
    }
    return _result;
  }
  factory JuridischOuderschapGerechtelijkVastgesteld.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JuridischOuderschapGerechtelijkVastgesteld.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JuridischOuderschapGerechtelijkVastgesteld clone() => JuridischOuderschapGerechtelijkVastgesteld()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JuridischOuderschapGerechtelijkVastgesteld copyWith(void Function(JuridischOuderschapGerechtelijkVastgesteld) updates) => super.copyWith((message) => updates(message as JuridischOuderschapGerechtelijkVastgesteld)) as JuridischOuderschapGerechtelijkVastgesteld; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JuridischOuderschapGerechtelijkVastgesteld create() => JuridischOuderschapGerechtelijkVastgesteld._();
  JuridischOuderschapGerechtelijkVastgesteld createEmptyInstance() => create();
  static $pb.PbList<JuridischOuderschapGerechtelijkVastgesteld> createRepeated() => $pb.PbList<JuridischOuderschapGerechtelijkVastgesteld>();
  @$core.pragma('dart2js:noInline')
  static JuridischOuderschapGerechtelijkVastgesteld getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JuridischOuderschapGerechtelijkVastgesteld>(create);
  static JuridischOuderschapGerechtelijkVastgesteld? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get ouderPr => $_getN(1);
  @$pb.TagNumber(2)
  set ouderPr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOuderPr() => $_has(1);
  @$pb.TagNumber(2)
  void clearOuderPr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get ingangsdatumOuderschap => $_getN(2);
  @$pb.TagNumber(3)
  set ingangsdatumOuderschap($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasIngangsdatumOuderschap() => $_has(2);
  @$pb.TagNumber(3)
  void clearIngangsdatumOuderschap() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureIngangsdatumOuderschap() => $_ensure(2);
}

class HuwelijkGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'HuwelijkGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon1Pr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon2Pr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumHuwelijkVoltrokken', subBuilder: $0.Timestamp.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'plaatsHuwelijkVoltrokken')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landHuwelijkVoltrokken')
    ..hasRequiredFields = false
  ;

  HuwelijkGeregistreerd._() : super();
  factory HuwelijkGeregistreerd({
    $core.List<$core.int>? persoon1Pr,
    $core.List<$core.int>? persoon2Pr,
    $0.Timestamp? datumHuwelijkVoltrokken,
    $core.String? plaatsHuwelijkVoltrokken,
    $core.String? landHuwelijkVoltrokken,
  }) {
    final _result = create();
    if (persoon1Pr != null) {
      _result.persoon1Pr = persoon1Pr;
    }
    if (persoon2Pr != null) {
      _result.persoon2Pr = persoon2Pr;
    }
    if (datumHuwelijkVoltrokken != null) {
      _result.datumHuwelijkVoltrokken = datumHuwelijkVoltrokken;
    }
    if (plaatsHuwelijkVoltrokken != null) {
      _result.plaatsHuwelijkVoltrokken = plaatsHuwelijkVoltrokken;
    }
    if (landHuwelijkVoltrokken != null) {
      _result.landHuwelijkVoltrokken = landHuwelijkVoltrokken;
    }
    return _result;
  }
  factory HuwelijkGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory HuwelijkGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  HuwelijkGeregistreerd clone() => HuwelijkGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  HuwelijkGeregistreerd copyWith(void Function(HuwelijkGeregistreerd) updates) => super.copyWith((message) => updates(message as HuwelijkGeregistreerd)) as HuwelijkGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HuwelijkGeregistreerd create() => HuwelijkGeregistreerd._();
  HuwelijkGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<HuwelijkGeregistreerd> createRepeated() => $pb.PbList<HuwelijkGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static HuwelijkGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<HuwelijkGeregistreerd>(create);
  static HuwelijkGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoon1Pr => $_getN(0);
  @$pb.TagNumber(1)
  set persoon1Pr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoon1Pr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoon1Pr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get persoon2Pr => $_getN(1);
  @$pb.TagNumber(2)
  set persoon2Pr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersoon2Pr() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersoon2Pr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumHuwelijkVoltrokken => $_getN(2);
  @$pb.TagNumber(3)
  set datumHuwelijkVoltrokken($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumHuwelijkVoltrokken() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumHuwelijkVoltrokken() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumHuwelijkVoltrokken() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get plaatsHuwelijkVoltrokken => $_getSZ(3);
  @$pb.TagNumber(4)
  set plaatsHuwelijkVoltrokken($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlaatsHuwelijkVoltrokken() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlaatsHuwelijkVoltrokken() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get landHuwelijkVoltrokken => $_getSZ(4);
  @$pb.TagNumber(5)
  set landHuwelijkVoltrokken($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLandHuwelijkVoltrokken() => $_has(4);
  @$pb.TagNumber(5)
  void clearLandHuwelijkVoltrokken() => clearField(5);
}

class Geregistreerd_PartnerschapGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Geregistreerd_PartnerschapGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon1Pr')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon2Pr')
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumGeregistreerdPartnerschap', subBuilder: $0.Timestamp.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'plaatsGeregistreerdPartnerschap')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landGeregistreerdPartnerschap')
    ..hasRequiredFields = false
  ;

  Geregistreerd_PartnerschapGeregistreerd._() : super();
  factory Geregistreerd_PartnerschapGeregistreerd({
    $core.String? persoon1Pr,
    $core.String? persoon2Pr,
    $0.Timestamp? startdatumGeregistreerdPartnerschap,
    $core.String? plaatsGeregistreerdPartnerschap,
    $core.String? landGeregistreerdPartnerschap,
  }) {
    final _result = create();
    if (persoon1Pr != null) {
      _result.persoon1Pr = persoon1Pr;
    }
    if (persoon2Pr != null) {
      _result.persoon2Pr = persoon2Pr;
    }
    if (startdatumGeregistreerdPartnerschap != null) {
      _result.startdatumGeregistreerdPartnerschap = startdatumGeregistreerdPartnerschap;
    }
    if (plaatsGeregistreerdPartnerschap != null) {
      _result.plaatsGeregistreerdPartnerschap = plaatsGeregistreerdPartnerschap;
    }
    if (landGeregistreerdPartnerschap != null) {
      _result.landGeregistreerdPartnerschap = landGeregistreerdPartnerschap;
    }
    return _result;
  }
  factory Geregistreerd_PartnerschapGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Geregistreerd_PartnerschapGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Geregistreerd_PartnerschapGeregistreerd clone() => Geregistreerd_PartnerschapGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Geregistreerd_PartnerschapGeregistreerd copyWith(void Function(Geregistreerd_PartnerschapGeregistreerd) updates) => super.copyWith((message) => updates(message as Geregistreerd_PartnerschapGeregistreerd)) as Geregistreerd_PartnerschapGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Geregistreerd_PartnerschapGeregistreerd create() => Geregistreerd_PartnerschapGeregistreerd._();
  Geregistreerd_PartnerschapGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<Geregistreerd_PartnerschapGeregistreerd> createRepeated() => $pb.PbList<Geregistreerd_PartnerschapGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static Geregistreerd_PartnerschapGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Geregistreerd_PartnerschapGeregistreerd>(create);
  static Geregistreerd_PartnerschapGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get persoon1Pr => $_getSZ(0);
  @$pb.TagNumber(1)
  set persoon1Pr($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoon1Pr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoon1Pr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get persoon2Pr => $_getSZ(1);
  @$pb.TagNumber(2)
  set persoon2Pr($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersoon2Pr() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersoon2Pr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get startdatumGeregistreerdPartnerschap => $_getN(2);
  @$pb.TagNumber(3)
  set startdatumGeregistreerdPartnerschap($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartdatumGeregistreerdPartnerschap() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartdatumGeregistreerdPartnerschap() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureStartdatumGeregistreerdPartnerschap() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get plaatsGeregistreerdPartnerschap => $_getSZ(3);
  @$pb.TagNumber(4)
  set plaatsGeregistreerdPartnerschap($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlaatsGeregistreerdPartnerschap() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlaatsGeregistreerdPartnerschap() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get landGeregistreerdPartnerschap => $_getSZ(4);
  @$pb.TagNumber(5)
  set landGeregistreerdPartnerschap($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLandGeregistreerdPartnerschap() => $_has(4);
  @$pb.TagNumber(5)
  void clearLandGeregistreerdPartnerschap() => clearField(5);
}

class HuwelijkOntbonden extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'HuwelijkOntbonden', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon1Pr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon2Pr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumHuwelijkOntbonden', subBuilder: $0.Timestamp.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'plaatsHuwelijkOntbonden')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landHuwelijkOntbonden')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redenHuwelijkOntbonden')
    ..hasRequiredFields = false
  ;

  HuwelijkOntbonden._() : super();
  factory HuwelijkOntbonden({
    $core.List<$core.int>? persoon1Pr,
    $core.List<$core.int>? persoon2Pr,
    $0.Timestamp? datumHuwelijkOntbonden,
    $core.String? plaatsHuwelijkOntbonden,
    $core.String? landHuwelijkOntbonden,
    $core.String? redenHuwelijkOntbonden,
  }) {
    final _result = create();
    if (persoon1Pr != null) {
      _result.persoon1Pr = persoon1Pr;
    }
    if (persoon2Pr != null) {
      _result.persoon2Pr = persoon2Pr;
    }
    if (datumHuwelijkOntbonden != null) {
      _result.datumHuwelijkOntbonden = datumHuwelijkOntbonden;
    }
    if (plaatsHuwelijkOntbonden != null) {
      _result.plaatsHuwelijkOntbonden = plaatsHuwelijkOntbonden;
    }
    if (landHuwelijkOntbonden != null) {
      _result.landHuwelijkOntbonden = landHuwelijkOntbonden;
    }
    if (redenHuwelijkOntbonden != null) {
      _result.redenHuwelijkOntbonden = redenHuwelijkOntbonden;
    }
    return _result;
  }
  factory HuwelijkOntbonden.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory HuwelijkOntbonden.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  HuwelijkOntbonden clone() => HuwelijkOntbonden()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  HuwelijkOntbonden copyWith(void Function(HuwelijkOntbonden) updates) => super.copyWith((message) => updates(message as HuwelijkOntbonden)) as HuwelijkOntbonden; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HuwelijkOntbonden create() => HuwelijkOntbonden._();
  HuwelijkOntbonden createEmptyInstance() => create();
  static $pb.PbList<HuwelijkOntbonden> createRepeated() => $pb.PbList<HuwelijkOntbonden>();
  @$core.pragma('dart2js:noInline')
  static HuwelijkOntbonden getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<HuwelijkOntbonden>(create);
  static HuwelijkOntbonden? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoon1Pr => $_getN(0);
  @$pb.TagNumber(1)
  set persoon1Pr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoon1Pr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoon1Pr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get persoon2Pr => $_getN(1);
  @$pb.TagNumber(2)
  set persoon2Pr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersoon2Pr() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersoon2Pr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumHuwelijkOntbonden => $_getN(2);
  @$pb.TagNumber(3)
  set datumHuwelijkOntbonden($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumHuwelijkOntbonden() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumHuwelijkOntbonden() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumHuwelijkOntbonden() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get plaatsHuwelijkOntbonden => $_getSZ(3);
  @$pb.TagNumber(4)
  set plaatsHuwelijkOntbonden($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlaatsHuwelijkOntbonden() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlaatsHuwelijkOntbonden() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get landHuwelijkOntbonden => $_getSZ(4);
  @$pb.TagNumber(5)
  set landHuwelijkOntbonden($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLandHuwelijkOntbonden() => $_has(4);
  @$pb.TagNumber(5)
  void clearLandHuwelijkOntbonden() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get redenHuwelijkOntbonden => $_getSZ(5);
  @$pb.TagNumber(6)
  set redenHuwelijkOntbonden($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasRedenHuwelijkOntbonden() => $_has(5);
  @$pb.TagNumber(6)
  void clearRedenHuwelijkOntbonden() => clearField(6);
}

class GeregistreerdPartnerschapOntbonden extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GeregistreerdPartnerschapOntbonden', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon1Pr', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoon2Pr', $pb.PbFieldType.OY)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumGeregistreerdPartnerschapOntbonden', subBuilder: $0.Timestamp.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'plaatsGeregistreerdPartnerschapOntbonden')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'landGeregistreerdPartnerschapOntbonden')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'redenGeregistreerdPartnerschapOntbonden')
    ..hasRequiredFields = false
  ;

  GeregistreerdPartnerschapOntbonden._() : super();
  factory GeregistreerdPartnerschapOntbonden({
    $core.List<$core.int>? persoon1Pr,
    $core.List<$core.int>? persoon2Pr,
    $0.Timestamp? datumGeregistreerdPartnerschapOntbonden,
    $core.String? plaatsGeregistreerdPartnerschapOntbonden,
    $core.String? landGeregistreerdPartnerschapOntbonden,
    $core.String? redenGeregistreerdPartnerschapOntbonden,
  }) {
    final _result = create();
    if (persoon1Pr != null) {
      _result.persoon1Pr = persoon1Pr;
    }
    if (persoon2Pr != null) {
      _result.persoon2Pr = persoon2Pr;
    }
    if (datumGeregistreerdPartnerschapOntbonden != null) {
      _result.datumGeregistreerdPartnerschapOntbonden = datumGeregistreerdPartnerschapOntbonden;
    }
    if (plaatsGeregistreerdPartnerschapOntbonden != null) {
      _result.plaatsGeregistreerdPartnerschapOntbonden = plaatsGeregistreerdPartnerschapOntbonden;
    }
    if (landGeregistreerdPartnerschapOntbonden != null) {
      _result.landGeregistreerdPartnerschapOntbonden = landGeregistreerdPartnerschapOntbonden;
    }
    if (redenGeregistreerdPartnerschapOntbonden != null) {
      _result.redenGeregistreerdPartnerschapOntbonden = redenGeregistreerdPartnerschapOntbonden;
    }
    return _result;
  }
  factory GeregistreerdPartnerschapOntbonden.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GeregistreerdPartnerschapOntbonden.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GeregistreerdPartnerschapOntbonden clone() => GeregistreerdPartnerschapOntbonden()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GeregistreerdPartnerschapOntbonden copyWith(void Function(GeregistreerdPartnerschapOntbonden) updates) => super.copyWith((message) => updates(message as GeregistreerdPartnerschapOntbonden)) as GeregistreerdPartnerschapOntbonden; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GeregistreerdPartnerschapOntbonden create() => GeregistreerdPartnerschapOntbonden._();
  GeregistreerdPartnerschapOntbonden createEmptyInstance() => create();
  static $pb.PbList<GeregistreerdPartnerschapOntbonden> createRepeated() => $pb.PbList<GeregistreerdPartnerschapOntbonden>();
  @$core.pragma('dart2js:noInline')
  static GeregistreerdPartnerschapOntbonden getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GeregistreerdPartnerschapOntbonden>(create);
  static GeregistreerdPartnerschapOntbonden? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoon1Pr => $_getN(0);
  @$pb.TagNumber(1)
  set persoon1Pr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoon1Pr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoon1Pr() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get persoon2Pr => $_getN(1);
  @$pb.TagNumber(2)
  set persoon2Pr($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersoon2Pr() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersoon2Pr() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumGeregistreerdPartnerschapOntbonden => $_getN(2);
  @$pb.TagNumber(3)
  set datumGeregistreerdPartnerschapOntbonden($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumGeregistreerdPartnerschapOntbonden() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumGeregistreerdPartnerschapOntbonden() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumGeregistreerdPartnerschapOntbonden() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get plaatsGeregistreerdPartnerschapOntbonden => $_getSZ(3);
  @$pb.TagNumber(4)
  set plaatsGeregistreerdPartnerschapOntbonden($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlaatsGeregistreerdPartnerschapOntbonden() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlaatsGeregistreerdPartnerschapOntbonden() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get landGeregistreerdPartnerschapOntbonden => $_getSZ(4);
  @$pb.TagNumber(5)
  set landGeregistreerdPartnerschapOntbonden($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLandGeregistreerdPartnerschapOntbonden() => $_has(4);
  @$pb.TagNumber(5)
  void clearLandGeregistreerdPartnerschapOntbonden() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get redenGeregistreerdPartnerschapOntbonden => $_getSZ(5);
  @$pb.TagNumber(6)
  set redenGeregistreerdPartnerschapOntbonden($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasRedenGeregistreerdPartnerschapOntbonden() => $_has(5);
  @$pb.TagNumber(6)
  void clearRedenGeregistreerdPartnerschapOntbonden() => clearField(6);
}

class ContactgegevensGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ContactgegevensGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'emailadres')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mobielTelefoonnummer')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumContactgegevens', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ContactgegevensGeregistreerd._() : super();
  factory ContactgegevensGeregistreerd({
    $core.List<$core.int>? persoonPr,
    $core.String? emailadres,
    $core.String? mobielTelefoonnummer,
    $0.Timestamp? startdatumContactgegevens,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (emailadres != null) {
      _result.emailadres = emailadres;
    }
    if (mobielTelefoonnummer != null) {
      _result.mobielTelefoonnummer = mobielTelefoonnummer;
    }
    if (startdatumContactgegevens != null) {
      _result.startdatumContactgegevens = startdatumContactgegevens;
    }
    return _result;
  }
  factory ContactgegevensGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ContactgegevensGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ContactgegevensGeregistreerd clone() => ContactgegevensGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ContactgegevensGeregistreerd copyWith(void Function(ContactgegevensGeregistreerd) updates) => super.copyWith((message) => updates(message as ContactgegevensGeregistreerd)) as ContactgegevensGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ContactgegevensGeregistreerd create() => ContactgegevensGeregistreerd._();
  ContactgegevensGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<ContactgegevensGeregistreerd> createRepeated() => $pb.PbList<ContactgegevensGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static ContactgegevensGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ContactgegevensGeregistreerd>(create);
  static ContactgegevensGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get emailadres => $_getSZ(1);
  @$pb.TagNumber(2)
  set emailadres($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmailadres() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmailadres() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get mobielTelefoonnummer => $_getSZ(2);
  @$pb.TagNumber(3)
  set mobielTelefoonnummer($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMobielTelefoonnummer() => $_has(2);
  @$pb.TagNumber(3)
  void clearMobielTelefoonnummer() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get startdatumContactgegevens => $_getN(3);
  @$pb.TagNumber(4)
  set startdatumContactgegevens($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStartdatumContactgegevens() => $_has(3);
  @$pb.TagNumber(4)
  void clearStartdatumContactgegevens() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureStartdatumContactgegevens() => $_ensure(3);
}

class ContactgegevensGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ContactgegevensGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'emailadres')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'mobielTelefoonnummer')
    ..aOM<$0.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumContactgegevensGewijzigd', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  ContactgegevensGewijzigd._() : super();
  factory ContactgegevensGewijzigd({
    $core.List<$core.int>? persoonPr,
    $core.String? emailadres,
    $core.String? mobielTelefoonnummer,
    $0.Timestamp? datumContactgegevensGewijzigd,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (emailadres != null) {
      _result.emailadres = emailadres;
    }
    if (mobielTelefoonnummer != null) {
      _result.mobielTelefoonnummer = mobielTelefoonnummer;
    }
    if (datumContactgegevensGewijzigd != null) {
      _result.datumContactgegevensGewijzigd = datumContactgegevensGewijzigd;
    }
    return _result;
  }
  factory ContactgegevensGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ContactgegevensGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ContactgegevensGewijzigd clone() => ContactgegevensGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ContactgegevensGewijzigd copyWith(void Function(ContactgegevensGewijzigd) updates) => super.copyWith((message) => updates(message as ContactgegevensGewijzigd)) as ContactgegevensGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ContactgegevensGewijzigd create() => ContactgegevensGewijzigd._();
  ContactgegevensGewijzigd createEmptyInstance() => create();
  static $pb.PbList<ContactgegevensGewijzigd> createRepeated() => $pb.PbList<ContactgegevensGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static ContactgegevensGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ContactgegevensGewijzigd>(create);
  static ContactgegevensGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get emailadres => $_getSZ(1);
  @$pb.TagNumber(2)
  set emailadres($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmailadres() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmailadres() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get mobielTelefoonnummer => $_getSZ(2);
  @$pb.TagNumber(3)
  set mobielTelefoonnummer($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMobielTelefoonnummer() => $_has(2);
  @$pb.TagNumber(3)
  void clearMobielTelefoonnummer() => clearField(3);

  @$pb.TagNumber(4)
  $0.Timestamp get datumContactgegevensGewijzigd => $_getN(3);
  @$pb.TagNumber(4)
  set datumContactgegevensGewijzigd($0.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDatumContactgegevensGewijzigd() => $_has(3);
  @$pb.TagNumber(4)
  void clearDatumContactgegevensGewijzigd() => clearField(4);
  @$pb.TagNumber(4)
  $0.Timestamp ensureDatumContactgegevensGewijzigd() => $_ensure(3);
}

class VerstrekkingsbeperkingGeregistreerd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerstrekkingsbeperkingGeregistreerd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Verstrekkingsbeperking>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verstrekkingsbeperking', $pb.PbFieldType.OE, defaultOrMaker: Verstrekkingsbeperking.VERSTREKKINGSBEPERKING_Geen_beperking, valueOf: Verstrekkingsbeperking.valueOf, enumValues: Verstrekkingsbeperking.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startdatumVerstrekkingsbeperking', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  VerstrekkingsbeperkingGeregistreerd._() : super();
  factory VerstrekkingsbeperkingGeregistreerd({
    $core.List<$core.int>? persoonPr,
    Verstrekkingsbeperking? verstrekkingsbeperking,
    $0.Timestamp? startdatumVerstrekkingsbeperking,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verstrekkingsbeperking != null) {
      _result.verstrekkingsbeperking = verstrekkingsbeperking;
    }
    if (startdatumVerstrekkingsbeperking != null) {
      _result.startdatumVerstrekkingsbeperking = startdatumVerstrekkingsbeperking;
    }
    return _result;
  }
  factory VerstrekkingsbeperkingGeregistreerd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerstrekkingsbeperkingGeregistreerd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerstrekkingsbeperkingGeregistreerd clone() => VerstrekkingsbeperkingGeregistreerd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerstrekkingsbeperkingGeregistreerd copyWith(void Function(VerstrekkingsbeperkingGeregistreerd) updates) => super.copyWith((message) => updates(message as VerstrekkingsbeperkingGeregistreerd)) as VerstrekkingsbeperkingGeregistreerd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerstrekkingsbeperkingGeregistreerd create() => VerstrekkingsbeperkingGeregistreerd._();
  VerstrekkingsbeperkingGeregistreerd createEmptyInstance() => create();
  static $pb.PbList<VerstrekkingsbeperkingGeregistreerd> createRepeated() => $pb.PbList<VerstrekkingsbeperkingGeregistreerd>();
  @$core.pragma('dart2js:noInline')
  static VerstrekkingsbeperkingGeregistreerd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerstrekkingsbeperkingGeregistreerd>(create);
  static VerstrekkingsbeperkingGeregistreerd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Verstrekkingsbeperking get verstrekkingsbeperking => $_getN(1);
  @$pb.TagNumber(2)
  set verstrekkingsbeperking(Verstrekkingsbeperking v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerstrekkingsbeperking() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerstrekkingsbeperking() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get startdatumVerstrekkingsbeperking => $_getN(2);
  @$pb.TagNumber(3)
  set startdatumVerstrekkingsbeperking($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartdatumVerstrekkingsbeperking() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartdatumVerstrekkingsbeperking() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureStartdatumVerstrekkingsbeperking() => $_ensure(2);
}

class VerstrekkingsbeperkingGewijzigd extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VerstrekkingsbeperkingGewijzigd', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'events'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'persoonPr', $pb.PbFieldType.OY)
    ..e<Verstrekkingsbeperking>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'verstrekkingsbeperking', $pb.PbFieldType.OE, defaultOrMaker: Verstrekkingsbeperking.VERSTREKKINGSBEPERKING_Geen_beperking, valueOf: Verstrekkingsbeperking.valueOf, enumValues: Verstrekkingsbeperking.values)
    ..aOM<$0.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumVerstrekkingsbeperkingGewijzigd', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  VerstrekkingsbeperkingGewijzigd._() : super();
  factory VerstrekkingsbeperkingGewijzigd({
    $core.List<$core.int>? persoonPr,
    Verstrekkingsbeperking? verstrekkingsbeperking,
    $0.Timestamp? datumVerstrekkingsbeperkingGewijzigd,
  }) {
    final _result = create();
    if (persoonPr != null) {
      _result.persoonPr = persoonPr;
    }
    if (verstrekkingsbeperking != null) {
      _result.verstrekkingsbeperking = verstrekkingsbeperking;
    }
    if (datumVerstrekkingsbeperkingGewijzigd != null) {
      _result.datumVerstrekkingsbeperkingGewijzigd = datumVerstrekkingsbeperkingGewijzigd;
    }
    return _result;
  }
  factory VerstrekkingsbeperkingGewijzigd.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerstrekkingsbeperkingGewijzigd.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VerstrekkingsbeperkingGewijzigd clone() => VerstrekkingsbeperkingGewijzigd()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VerstrekkingsbeperkingGewijzigd copyWith(void Function(VerstrekkingsbeperkingGewijzigd) updates) => super.copyWith((message) => updates(message as VerstrekkingsbeperkingGewijzigd)) as VerstrekkingsbeperkingGewijzigd; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerstrekkingsbeperkingGewijzigd create() => VerstrekkingsbeperkingGewijzigd._();
  VerstrekkingsbeperkingGewijzigd createEmptyInstance() => create();
  static $pb.PbList<VerstrekkingsbeperkingGewijzigd> createRepeated() => $pb.PbList<VerstrekkingsbeperkingGewijzigd>();
  @$core.pragma('dart2js:noInline')
  static VerstrekkingsbeperkingGewijzigd getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerstrekkingsbeperkingGewijzigd>(create);
  static VerstrekkingsbeperkingGewijzigd? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get persoonPr => $_getN(0);
  @$pb.TagNumber(1)
  set persoonPr($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersoonPr() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersoonPr() => clearField(1);

  @$pb.TagNumber(2)
  Verstrekkingsbeperking get verstrekkingsbeperking => $_getN(1);
  @$pb.TagNumber(2)
  set verstrekkingsbeperking(Verstrekkingsbeperking v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasVerstrekkingsbeperking() => $_has(1);
  @$pb.TagNumber(2)
  void clearVerstrekkingsbeperking() => clearField(2);

  @$pb.TagNumber(3)
  $0.Timestamp get datumVerstrekkingsbeperkingGewijzigd => $_getN(2);
  @$pb.TagNumber(3)
  set datumVerstrekkingsbeperkingGewijzigd($0.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDatumVerstrekkingsbeperkingGewijzigd() => $_has(2);
  @$pb.TagNumber(3)
  void clearDatumVerstrekkingsbeperkingGewijzigd() => clearField(3);
  @$pb.TagNumber(3)
  $0.Timestamp ensureDatumVerstrekkingsbeperkingGewijzigd() => $_ensure(2);
}

