///
//  Generated code. Do not modify.
//  source: management-api/management/management.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use networkStatusRequestDescriptor instead')
const NetworkStatusRequest$json = const {
  '1': 'NetworkStatusRequest',
};

/// Descriptor for `NetworkStatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkStatusRequestDescriptor = $convert.base64Decode('ChROZXR3b3JrU3RhdHVzUmVxdWVzdA==');
@$core.Deprecated('Use networkStatusReplyDescriptor instead')
const NetworkStatusReply$json = const {
  '1': 'NetworkStatusReply',
  '2': const [
    const {'1': 'zones', '3': 1, '4': 3, '5': 11, '6': '.zone.ZoneStatus', '10': 'zones'},
  ],
};

/// Descriptor for `NetworkStatusReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkStatusReplyDescriptor = $convert.base64Decode('ChJOZXR3b3JrU3RhdHVzUmVwbHkSJgoFem9uZXMYASADKAsyEC56b25lLlpvbmVTdGF0dXNSBXpvbmVz');
@$core.Deprecated('Use eventsRequestDescriptor instead')
const EventsRequest$json = const {
  '1': 'EventsRequest',
};

/// Descriptor for `EventsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventsRequestDescriptor = $convert.base64Decode('Cg1FdmVudHNSZXF1ZXN0');
@$core.Deprecated('Use eventsReplyDescriptor instead')
const EventsReply$json = const {
  '1': 'EventsReply',
  '2': const [
    const {'1': 'events', '3': 1, '4': 3, '5': 11, '6': '.management.ManagementAPIEvent', '10': 'events'},
  ],
};

/// Descriptor for `EventsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List eventsReplyDescriptor = $convert.base64Decode('CgtFdmVudHNSZXBseRI2CgZldmVudHMYASADKAsyHi5tYW5hZ2VtZW50Lk1hbmFnZW1lbnRBUElFdmVudFIGZXZlbnRz');
@$core.Deprecated('Use journalsRequestDescriptor instead')
const JournalsRequest$json = const {
  '1': 'JournalsRequest',
};

/// Descriptor for `JournalsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List journalsRequestDescriptor = $convert.base64Decode('Cg9Kb3VybmFsc1JlcXVlc3Q=');
@$core.Deprecated('Use journalsReplyDescriptor instead')
const JournalsReply$json = const {
  '1': 'JournalsReply',
  '2': const [
    const {'1': 'journals', '3': 1, '4': 3, '5': 11, '6': '.management.Journal', '10': 'journals'},
  ],
};

/// Descriptor for `JournalsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List journalsReplyDescriptor = $convert.base64Decode('Cg1Kb3VybmFsc1JlcGx5Ei8KCGpvdXJuYWxzGAEgAygLMhMubWFuYWdlbWVudC5Kb3VybmFsUghqb3VybmFscw==');
@$core.Deprecated('Use managementAPIEventDescriptor instead')
const ManagementAPIEvent$json = const {
  '1': 'ManagementAPIEvent',
  '2': const [
    const {'1': 'zid', '3': 1, '4': 1, '5': 9, '10': 'zid'},
    const {'1': 'event', '3': 2, '4': 1, '5': 12, '10': 'event'},
  ],
};

/// Descriptor for `ManagementAPIEvent`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List managementAPIEventDescriptor = $convert.base64Decode('ChJNYW5hZ2VtZW50QVBJRXZlbnQSEAoDemlkGAEgASgJUgN6aWQSFAoFZXZlbnQYAiABKAxSBWV2ZW50');
@$core.Deprecated('Use journalDescriptor instead')
const Journal$json = const {
  '1': 'Journal',
  '2': const [
    const {'1': 'person_reference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
  ],
};

/// Descriptor for `Journal`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List journalDescriptor = $convert.base64Decode('CgdKb3VybmFsEikKEHBlcnNvbl9yZWZlcmVuY2UYASABKAxSD3BlcnNvblJlZmVyZW5jZQ==');
