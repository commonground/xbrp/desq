///
//  Generated code. Do not modify.
//  source: management-api/management/management.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../zone-api/zone/zone.pb.dart' as $0;

class NetworkStatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NetworkStatusRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  NetworkStatusRequest._() : super();
  factory NetworkStatusRequest() => create();
  factory NetworkStatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NetworkStatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NetworkStatusRequest clone() => NetworkStatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NetworkStatusRequest copyWith(void Function(NetworkStatusRequest) updates) => super.copyWith((message) => updates(message as NetworkStatusRequest)) as NetworkStatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NetworkStatusRequest create() => NetworkStatusRequest._();
  NetworkStatusRequest createEmptyInstance() => create();
  static $pb.PbList<NetworkStatusRequest> createRepeated() => $pb.PbList<NetworkStatusRequest>();
  @$core.pragma('dart2js:noInline')
  static NetworkStatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NetworkStatusRequest>(create);
  static NetworkStatusRequest? _defaultInstance;
}

class NetworkStatusReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NetworkStatusReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..pc<$0.ZoneStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zones', $pb.PbFieldType.PM, subBuilder: $0.ZoneStatus.create)
    ..hasRequiredFields = false
  ;

  NetworkStatusReply._() : super();
  factory NetworkStatusReply({
    $core.Iterable<$0.ZoneStatus>? zones,
  }) {
    final _result = create();
    if (zones != null) {
      _result.zones.addAll(zones);
    }
    return _result;
  }
  factory NetworkStatusReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NetworkStatusReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NetworkStatusReply clone() => NetworkStatusReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NetworkStatusReply copyWith(void Function(NetworkStatusReply) updates) => super.copyWith((message) => updates(message as NetworkStatusReply)) as NetworkStatusReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NetworkStatusReply create() => NetworkStatusReply._();
  NetworkStatusReply createEmptyInstance() => create();
  static $pb.PbList<NetworkStatusReply> createRepeated() => $pb.PbList<NetworkStatusReply>();
  @$core.pragma('dart2js:noInline')
  static NetworkStatusReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NetworkStatusReply>(create);
  static NetworkStatusReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$0.ZoneStatus> get zones => $_getList(0);
}

class EventsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EventsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  EventsRequest._() : super();
  factory EventsRequest() => create();
  factory EventsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EventsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EventsRequest clone() => EventsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EventsRequest copyWith(void Function(EventsRequest) updates) => super.copyWith((message) => updates(message as EventsRequest)) as EventsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EventsRequest create() => EventsRequest._();
  EventsRequest createEmptyInstance() => create();
  static $pb.PbList<EventsRequest> createRepeated() => $pb.PbList<EventsRequest>();
  @$core.pragma('dart2js:noInline')
  static EventsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EventsRequest>(create);
  static EventsRequest? _defaultInstance;
}

class EventsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'EventsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..pc<ManagementAPIEvent>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'events', $pb.PbFieldType.PM, subBuilder: ManagementAPIEvent.create)
    ..hasRequiredFields = false
  ;

  EventsReply._() : super();
  factory EventsReply({
    $core.Iterable<ManagementAPIEvent>? events,
  }) {
    final _result = create();
    if (events != null) {
      _result.events.addAll(events);
    }
    return _result;
  }
  factory EventsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EventsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EventsReply clone() => EventsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EventsReply copyWith(void Function(EventsReply) updates) => super.copyWith((message) => updates(message as EventsReply)) as EventsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static EventsReply create() => EventsReply._();
  EventsReply createEmptyInstance() => create();
  static $pb.PbList<EventsReply> createRepeated() => $pb.PbList<EventsReply>();
  @$core.pragma('dart2js:noInline')
  static EventsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EventsReply>(create);
  static EventsReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ManagementAPIEvent> get events => $_getList(0);
}

class JournalsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JournalsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  JournalsRequest._() : super();
  factory JournalsRequest() => create();
  factory JournalsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JournalsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JournalsRequest clone() => JournalsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JournalsRequest copyWith(void Function(JournalsRequest) updates) => super.copyWith((message) => updates(message as JournalsRequest)) as JournalsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JournalsRequest create() => JournalsRequest._();
  JournalsRequest createEmptyInstance() => create();
  static $pb.PbList<JournalsRequest> createRepeated() => $pb.PbList<JournalsRequest>();
  @$core.pragma('dart2js:noInline')
  static JournalsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JournalsRequest>(create);
  static JournalsRequest? _defaultInstance;
}

class JournalsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'JournalsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..pc<Journal>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journals', $pb.PbFieldType.PM, subBuilder: Journal.create)
    ..hasRequiredFields = false
  ;

  JournalsReply._() : super();
  factory JournalsReply({
    $core.Iterable<Journal>? journals,
  }) {
    final _result = create();
    if (journals != null) {
      _result.journals.addAll(journals);
    }
    return _result;
  }
  factory JournalsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JournalsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JournalsReply clone() => JournalsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JournalsReply copyWith(void Function(JournalsReply) updates) => super.copyWith((message) => updates(message as JournalsReply)) as JournalsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static JournalsReply create() => JournalsReply._();
  JournalsReply createEmptyInstance() => create();
  static $pb.PbList<JournalsReply> createRepeated() => $pb.PbList<JournalsReply>();
  @$core.pragma('dart2js:noInline')
  static JournalsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JournalsReply>(create);
  static JournalsReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Journal> get journals => $_getList(0);
}

class ManagementAPIEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ManagementAPIEvent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zid')
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'event', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  ManagementAPIEvent._() : super();
  factory ManagementAPIEvent({
    $core.String? zid,
    $core.List<$core.int>? event,
  }) {
    final _result = create();
    if (zid != null) {
      _result.zid = zid;
    }
    if (event != null) {
      _result.event = event;
    }
    return _result;
  }
  factory ManagementAPIEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ManagementAPIEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ManagementAPIEvent clone() => ManagementAPIEvent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ManagementAPIEvent copyWith(void Function(ManagementAPIEvent) updates) => super.copyWith((message) => updates(message as ManagementAPIEvent)) as ManagementAPIEvent; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ManagementAPIEvent create() => ManagementAPIEvent._();
  ManagementAPIEvent createEmptyInstance() => create();
  static $pb.PbList<ManagementAPIEvent> createRepeated() => $pb.PbList<ManagementAPIEvent>();
  @$core.pragma('dart2js:noInline')
  static ManagementAPIEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ManagementAPIEvent>(create);
  static ManagementAPIEvent? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get zid => $_getSZ(0);
  @$pb.TagNumber(1)
  set zid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasZid() => $_has(0);
  @$pb.TagNumber(1)
  void clearZid() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get event => $_getN(1);
  @$pb.TagNumber(2)
  set event($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEvent() => $_has(1);
  @$pb.TagNumber(2)
  void clearEvent() => clearField(2);
}

class Journal extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Journal', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'management'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  Journal._() : super();
  factory Journal({
    $core.List<$core.int>? personReference,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    return _result;
  }
  factory Journal.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Journal.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Journal clone() => Journal()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Journal copyWith(void Function(Journal) updates) => super.copyWith((message) => updates(message as Journal)) as Journal; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Journal create() => Journal._();
  Journal createEmptyInstance() => create();
  static $pb.PbList<Journal> createRepeated() => $pb.PbList<Journal>();
  @$core.pragma('dart2js:noInline')
  static Journal getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Journal>(create);
  static Journal? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);
}

