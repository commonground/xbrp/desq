///
//  Generated code. Do not modify.
//  source: management-api/management/management.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import '../../zone-api/zone/zone.pb.dart' as $0;
import 'management.pb.dart' as $1;
export 'management.pb.dart';

class ManagementClient extends $grpc.Client {
  static final _$zoneStatus =
      $grpc.ClientMethod<$0.ZoneStatusRequest, $0.ZoneStatusReply>(
          '/management.Management/ZoneStatus',
          ($0.ZoneStatusRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ZoneStatusReply.fromBuffer(value));
  static final _$networkStatus =
      $grpc.ClientMethod<$1.NetworkStatusRequest, $1.NetworkStatusReply>(
          '/management.Management/NetworkStatus',
          ($1.NetworkStatusRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.NetworkStatusReply.fromBuffer(value));
  static final _$events = $grpc.ClientMethod<$1.EventsRequest, $1.EventsReply>(
      '/management.Management/Events',
      ($1.EventsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.EventsReply.fromBuffer(value));
  static final _$journals =
      $grpc.ClientMethod<$1.JournalsRequest, $1.JournalsReply>(
          '/management.Management/Journals',
          ($1.JournalsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.JournalsReply.fromBuffer(value));

  ManagementClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ZoneStatusReply> zoneStatus(
      $0.ZoneStatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$zoneStatus, request, options: options);
  }

  $grpc.ResponseFuture<$1.NetworkStatusReply> networkStatus(
      $1.NetworkStatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$networkStatus, request, options: options);
  }

  $grpc.ResponseFuture<$1.EventsReply> events($1.EventsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$events, request, options: options);
  }

  $grpc.ResponseFuture<$1.JournalsReply> journals($1.JournalsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$journals, request, options: options);
  }
}

abstract class ManagementServiceBase extends $grpc.Service {
  $core.String get $name => 'management.Management';

  ManagementServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.ZoneStatusRequest, $0.ZoneStatusReply>(
        'ZoneStatus',
        zoneStatus_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ZoneStatusRequest.fromBuffer(value),
        ($0.ZoneStatusReply value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$1.NetworkStatusRequest, $1.NetworkStatusReply>(
            'NetworkStatus',
            networkStatus_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $1.NetworkStatusRequest.fromBuffer(value),
            ($1.NetworkStatusReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.EventsRequest, $1.EventsReply>(
        'Events',
        events_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.EventsRequest.fromBuffer(value),
        ($1.EventsReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.JournalsRequest, $1.JournalsReply>(
        'Journals',
        journals_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.JournalsRequest.fromBuffer(value),
        ($1.JournalsReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.ZoneStatusReply> zoneStatus_Pre($grpc.ServiceCall call,
      $async.Future<$0.ZoneStatusRequest> request) async {
    return zoneStatus(call, await request);
  }

  $async.Future<$1.NetworkStatusReply> networkStatus_Pre($grpc.ServiceCall call,
      $async.Future<$1.NetworkStatusRequest> request) async {
    return networkStatus(call, await request);
  }

  $async.Future<$1.EventsReply> events_Pre(
      $grpc.ServiceCall call, $async.Future<$1.EventsRequest> request) async {
    return events(call, await request);
  }

  $async.Future<$1.JournalsReply> journals_Pre(
      $grpc.ServiceCall call, $async.Future<$1.JournalsRequest> request) async {
    return journals(call, await request);
  }

  $async.Future<$0.ZoneStatusReply> zoneStatus(
      $grpc.ServiceCall call, $0.ZoneStatusRequest request);
  $async.Future<$1.NetworkStatusReply> networkStatus(
      $grpc.ServiceCall call, $1.NetworkStatusRequest request);
  $async.Future<$1.EventsReply> events(
      $grpc.ServiceCall call, $1.EventsRequest request);
  $async.Future<$1.JournalsReply> journals(
      $grpc.ServiceCall call, $1.JournalsRequest request);
}
