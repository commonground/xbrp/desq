///
//  Generated code. Do not modify.
//  source: query-api/query/query.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'query.pb.dart' as $0;
export 'query.pb.dart';

class QueryClient extends $grpc.Client {
  static final _$birthDetails =
      $grpc.ClientMethod<$0.BirthDetailsRequest, $0.BirthDetailsReply>(
          '/query.Query/BirthDetails',
          ($0.BirthDetailsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.BirthDetailsReply.fromBuffer(value));
  static final _$nameDetails =
      $grpc.ClientMethod<$0.NameDetailsRequest, $0.NameDetailsReply>(
          '/query.Query/NameDetails',
          ($0.NameDetailsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.NameDetailsReply.fromBuffer(value));
  static final _$residentialAddress = $grpc.ClientMethod<
          $0.ResidentialAddressRequest, $0.ResidentialAddressReply>(
      '/query.Query/ResidentialAddress',
      ($0.ResidentialAddressRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ResidentialAddressReply.fromBuffer(value));
  static final _$marriageDetails =
      $grpc.ClientMethod<$0.MarriageDetailsRequest, $0.MarriageDetailsReply>(
          '/query.Query/MarriageDetails',
          ($0.MarriageDetailsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.MarriageDetailsReply.fromBuffer(value));
  static final _$personDetails =
      $grpc.ClientMethod<$0.PersonDetailsRequest, $0.PersonDetailsReply>(
          '/query.Query/PersonDetails',
          ($0.PersonDetailsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.PersonDetailsReply.fromBuffer(value));

  QueryClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.BirthDetailsReply> birthDetails(
      $0.BirthDetailsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$birthDetails, request, options: options);
  }

  $grpc.ResponseFuture<$0.NameDetailsReply> nameDetails(
      $0.NameDetailsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nameDetails, request, options: options);
  }

  $grpc.ResponseFuture<$0.ResidentialAddressReply> residentialAddress(
      $0.ResidentialAddressRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$residentialAddress, request, options: options);
  }

  $grpc.ResponseFuture<$0.MarriageDetailsReply> marriageDetails(
      $0.MarriageDetailsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$marriageDetails, request, options: options);
  }

  $grpc.ResponseFuture<$0.PersonDetailsReply> personDetails(
      $0.PersonDetailsRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$personDetails, request, options: options);
  }
}

abstract class QueryServiceBase extends $grpc.Service {
  $core.String get $name => 'query.Query';

  QueryServiceBase() {
    $addMethod(
        $grpc.ServiceMethod<$0.BirthDetailsRequest, $0.BirthDetailsReply>(
            'BirthDetails',
            birthDetails_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.BirthDetailsRequest.fromBuffer(value),
            ($0.BirthDetailsReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.NameDetailsRequest, $0.NameDetailsReply>(
        'NameDetails',
        nameDetails_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.NameDetailsRequest.fromBuffer(value),
        ($0.NameDetailsReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ResidentialAddressRequest,
            $0.ResidentialAddressReply>(
        'ResidentialAddress',
        residentialAddress_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ResidentialAddressRequest.fromBuffer(value),
        ($0.ResidentialAddressReply value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.MarriageDetailsRequest, $0.MarriageDetailsReply>(
            'MarriageDetails',
            marriageDetails_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.MarriageDetailsRequest.fromBuffer(value),
            ($0.MarriageDetailsReply value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.PersonDetailsRequest, $0.PersonDetailsReply>(
            'PersonDetails',
            personDetails_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.PersonDetailsRequest.fromBuffer(value),
            ($0.PersonDetailsReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.BirthDetailsReply> birthDetails_Pre($grpc.ServiceCall call,
      $async.Future<$0.BirthDetailsRequest> request) async {
    return birthDetails(call, await request);
  }

  $async.Future<$0.NameDetailsReply> nameDetails_Pre($grpc.ServiceCall call,
      $async.Future<$0.NameDetailsRequest> request) async {
    return nameDetails(call, await request);
  }

  $async.Future<$0.ResidentialAddressReply> residentialAddress_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ResidentialAddressRequest> request) async {
    return residentialAddress(call, await request);
  }

  $async.Future<$0.MarriageDetailsReply> marriageDetails_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.MarriageDetailsRequest> request) async {
    return marriageDetails(call, await request);
  }

  $async.Future<$0.PersonDetailsReply> personDetails_Pre($grpc.ServiceCall call,
      $async.Future<$0.PersonDetailsRequest> request) async {
    return personDetails(call, await request);
  }

  $async.Future<$0.BirthDetailsReply> birthDetails(
      $grpc.ServiceCall call, $0.BirthDetailsRequest request);
  $async.Future<$0.NameDetailsReply> nameDetails(
      $grpc.ServiceCall call, $0.NameDetailsRequest request);
  $async.Future<$0.ResidentialAddressReply> residentialAddress(
      $grpc.ServiceCall call, $0.ResidentialAddressRequest request);
  $async.Future<$0.MarriageDetailsReply> marriageDetails(
      $grpc.ServiceCall call, $0.MarriageDetailsRequest request);
  $async.Future<$0.PersonDetailsReply> personDetails(
      $grpc.ServiceCall call, $0.PersonDetailsRequest request);
}
