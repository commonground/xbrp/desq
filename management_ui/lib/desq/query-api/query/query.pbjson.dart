///
//  Generated code. Do not modify.
//  source: query-api/query/query.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use aggregationQueryDescriptor instead')
const AggregationQuery$json = const {
  '1': 'AggregationQuery',
  '2': const [
    const {'1': 'person_identifier', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery.PersonIdentifier', '10': 'personIdentifier'},
    const {'1': 'timetravel', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'timetravel'},
  ],
  '3': const [AggregationQuery_PersonIdentifier$json],
};

@$core.Deprecated('Use aggregationQueryDescriptor instead')
const AggregationQuery_PersonIdentifier$json = const {
  '1': 'PersonIdentifier',
  '2': const [
    const {'1': 'eid', '3': 1, '4': 1, '5': 9, '9': 0, '10': 'eid'},
    const {'1': 'bsn', '3': 2, '4': 1, '5': 9, '9': 0, '10': 'bsn'},
  ],
  '8': const [
    const {'1': 'person_identifier'},
  ],
};

/// Descriptor for `AggregationQuery`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List aggregationQueryDescriptor = $convert.base64Decode('ChBBZ2dyZWdhdGlvblF1ZXJ5ElUKEXBlcnNvbl9pZGVudGlmaWVyGAEgASgLMigucXVlcnkuQWdncmVnYXRpb25RdWVyeS5QZXJzb25JZGVudGlmaWVyUhBwZXJzb25JZGVudGlmaWVyEjoKCnRpbWV0cmF2ZWwYAiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgp0aW1ldHJhdmVsGk8KEFBlcnNvbklkZW50aWZpZXISEgoDZWlkGAEgASgJSABSA2VpZBISCgNic24YAiABKAlIAFIDYnNuQhMKEXBlcnNvbl9pZGVudGlmaWVy');
@$core.Deprecated('Use birthDetailsRequestDescriptor instead')
const BirthDetailsRequest$json = const {
  '1': 'BirthDetailsRequest',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery', '10': 'query'},
  ],
};

/// Descriptor for `BirthDetailsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List birthDetailsRequestDescriptor = $convert.base64Decode('ChNCaXJ0aERldGFpbHNSZXF1ZXN0Ei0KBXF1ZXJ5GAEgASgLMhcucXVlcnkuQWdncmVnYXRpb25RdWVyeVIFcXVlcnk=');
@$core.Deprecated('Use birthDetailsDescriptor instead')
const BirthDetails$json = const {
  '1': 'BirthDetails',
  '2': const [
    const {'1': 'birthdate', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'birthdate'},
    const {'1': 'birthplace', '3': 2, '4': 1, '5': 9, '10': 'birthplace'},
    const {'1': 'birthcountry', '3': 3, '4': 1, '5': 9, '10': 'birthcountry'},
    const {'1': 'parents', '3': 4, '4': 3, '5': 9, '10': 'parents'},
  ],
};

/// Descriptor for `BirthDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List birthDetailsDescriptor = $convert.base64Decode('CgxCaXJ0aERldGFpbHMSOAoJYmlydGhkYXRlGAEgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIJYmlydGhkYXRlEh4KCmJpcnRocGxhY2UYAiABKAlSCmJpcnRocGxhY2USIgoMYmlydGhjb3VudHJ5GAMgASgJUgxiaXJ0aGNvdW50cnkSGAoHcGFyZW50cxgEIAMoCVIHcGFyZW50cw==');
@$core.Deprecated('Use birthDetailsReplyDescriptor instead')
const BirthDetailsReply$json = const {
  '1': 'BirthDetailsReply',
  '2': const [
    const {'1': 'birthDetails', '3': 1, '4': 1, '5': 11, '6': '.query.BirthDetails', '10': 'birthDetails'},
  ],
};

/// Descriptor for `BirthDetailsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List birthDetailsReplyDescriptor = $convert.base64Decode('ChFCaXJ0aERldGFpbHNSZXBseRI3CgxiaXJ0aERldGFpbHMYASABKAsyEy5xdWVyeS5CaXJ0aERldGFpbHNSDGJpcnRoRGV0YWlscw==');
@$core.Deprecated('Use marriageDetailsRequestDescriptor instead')
const MarriageDetailsRequest$json = const {
  '1': 'MarriageDetailsRequest',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery', '10': 'query'},
  ],
};

/// Descriptor for `MarriageDetailsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List marriageDetailsRequestDescriptor = $convert.base64Decode('ChZNYXJyaWFnZURldGFpbHNSZXF1ZXN0Ei0KBXF1ZXJ5GAEgASgLMhcucXVlcnkuQWdncmVnYXRpb25RdWVyeVIFcXVlcnk=');
@$core.Deprecated('Use marriageDetailsDescriptor instead')
const MarriageDetails$json = const {
  '1': 'MarriageDetails',
  '2': const [
    const {'1': 'participants', '3': 1, '4': 3, '5': 9, '10': 'participants'},
    const {'1': 'date', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'date'},
    const {'1': 'place', '3': 3, '4': 1, '5': 9, '10': 'place'},
    const {'1': 'country', '3': 4, '4': 1, '5': 9, '10': 'country'},
  ],
};

/// Descriptor for `MarriageDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List marriageDetailsDescriptor = $convert.base64Decode('Cg9NYXJyaWFnZURldGFpbHMSIgoMcGFydGljaXBhbnRzGAEgAygJUgxwYXJ0aWNpcGFudHMSLgoEZGF0ZRgCIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSBGRhdGUSFAoFcGxhY2UYAyABKAlSBXBsYWNlEhgKB2NvdW50cnkYBCABKAlSB2NvdW50cnk=');
@$core.Deprecated('Use marriageDetailsReplyDescriptor instead')
const MarriageDetailsReply$json = const {
  '1': 'MarriageDetailsReply',
  '2': const [
    const {'1': 'marriageDetails', '3': 1, '4': 1, '5': 11, '6': '.query.MarriageDetails', '10': 'marriageDetails'},
  ],
};

/// Descriptor for `MarriageDetailsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List marriageDetailsReplyDescriptor = $convert.base64Decode('ChRNYXJyaWFnZURldGFpbHNSZXBseRJACg9tYXJyaWFnZURldGFpbHMYASABKAsyFi5xdWVyeS5NYXJyaWFnZURldGFpbHNSD21hcnJpYWdlRGV0YWlscw==');
@$core.Deprecated('Use nameDetailsRequestDescriptor instead')
const NameDetailsRequest$json = const {
  '1': 'NameDetailsRequest',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery', '10': 'query'},
  ],
};

/// Descriptor for `NameDetailsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nameDetailsRequestDescriptor = $convert.base64Decode('ChJOYW1lRGV0YWlsc1JlcXVlc3QSLQoFcXVlcnkYASABKAsyFy5xdWVyeS5BZ2dyZWdhdGlvblF1ZXJ5UgVxdWVyeQ==');
@$core.Deprecated('Use nameDetailsDescriptor instead')
const NameDetails$json = const {
  '1': 'NameDetails',
  '2': const [
    const {'1': 'firstnames', '3': 1, '4': 1, '5': 9, '10': 'firstnames'},
    const {'1': 'lastnamePrefix', '3': 2, '4': 1, '5': 9, '10': 'lastnamePrefix'},
    const {'1': 'lastnames', '3': 3, '4': 1, '5': 9, '10': 'lastnames'},
    const {'1': 'namePreference', '3': 4, '4': 1, '5': 9, '10': 'namePreference'},
  ],
};

/// Descriptor for `NameDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nameDetailsDescriptor = $convert.base64Decode('CgtOYW1lRGV0YWlscxIeCgpmaXJzdG5hbWVzGAEgASgJUgpmaXJzdG5hbWVzEiYKDmxhc3RuYW1lUHJlZml4GAIgASgJUg5sYXN0bmFtZVByZWZpeBIcCglsYXN0bmFtZXMYAyABKAlSCWxhc3RuYW1lcxImCg5uYW1lUHJlZmVyZW5jZRgEIAEoCVIObmFtZVByZWZlcmVuY2U=');
@$core.Deprecated('Use nameDetailsReplyDescriptor instead')
const NameDetailsReply$json = const {
  '1': 'NameDetailsReply',
  '2': const [
    const {'1': 'nameDetails', '3': 1, '4': 1, '5': 11, '6': '.query.NameDetails', '10': 'nameDetails'},
  ],
};

/// Descriptor for `NameDetailsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nameDetailsReplyDescriptor = $convert.base64Decode('ChBOYW1lRGV0YWlsc1JlcGx5EjQKC25hbWVEZXRhaWxzGAEgASgLMhIucXVlcnkuTmFtZURldGFpbHNSC25hbWVEZXRhaWxz');
@$core.Deprecated('Use residentialAddressRequestDescriptor instead')
const ResidentialAddressRequest$json = const {
  '1': 'ResidentialAddressRequest',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery', '10': 'query'},
  ],
};

/// Descriptor for `ResidentialAddressRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List residentialAddressRequestDescriptor = $convert.base64Decode('ChlSZXNpZGVudGlhbEFkZHJlc3NSZXF1ZXN0Ei0KBXF1ZXJ5GAEgASgLMhcucXVlcnkuQWdncmVnYXRpb25RdWVyeVIFcXVlcnk=');
@$core.Deprecated('Use residentialAddressDescriptor instead')
const ResidentialAddress$json = const {
  '1': 'ResidentialAddress',
  '2': const [
    const {'1': 'address', '3': 1, '4': 1, '5': 9, '10': 'address'},
  ],
};

/// Descriptor for `ResidentialAddress`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List residentialAddressDescriptor = $convert.base64Decode('ChJSZXNpZGVudGlhbEFkZHJlc3MSGAoHYWRkcmVzcxgBIAEoCVIHYWRkcmVzcw==');
@$core.Deprecated('Use residentialAddressReplyDescriptor instead')
const ResidentialAddressReply$json = const {
  '1': 'ResidentialAddressReply',
  '2': const [
    const {'1': 'residentialAddress', '3': 1, '4': 1, '5': 11, '6': '.query.ResidentialAddress', '10': 'residentialAddress'},
  ],
};

/// Descriptor for `ResidentialAddressReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List residentialAddressReplyDescriptor = $convert.base64Decode('ChdSZXNpZGVudGlhbEFkZHJlc3NSZXBseRJJChJyZXNpZGVudGlhbEFkZHJlc3MYASABKAsyGS5xdWVyeS5SZXNpZGVudGlhbEFkZHJlc3NSEnJlc2lkZW50aWFsQWRkcmVzcw==');
@$core.Deprecated('Use personDetailsRequestDescriptor instead')
const PersonDetailsRequest$json = const {
  '1': 'PersonDetailsRequest',
  '2': const [
    const {'1': 'query', '3': 1, '4': 1, '5': 11, '6': '.query.AggregationQuery', '10': 'query'},
  ],
};

/// Descriptor for `PersonDetailsRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List personDetailsRequestDescriptor = $convert.base64Decode('ChRQZXJzb25EZXRhaWxzUmVxdWVzdBItCgVxdWVyeRgBIAEoCzIXLnF1ZXJ5LkFnZ3JlZ2F0aW9uUXVlcnlSBXF1ZXJ5');
@$core.Deprecated('Use identifiersDescriptor instead')
const Identifiers$json = const {
  '1': 'Identifiers',
  '2': const [
    const {'1': 'zidEid', '3': 1, '4': 3, '5': 11, '6': '.query.Identifiers.ZidEidEntry', '10': 'zidEid'},
    const {'1': 'bsn', '3': 2, '4': 1, '5': 9, '10': 'bsn'},
  ],
  '3': const [Identifiers_ZidEidEntry$json],
};

@$core.Deprecated('Use identifiersDescriptor instead')
const Identifiers_ZidEidEntry$json = const {
  '1': 'ZidEidEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `Identifiers`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List identifiersDescriptor = $convert.base64Decode('CgtJZGVudGlmaWVycxI2CgZ6aWRFaWQYASADKAsyHi5xdWVyeS5JZGVudGlmaWVycy5aaWRFaWRFbnRyeVIGemlkRWlkEhAKA2JzbhgCIAEoCVIDYnNuGjkKC1ppZEVpZEVudHJ5EhAKA2tleRgBIAEoCVIDa2V5EhQKBXZhbHVlGAIgASgJUgV2YWx1ZToCOAE=');
@$core.Deprecated('Use legalDetailsDescriptor instead')
const LegalDetails$json = const {
  '1': 'LegalDetails',
  '2': const [
    const {'1': 'nationality', '3': 1, '4': 1, '5': 9, '10': 'nationality'},
  ],
};

/// Descriptor for `LegalDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List legalDetailsDescriptor = $convert.base64Decode('CgxMZWdhbERldGFpbHMSIAoLbmF0aW9uYWxpdHkYASABKAlSC25hdGlvbmFsaXR5');
@$core.Deprecated('Use personDetailsReplyDescriptor instead')
const PersonDetailsReply$json = const {
  '1': 'PersonDetailsReply',
  '2': const [
    const {'1': 'identifiers', '3': 1, '4': 1, '5': 11, '6': '.query.Identifiers', '10': 'identifiers'},
    const {'1': 'birthDetails', '3': 2, '4': 1, '5': 11, '6': '.query.BirthDetails', '10': 'birthDetails'},
    const {'1': 'nameDetails', '3': 3, '4': 1, '5': 11, '6': '.query.NameDetails', '10': 'nameDetails'},
    const {'1': 'residentialAddress', '3': 4, '4': 1, '5': 11, '6': '.query.ResidentialAddress', '10': 'residentialAddress'},
    const {'1': 'marriageDetails', '3': 5, '4': 1, '5': 11, '6': '.query.MarriageDetails', '10': 'marriageDetails'},
    const {'1': 'legalDetails', '3': 6, '4': 1, '5': 11, '6': '.query.LegalDetails', '10': 'legalDetails'},
    const {'1': 'municipalityOfRegistration', '3': 7, '4': 1, '5': 9, '10': 'municipalityOfRegistration'},
  ],
};

/// Descriptor for `PersonDetailsReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List personDetailsReplyDescriptor = $convert.base64Decode('ChJQZXJzb25EZXRhaWxzUmVwbHkSNAoLaWRlbnRpZmllcnMYASABKAsyEi5xdWVyeS5JZGVudGlmaWVyc1ILaWRlbnRpZmllcnMSNwoMYmlydGhEZXRhaWxzGAIgASgLMhMucXVlcnkuQmlydGhEZXRhaWxzUgxiaXJ0aERldGFpbHMSNAoLbmFtZURldGFpbHMYAyABKAsyEi5xdWVyeS5OYW1lRGV0YWlsc1ILbmFtZURldGFpbHMSSQoScmVzaWRlbnRpYWxBZGRyZXNzGAQgASgLMhkucXVlcnkuUmVzaWRlbnRpYWxBZGRyZXNzUhJyZXNpZGVudGlhbEFkZHJlc3MSQAoPbWFycmlhZ2VEZXRhaWxzGAUgASgLMhYucXVlcnkuTWFycmlhZ2VEZXRhaWxzUg9tYXJyaWFnZURldGFpbHMSNwoMbGVnYWxEZXRhaWxzGAYgASgLMhMucXVlcnkuTGVnYWxEZXRhaWxzUgxsZWdhbERldGFpbHMSPgoabXVuaWNpcGFsaXR5T2ZSZWdpc3RyYXRpb24YByABKAlSGm11bmljaXBhbGl0eU9mUmVnaXN0cmF0aW9u');
