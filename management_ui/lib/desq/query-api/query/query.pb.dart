///
//  Generated code. Do not modify.
//  source: query-api/query/query.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../google/protobuf/timestamp.pb.dart' as $1;

enum AggregationQuery_PersonIdentifier_PersonIdentifier {
  eid, 
  bsn, 
  notSet
}

class AggregationQuery_PersonIdentifier extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, AggregationQuery_PersonIdentifier_PersonIdentifier> _AggregationQuery_PersonIdentifier_PersonIdentifierByTag = {
    1 : AggregationQuery_PersonIdentifier_PersonIdentifier.eid,
    2 : AggregationQuery_PersonIdentifier_PersonIdentifier.bsn,
    0 : AggregationQuery_PersonIdentifier_PersonIdentifier.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AggregationQuery.PersonIdentifier', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..oo(0, [1, 2])
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eid')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bsn')
    ..hasRequiredFields = false
  ;

  AggregationQuery_PersonIdentifier._() : super();
  factory AggregationQuery_PersonIdentifier({
    $core.String? eid,
    $core.String? bsn,
  }) {
    final _result = create();
    if (eid != null) {
      _result.eid = eid;
    }
    if (bsn != null) {
      _result.bsn = bsn;
    }
    return _result;
  }
  factory AggregationQuery_PersonIdentifier.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AggregationQuery_PersonIdentifier.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AggregationQuery_PersonIdentifier clone() => AggregationQuery_PersonIdentifier()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AggregationQuery_PersonIdentifier copyWith(void Function(AggregationQuery_PersonIdentifier) updates) => super.copyWith((message) => updates(message as AggregationQuery_PersonIdentifier)) as AggregationQuery_PersonIdentifier; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AggregationQuery_PersonIdentifier create() => AggregationQuery_PersonIdentifier._();
  AggregationQuery_PersonIdentifier createEmptyInstance() => create();
  static $pb.PbList<AggregationQuery_PersonIdentifier> createRepeated() => $pb.PbList<AggregationQuery_PersonIdentifier>();
  @$core.pragma('dart2js:noInline')
  static AggregationQuery_PersonIdentifier getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AggregationQuery_PersonIdentifier>(create);
  static AggregationQuery_PersonIdentifier? _defaultInstance;

  AggregationQuery_PersonIdentifier_PersonIdentifier whichPersonIdentifier() => _AggregationQuery_PersonIdentifier_PersonIdentifierByTag[$_whichOneof(0)]!;
  void clearPersonIdentifier() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  $core.String get eid => $_getSZ(0);
  @$pb.TagNumber(1)
  set eid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEid() => $_has(0);
  @$pb.TagNumber(1)
  void clearEid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get bsn => $_getSZ(1);
  @$pb.TagNumber(2)
  set bsn($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBsn() => $_has(1);
  @$pb.TagNumber(2)
  void clearBsn() => clearField(2);
}

class AggregationQuery extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AggregationQuery', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery_PersonIdentifier>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personIdentifier', subBuilder: AggregationQuery_PersonIdentifier.create)
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timetravel', subBuilder: $1.Timestamp.create)
    ..hasRequiredFields = false
  ;

  AggregationQuery._() : super();
  factory AggregationQuery({
    AggregationQuery_PersonIdentifier? personIdentifier,
    $1.Timestamp? timetravel,
  }) {
    final _result = create();
    if (personIdentifier != null) {
      _result.personIdentifier = personIdentifier;
    }
    if (timetravel != null) {
      _result.timetravel = timetravel;
    }
    return _result;
  }
  factory AggregationQuery.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AggregationQuery.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AggregationQuery clone() => AggregationQuery()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AggregationQuery copyWith(void Function(AggregationQuery) updates) => super.copyWith((message) => updates(message as AggregationQuery)) as AggregationQuery; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AggregationQuery create() => AggregationQuery._();
  AggregationQuery createEmptyInstance() => create();
  static $pb.PbList<AggregationQuery> createRepeated() => $pb.PbList<AggregationQuery>();
  @$core.pragma('dart2js:noInline')
  static AggregationQuery getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AggregationQuery>(create);
  static AggregationQuery? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery_PersonIdentifier get personIdentifier => $_getN(0);
  @$pb.TagNumber(1)
  set personIdentifier(AggregationQuery_PersonIdentifier v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonIdentifier() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonIdentifier() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery_PersonIdentifier ensurePersonIdentifier() => $_ensure(0);

  @$pb.TagNumber(2)
  $1.Timestamp get timetravel => $_getN(1);
  @$pb.TagNumber(2)
  set timetravel($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTimetravel() => $_has(1);
  @$pb.TagNumber(2)
  void clearTimetravel() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureTimetravel() => $_ensure(1);
}

class BirthDetailsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BirthDetailsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'query', subBuilder: AggregationQuery.create)
    ..hasRequiredFields = false
  ;

  BirthDetailsRequest._() : super();
  factory BirthDetailsRequest({
    AggregationQuery? query,
  }) {
    final _result = create();
    if (query != null) {
      _result.query = query;
    }
    return _result;
  }
  factory BirthDetailsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BirthDetailsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BirthDetailsRequest clone() => BirthDetailsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BirthDetailsRequest copyWith(void Function(BirthDetailsRequest) updates) => super.copyWith((message) => updates(message as BirthDetailsRequest)) as BirthDetailsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BirthDetailsRequest create() => BirthDetailsRequest._();
  BirthDetailsRequest createEmptyInstance() => create();
  static $pb.PbList<BirthDetailsRequest> createRepeated() => $pb.PbList<BirthDetailsRequest>();
  @$core.pragma('dart2js:noInline')
  static BirthDetailsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BirthDetailsRequest>(create);
  static BirthDetailsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery get query => $_getN(0);
  @$pb.TagNumber(1)
  set query(AggregationQuery v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery ensureQuery() => $_ensure(0);
}

class BirthDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BirthDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<$1.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthdate', subBuilder: $1.Timestamp.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthplace')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthcountry')
    ..pPS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'parents')
    ..hasRequiredFields = false
  ;

  BirthDetails._() : super();
  factory BirthDetails({
    $1.Timestamp? birthdate,
    $core.String? birthplace,
    $core.String? birthcountry,
    $core.Iterable<$core.String>? parents,
  }) {
    final _result = create();
    if (birthdate != null) {
      _result.birthdate = birthdate;
    }
    if (birthplace != null) {
      _result.birthplace = birthplace;
    }
    if (birthcountry != null) {
      _result.birthcountry = birthcountry;
    }
    if (parents != null) {
      _result.parents.addAll(parents);
    }
    return _result;
  }
  factory BirthDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BirthDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BirthDetails clone() => BirthDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BirthDetails copyWith(void Function(BirthDetails) updates) => super.copyWith((message) => updates(message as BirthDetails)) as BirthDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BirthDetails create() => BirthDetails._();
  BirthDetails createEmptyInstance() => create();
  static $pb.PbList<BirthDetails> createRepeated() => $pb.PbList<BirthDetails>();
  @$core.pragma('dart2js:noInline')
  static BirthDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BirthDetails>(create);
  static BirthDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $1.Timestamp get birthdate => $_getN(0);
  @$pb.TagNumber(1)
  set birthdate($1.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasBirthdate() => $_has(0);
  @$pb.TagNumber(1)
  void clearBirthdate() => clearField(1);
  @$pb.TagNumber(1)
  $1.Timestamp ensureBirthdate() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get birthplace => $_getSZ(1);
  @$pb.TagNumber(2)
  set birthplace($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBirthplace() => $_has(1);
  @$pb.TagNumber(2)
  void clearBirthplace() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get birthcountry => $_getSZ(2);
  @$pb.TagNumber(3)
  set birthcountry($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasBirthcountry() => $_has(2);
  @$pb.TagNumber(3)
  void clearBirthcountry() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.String> get parents => $_getList(3);
}

class BirthDetailsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BirthDetailsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<BirthDetails>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthDetails', protoName: 'birthDetails', subBuilder: BirthDetails.create)
    ..hasRequiredFields = false
  ;

  BirthDetailsReply._() : super();
  factory BirthDetailsReply({
    BirthDetails? birthDetails,
  }) {
    final _result = create();
    if (birthDetails != null) {
      _result.birthDetails = birthDetails;
    }
    return _result;
  }
  factory BirthDetailsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BirthDetailsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BirthDetailsReply clone() => BirthDetailsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BirthDetailsReply copyWith(void Function(BirthDetailsReply) updates) => super.copyWith((message) => updates(message as BirthDetailsReply)) as BirthDetailsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BirthDetailsReply create() => BirthDetailsReply._();
  BirthDetailsReply createEmptyInstance() => create();
  static $pb.PbList<BirthDetailsReply> createRepeated() => $pb.PbList<BirthDetailsReply>();
  @$core.pragma('dart2js:noInline')
  static BirthDetailsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BirthDetailsReply>(create);
  static BirthDetailsReply? _defaultInstance;

  @$pb.TagNumber(1)
  BirthDetails get birthDetails => $_getN(0);
  @$pb.TagNumber(1)
  set birthDetails(BirthDetails v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasBirthDetails() => $_has(0);
  @$pb.TagNumber(1)
  void clearBirthDetails() => clearField(1);
  @$pb.TagNumber(1)
  BirthDetails ensureBirthDetails() => $_ensure(0);
}

class MarriageDetailsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MarriageDetailsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'query', subBuilder: AggregationQuery.create)
    ..hasRequiredFields = false
  ;

  MarriageDetailsRequest._() : super();
  factory MarriageDetailsRequest({
    AggregationQuery? query,
  }) {
    final _result = create();
    if (query != null) {
      _result.query = query;
    }
    return _result;
  }
  factory MarriageDetailsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MarriageDetailsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MarriageDetailsRequest clone() => MarriageDetailsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MarriageDetailsRequest copyWith(void Function(MarriageDetailsRequest) updates) => super.copyWith((message) => updates(message as MarriageDetailsRequest)) as MarriageDetailsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MarriageDetailsRequest create() => MarriageDetailsRequest._();
  MarriageDetailsRequest createEmptyInstance() => create();
  static $pb.PbList<MarriageDetailsRequest> createRepeated() => $pb.PbList<MarriageDetailsRequest>();
  @$core.pragma('dart2js:noInline')
  static MarriageDetailsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MarriageDetailsRequest>(create);
  static MarriageDetailsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery get query => $_getN(0);
  @$pb.TagNumber(1)
  set query(AggregationQuery v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery ensureQuery() => $_ensure(0);
}

class MarriageDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MarriageDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..pPS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'participants')
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'date', subBuilder: $1.Timestamp.create)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'place')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'country')
    ..hasRequiredFields = false
  ;

  MarriageDetails._() : super();
  factory MarriageDetails({
    $core.Iterable<$core.String>? participants,
    $1.Timestamp? date,
    $core.String? place,
    $core.String? country,
  }) {
    final _result = create();
    if (participants != null) {
      _result.participants.addAll(participants);
    }
    if (date != null) {
      _result.date = date;
    }
    if (place != null) {
      _result.place = place;
    }
    if (country != null) {
      _result.country = country;
    }
    return _result;
  }
  factory MarriageDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MarriageDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MarriageDetails clone() => MarriageDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MarriageDetails copyWith(void Function(MarriageDetails) updates) => super.copyWith((message) => updates(message as MarriageDetails)) as MarriageDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MarriageDetails create() => MarriageDetails._();
  MarriageDetails createEmptyInstance() => create();
  static $pb.PbList<MarriageDetails> createRepeated() => $pb.PbList<MarriageDetails>();
  @$core.pragma('dart2js:noInline')
  static MarriageDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MarriageDetails>(create);
  static MarriageDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get participants => $_getList(0);

  @$pb.TagNumber(2)
  $1.Timestamp get date => $_getN(1);
  @$pb.TagNumber(2)
  set date($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDate() => $_has(1);
  @$pb.TagNumber(2)
  void clearDate() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureDate() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get place => $_getSZ(2);
  @$pb.TagNumber(3)
  set place($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPlace() => $_has(2);
  @$pb.TagNumber(3)
  void clearPlace() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get country => $_getSZ(3);
  @$pb.TagNumber(4)
  set country($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCountry() => $_has(3);
  @$pb.TagNumber(4)
  void clearCountry() => clearField(4);
}

class MarriageDetailsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MarriageDetailsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<MarriageDetails>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'marriageDetails', protoName: 'marriageDetails', subBuilder: MarriageDetails.create)
    ..hasRequiredFields = false
  ;

  MarriageDetailsReply._() : super();
  factory MarriageDetailsReply({
    MarriageDetails? marriageDetails,
  }) {
    final _result = create();
    if (marriageDetails != null) {
      _result.marriageDetails = marriageDetails;
    }
    return _result;
  }
  factory MarriageDetailsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MarriageDetailsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MarriageDetailsReply clone() => MarriageDetailsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MarriageDetailsReply copyWith(void Function(MarriageDetailsReply) updates) => super.copyWith((message) => updates(message as MarriageDetailsReply)) as MarriageDetailsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MarriageDetailsReply create() => MarriageDetailsReply._();
  MarriageDetailsReply createEmptyInstance() => create();
  static $pb.PbList<MarriageDetailsReply> createRepeated() => $pb.PbList<MarriageDetailsReply>();
  @$core.pragma('dart2js:noInline')
  static MarriageDetailsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MarriageDetailsReply>(create);
  static MarriageDetailsReply? _defaultInstance;

  @$pb.TagNumber(1)
  MarriageDetails get marriageDetails => $_getN(0);
  @$pb.TagNumber(1)
  set marriageDetails(MarriageDetails v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMarriageDetails() => $_has(0);
  @$pb.TagNumber(1)
  void clearMarriageDetails() => clearField(1);
  @$pb.TagNumber(1)
  MarriageDetails ensureMarriageDetails() => $_ensure(0);
}

class NameDetailsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NameDetailsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'query', subBuilder: AggregationQuery.create)
    ..hasRequiredFields = false
  ;

  NameDetailsRequest._() : super();
  factory NameDetailsRequest({
    AggregationQuery? query,
  }) {
    final _result = create();
    if (query != null) {
      _result.query = query;
    }
    return _result;
  }
  factory NameDetailsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NameDetailsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NameDetailsRequest clone() => NameDetailsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NameDetailsRequest copyWith(void Function(NameDetailsRequest) updates) => super.copyWith((message) => updates(message as NameDetailsRequest)) as NameDetailsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NameDetailsRequest create() => NameDetailsRequest._();
  NameDetailsRequest createEmptyInstance() => create();
  static $pb.PbList<NameDetailsRequest> createRepeated() => $pb.PbList<NameDetailsRequest>();
  @$core.pragma('dart2js:noInline')
  static NameDetailsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NameDetailsRequest>(create);
  static NameDetailsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery get query => $_getN(0);
  @$pb.TagNumber(1)
  set query(AggregationQuery v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery ensureQuery() => $_ensure(0);
}

class NameDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NameDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'firstnames')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastnamePrefix', protoName: 'lastnamePrefix')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lastnames')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'namePreference', protoName: 'namePreference')
    ..hasRequiredFields = false
  ;

  NameDetails._() : super();
  factory NameDetails({
    $core.String? firstnames,
    $core.String? lastnamePrefix,
    $core.String? lastnames,
    $core.String? namePreference,
  }) {
    final _result = create();
    if (firstnames != null) {
      _result.firstnames = firstnames;
    }
    if (lastnamePrefix != null) {
      _result.lastnamePrefix = lastnamePrefix;
    }
    if (lastnames != null) {
      _result.lastnames = lastnames;
    }
    if (namePreference != null) {
      _result.namePreference = namePreference;
    }
    return _result;
  }
  factory NameDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NameDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NameDetails clone() => NameDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NameDetails copyWith(void Function(NameDetails) updates) => super.copyWith((message) => updates(message as NameDetails)) as NameDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NameDetails create() => NameDetails._();
  NameDetails createEmptyInstance() => create();
  static $pb.PbList<NameDetails> createRepeated() => $pb.PbList<NameDetails>();
  @$core.pragma('dart2js:noInline')
  static NameDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NameDetails>(create);
  static NameDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get firstnames => $_getSZ(0);
  @$pb.TagNumber(1)
  set firstnames($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFirstnames() => $_has(0);
  @$pb.TagNumber(1)
  void clearFirstnames() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get lastnamePrefix => $_getSZ(1);
  @$pb.TagNumber(2)
  set lastnamePrefix($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLastnamePrefix() => $_has(1);
  @$pb.TagNumber(2)
  void clearLastnamePrefix() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get lastnames => $_getSZ(2);
  @$pb.TagNumber(3)
  set lastnames($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasLastnames() => $_has(2);
  @$pb.TagNumber(3)
  void clearLastnames() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get namePreference => $_getSZ(3);
  @$pb.TagNumber(4)
  set namePreference($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNamePreference() => $_has(3);
  @$pb.TagNumber(4)
  void clearNamePreference() => clearField(4);
}

class NameDetailsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NameDetailsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<NameDetails>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nameDetails', protoName: 'nameDetails', subBuilder: NameDetails.create)
    ..hasRequiredFields = false
  ;

  NameDetailsReply._() : super();
  factory NameDetailsReply({
    NameDetails? nameDetails,
  }) {
    final _result = create();
    if (nameDetails != null) {
      _result.nameDetails = nameDetails;
    }
    return _result;
  }
  factory NameDetailsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NameDetailsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NameDetailsReply clone() => NameDetailsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NameDetailsReply copyWith(void Function(NameDetailsReply) updates) => super.copyWith((message) => updates(message as NameDetailsReply)) as NameDetailsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NameDetailsReply create() => NameDetailsReply._();
  NameDetailsReply createEmptyInstance() => create();
  static $pb.PbList<NameDetailsReply> createRepeated() => $pb.PbList<NameDetailsReply>();
  @$core.pragma('dart2js:noInline')
  static NameDetailsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NameDetailsReply>(create);
  static NameDetailsReply? _defaultInstance;

  @$pb.TagNumber(1)
  NameDetails get nameDetails => $_getN(0);
  @$pb.TagNumber(1)
  set nameDetails(NameDetails v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasNameDetails() => $_has(0);
  @$pb.TagNumber(1)
  void clearNameDetails() => clearField(1);
  @$pb.TagNumber(1)
  NameDetails ensureNameDetails() => $_ensure(0);
}

class ResidentialAddressRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResidentialAddressRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'query', subBuilder: AggregationQuery.create)
    ..hasRequiredFields = false
  ;

  ResidentialAddressRequest._() : super();
  factory ResidentialAddressRequest({
    AggregationQuery? query,
  }) {
    final _result = create();
    if (query != null) {
      _result.query = query;
    }
    return _result;
  }
  factory ResidentialAddressRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResidentialAddressRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResidentialAddressRequest clone() => ResidentialAddressRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResidentialAddressRequest copyWith(void Function(ResidentialAddressRequest) updates) => super.copyWith((message) => updates(message as ResidentialAddressRequest)) as ResidentialAddressRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResidentialAddressRequest create() => ResidentialAddressRequest._();
  ResidentialAddressRequest createEmptyInstance() => create();
  static $pb.PbList<ResidentialAddressRequest> createRepeated() => $pb.PbList<ResidentialAddressRequest>();
  @$core.pragma('dart2js:noInline')
  static ResidentialAddressRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResidentialAddressRequest>(create);
  static ResidentialAddressRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery get query => $_getN(0);
  @$pb.TagNumber(1)
  set query(AggregationQuery v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery ensureQuery() => $_ensure(0);
}

class ResidentialAddress extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResidentialAddress', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..hasRequiredFields = false
  ;

  ResidentialAddress._() : super();
  factory ResidentialAddress({
    $core.String? address,
  }) {
    final _result = create();
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory ResidentialAddress.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResidentialAddress.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResidentialAddress clone() => ResidentialAddress()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResidentialAddress copyWith(void Function(ResidentialAddress) updates) => super.copyWith((message) => updates(message as ResidentialAddress)) as ResidentialAddress; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResidentialAddress create() => ResidentialAddress._();
  ResidentialAddress createEmptyInstance() => create();
  static $pb.PbList<ResidentialAddress> createRepeated() => $pb.PbList<ResidentialAddress>();
  @$core.pragma('dart2js:noInline')
  static ResidentialAddress getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResidentialAddress>(create);
  static ResidentialAddress? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get address => $_getSZ(0);
  @$pb.TagNumber(1)
  set address($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAddress() => $_has(0);
  @$pb.TagNumber(1)
  void clearAddress() => clearField(1);
}

class ResidentialAddressReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResidentialAddressReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<ResidentialAddress>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'residentialAddress', protoName: 'residentialAddress', subBuilder: ResidentialAddress.create)
    ..hasRequiredFields = false
  ;

  ResidentialAddressReply._() : super();
  factory ResidentialAddressReply({
    ResidentialAddress? residentialAddress,
  }) {
    final _result = create();
    if (residentialAddress != null) {
      _result.residentialAddress = residentialAddress;
    }
    return _result;
  }
  factory ResidentialAddressReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResidentialAddressReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResidentialAddressReply clone() => ResidentialAddressReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResidentialAddressReply copyWith(void Function(ResidentialAddressReply) updates) => super.copyWith((message) => updates(message as ResidentialAddressReply)) as ResidentialAddressReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResidentialAddressReply create() => ResidentialAddressReply._();
  ResidentialAddressReply createEmptyInstance() => create();
  static $pb.PbList<ResidentialAddressReply> createRepeated() => $pb.PbList<ResidentialAddressReply>();
  @$core.pragma('dart2js:noInline')
  static ResidentialAddressReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResidentialAddressReply>(create);
  static ResidentialAddressReply? _defaultInstance;

  @$pb.TagNumber(1)
  ResidentialAddress get residentialAddress => $_getN(0);
  @$pb.TagNumber(1)
  set residentialAddress(ResidentialAddress v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasResidentialAddress() => $_has(0);
  @$pb.TagNumber(1)
  void clearResidentialAddress() => clearField(1);
  @$pb.TagNumber(1)
  ResidentialAddress ensureResidentialAddress() => $_ensure(0);
}

class PersonDetailsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersonDetailsRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<AggregationQuery>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'query', subBuilder: AggregationQuery.create)
    ..hasRequiredFields = false
  ;

  PersonDetailsRequest._() : super();
  factory PersonDetailsRequest({
    AggregationQuery? query,
  }) {
    final _result = create();
    if (query != null) {
      _result.query = query;
    }
    return _result;
  }
  factory PersonDetailsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersonDetailsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersonDetailsRequest clone() => PersonDetailsRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersonDetailsRequest copyWith(void Function(PersonDetailsRequest) updates) => super.copyWith((message) => updates(message as PersonDetailsRequest)) as PersonDetailsRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersonDetailsRequest create() => PersonDetailsRequest._();
  PersonDetailsRequest createEmptyInstance() => create();
  static $pb.PbList<PersonDetailsRequest> createRepeated() => $pb.PbList<PersonDetailsRequest>();
  @$core.pragma('dart2js:noInline')
  static PersonDetailsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersonDetailsRequest>(create);
  static PersonDetailsRequest? _defaultInstance;

  @$pb.TagNumber(1)
  AggregationQuery get query => $_getN(0);
  @$pb.TagNumber(1)
  set query(AggregationQuery v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasQuery() => $_has(0);
  @$pb.TagNumber(1)
  void clearQuery() => clearField(1);
  @$pb.TagNumber(1)
  AggregationQuery ensureQuery() => $_ensure(0);
}

class Identifiers extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Identifiers', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..m<$core.String, $core.String>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zidEid', protoName: 'zidEid', entryClassName: 'Identifiers.ZidEidEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('query'))
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'bsn')
    ..hasRequiredFields = false
  ;

  Identifiers._() : super();
  factory Identifiers({
    $core.Map<$core.String, $core.String>? zidEid,
    $core.String? bsn,
  }) {
    final _result = create();
    if (zidEid != null) {
      _result.zidEid.addAll(zidEid);
    }
    if (bsn != null) {
      _result.bsn = bsn;
    }
    return _result;
  }
  factory Identifiers.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Identifiers.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Identifiers clone() => Identifiers()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Identifiers copyWith(void Function(Identifiers) updates) => super.copyWith((message) => updates(message as Identifiers)) as Identifiers; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Identifiers create() => Identifiers._();
  Identifiers createEmptyInstance() => create();
  static $pb.PbList<Identifiers> createRepeated() => $pb.PbList<Identifiers>();
  @$core.pragma('dart2js:noInline')
  static Identifiers getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Identifiers>(create);
  static Identifiers? _defaultInstance;

  @$pb.TagNumber(1)
  $core.Map<$core.String, $core.String> get zidEid => $_getMap(0);

  @$pb.TagNumber(2)
  $core.String get bsn => $_getSZ(1);
  @$pb.TagNumber(2)
  set bsn($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasBsn() => $_has(1);
  @$pb.TagNumber(2)
  void clearBsn() => clearField(2);
}

class LegalDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LegalDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nationality')
    ..hasRequiredFields = false
  ;

  LegalDetails._() : super();
  factory LegalDetails({
    $core.String? nationality,
  }) {
    final _result = create();
    if (nationality != null) {
      _result.nationality = nationality;
    }
    return _result;
  }
  factory LegalDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LegalDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LegalDetails clone() => LegalDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LegalDetails copyWith(void Function(LegalDetails) updates) => super.copyWith((message) => updates(message as LegalDetails)) as LegalDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LegalDetails create() => LegalDetails._();
  LegalDetails createEmptyInstance() => create();
  static $pb.PbList<LegalDetails> createRepeated() => $pb.PbList<LegalDetails>();
  @$core.pragma('dart2js:noInline')
  static LegalDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LegalDetails>(create);
  static LegalDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get nationality => $_getSZ(0);
  @$pb.TagNumber(1)
  set nationality($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNationality() => $_has(0);
  @$pb.TagNumber(1)
  void clearNationality() => clearField(1);
}

class PersonDetailsReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersonDetailsReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'query'), createEmptyInstance: create)
    ..aOM<Identifiers>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'identifiers', subBuilder: Identifiers.create)
    ..aOM<BirthDetails>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'birthDetails', protoName: 'birthDetails', subBuilder: BirthDetails.create)
    ..aOM<NameDetails>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nameDetails', protoName: 'nameDetails', subBuilder: NameDetails.create)
    ..aOM<ResidentialAddress>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'residentialAddress', protoName: 'residentialAddress', subBuilder: ResidentialAddress.create)
    ..aOM<MarriageDetails>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'marriageDetails', protoName: 'marriageDetails', subBuilder: MarriageDetails.create)
    ..aOM<LegalDetails>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'legalDetails', protoName: 'legalDetails', subBuilder: LegalDetails.create)
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'municipalityOfRegistration', protoName: 'municipalityOfRegistration')
    ..hasRequiredFields = false
  ;

  PersonDetailsReply._() : super();
  factory PersonDetailsReply({
    Identifiers? identifiers,
    BirthDetails? birthDetails,
    NameDetails? nameDetails,
    ResidentialAddress? residentialAddress,
    MarriageDetails? marriageDetails,
    LegalDetails? legalDetails,
    $core.String? municipalityOfRegistration,
  }) {
    final _result = create();
    if (identifiers != null) {
      _result.identifiers = identifiers;
    }
    if (birthDetails != null) {
      _result.birthDetails = birthDetails;
    }
    if (nameDetails != null) {
      _result.nameDetails = nameDetails;
    }
    if (residentialAddress != null) {
      _result.residentialAddress = residentialAddress;
    }
    if (marriageDetails != null) {
      _result.marriageDetails = marriageDetails;
    }
    if (legalDetails != null) {
      _result.legalDetails = legalDetails;
    }
    if (municipalityOfRegistration != null) {
      _result.municipalityOfRegistration = municipalityOfRegistration;
    }
    return _result;
  }
  factory PersonDetailsReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersonDetailsReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersonDetailsReply clone() => PersonDetailsReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersonDetailsReply copyWith(void Function(PersonDetailsReply) updates) => super.copyWith((message) => updates(message as PersonDetailsReply)) as PersonDetailsReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersonDetailsReply create() => PersonDetailsReply._();
  PersonDetailsReply createEmptyInstance() => create();
  static $pb.PbList<PersonDetailsReply> createRepeated() => $pb.PbList<PersonDetailsReply>();
  @$core.pragma('dart2js:noInline')
  static PersonDetailsReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersonDetailsReply>(create);
  static PersonDetailsReply? _defaultInstance;

  @$pb.TagNumber(1)
  Identifiers get identifiers => $_getN(0);
  @$pb.TagNumber(1)
  set identifiers(Identifiers v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasIdentifiers() => $_has(0);
  @$pb.TagNumber(1)
  void clearIdentifiers() => clearField(1);
  @$pb.TagNumber(1)
  Identifiers ensureIdentifiers() => $_ensure(0);

  @$pb.TagNumber(2)
  BirthDetails get birthDetails => $_getN(1);
  @$pb.TagNumber(2)
  set birthDetails(BirthDetails v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasBirthDetails() => $_has(1);
  @$pb.TagNumber(2)
  void clearBirthDetails() => clearField(2);
  @$pb.TagNumber(2)
  BirthDetails ensureBirthDetails() => $_ensure(1);

  @$pb.TagNumber(3)
  NameDetails get nameDetails => $_getN(2);
  @$pb.TagNumber(3)
  set nameDetails(NameDetails v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasNameDetails() => $_has(2);
  @$pb.TagNumber(3)
  void clearNameDetails() => clearField(3);
  @$pb.TagNumber(3)
  NameDetails ensureNameDetails() => $_ensure(2);

  @$pb.TagNumber(4)
  ResidentialAddress get residentialAddress => $_getN(3);
  @$pb.TagNumber(4)
  set residentialAddress(ResidentialAddress v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasResidentialAddress() => $_has(3);
  @$pb.TagNumber(4)
  void clearResidentialAddress() => clearField(4);
  @$pb.TagNumber(4)
  ResidentialAddress ensureResidentialAddress() => $_ensure(3);

  @$pb.TagNumber(5)
  MarriageDetails get marriageDetails => $_getN(4);
  @$pb.TagNumber(5)
  set marriageDetails(MarriageDetails v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMarriageDetails() => $_has(4);
  @$pb.TagNumber(5)
  void clearMarriageDetails() => clearField(5);
  @$pb.TagNumber(5)
  MarriageDetails ensureMarriageDetails() => $_ensure(4);

  @$pb.TagNumber(6)
  LegalDetails get legalDetails => $_getN(5);
  @$pb.TagNumber(6)
  set legalDetails(LegalDetails v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasLegalDetails() => $_has(5);
  @$pb.TagNumber(6)
  void clearLegalDetails() => clearField(6);
  @$pb.TagNumber(6)
  LegalDetails ensureLegalDetails() => $_ensure(5);

  @$pb.TagNumber(7)
  $core.String get municipalityOfRegistration => $_getSZ(6);
  @$pb.TagNumber(7)
  set municipalityOfRegistration($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasMunicipalityOfRegistration() => $_has(6);
  @$pb.TagNumber(7)
  void clearMunicipalityOfRegistration() => clearField(7);
}

