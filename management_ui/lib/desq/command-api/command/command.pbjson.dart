///
//  Generated code. Do not modify.
//  source: command-api/command/command.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use basicReplyDescriptor instead')
const BasicReply$json = const {
  '1': 'BasicReply',
  '2': const [
    const {'1': 'commandReference', '3': 1, '4': 1, '5': 9, '10': 'commandReference'},
  ],
};

/// Descriptor for `BasicReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List basicReplyDescriptor = $convert.base64Decode('CgpCYXNpY1JlcGx5EioKEGNvbW1hbmRSZWZlcmVuY2UYASABKAlSEGNvbW1hbmRSZWZlcmVuY2U=');
@$core.Deprecated('Use persoonIngeschrevenCommandDescriptor instead')
const PersoonIngeschrevenCommand$json = const {
  '1': 'PersoonIngeschrevenCommand',
  '2': const [
    const {'1': 'datum_ingeschreven', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'datumIngeschreven'},
  ],
};

/// Descriptor for `PersoonIngeschrevenCommand`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List persoonIngeschrevenCommandDescriptor = $convert.base64Decode('ChpQZXJzb29uSW5nZXNjaHJldmVuQ29tbWFuZBJJChJkYXR1bV9pbmdlc2NocmV2ZW4YASABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUhFkYXR1bUluZ2VzY2hyZXZlbg==');
@$core.Deprecated('Use persoonIngeschrevenCommandReplyDescriptor instead')
const PersoonIngeschrevenCommandReply$json = const {
  '1': 'PersoonIngeschrevenCommandReply',
  '2': const [
    const {'1': 'commandReference', '3': 1, '4': 1, '5': 9, '10': 'commandReference'},
    const {'1': 'personReference', '3': 2, '4': 1, '5': 12, '10': 'personReference'},
  ],
};

/// Descriptor for `PersoonIngeschrevenCommandReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List persoonIngeschrevenCommandReplyDescriptor = $convert.base64Decode('Ch9QZXJzb29uSW5nZXNjaHJldmVuQ29tbWFuZFJlcGx5EioKEGNvbW1hbmRSZWZlcmVuY2UYASABKAlSEGNvbW1hbmRSZWZlcmVuY2USKAoPcGVyc29uUmVmZXJlbmNlGAIgASgMUg9wZXJzb25SZWZlcmVuY2U=');
@$core.Deprecated('Use newPersonRequestDescriptor instead')
const NewPersonRequest$json = const {
  '1': 'NewPersonRequest',
};

/// Descriptor for `NewPersonRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newPersonRequestDescriptor = $convert.base64Decode('ChBOZXdQZXJzb25SZXF1ZXN0');
@$core.Deprecated('Use newPersonReplyDescriptor instead')
const NewPersonReply$json = const {
  '1': 'NewPersonReply',
  '2': const [
    const {'1': 'commandReference', '3': 1, '4': 1, '5': 9, '10': 'commandReference'},
    const {'1': 'personReference', '3': 2, '4': 1, '5': 12, '10': 'personReference'},
  ],
};

/// Descriptor for `NewPersonReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newPersonReplyDescriptor = $convert.base64Decode('Cg5OZXdQZXJzb25SZXBseRIqChBjb21tYW5kUmVmZXJlbmNlGAEgASgJUhBjb21tYW5kUmVmZXJlbmNlEigKD3BlcnNvblJlZmVyZW5jZRgCIAEoDFIPcGVyc29uUmVmZXJlbmNl');
@$core.Deprecated('Use personNoteRequestDescriptor instead')
const PersonNoteRequest$json = const {
  '1': 'PersonNoteRequest',
  '2': const [
    const {'1': 'personReference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
    const {'1': 'note', '3': 2, '4': 1, '5': 9, '10': 'note'},
  ],
};

/// Descriptor for `PersonNoteRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List personNoteRequestDescriptor = $convert.base64Decode('ChFQZXJzb25Ob3RlUmVxdWVzdBIoCg9wZXJzb25SZWZlcmVuY2UYASABKAxSD3BlcnNvblJlZmVyZW5jZRISCgRub3RlGAIgASgJUgRub3Rl');
@$core.Deprecated('Use statusRequestDescriptor instead')
const StatusRequest$json = const {
  '1': 'StatusRequest',
  '2': const [
    const {'1': 'commandReference', '3': 1, '4': 1, '5': 9, '10': 'commandReference'},
  ],
};

/// Descriptor for `StatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statusRequestDescriptor = $convert.base64Decode('Cg1TdGF0dXNSZXF1ZXN0EioKEGNvbW1hbmRSZWZlcmVuY2UYASABKAlSEGNvbW1hbmRSZWZlcmVuY2U=');
@$core.Deprecated('Use statusReplyDescriptor instead')
const StatusReply$json = const {
  '1': 'StatusReply',
  '2': const [
    const {'1': 'journaled', '3': 1, '4': 1, '5': 8, '10': 'journaled'},
    const {'1': 'published', '3': 2, '4': 1, '5': 8, '10': 'published'},
  ],
};

/// Descriptor for `StatusReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statusReplyDescriptor = $convert.base64Decode('CgtTdGF0dXNSZXBseRIcCglqb3VybmFsZWQYASABKAhSCWpvdXJuYWxlZBIcCglwdWJsaXNoZWQYAiABKAhSCXB1Ymxpc2hlZA==');
