///
//  Generated code. Do not modify.
//  source: command-api/command/command.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../../google/protobuf/timestamp.pb.dart' as $2;

class BasicReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BasicReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commandReference', protoName: 'commandReference')
    ..hasRequiredFields = false
  ;

  BasicReply._() : super();
  factory BasicReply({
    $core.String? commandReference,
  }) {
    final _result = create();
    if (commandReference != null) {
      _result.commandReference = commandReference;
    }
    return _result;
  }
  factory BasicReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BasicReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BasicReply clone() => BasicReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BasicReply copyWith(void Function(BasicReply) updates) => super.copyWith((message) => updates(message as BasicReply)) as BasicReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BasicReply create() => BasicReply._();
  BasicReply createEmptyInstance() => create();
  static $pb.PbList<BasicReply> createRepeated() => $pb.PbList<BasicReply>();
  @$core.pragma('dart2js:noInline')
  static BasicReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BasicReply>(create);
  static BasicReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get commandReference => $_getSZ(0);
  @$pb.TagNumber(1)
  set commandReference($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCommandReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearCommandReference() => clearField(1);
}

class PersoonIngeschrevenCommand extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersoonIngeschrevenCommand', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOM<$2.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'datumIngeschreven', subBuilder: $2.Timestamp.create)
    ..hasRequiredFields = false
  ;

  PersoonIngeschrevenCommand._() : super();
  factory PersoonIngeschrevenCommand({
    $2.Timestamp? datumIngeschreven,
  }) {
    final _result = create();
    if (datumIngeschreven != null) {
      _result.datumIngeschreven = datumIngeschreven;
    }
    return _result;
  }
  factory PersoonIngeschrevenCommand.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersoonIngeschrevenCommand.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersoonIngeschrevenCommand clone() => PersoonIngeschrevenCommand()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersoonIngeschrevenCommand copyWith(void Function(PersoonIngeschrevenCommand) updates) => super.copyWith((message) => updates(message as PersoonIngeschrevenCommand)) as PersoonIngeschrevenCommand; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschrevenCommand create() => PersoonIngeschrevenCommand._();
  PersoonIngeschrevenCommand createEmptyInstance() => create();
  static $pb.PbList<PersoonIngeschrevenCommand> createRepeated() => $pb.PbList<PersoonIngeschrevenCommand>();
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschrevenCommand getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersoonIngeschrevenCommand>(create);
  static PersoonIngeschrevenCommand? _defaultInstance;

  @$pb.TagNumber(1)
  $2.Timestamp get datumIngeschreven => $_getN(0);
  @$pb.TagNumber(1)
  set datumIngeschreven($2.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasDatumIngeschreven() => $_has(0);
  @$pb.TagNumber(1)
  void clearDatumIngeschreven() => clearField(1);
  @$pb.TagNumber(1)
  $2.Timestamp ensureDatumIngeschreven() => $_ensure(0);
}

class PersoonIngeschrevenCommandReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersoonIngeschrevenCommandReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commandReference', protoName: 'commandReference')
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY, protoName: 'personReference')
    ..hasRequiredFields = false
  ;

  PersoonIngeschrevenCommandReply._() : super();
  factory PersoonIngeschrevenCommandReply({
    $core.String? commandReference,
    $core.List<$core.int>? personReference,
  }) {
    final _result = create();
    if (commandReference != null) {
      _result.commandReference = commandReference;
    }
    if (personReference != null) {
      _result.personReference = personReference;
    }
    return _result;
  }
  factory PersoonIngeschrevenCommandReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersoonIngeschrevenCommandReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersoonIngeschrevenCommandReply clone() => PersoonIngeschrevenCommandReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersoonIngeschrevenCommandReply copyWith(void Function(PersoonIngeschrevenCommandReply) updates) => super.copyWith((message) => updates(message as PersoonIngeschrevenCommandReply)) as PersoonIngeschrevenCommandReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschrevenCommandReply create() => PersoonIngeschrevenCommandReply._();
  PersoonIngeschrevenCommandReply createEmptyInstance() => create();
  static $pb.PbList<PersoonIngeschrevenCommandReply> createRepeated() => $pb.PbList<PersoonIngeschrevenCommandReply>();
  @$core.pragma('dart2js:noInline')
  static PersoonIngeschrevenCommandReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersoonIngeschrevenCommandReply>(create);
  static PersoonIngeschrevenCommandReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get commandReference => $_getSZ(0);
  @$pb.TagNumber(1)
  set commandReference($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCommandReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearCommandReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get personReference => $_getN(1);
  @$pb.TagNumber(2)
  set personReference($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersonReference() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersonReference() => clearField(2);
}

class NewPersonRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewPersonRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  NewPersonRequest._() : super();
  factory NewPersonRequest() => create();
  factory NewPersonRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewPersonRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewPersonRequest clone() => NewPersonRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewPersonRequest copyWith(void Function(NewPersonRequest) updates) => super.copyWith((message) => updates(message as NewPersonRequest)) as NewPersonRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewPersonRequest create() => NewPersonRequest._();
  NewPersonRequest createEmptyInstance() => create();
  static $pb.PbList<NewPersonRequest> createRepeated() => $pb.PbList<NewPersonRequest>();
  @$core.pragma('dart2js:noInline')
  static NewPersonRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewPersonRequest>(create);
  static NewPersonRequest? _defaultInstance;
}

class NewPersonReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewPersonReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commandReference', protoName: 'commandReference')
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY, protoName: 'personReference')
    ..hasRequiredFields = false
  ;

  NewPersonReply._() : super();
  factory NewPersonReply({
    $core.String? commandReference,
    $core.List<$core.int>? personReference,
  }) {
    final _result = create();
    if (commandReference != null) {
      _result.commandReference = commandReference;
    }
    if (personReference != null) {
      _result.personReference = personReference;
    }
    return _result;
  }
  factory NewPersonReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewPersonReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewPersonReply clone() => NewPersonReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewPersonReply copyWith(void Function(NewPersonReply) updates) => super.copyWith((message) => updates(message as NewPersonReply)) as NewPersonReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewPersonReply create() => NewPersonReply._();
  NewPersonReply createEmptyInstance() => create();
  static $pb.PbList<NewPersonReply> createRepeated() => $pb.PbList<NewPersonReply>();
  @$core.pragma('dart2js:noInline')
  static NewPersonReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewPersonReply>(create);
  static NewPersonReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get commandReference => $_getSZ(0);
  @$pb.TagNumber(1)
  set commandReference($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCommandReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearCommandReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get personReference => $_getN(1);
  @$pb.TagNumber(2)
  set personReference($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPersonReference() => $_has(1);
  @$pb.TagNumber(2)
  void clearPersonReference() => clearField(2);
}

class PersonNoteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PersonNoteRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY, protoName: 'personReference')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'note')
    ..hasRequiredFields = false
  ;

  PersonNoteRequest._() : super();
  factory PersonNoteRequest({
    $core.List<$core.int>? personReference,
    $core.String? note,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    if (note != null) {
      _result.note = note;
    }
    return _result;
  }
  factory PersonNoteRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PersonNoteRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PersonNoteRequest clone() => PersonNoteRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PersonNoteRequest copyWith(void Function(PersonNoteRequest) updates) => super.copyWith((message) => updates(message as PersonNoteRequest)) as PersonNoteRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PersonNoteRequest create() => PersonNoteRequest._();
  PersonNoteRequest createEmptyInstance() => create();
  static $pb.PbList<PersonNoteRequest> createRepeated() => $pb.PbList<PersonNoteRequest>();
  @$core.pragma('dart2js:noInline')
  static PersonNoteRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PersonNoteRequest>(create);
  static PersonNoteRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get note => $_getSZ(1);
  @$pb.TagNumber(2)
  set note($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNote() => $_has(1);
  @$pb.TagNumber(2)
  void clearNote() => clearField(2);
}

class StatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StatusRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'commandReference', protoName: 'commandReference')
    ..hasRequiredFields = false
  ;

  StatusRequest._() : super();
  factory StatusRequest({
    $core.String? commandReference,
  }) {
    final _result = create();
    if (commandReference != null) {
      _result.commandReference = commandReference;
    }
    return _result;
  }
  factory StatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StatusRequest clone() => StatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StatusRequest copyWith(void Function(StatusRequest) updates) => super.copyWith((message) => updates(message as StatusRequest)) as StatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusRequest create() => StatusRequest._();
  StatusRequest createEmptyInstance() => create();
  static $pb.PbList<StatusRequest> createRepeated() => $pb.PbList<StatusRequest>();
  @$core.pragma('dart2js:noInline')
  static StatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StatusRequest>(create);
  static StatusRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get commandReference => $_getSZ(0);
  @$pb.TagNumber(1)
  set commandReference($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCommandReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearCommandReference() => clearField(1);
}

class StatusReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StatusReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'command'), createEmptyInstance: create)
    ..aOB(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journaled')
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'published')
    ..hasRequiredFields = false
  ;

  StatusReply._() : super();
  factory StatusReply({
    $core.bool? journaled,
    $core.bool? published,
  }) {
    final _result = create();
    if (journaled != null) {
      _result.journaled = journaled;
    }
    if (published != null) {
      _result.published = published;
    }
    return _result;
  }
  factory StatusReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StatusReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StatusReply clone() => StatusReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StatusReply copyWith(void Function(StatusReply) updates) => super.copyWith((message) => updates(message as StatusReply)) as StatusReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StatusReply create() => StatusReply._();
  StatusReply createEmptyInstance() => create();
  static $pb.PbList<StatusReply> createRepeated() => $pb.PbList<StatusReply>();
  @$core.pragma('dart2js:noInline')
  static StatusReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StatusReply>(create);
  static StatusReply? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get journaled => $_getBF(0);
  @$pb.TagNumber(1)
  set journaled($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasJournaled() => $_has(0);
  @$pb.TagNumber(1)
  void clearJournaled() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get published => $_getBF(1);
  @$pb.TagNumber(2)
  set published($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPublished() => $_has(1);
  @$pb.TagNumber(2)
  void clearPublished() => clearField(2);
}

