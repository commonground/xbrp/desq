///
//  Generated code. Do not modify.
//  source: command-api/command/command.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'command.pb.dart' as $0;
import '../../common/events/events.pb.dart' as $1;
export 'command.pb.dart';

class CommandClient extends $grpc.Client {
  static final _$newPerson =
      $grpc.ClientMethod<$0.NewPersonRequest, $0.NewPersonReply>(
          '/command.Command/NewPerson',
          ($0.NewPersonRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.NewPersonReply.fromBuffer(value));
  static final _$personNote =
      $grpc.ClientMethod<$0.PersonNoteRequest, $0.BasicReply>(
          '/command.Command/PersonNote',
          ($0.PersonNoteRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$namenVastgesteld =
      $grpc.ClientMethod<$1.NamenVastgesteld, $0.BasicReply>(
          '/command.Command/NamenVastgesteld',
          ($1.NamenVastgesteld value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$persoonIngeschreven = $grpc.ClientMethod<
          $0.PersoonIngeschrevenCommand, $0.PersoonIngeschrevenCommandReply>(
      '/command.Command/PersoonIngeschreven',
      ($0.PersoonIngeschrevenCommand value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.PersoonIngeschrevenCommandReply.fromBuffer(value));
  static final _$bSNToegekend =
      $grpc.ClientMethod<$1.BSNToegekend, $0.BasicReply>(
          '/command.Command/BSNToegekend',
          ($1.BSNToegekend value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$woonadresNLGeregistreerd =
      $grpc.ClientMethod<$1.WoonadresNLGeregistreerd, $0.BasicReply>(
          '/command.Command/WoonadresNLGeregistreerd',
          ($1.WoonadresNLGeregistreerd value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$namenGeregistreerd =
      $grpc.ClientMethod<$1.NamenGeregistreerd, $0.BasicReply>(
          '/command.Command/NamenGeregistreerd',
          ($1.NamenGeregistreerd value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$geslachtsaanduidingGeregistreerd =
      $grpc.ClientMethod<$1.GeslachtsaanduidingGeregistreerd, $0.BasicReply>(
          '/command.Command/GeslachtsaanduidingGeregistreerd',
          ($1.GeslachtsaanduidingGeregistreerd value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.BasicReply.fromBuffer(value));
  static final _$status = $grpc.ClientMethod<$0.StatusRequest, $0.StatusReply>(
      '/command.Command/Status',
      ($0.StatusRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.StatusReply.fromBuffer(value));

  CommandClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.NewPersonReply> newPerson($0.NewPersonRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$newPerson, request, options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> personNote($0.PersonNoteRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$personNote, request, options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> namenVastgesteld(
      $1.NamenVastgesteld request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$namenVastgesteld, request, options: options);
  }

  $grpc.ResponseFuture<$0.PersoonIngeschrevenCommandReply> persoonIngeschreven(
      $0.PersoonIngeschrevenCommand request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$persoonIngeschreven, request, options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> bSNToegekend($1.BSNToegekend request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bSNToegekend, request, options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> woonadresNLGeregistreerd(
      $1.WoonadresNLGeregistreerd request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$woonadresNLGeregistreerd, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> namenGeregistreerd(
      $1.NamenGeregistreerd request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$namenGeregistreerd, request, options: options);
  }

  $grpc.ResponseFuture<$0.BasicReply> geslachtsaanduidingGeregistreerd(
      $1.GeslachtsaanduidingGeregistreerd request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$geslachtsaanduidingGeregistreerd, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.StatusReply> status($0.StatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$status, request, options: options);
  }
}

abstract class CommandServiceBase extends $grpc.Service {
  $core.String get $name => 'command.Command';

  CommandServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.NewPersonRequest, $0.NewPersonReply>(
        'NewPerson',
        newPerson_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.NewPersonRequest.fromBuffer(value),
        ($0.NewPersonReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PersonNoteRequest, $0.BasicReply>(
        'PersonNote',
        personNote_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PersonNoteRequest.fromBuffer(value),
        ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.NamenVastgesteld, $0.BasicReply>(
        'NamenVastgesteld',
        namenVastgesteld_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.NamenVastgesteld.fromBuffer(value),
        ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PersoonIngeschrevenCommand,
            $0.PersoonIngeschrevenCommandReply>(
        'PersoonIngeschreven',
        persoonIngeschreven_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PersoonIngeschrevenCommand.fromBuffer(value),
        ($0.PersoonIngeschrevenCommandReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.BSNToegekend, $0.BasicReply>(
        'BSNToegekend',
        bSNToegekend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.BSNToegekend.fromBuffer(value),
        ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.WoonadresNLGeregistreerd, $0.BasicReply>(
        'WoonadresNLGeregistreerd',
        woonadresNLGeregistreerd_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $1.WoonadresNLGeregistreerd.fromBuffer(value),
        ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.NamenGeregistreerd, $0.BasicReply>(
        'NamenGeregistreerd',
        namenGeregistreerd_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $1.NamenGeregistreerd.fromBuffer(value),
        ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$1.GeslachtsaanduidingGeregistreerd, $0.BasicReply>(
            'GeslachtsaanduidingGeregistreerd',
            geslachtsaanduidingGeregistreerd_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $1.GeslachtsaanduidingGeregistreerd.fromBuffer(value),
            ($0.BasicReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.StatusRequest, $0.StatusReply>(
        'Status',
        status_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.StatusRequest.fromBuffer(value),
        ($0.StatusReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.NewPersonReply> newPerson_Pre($grpc.ServiceCall call,
      $async.Future<$0.NewPersonRequest> request) async {
    return newPerson(call, await request);
  }

  $async.Future<$0.BasicReply> personNote_Pre($grpc.ServiceCall call,
      $async.Future<$0.PersonNoteRequest> request) async {
    return personNote(call, await request);
  }

  $async.Future<$0.BasicReply> namenVastgesteld_Pre($grpc.ServiceCall call,
      $async.Future<$1.NamenVastgesteld> request) async {
    return namenVastgesteld(call, await request);
  }

  $async.Future<$0.PersoonIngeschrevenCommandReply> persoonIngeschreven_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PersoonIngeschrevenCommand> request) async {
    return persoonIngeschreven(call, await request);
  }

  $async.Future<$0.BasicReply> bSNToegekend_Pre(
      $grpc.ServiceCall call, $async.Future<$1.BSNToegekend> request) async {
    return bSNToegekend(call, await request);
  }

  $async.Future<$0.BasicReply> woonadresNLGeregistreerd_Pre(
      $grpc.ServiceCall call,
      $async.Future<$1.WoonadresNLGeregistreerd> request) async {
    return woonadresNLGeregistreerd(call, await request);
  }

  $async.Future<$0.BasicReply> namenGeregistreerd_Pre($grpc.ServiceCall call,
      $async.Future<$1.NamenGeregistreerd> request) async {
    return namenGeregistreerd(call, await request);
  }

  $async.Future<$0.BasicReply> geslachtsaanduidingGeregistreerd_Pre(
      $grpc.ServiceCall call,
      $async.Future<$1.GeslachtsaanduidingGeregistreerd> request) async {
    return geslachtsaanduidingGeregistreerd(call, await request);
  }

  $async.Future<$0.StatusReply> status_Pre(
      $grpc.ServiceCall call, $async.Future<$0.StatusRequest> request) async {
    return status(call, await request);
  }

  $async.Future<$0.NewPersonReply> newPerson(
      $grpc.ServiceCall call, $0.NewPersonRequest request);
  $async.Future<$0.BasicReply> personNote(
      $grpc.ServiceCall call, $0.PersonNoteRequest request);
  $async.Future<$0.BasicReply> namenVastgesteld(
      $grpc.ServiceCall call, $1.NamenVastgesteld request);
  $async.Future<$0.PersoonIngeschrevenCommandReply> persoonIngeschreven(
      $grpc.ServiceCall call, $0.PersoonIngeschrevenCommand request);
  $async.Future<$0.BasicReply> bSNToegekend(
      $grpc.ServiceCall call, $1.BSNToegekend request);
  $async.Future<$0.BasicReply> woonadresNLGeregistreerd(
      $grpc.ServiceCall call, $1.WoonadresNLGeregistreerd request);
  $async.Future<$0.BasicReply> namenGeregistreerd(
      $grpc.ServiceCall call, $1.NamenGeregistreerd request);
  $async.Future<$0.BasicReply> geslachtsaanduidingGeregistreerd(
      $grpc.ServiceCall call, $1.GeslachtsaanduidingGeregistreerd request);
  $async.Future<$0.StatusReply> status(
      $grpc.ServiceCall call, $0.StatusRequest request);
}
