///
//  Generated code. Do not modify.
//  source: catalog-update-api/catalogupdate/catalog-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class UpdatePersonRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdatePersonRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'catalogupdate'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journalZone')
    ..a<$fixnum.Int64>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journalHeight', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.List<$core.int>>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journalHash', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  UpdatePersonRequest._() : super();
  factory UpdatePersonRequest({
    $core.List<$core.int>? personReference,
    $core.String? journalZone,
    $fixnum.Int64? journalHeight,
    $core.List<$core.int>? journalHash,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    if (journalZone != null) {
      _result.journalZone = journalZone;
    }
    if (journalHeight != null) {
      _result.journalHeight = journalHeight;
    }
    if (journalHash != null) {
      _result.journalHash = journalHash;
    }
    return _result;
  }
  factory UpdatePersonRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdatePersonRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdatePersonRequest clone() => UpdatePersonRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdatePersonRequest copyWith(void Function(UpdatePersonRequest) updates) => super.copyWith((message) => updates(message as UpdatePersonRequest)) as UpdatePersonRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdatePersonRequest create() => UpdatePersonRequest._();
  UpdatePersonRequest createEmptyInstance() => create();
  static $pb.PbList<UpdatePersonRequest> createRepeated() => $pb.PbList<UpdatePersonRequest>();
  @$core.pragma('dart2js:noInline')
  static UpdatePersonRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdatePersonRequest>(create);
  static UpdatePersonRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get journalZone => $_getSZ(1);
  @$pb.TagNumber(2)
  set journalZone($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasJournalZone() => $_has(1);
  @$pb.TagNumber(2)
  void clearJournalZone() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get journalHeight => $_getI64(2);
  @$pb.TagNumber(3)
  set journalHeight($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasJournalHeight() => $_has(2);
  @$pb.TagNumber(3)
  void clearJournalHeight() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get journalHash => $_getN(3);
  @$pb.TagNumber(4)
  set journalHash($core.List<$core.int> v) { $_setBytes(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasJournalHash() => $_has(3);
  @$pb.TagNumber(4)
  void clearJournalHash() => clearField(4);
}

class UpdatePersonReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdatePersonReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'catalogupdate'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  UpdatePersonReply._() : super();
  factory UpdatePersonReply() => create();
  factory UpdatePersonReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdatePersonReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdatePersonReply clone() => UpdatePersonReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdatePersonReply copyWith(void Function(UpdatePersonReply) updates) => super.copyWith((message) => updates(message as UpdatePersonReply)) as UpdatePersonReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdatePersonReply create() => UpdatePersonReply._();
  UpdatePersonReply createEmptyInstance() => create();
  static $pb.PbList<UpdatePersonReply> createRepeated() => $pb.PbList<UpdatePersonReply>();
  @$core.pragma('dart2js:noInline')
  static UpdatePersonReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdatePersonReply>(create);
  static UpdatePersonReply? _defaultInstance;
}

