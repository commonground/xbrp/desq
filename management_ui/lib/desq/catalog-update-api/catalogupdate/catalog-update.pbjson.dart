///
//  Generated code. Do not modify.
//  source: catalog-update-api/catalogupdate/catalog-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use updatePersonRequestDescriptor instead')
const UpdatePersonRequest$json = const {
  '1': 'UpdatePersonRequest',
  '2': const [
    const {'1': 'person_reference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
    const {'1': 'journal_zone', '3': 2, '4': 1, '5': 9, '10': 'journalZone'},
    const {'1': 'journal_height', '3': 3, '4': 1, '5': 4, '10': 'journalHeight'},
    const {'1': 'journal_hash', '3': 4, '4': 1, '5': 12, '10': 'journalHash'},
  ],
};

/// Descriptor for `UpdatePersonRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updatePersonRequestDescriptor = $convert.base64Decode('ChNVcGRhdGVQZXJzb25SZXF1ZXN0EikKEHBlcnNvbl9yZWZlcmVuY2UYASABKAxSD3BlcnNvblJlZmVyZW5jZRIhCgxqb3VybmFsX3pvbmUYAiABKAlSC2pvdXJuYWxab25lEiUKDmpvdXJuYWxfaGVpZ2h0GAMgASgEUg1qb3VybmFsSGVpZ2h0EiEKDGpvdXJuYWxfaGFzaBgEIAEoDFILam91cm5hbEhhc2g=');
@$core.Deprecated('Use updatePersonReplyDescriptor instead')
const UpdatePersonReply$json = const {
  '1': 'UpdatePersonReply',
};

/// Descriptor for `UpdatePersonReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updatePersonReplyDescriptor = $convert.base64Decode('ChFVcGRhdGVQZXJzb25SZXBseQ==');
