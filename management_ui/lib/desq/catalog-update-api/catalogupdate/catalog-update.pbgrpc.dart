///
//  Generated code. Do not modify.
//  source: catalog-update-api/catalogupdate/catalog-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'catalog-update.pb.dart' as $0;
export 'catalog-update.pb.dart';

class CatalogUpdateClient extends $grpc.Client {
  static final _$updatePerson =
      $grpc.ClientMethod<$0.UpdatePersonRequest, $0.UpdatePersonReply>(
          '/catalogupdate.CatalogUpdate/UpdatePerson',
          ($0.UpdatePersonRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UpdatePersonReply.fromBuffer(value));

  CatalogUpdateClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.UpdatePersonReply> updatePerson(
      $0.UpdatePersonRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updatePerson, request, options: options);
  }
}

abstract class CatalogUpdateServiceBase extends $grpc.Service {
  $core.String get $name => 'catalogupdate.CatalogUpdate';

  CatalogUpdateServiceBase() {
    $addMethod(
        $grpc.ServiceMethod<$0.UpdatePersonRequest, $0.UpdatePersonReply>(
            'UpdatePerson',
            updatePerson_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.UpdatePersonRequest.fromBuffer(value),
            ($0.UpdatePersonReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.UpdatePersonReply> updatePerson_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdatePersonRequest> request) async {
    return updatePerson(call, await request);
  }

  $async.Future<$0.UpdatePersonReply> updatePerson(
      $grpc.ServiceCall call, $0.UpdatePersonRequest request);
}
