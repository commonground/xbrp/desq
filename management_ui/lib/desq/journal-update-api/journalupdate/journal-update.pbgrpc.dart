///
//  Generated code. Do not modify.
//  source: journal-update-api/journalupdate/journal-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'journal-update.pb.dart' as $0;
export 'journal-update.pb.dart';

class JournalUpdateClient extends $grpc.Client {
  static final _$addEntry =
      $grpc.ClientMethod<$0.AddEntryRequest, $0.AddEntryReply>(
          '/journalupdate.JournalUpdate/AddEntry',
          ($0.AddEntryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.AddEntryReply.fromBuffer(value));

  JournalUpdateClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.AddEntryReply> addEntry($0.AddEntryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addEntry, request, options: options);
  }
}

abstract class JournalUpdateServiceBase extends $grpc.Service {
  $core.String get $name => 'journalupdate.JournalUpdate';

  JournalUpdateServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.AddEntryRequest, $0.AddEntryReply>(
        'AddEntry',
        addEntry_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AddEntryRequest.fromBuffer(value),
        ($0.AddEntryReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.AddEntryReply> addEntry_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AddEntryRequest> request) async {
    return addEntry(call, await request);
  }

  $async.Future<$0.AddEntryReply> addEntry(
      $grpc.ServiceCall call, $0.AddEntryRequest request);
}
