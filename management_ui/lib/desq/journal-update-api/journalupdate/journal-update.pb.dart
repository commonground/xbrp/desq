///
//  Generated code. Do not modify.
//  source: journal-update-api/journalupdate/journal-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class AddEntryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddEntryRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'journalupdate'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'personReference', $pb.PbFieldType.OY, protoName: 'personReference')
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eventReference', $pb.PbFieldType.OY, protoName: 'eventReference')
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eventHash', $pb.PbFieldType.OY, protoName: 'eventHash')
    ..hasRequiredFields = false
  ;

  AddEntryRequest._() : super();
  factory AddEntryRequest({
    $core.List<$core.int>? personReference,
    $core.List<$core.int>? eventReference,
    $core.List<$core.int>? eventHash,
  }) {
    final _result = create();
    if (personReference != null) {
      _result.personReference = personReference;
    }
    if (eventReference != null) {
      _result.eventReference = eventReference;
    }
    if (eventHash != null) {
      _result.eventHash = eventHash;
    }
    return _result;
  }
  factory AddEntryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddEntryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddEntryRequest clone() => AddEntryRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddEntryRequest copyWith(void Function(AddEntryRequest) updates) => super.copyWith((message) => updates(message as AddEntryRequest)) as AddEntryRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddEntryRequest create() => AddEntryRequest._();
  AddEntryRequest createEmptyInstance() => create();
  static $pb.PbList<AddEntryRequest> createRepeated() => $pb.PbList<AddEntryRequest>();
  @$core.pragma('dart2js:noInline')
  static AddEntryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddEntryRequest>(create);
  static AddEntryRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get personReference => $_getN(0);
  @$pb.TagNumber(1)
  set personReference($core.List<$core.int> v) { $_setBytes(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPersonReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearPersonReference() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get eventReference => $_getN(1);
  @$pb.TagNumber(2)
  set eventReference($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEventReference() => $_has(1);
  @$pb.TagNumber(2)
  void clearEventReference() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get eventHash => $_getN(2);
  @$pb.TagNumber(3)
  set eventHash($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasEventHash() => $_has(2);
  @$pb.TagNumber(3)
  void clearEventHash() => clearField(3);
}

class AddEntryReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddEntryReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'journalupdate'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journalHeight', $pb.PbFieldType.OU6, protoName: 'journalHeight', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'journalHeightHash', $pb.PbFieldType.OY, protoName: 'journalHeightHash')
    ..hasRequiredFields = false
  ;

  AddEntryReply._() : super();
  factory AddEntryReply({
    $fixnum.Int64? journalHeight,
    $core.List<$core.int>? journalHeightHash,
  }) {
    final _result = create();
    if (journalHeight != null) {
      _result.journalHeight = journalHeight;
    }
    if (journalHeightHash != null) {
      _result.journalHeightHash = journalHeightHash;
    }
    return _result;
  }
  factory AddEntryReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddEntryReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddEntryReply clone() => AddEntryReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddEntryReply copyWith(void Function(AddEntryReply) updates) => super.copyWith((message) => updates(message as AddEntryReply)) as AddEntryReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddEntryReply create() => AddEntryReply._();
  AddEntryReply createEmptyInstance() => create();
  static $pb.PbList<AddEntryReply> createRepeated() => $pb.PbList<AddEntryReply>();
  @$core.pragma('dart2js:noInline')
  static AddEntryReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddEntryReply>(create);
  static AddEntryReply? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get journalHeight => $_getI64(0);
  @$pb.TagNumber(1)
  set journalHeight($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasJournalHeight() => $_has(0);
  @$pb.TagNumber(1)
  void clearJournalHeight() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get journalHeightHash => $_getN(1);
  @$pb.TagNumber(2)
  set journalHeightHash($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasJournalHeightHash() => $_has(1);
  @$pb.TagNumber(2)
  void clearJournalHeightHash() => clearField(2);
}

