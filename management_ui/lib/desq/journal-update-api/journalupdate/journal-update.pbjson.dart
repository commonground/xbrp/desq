///
//  Generated code. Do not modify.
//  source: journal-update-api/journalupdate/journal-update.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use addEntryRequestDescriptor instead')
const AddEntryRequest$json = const {
  '1': 'AddEntryRequest',
  '2': const [
    const {'1': 'personReference', '3': 1, '4': 1, '5': 12, '10': 'personReference'},
    const {'1': 'eventReference', '3': 2, '4': 1, '5': 12, '10': 'eventReference'},
    const {'1': 'eventHash', '3': 3, '4': 1, '5': 12, '10': 'eventHash'},
  ],
};

/// Descriptor for `AddEntryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addEntryRequestDescriptor = $convert.base64Decode('Cg9BZGRFbnRyeVJlcXVlc3QSKAoPcGVyc29uUmVmZXJlbmNlGAEgASgMUg9wZXJzb25SZWZlcmVuY2USJgoOZXZlbnRSZWZlcmVuY2UYAiABKAxSDmV2ZW50UmVmZXJlbmNlEhwKCWV2ZW50SGFzaBgDIAEoDFIJZXZlbnRIYXNo');
@$core.Deprecated('Use addEntryReplyDescriptor instead')
const AddEntryReply$json = const {
  '1': 'AddEntryReply',
  '2': const [
    const {'1': 'journalHeight', '3': 1, '4': 1, '5': 4, '10': 'journalHeight'},
    const {'1': 'journalHeightHash', '3': 2, '4': 1, '5': 12, '10': 'journalHeightHash'},
  ],
};

/// Descriptor for `AddEntryReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addEntryReplyDescriptor = $convert.base64Decode('Cg1BZGRFbnRyeVJlcGx5EiQKDWpvdXJuYWxIZWlnaHQYASABKARSDWpvdXJuYWxIZWlnaHQSLAoRam91cm5hbEhlaWdodEhhc2gYAiABKAxSEWpvdXJuYWxIZWlnaHRIYXNo');
