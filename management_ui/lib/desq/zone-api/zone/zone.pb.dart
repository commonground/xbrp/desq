///
//  Generated code. Do not modify.
//  source: zone-api/zone/zone.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ZoneStatusRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ZoneStatusRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'zone'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ZoneStatusRequest._() : super();
  factory ZoneStatusRequest() => create();
  factory ZoneStatusRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ZoneStatusRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ZoneStatusRequest clone() => ZoneStatusRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ZoneStatusRequest copyWith(void Function(ZoneStatusRequest) updates) => super.copyWith((message) => updates(message as ZoneStatusRequest)) as ZoneStatusRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ZoneStatusRequest create() => ZoneStatusRequest._();
  ZoneStatusRequest createEmptyInstance() => create();
  static $pb.PbList<ZoneStatusRequest> createRepeated() => $pb.PbList<ZoneStatusRequest>();
  @$core.pragma('dart2js:noInline')
  static ZoneStatusRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ZoneStatusRequest>(create);
  static ZoneStatusRequest? _defaultInstance;
}

class ZoneStatusReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ZoneStatusReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'zone'), createEmptyInstance: create)
    ..aOM<ZoneStatus>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', subBuilder: ZoneStatus.create)
    ..hasRequiredFields = false
  ;

  ZoneStatusReply._() : super();
  factory ZoneStatusReply({
    ZoneStatus? status,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory ZoneStatusReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ZoneStatusReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ZoneStatusReply clone() => ZoneStatusReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ZoneStatusReply copyWith(void Function(ZoneStatusReply) updates) => super.copyWith((message) => updates(message as ZoneStatusReply)) as ZoneStatusReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ZoneStatusReply create() => ZoneStatusReply._();
  ZoneStatusReply createEmptyInstance() => create();
  static $pb.PbList<ZoneStatusReply> createRepeated() => $pb.PbList<ZoneStatusReply>();
  @$core.pragma('dart2js:noInline')
  static ZoneStatusReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ZoneStatusReply>(create);
  static ZoneStatusReply? _defaultInstance;

  @$pb.TagNumber(1)
  ZoneStatus get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(ZoneStatus v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);
  @$pb.TagNumber(1)
  ZoneStatus ensureStatus() => $_ensure(0);
}

class ZoneStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ZoneStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'zone'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zid')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version')
    ..hasRequiredFields = false
  ;

  ZoneStatus._() : super();
  factory ZoneStatus({
    $core.String? zid,
    $core.String? status,
    $core.String? version,
  }) {
    final _result = create();
    if (zid != null) {
      _result.zid = zid;
    }
    if (status != null) {
      _result.status = status;
    }
    if (version != null) {
      _result.version = version;
    }
    return _result;
  }
  factory ZoneStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ZoneStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ZoneStatus clone() => ZoneStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ZoneStatus copyWith(void Function(ZoneStatus) updates) => super.copyWith((message) => updates(message as ZoneStatus)) as ZoneStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ZoneStatus create() => ZoneStatus._();
  ZoneStatus createEmptyInstance() => create();
  static $pb.PbList<ZoneStatus> createRepeated() => $pb.PbList<ZoneStatus>();
  @$core.pragma('dart2js:noInline')
  static ZoneStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ZoneStatus>(create);
  static ZoneStatus? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get zid => $_getSZ(0);
  @$pb.TagNumber(1)
  set zid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasZid() => $_has(0);
  @$pb.TagNumber(1)
  void clearZid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get version => $_getSZ(2);
  @$pb.TagNumber(3)
  set version($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasVersion() => $_has(2);
  @$pb.TagNumber(3)
  void clearVersion() => clearField(3);
}

