///
//  Generated code. Do not modify.
//  source: zone-api/zone/zone.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use zoneStatusRequestDescriptor instead')
const ZoneStatusRequest$json = const {
  '1': 'ZoneStatusRequest',
};

/// Descriptor for `ZoneStatusRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List zoneStatusRequestDescriptor = $convert.base64Decode('ChFab25lU3RhdHVzUmVxdWVzdA==');
@$core.Deprecated('Use zoneStatusReplyDescriptor instead')
const ZoneStatusReply$json = const {
  '1': 'ZoneStatusReply',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 11, '6': '.zone.ZoneStatus', '10': 'status'},
  ],
};

/// Descriptor for `ZoneStatusReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List zoneStatusReplyDescriptor = $convert.base64Decode('Cg9ab25lU3RhdHVzUmVwbHkSKAoGc3RhdHVzGAEgASgLMhAuem9uZS5ab25lU3RhdHVzUgZzdGF0dXM=');
@$core.Deprecated('Use zoneStatusDescriptor instead')
const ZoneStatus$json = const {
  '1': 'ZoneStatus',
  '2': const [
    const {'1': 'zid', '3': 1, '4': 1, '5': 9, '10': 'zid'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'version', '3': 3, '4': 1, '5': 9, '10': 'version'},
  ],
};

/// Descriptor for `ZoneStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List zoneStatusDescriptor = $convert.base64Decode('Cgpab25lU3RhdHVzEhAKA3ppZBgBIAEoCVIDemlkEhYKBnN0YXR1cxgCIAEoCVIGc3RhdHVzEhgKB3ZlcnNpb24YAyABKAlSB3ZlcnNpb24=');
