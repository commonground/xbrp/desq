///
//  Generated code. Do not modify.
//  source: zone-api/zone/zone.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'zone.pb.dart' as $0;
export 'zone.pb.dart';

class ZoneClient extends $grpc.Client {
  static final _$zoneStatus =
      $grpc.ClientMethod<$0.ZoneStatusRequest, $0.ZoneStatusReply>(
          '/zone.Zone/ZoneStatus',
          ($0.ZoneStatusRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ZoneStatusReply.fromBuffer(value));

  ZoneClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.ZoneStatusReply> zoneStatus(
      $0.ZoneStatusRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$zoneStatus, request, options: options);
  }
}

abstract class ZoneServiceBase extends $grpc.Service {
  $core.String get $name => 'zone.Zone';

  ZoneServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.ZoneStatusRequest, $0.ZoneStatusReply>(
        'ZoneStatus',
        zoneStatus_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ZoneStatusRequest.fromBuffer(value),
        ($0.ZoneStatusReply value) => value.writeToBuffer()));
  }

  $async.Future<$0.ZoneStatusReply> zoneStatus_Pre($grpc.ServiceCall call,
      $async.Future<$0.ZoneStatusRequest> request) async {
    return zoneStatus(call, await request);
  }

  $async.Future<$0.ZoneStatusReply> zoneStatus(
      $grpc.ServiceCall call, $0.ZoneStatusRequest request);
}
