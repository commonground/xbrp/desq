// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/grpc/grpc_client.dart';
import 'package:management_ui/desq/zone-api/zone/zone.pb.dart';
import 'package:management_ui/desq/management-api/management/management.pb.dart';
import 'package:management_ui/desq/management-api/management/management.pbgrpc.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';

class EcosystemProviderLive implements EcosystemProvider {
  final String municipality;
  final GrpcClient grpcClient;

  EcosystemProviderLive({this.municipality, this.grpcClient});

  @override
  Future<ZoneStatus> getZoneStatus() async {
    final client = ManagementClient(
      grpcClient.client,
    );

    try {
      final response = await client.zoneStatus(
        ZoneStatusRequest(),
      );
      return Future.value(response.status);
    } catch (e) {
      debugPrint('Caught error: $e while connecting to ${client.toString()}');
      return Future.value(ZoneStatus(
        zid: municipality,
        status: "UNAVAILABLE",
      ));
    }
  }

  @override
  Future<List<ZoneStatus>> getNetworkStatus() async {
    final client = ManagementClient(
      grpcClient.client,
    );

    try {
      final response = await client.networkStatus(
        NetworkStatusRequest(),
      );
      return Future.value(response.zones);
    } catch (e) {
      debugPrint('Caught error: $e while connecting to ${client.toString()}');
      return Future.value([]);
    }
  }
}
