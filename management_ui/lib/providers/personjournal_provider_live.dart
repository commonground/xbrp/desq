// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/grpc/grpc_client.dart';
import 'package:management_ui/desq/management-api/management/management.pb.dart';
import 'package:management_ui/desq/management-api/management/management.pbgrpc.dart';
import 'package:management_ui/providers/personjournal_provider.dart';

class PersonJournalProviderLive implements PersonJournalProvider {
  final GrpcClient grpcClient;
  PersonJournalProviderLive({this.grpcClient});

  @override
  Future<List<Journal>> getJournals() async {
    final client = ManagementClient(
      grpcClient.client,
    );

    try {
      final response = await client.journals(
        JournalsRequest(),
      );
      debugPrint("Num journals: ${response.journals.length}");
      return Future.value(response.journals);
    } catch (e) {
      debugPrint('Caught error: $e while connecting to ${client.toString()}');
      return Future.value([]);
    }
  }
}
