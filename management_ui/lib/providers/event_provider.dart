// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:management_ui/models/management_ui_event_model.dart';

abstract class EventProvider {
  Future<List<ManagementUIEvent>> getEvents();
}
