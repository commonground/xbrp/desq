// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:management_ui/models/management_ui_event_model.dart';
import 'package:management_ui/providers/event_provider.dart';
import 'package:management_ui/desq/google/protobuf/timestamp.pb.dart';
import 'package:management_ui/desq/common/events/events.pb.dart' as desq;

class MockEventProvider implements EventProvider {
  @override
  Future<List<ManagementUIEvent>> getEvents() {
    return Future.value([
      ManagementUIEvent(
        "bovenhove",
        "desq.NamenVastgesteld",
        DateTime.now(),
        desq.NamenVastgesteld(
          voornamen: ["Pim"],
          geslachtsnaam: "Hierden",
          ingangsdatumNamen: Timestamp.fromDateTime(DateTime.now()),
          persoonHr: [], // TODO generate personreferences correctly in Dart
          voorvoegselGeslachtsnaam: "van",
        ),
      ),
    ]);
  }
}
