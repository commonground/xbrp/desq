// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:flutter/material.dart';
import 'package:management_ui/grpc/grpc_client.dart';
import 'package:management_ui/models/management_ui_event_model.dart';
import 'package:management_ui/desq/management-api/management/management.pb.dart';
import 'package:management_ui/desq/management-api/management/management.pbgrpc.dart';
import 'package:management_ui/providers/event_provider.dart';

class EventProviderLive implements EventProvider {
  final GrpcClient grpcClient;
  EventProviderLive({this.grpcClient});

  @override
  Future<List<ManagementUIEvent>> getEvents() async {
    final client = ManagementClient(
      grpcClient.client,
    );

    try {
      final response = await client.events(
        EventsRequest(),
      );
      return response.events.map(ManagementUIEvent.fromAPIEvent).toList();
    } catch (e) {
      debugPrint('Caught error: $e while connecting to ${client.toString()}');
      return Future.value([]);
    }
  }
}
