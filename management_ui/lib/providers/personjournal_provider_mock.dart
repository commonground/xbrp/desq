// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:management_ui/desq/management-api/management/management.pb.dart';
import 'package:management_ui/providers/personjournal_provider.dart';
import 'package:management_ui/util/base62.dart';

class MockPersonJournalProvider implements PersonJournalProvider {
  @override
  Future<List<Journal>> getJournals() {
    return Future.value([
      Journal(personReference: Base62Codec.decode("1234 1234 4321 4321 1234 1234")),
      Journal(personReference: Base62Codec.decode("4466 2233 4466 2233 4466 8844")),
      Journal(personReference: Base62Codec.decode("6543 9900 6543 0000 2222 7777")),
    ]);
  }
}
