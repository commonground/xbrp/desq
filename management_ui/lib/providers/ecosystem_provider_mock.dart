// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:management_ui/desq/zone-api/zone/zone.pb.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';

class MockEcosystemProvider implements EcosystemProvider {
  @override
  Future<ZoneStatus> getZoneStatus() async {
    // TODO: better mock data
    return Future.value(ZoneStatus(
      zid: 'groenestede',
      status: 'online vMOCK',
    ));
  }

  @override
  Future<List<ZoneStatus>> getNetworkStatus() async {
    // TODO: better mock data
    return Future.value([
      ZoneStatus(
        zid: 'bovenhove',
        status: 'offline',
      ),
      ZoneStatus(
        zid: 'oudescha',
        status: 'error',
      ),
      ZoneStatus(
        zid: 'groenestede',
        status: 'online',
      ),
      ZoneStatus(
        zid: 'heidedal',
        status: 'offline',
      ),
    ]);
  }
}
