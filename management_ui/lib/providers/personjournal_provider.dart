// Copyright © Gemeente Amsterdam 2020
// Licensed under the MIT license

import 'package:management_ui/desq/management-api/management/management.pb.dart';

abstract class PersonJournalProvider {
  Future<List<Journal>> getJournals();
}
