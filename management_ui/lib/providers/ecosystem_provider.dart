// Copyright © Gemeente Amsterdam 2021
// Licensed under the MIT license

import 'package:management_ui/desq/zone-api/zone/zone.pb.dart';

abstract class EcosystemProvider {
  Future<ZoneStatus> getZoneStatus();
  Future<List<ZoneStatus>> getNetworkStatus();
}
