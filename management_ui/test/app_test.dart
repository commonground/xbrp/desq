// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';

import 'package:management_ui/app.dart';
import 'package:management_ui/providers/ecosystem_provider.dart';
import 'package:management_ui/providers/ecosystem_provider_mock.dart';
import 'package:management_ui/providers/event_provider.dart';
import 'package:management_ui/providers/event_provider_mock.dart';
import 'package:management_ui/repositories/environment_repository.dart';
import 'package:provider/provider.dart';

void main() {
  testWidgets('App starts and displays LoginScreen', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<EnvironmentRepository>(
            create: (_) => EnvironmentRepository(
              municipality: "bovenhove",
            ),
          ),
          Provider<EventProvider>(
            create: (_) => MockEventProvider(),
          ),
          Provider<EcosystemProvider>(
            create: (_) => MockEcosystemProvider(),
          ),
        ],
        child: const ManagementApp(
          municipality: "bovenhove",
        ),
      ),
    );

    // Verify that text is displayed at the LoginScreen
    expect(find.text('Beheeromgeving Bovenhove'), findsOneWidget);
    expect(find.text('Inloggen'), findsOneWidget);
  });
}
