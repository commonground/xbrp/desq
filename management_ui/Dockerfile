FROM ubuntu:20.04 AS build

# Install build-time dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        git curl unzip ca-certificates \
	&& rm -rf /var/lib/apt/lists/*

# Install Flutter from the beta channel and enable web
RUN git clone --single-branch --depth=1 --branch stable https://github.com/flutter/flutter /opt/flutter 2>&1 \
	&& ln -sf /opt/flutter/bin/flutter /usr/local/bin/flutter \
    && flutter doctor -v

# Copy and compile management_ui compnent
COPY management_ui /desq/management_ui
WORKDIR /desq/management_ui

RUN flutter pub get
RUN flutter build web

FROM nginx:alpine

# Add non-privileged user as default user
RUN adduser -D -u 1001 appuser
USER appuser

COPY --from=build /desq/management_ui/build/web /usr/share/nginx/html
RUN chown -R nginx:nginx /usr/share/nginx/html/*
