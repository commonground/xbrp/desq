# management_ui

A simple management UI for the XBRP prototype.

## Getting Started

The management UI requires the other desq components to be running.

The management UI can be started using

`flutter run -d chrome`

## Starting the app for a specific municipality

- During development, use the a command line parameter to select a municipality, for example: `flutter run -d chrome --dart-define=municipality="groenestede"`
- On staging and production, the app will auto-detect it's municipality.

## Code organizations

Management UI data models are located in `lib/models`.
Management UI screens are located in `lib/src/screens`.
Reusable widgets are in `lib/widgets`.
The animated progress indicator was copied from the flutter website.
Communication with other XBRP components is implemented in `lib/api.dart`.
 